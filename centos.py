#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f centos.py -u root -H aa.bb.cc.dd base_gpgcheck
# fab -a -f centos.py -u root -H aa.bb.cc.dd base_updates_mentioned
# fab -a -f centos.py -u root -H aa.bb.cc.dd print_preamble_fast
# fab -a -f centos.py -u root -H aa.bb.cc.dd print_preamble
# fab -a -f centos.py -u root -H aa.bb.cc.dd sshd_summary
# fab -a -f centos.py -u root -H aa.bb.cc.dd postgres_summary

import fabutil as futil
from fabutil import conf_get_set as getset
# import pprint; pp2 = pprint.PrettyPrinter(indent=2)


def first_arg_false_to_false(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is True or first_arg in ['true','True','TRUE']:
                # Ensure we never interfere with any first arg intended as positive boolean
                pass
            elif first_arg is False:
                # This branch will likely never be encountered
                pass
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def first_arg_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if len(args) > 0:
            #print 'Wrapped function', function_to_decorate.__name__, 'has args'
            first_arg = args[0]
            if first_arg is True:
                # Ensure we never interfere with any first arg intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as True'
                pass
            elif first_arg is False:
                # Ensure we never interfere with any first arg intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as False'
                pass
            elif first_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to True'
                args_list = list(args)
                args_list[0] = True
                args = tuple(args_list)
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to False'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if 'verbose' in kwargs:
            #print "Wrapped function", function_to_decorate.__name__, "has kwarg 'verbose'"

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as True'
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as False'
                pass
            elif verbose_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to True'
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to False'
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


@first_arg_forced_boolean
@verbose_forced_boolean
def centoslike_contract(verbose=False):
    """ Returns several indicators. Most useful where network delays are not a factor
    Do use the wrapper functions centoslike() or centoslike2() as appropriate.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    #print pretty_tuple
    rline = futil.cmdbin.release_line()
    #print rline
    #if futil.cmdbin.cpu64():
    #    print "CPU has 64 bit support"
    centos_or_centoslike = None
    debian_or_debianlike = False
    if pretty_tuple[1] in futil.cmdbin.DEBIAN_PLUS_DERIVATIVES2:
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif 'ebianlike' in rline:
        debian_or_debianlike = True
        centos_or_centoslike = False
    else:
        pass
    if pretty_tuple[1] == 'debian':
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif debian_or_debianlike:
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        centos_or_centoslike = True
        debian_or_debianlike = False
        if verbose is True:
            print "EPEL: {0}".format(futil.package_rpm.has_epel())
            print futil.package_rpm.repolist_dict()

    if centos_or_centoslike is None:
        # If inconclusive then set as False
        centos_or_centoslike = False

    return centos_or_centoslike


@first_arg_forced_boolean
@verbose_forced_boolean
def centoslike(verbose=False):
    """ Wrapper around centoslike_contract() to which you can add any
    optional behaviour you choose later
    """
    centos_or_centoslike = centoslike_contract(verbose)
    return centos_or_centoslike


def centoslike2(verbose=True):
    """ Wrapper around centoslike_contract with verbose forced True
    Because selinux config file might be considered sensitive we make 
    pulling that file locally a feature of this wrapper rather than
    being default behaviour of main centoslike() function
    Avoid adding time consuming things like 'repolist' as centoslike2() is
    intended to be callable from _fast() type wrappers and repolist is too slow
    """
    centos_or_centoslike = centoslike_contract(verbose)
    if centos_or_centoslike:
        futil.selinuxstatus.sestatus1print()
        keyed = futil.selinuxstatus.seconfig_keyed()
        #print "keyed has length {0}".format(len(keyed))
        #print keyed
        if 'SELINUX' in keyed:
            print "SELINUX set {0}".format(keyed['SELINUX'])
        if 'SELINUXTYPE' in keyed:
            print "SELINUXTYPE set {0}".format(keyed['SELINUXTYPE'])
        eline = futil.selinuxstatus.enforced1()
        if len(eline) > 0:
            print("SE Linux enforced? Current running system is set {0}".format(eline[0]))
    if not centos_or_centoslike:
        return centos_or_centoslike
    md5text = futil.cmdbin.md5text_or_sudo('/etc/ssh/sshd_config')
    print("sshd_config md5 is {0}".format(md5text))
    #from ordered2dict import KeyedOD,KeyedEqualsOD
    EXPRESSIONS=['sha512','sha256','md5sum']
    with getset.Keyedequals('/etc/sysconfig/authconfig') as conf:
        line = ''
	if 'PASSWDALGORITHM' in conf:
            #print(conf.keylines_typed(1,['PASSWDALGORITHM']))
            line = conf.keyline1rstripped('PASSWDALGORITHM')
        lines = []
	if line is not None and len(line) > 2:
            print(line)
        else:
            # Not enough printed yet so put in some more effort below
            lines = conf.lines_noncomment_containing_any(EXPRESSIONS)
        for line in lines:
            print(line)
    return centos_or_centoslike


@first_arg_false_to_false
def print_preamble(backports_epel_report=True):
    """ Print preamble showing some short fingerprint style markers for the server.
    Even if we know we are Debian or RPM based, we might
    still distinguish servers based on availability of extra repositories.
    backports_epel_true=True (default) gives you some feedback about extra
    repositories.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print pretty_tuple
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    if not backports_epel_report:
        # Do not bother reporting about backports / epel
        pass
    elif pretty_tuple[1] == 'debian':
        print "Backports: {0}".format(futil.package_deb.has_backports())
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        print "EPEL: {0}".format(futil.package_rpm.has_epel())
        print futil.package_rpm.repolist_dict()

    if backports_epel_report:
        """ Consider backports_epel_report similar to verbose=true
        way of requesting more info """
        futil.selinuxstatus.sestatus1print()
    return


@first_arg_false_to_false
def print_preamble_fast(verbosity=0):
    """ Print preamble showing some short fingerprint style markers for the server.
    This variant avoids checking centos for epel to avoid time consuming 'yum repolist'.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print(pretty_tuple)
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    if pretty_tuple[1] == 'debian':
        pass
        # fast so skip: print "Backports: {0}".format(futil.package_deb.has_backports())
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        # Best guess: We are centos or centoslike
        centoslike2(verbose=False)
        """ futil.selinuxstatus.sestatus1print()
        By calling centoslike2() we already get some commentary about
        selinux status
        """
    else:
        pass
    return


def sshd_summary_verbose():
    """ Print ssh summary - list 'enabled' ssh config lines """
    for line in futil.ssh_lokkit.sshd_config_keylines_enabled():
        print line
    #print futil.ssh_lokkit.sshd_config_keylines_enabled_linesep()
    print futil.ssh_lokkit.sshd_motd_yes_preview(verbose=True)
    return


def sshd_summary():
    """ Print ssh summary - shorter list of selected ssh config lines
    futil.ssh_lokkit.sshd_config_summary(True,'/etc/sshd/sshd_config ')
    """
    futil.ssh_lokkit.sshd_config_summary(True,'filepath')
    return


#def iptables_save():
#    """ Request remote server saves its iptables entries to a file. """
#    return futil.iptables_firewall.iptables_save()


#def iptables_save_verbose():
#    """ Request remote server saves its iptables entries to a file (and report count) """
#    return futil.iptables_firewall.iptables_save(verbose_counts=True)


def base_gpgcheck(verbose=False):
    """ Check base updates have gpg checking.
    Reminder about the contract of conf_get_set.py and order or operations next...
    Do the queries first. Do use the reliable write features (when implemented)
    However you are discouraged from performing updates and then attempt to query things further.

    More robust checking should be added via [new] functions in
    fabutil/package_rpm.py at a later date.
    """
    conf_filepath = '/etc/yum.repos.d/CentOS-Base.repo'
    base_return = False
    with getset.Keyedequals(conf_filepath) as conf:
        #print conf.lines_current_simple()
        if 'gpgcheck' in conf:
            #if conf['gpgcheck'] == 1:
            #    base_return = True
            # More robust checking should be added via [new] functions in
            # fabutil/package_rpm.py at a later date.
            print conf.keylines_typed(1,['gpgcheck'])

    return base_return


def base_updates_mentioned(verbose=True):
    """ Check base .repo file has mention of 'updates'
    This does not necessarily mean updates are enabled
    More robust checking should be added via [new] functions in
    fabutil/package_rpm.py at a later date.
    """
    conf_filepath = '/etc/yum.repos.d/CentOS-Base.repo'
    base_return = False
    with getset.Keyedequals(conf_filepath) as conf:
        #print conf.lines_current_simple()
        # Check for updates or /updates/ in value
        containing_list = conf.lines_noncomment_containing_any(['/updates/','=updates'])
        for uline in containing_list:
            print uline
        #if 'gpgcheck' in conf:
        #    print conf.keylines_typed(1,['gpgcheck'])
    return base_return


def sysstat_enabled(verbose=True):
    """ Print enabled lines in /etc/sysconfig/sysstat if any
    returns a numeric answer.
    1 means configured
    2 for configured and enabled (not implemented yet)
    """
    conf_filepath = '/etc/sysconfig/sysstat'
    sysstat_enabled = 0
    with getset.Keyedequals(conf_filepath) as conf:
        enabled = conf.lines_enabled()
        if len(enabled) > 0:
            sysstat_enabled += 1
        if verbose is True:
            for eline in enabled:
                print eline
    return sysstat_enabled


def postgres_summary():
    centos_or_centoslike = centoslike_contract(verbose=True)
    if centos_or_centoslike:
        EXPRESSIONS=['autovacuum','checkpoint_','cost_page','recovery','sync','syncronous_commit','io_concurrency',
                     'page_writes','wal_level','wal_senders','wal_writer_delay','io_concurrency']
        conf_filepath = '/var/lib/pgsql/data/postgresql.conf'
        with getset.Keyedequals(conf_filepath) as conf:
            lines = conf.lines_noncomment_containing_any(EXPRESSIONS)
            for line in lines:
                print line







