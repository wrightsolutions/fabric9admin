#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f postgres9debdiffer.py -u root -H aa.bb.cc.dd datadir
# fab -a -f postgres9debdiffer.py -u root -H aa.bb.cc.dd datadir_login
# fab -a -f postgres9debdiffer.py -u root -H aa.bb.cc.dd postgres_settings_differ
# fab -a -f postgres9debdiffer.py -u root -H aa.bb.cc.dd diff		# an alias to above postgres_settings_differ

from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
import fabutil as futil
from fabutil import conf_get_set as getset
from fabutil import postgres9deb as pg
#from fabutil import package_deb as pdeb

""" postgres_settings_differ - Report on differences in individual settings from default (boot val)
"""


CMD_PSQL_AS_POSTGRES_SETTINGS_DIFFER = """su -l -c 'psql -A --pset={0}footer=off{0} --pset={0}pager=off{0} -c \
{0}select name,setting,context,vartype,boot_val,reset_val,category,unit from pg_settings where setting <> boot_val;{0}' postgres
""".format(chr(34),futil.cmdbin.SQBACKSLASHSQSQ)



def first_arg_false_to_false(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is True or first_arg in ['true','True','TRUE']:
                # Ensure we never interfere with any first arg intended as positive boolean
                pass
            elif first_arg is False:
                # This branch will likely never be encountered
                pass
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def first_arg_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if len(args) > 0:
            #print 'Wrapped function', function_to_decorate.__name__, 'has args'
            first_arg = args[0]
            if first_arg is True:
                # Ensure we never interfere with any first arg intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as True'
                pass
            elif first_arg is False:
                # Ensure we never interfere with any first arg intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as False'
                pass
            elif first_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to True'
                args_list = list(args)
                args_list[0] = True
                args = tuple(args_list)
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to False'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if 'verbose' in kwargs:
            #print "Wrapped function", function_to_decorate.__name__, "has kwarg 'verbose'"

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as True'
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as False'
                pass
            elif verbose_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to True'
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to False'
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbosity_kwarg_int(function_to_decorate):
    """ Decorator to ensure, even if verbosity is supplied as a string,
    then we do our utmost to convert it to an int.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbosity' in kwargs:
            kval = kwargs['verbosity']
            verbosity_int=None
            try:
                verbosity_int = int(kval)
            except Exception:
                pass
            if verbosity_int is None:
                """ The user of this decorator would expect that
                non-numeric arguments blocked, so better to set
                verbosity zero than let through a bad value
                """
                #kwargs[verbosity]=0
                kwargs.pop('verbosity', None)
                """ Above we have removed verbosity if it cannot
                be converted, which means any default in wrapped
                function signature would take effect
                """
            else:
                kwargs['verbosity']=verbosity_int
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbosity_kwarg_int_verbose(function_to_decorate):
    """ Decorator to ensure, even if verbosity is supplied as a string,
    then we do our utmost to convert it to an int.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    Verbose variant which has print statements reporting before and after
    and also sets verbosity=0 on conversion error.
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbosity' in kwargs:
            kval = kwargs['verbosity']
            verbosity_int=None
            try:
                print("Attempting to int({0}) ".format(kval))
                verbosity_int = int(kval)
            except Exception:
                raise

            if verbosity_int is None:
                """ The user of this decorator would expect that
                non-numeric arguments blocked, so better to set
                verbosity zero than let through a bad value
                """
                #kwargs[verbosity]=0
                kwargs.pop('verbosity', None)
                print("kwarg_int wrapper has removed verbosity key word arg")
                """ Above we have removed verbosity if it cannot
                be converted, which means any default in wrapped
                function signature would take effect
                """
            else:
                kwargs['verbosity']=verbosity_int
        print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def datadir_getter():
	""" Internal function - see datadir() for a fab callable wrapper """
	settings_dict = pg.postgres_settings_dict(print_flag=False)
	settings_count = len(settings_dict)
	#print("Postgresql has {0} settings.".format(settings_count))
	if 'data_directory' in settings_dict:
		datadir = None
		try:
			datadir_list = settings_dict['data_directory']
			datadir = datadir_list[1]
		except:
			datadir = None
	return datadir


@verbose_forced_boolean
def datadir(verbose=False):
	settings_dict = pg.postgres_settings_dict(print_flag=(verbose is True))
	settings_count = len(settings_dict)
	print("Postgresql has {0} settings.".format(settings_count))
	if 'data_directory' in settings_dict:
		datadir = '[unknown]'
		try:
			datadir_list = settings_dict['data_directory']
			datadir = datadir_list[1]
		except:
			datadir = '[invalid]'
		print("Data directory of running Postgresql is {0}".format(datadir))
	return settings_count


def datadir_login(fieldnum=5):
	""" Print the home/data directory as recorded in password entry for postgres """
	return pg.postgres_passline(fieldnum,True)


def datadir_login_plus(fieldnum=5):
	""" Print the home/data directory as recorded in password entry for postgres
	and datadir as defined in running server
	"""
	datadir = datadir_getter()
	if datadir is None:
		print("Unable to determine actual datadir based on Postgresql running config")
	else:
		print("Data directory of running Postgresql is {0}".format(datadir))
	return pg.postgres_passline(fieldnum,True)


def postgres_settings_differ(run_or_sudo='sudo'):
	""" Similar to SHOW command but displays only those settings that differ
	from the server default (boot val).
	"""
	if 'sudo' == run_or_sudo:
		differ_res = sudo(CMD_PSQL_AS_POSTGRES_SETTINGS_DIFFER)
	else:
		differ_res = futil.cmdbin.run_or_sudo(CMD_PSQL_AS_POSTGRES_SETTINGS_DIFFER)
	return differ_res


def diff(run_or_sudo='sudo'):
	""" Simple alias 'diff' of the longer command name 'postgres_settings_differ' """
	return postgres_settings_differ(run_or_sudo)


"""
Aug 30 17:04:15 jessie64test sshd[3416]: Accepted password for rem1 from aa.bb.cc.dd port 55595 ssh2
Aug 30 17:04:15 jessie64test sshd[3416]: pam_unix(sshd:session): session opened for user rem1 by (uid=0)
Aug 30 17:04:15 jessie64test sudo:     rem1 : TTY=pts/0 ; PWD=/home/rem1 ; USER=root ; COMMAND=/bin/bash -l -c su -l -c 'psql -A --pset="footer=off" --pset="pager=off" -c "select name,setting,context,vartype,boot_val,reset_val,category,unit from pg_settings where setting <> boot_val;"' postgres
Aug 30 17:04:15 jessie64test sudo: pam_unix(sudo:session): session opened for user root by rem1(uid=0)
Aug 30 17:04:15 jessie64test su[3423]: Successful su for postgres by root
Aug 30 17:04:15 jessie64test su[3423]: + /dev/pts/0 root:postgres
Aug 30 17:04:15 jessie64test su[3423]: pam_unix(su:session): session opened for user postgres by rem1(uid=0)
Aug 30 17:04:15 jessie64test su[3423]: pam_unix(su:session): session closed for user postgres
Aug 30 17:04:15 jessie64test sudo: pam_unix(sudo:session): session closed for user root
Aug 30 17:04:15 jessie64test sshd[3416]: pam_unix(sshd:session): session closed for user rem1
Aug 30 17:04:15 jessie64test sshd[3414]: Connection closed by aa.bb.cc.dd [preauth]
"""
