#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_install
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_installed
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_configure_default
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_configure
## fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_configured
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_enable
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_detailed
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_enabled_lines
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_check
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_restart
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_start
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_stop
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_status
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collection_page_support
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_web_py
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_web_py_apache
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_web_py_apache_advanced
# fab -a -f collectd5debian.py -u root -H aa.bb.cc.dd collectd_web_py_nginx


from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
import fabutil as futil
from fabutil import conf_get_set as getset
from fabutil import collectd5deb as cold
#from fabutil import package_deb as pdeb
from fabutil import sign_repo
from fabutil import web_uwsgi_nginx as uwsging


def first_arg_false_to_false(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is True or first_arg in ['true','True','TRUE']:
                # Ensure we never interfere with any first arg intended as positive boolean
                pass
            elif first_arg is False:
                # This branch will likely never be encountered
                pass
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def first_arg_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if len(args) > 0:
            #print 'Wrapped function', function_to_decorate.__name__, 'has args'
            first_arg = args[0]
            if first_arg is True:
                # Ensure we never interfere with any first arg intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as True'
                pass
            elif first_arg is False:
                # Ensure we never interfere with any first arg intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as False'
                pass
            elif first_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to True'
                args_list = list(args)
                args_list[0] = True
                args = tuple(args_list)
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to False'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if 'verbose' in kwargs:
            #print "Wrapped function", function_to_decorate.__name__, "has kwarg 'verbose'"

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as True'
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as False'
                pass
            elif verbose_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to True'
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to False'
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbosity_kwarg_int(function_to_decorate):
    """ Decorator to ensure, even if verbosity is supplied as a string,
    then we do our utmost to convert it to an int.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbosity' in kwargs:
            kval = kwargs['verbosity']
            verbosity_int=None
            try:
                verbosity_int = int(kval)
            except Exception:
                pass
            if verbosity_int is None:
                """ The user of this decorator would expect that
                non-numeric arguments blocked, so better to set
                verbosity zero than let through a bad value
                """
                #kwargs[verbosity]=0
                kwargs.pop('verbosity', None)
                """ Above we have removed verbosity if it cannot
                be converted, which means any default in wrapped
                function signature would take effect
                """
            else:
                kwargs['verbosity']=verbosity_int
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbosity_kwarg_int_verbose(function_to_decorate):
    """ Decorator to ensure, even if verbosity is supplied as a string,
    then we do our utmost to convert it to an int.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    Verbose variant which has print statements reporting before and after
    and also sets verbosity=0 on conversion error.
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbosity' in kwargs:
            kval = kwargs['verbosity']
            verbosity_int=None
            try:
                print("Attempting to int({0}) ".format(kval))
                verbosity_int = int(kval)
            except Exception:
                raise

            if verbosity_int is None:
                """ The user of this decorator would expect that
                non-numeric arguments blocked, so better to set
                verbosity zero than let through a bad value
                """
                #kwargs[verbosity]=0
                kwargs.pop('verbosity', None)
                print("kwarg_int wrapper has removed verbosity key word arg")
                """ Above we have removed verbosity if it cannot
                be converted, which means any default in wrapped
                function signature would take effect
                """
            else:
                kwargs['verbosity']=verbosity_int
        print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


@first_arg_forced_boolean
def collectd_install(verbose=False):
    return cold.collectd_install(None,True,False)


def collectd_installed(version=14,packagename=cold.COLLECTD_PACKAGE):
    version_int = int(version)
    installed_version = futil.package_deb.dpkg_installed_version_numeric(packagename)
    if installed_version > 0:
        print "Collectd version {0} is installed".format(installed_version)
    else:
        pass
        #print "Collectd is not installed!"
    return installed_version


@first_arg_forced_boolean
def collectd_detailed(verbose=False):
    cold.detailed_extra_collectd()
    return


@first_arg_forced_boolean
def collectd_configure_default(suffixed=True):
    cold.collectd_configure_default(suffixed)
    return


@first_arg_forced_boolean
def collectd_enabled_lines(verbose=False):
    cold.lines_enabled_collectd()
    #cold.detailed_enabled_ndo2db(print_flag=True)
    cold.lines_enabled_collectd()
    return


def collectd_cfg_confnum(confnum=2,verbose=False):
    collectd_stop()
    reinstalled_flag = cold.collectd_configure_split(confnum)
    collectd_start()
    return reinstalled_flag


@first_arg_forced_boolean
def collectd_configure(confnum=None):
	if confnum is None or confnum < 0:
		confnum = 2

	if confnum == 0:
		pass
	elif confnum == 1:
		configure_res = collectd_cfg_confnum(confnum)
	elif confnum == 2:
		#cold.collectd_configure_default(suffixed)
		configure_res = collectd_cfg_confnum(confnum)
	else:
		pass

	return configure_res


@first_arg_forced_boolean
def collectd_cfg_check(verbose=False):
    """ A verbose=False defaulting variant of collectd_check """
    return cold.collectd_syntax_check(verbose)


@first_arg_forced_boolean
def collectd_check(verbose=True):
    return cold.collectd_syntax_check(verbose)


@first_arg_forced_boolean
def collectd_plugins_check(verbose=False):
    """ Check plugins. Default verbose=False """
    return cold.collectd_plugins_check(verbose)


def collectd_restart():
    return cold.collectd_restart()


def collectd_start():
    return cold.collectd_start()


def collectd_stop():
    return cold.collectd_stop()


def collectd_status():
    status_res = cold.collectd_status()
    if status_res is True:
        pass
    else:
        line = "Status check returned {0} indicating collectd is NOT running".format(
            status_res)
        print(line)
    return status_res


def collection_page_support(verbose=True):
    return cold.collection_page_support(verbose)


def collectd_web_py(repo_url=None):
    """ requires libjson-perl
    To have collectd-web listen on all interfaces run next command:
    python ./runserver.py 0.0.0.0 8888
    """
    repo_url_collectd_web = 'https://github.com/httpdss/collectd-web.git'
    revision = '8e0ee4ad47fe8c9ba9de6a766aba7c6ae6e10649'
    repo_parentdir = '/var/www'
    print "Cloning into directory {0}/".format(repo_parentdir)
    repo_localdir = "{0}/collectd-web".format(repo_parentdir)
    with cd(repo_parentdir):
        dir_res = futil.files_directories.mkdir_existing(repo_localdir,2755,remove_existing=True)
        if repo_url is None:
            clone_res = sign_repo.git_clone(repo_url_collectd_web)
            if clone_res:
                futil.package_deb.requires_dpkg_package('libjson-perl',True)
    return


def collectd_web_py_apache(repo_url=None):
    """ requires libjson-perl
    To have collectd-web listen on all interfaces run next command:
    python ./runserver.py 0.0.0.0 8888
    """
    repo_url_collectd_web = 'https://github.com/httpdss/collectd-web.git'
    revision = '8e0ee4ad47fe8c9ba9de6a766aba7c6ae6e10649'
    repo_parentdir = '/var/www'
    print "Cloning into directory {0}/".format(repo_parentdir)
    repo_localdir = "{0}/collectd-web".format(repo_parentdir)
    futil.package_deb.requires_dpkg_package('libapache2-mod-wsgi',True)
    futil.package_deb.requires_binary_package('apache2ctl','apache2',assumeyes=True)
    with cd(repo_parentdir):
        dir_res = futil.files_directories.mkdir_existing(repo_localdir,2755,remove_existing=True)
        if repo_url is None:
            clone_res = sign_repo.git_clone(repo_url_collectd_web)
            if clone_res:
                futil.package_deb.requires_dpkg_package('libjson-perl',True)
                futil.files_directories.group_writable_recursive(
                    repo_localdir,'/var/www/',None,['css','js','png'])
    return


def collectd_web_py_apache_advanced(repo_url=None):
    """ Assumes you have already run collectd_installed previously
    """
    collectd_web_py_apache(repo_url)
    """ futil.package_deb.requires_dpkg_package('libapache2-mod-wsgi',True) """
    # carbon_res = futil.package_deb.requires_dpkg_package('graphite-carbon',True)
    carbon_res = futil.package_deb.requires_packages_noninteractive(['graphite-carbon'],True,False,False)
    # Next install packages to support write_graphite plugin of collectd
    package_list = ['rrdcached',
                    'graphite-web',
                    'libjs-jquery',
                    'python-rrdtool',
                    'python-sqlite',
                    'python-simplejson',
                    'python-tz']
    graphite_res = futil.package_deb.requires_packages(package_list,True,False,False)

    if graphite_res:
        graphite_res = cold.collectd_plugin_write_graphite(True,0)
    
    """
    cd /usr/share/doc/graphite-web/
    graphite-manage syncdb
    chown _graphite:_graphite /var/lib/graphite/graphite.db
    su -s /bin/bash -c '/usr/bin/django-admin runserver --settings graphite.settings 0.0.0.0:8080' _graphite
    """
    return graphite_res


def collectd_web_py_nginx():
    uwsging.uwsgi_python()
    uwsging.nginx_uwsgi('all','collectd',None)
    uwsging.uwsgi_app('collectd','/srv/collectd/socket')
    return

