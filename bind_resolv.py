#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd print_preamble_fast
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd print_preamble
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd nameserver_check
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd nameserver_check:domain=wrightsolutions.co.uk
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd bind_status
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd bind_status_keychecking
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd bind_config_summary
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd bind_config_summary_selective
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd bind_configlines_enabled
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd bind_configlines_disabled
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd bind_config_keys_enabled
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd bind_config_keys_enabled:printable_only=False
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd bind_listenon_line_show
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd bind_listenonany4_preview
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd rndc_key_show
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd key_show:verbose=True
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd key_show_verbose
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd rndc_confgen_typical
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd rndc_confgen_root_output:keysize=256
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd rndc_querylog_toggle
# fab -a -f bind_resolv.py -u root -H aa.bb.cc.dd querylog_toggle


import fabutil as futil
from fabutil import conf_get_set as getset
# import pprint; pp2 = pprint.PrettyPrinter(indent=2)
from fabutil import dnsbind as bind



def first_arg_false_to_false(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is True or first_arg in ['true','True','TRUE']:
                # Ensure we never interfere with any first arg intended as positive boolean
                pass
            elif first_arg is False:
                # This branch will likely never be encountered
                pass
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def first_arg_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if len(args) > 0:
            #print 'Wrapped function', function_to_decorate.__name__, 'has args'
            first_arg = args[0]
            if first_arg is True:
                # Ensure we never interfere with any first arg intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as True'
                pass
            elif first_arg is False:
                # Ensure we never interfere with any first arg intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as False'
                pass
            elif first_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to True'
                args_list = list(args)
                args_list[0] = True
                args = tuple(args_list)
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to False'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if 'verbose' in kwargs:
            #print "Wrapped function", function_to_decorate.__name__, "has kwarg 'verbose'"

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as True'
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as False'
                pass
            elif verbose_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to True'
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to False'
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


@first_arg_forced_boolean
@verbose_forced_boolean
def centoslike_contract(verbose=False):
    """ Returns several indicators. Most useful where network delays are not a factor
    Do use the wrapper functions centoslike() or centoslike2() as appropriate.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    #print pretty_tuple
    rline = futil.cmdbin.release_line()
    #print rline
    #if futil.cmdbin.cpu64():
    #    print "CPU has 64 bit support"
    centos_or_centoslike = None
    debian_or_debianlike = False
    if pretty_tuple[1] in futil.cmdbin.DEBIAN_PLUS_DERIVATIVES2:
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif 'ebianlike' in rline:
        debian_or_debianlike = True
        centos_or_centoslike = False
    else:
        pass
    if pretty_tuple[1] == 'debian':
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif debian_or_debianlike:
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        centos_or_centoslike = True
        debian_or_debianlike = False
        if verbose is True:
            print "EPEL: {0}".format(futil.package_rpm.has_epel())
            print futil.package_rpm.repolist_dict()

    if centos_or_centoslike is None:
        # If inconclusive then set as False
        centos_or_centoslike = False

    return centos_or_centoslike


@first_arg_forced_boolean
@verbose_forced_boolean
def centoslike(verbose=False):
    """ Wrapper around centoslike_contract() to which you can add any
    optional behaviour you choose later
    """
    centos_or_centoslike = centoslike_contract(verbose)
    return centos_or_centoslike


def centoslike2(verbose=True):
    """ Wrapper around centoslike_contract with verbose forced True
    Because selinux config file might be considered sensitive we make 
    pulling that file locally a feature of this wrapper rather than
    being default behaviour of main centoslike() function
    Avoid adding time consuming things like 'repolist' as centoslike2() is
    intended to be callable from _fast() type wrappers and repolist is too slow
    """
    centos_or_centoslike = centoslike_contract(verbose)
    if centos_or_centoslike:
        futil.selinuxstatus.sestatus1print()
        keyed = futil.selinuxstatus.seconfig_keyed()
        #print "keyed has length {0}".format(len(keyed))
        #print keyed
        if 'SELINUX' in keyed:
            print "SELINUX set {0}".format(keyed['SELINUX'])
        if 'SELINUXTYPE' in keyed:
            print "SELINUXTYPE set {0}".format(keyed['SELINUXTYPE'])
        eline = futil.selinuxstatus.enforced1()
        if len(eline) > 0:
            print("SE Linux enforced? Current running system is set {0}".format(eline[0]))
    if not centos_or_centoslike:
        return centos_or_centoslike
    md5text = futil.cmdbin.md5text_or_sudo('/etc/ssh/sshd_config')
    print("sshd_config md5 is {0}".format(md5text))
    #from ordered2dict import KeyedOD,KeyedEqualsOD
    EXPRESSIONS=['sha512','sha256','md5sum']
    with getset.Keyedequals('/etc/sysconfig/authconfig') as conf:
        line = ''
	if 'PASSWDALGORITHM' in conf:
            #print(conf.keylines_typed(1,['PASSWDALGORITHM']))
            line = conf.keyline1rstripped('PASSWDALGORITHM')
        lines = []
	if line is not None and len(line) > 2:
            print(line)
        else:
            # Not enough printed yet so put in some more effort below
            lines = conf.lines_noncomment_containing_any(EXPRESSIONS)
        for line in lines:
            print(line)
    return centos_or_centoslike


@first_arg_false_to_false
def print_preamble(backports_epel_report=True):
    """ Print preamble showing some short fingerprint style markers for the server.
    Even if we know we are Debian or RPM based, we might
    still distinguish servers based on availability of extra repositories.
    backports_epel_true=True (default) gives you some feedback about extra
    repositories.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print pretty_tuple
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    if not backports_epel_report:
        # Do not bother reporting about backports / epel
        pass
    elif pretty_tuple[1] == 'debian':
        print "Backports: {0}".format(futil.package_deb.has_backports())
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        print "EPEL: {0}".format(futil.package_rpm.has_epel())
        print futil.package_rpm.repolist_dict()

    if backports_epel_report:
        """ Consider backports_epel_report similar to verbose=true
        way of requesting more info """
        futil.selinuxstatus.sestatus1print()
    return


@first_arg_false_to_false
def print_preamble_fast(verbosity=0):
    """ Print preamble showing some short fingerprint style markers for the server.
    This variant avoids checking centos for epel to avoid time consuming 'yum repolist'.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print(pretty_tuple)
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    if pretty_tuple[1] == 'debian':
        pass
        # fast so skip: print "Backports: {0}".format(futil.package_deb.has_backports())
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        # Best guess: We are centos or centoslike
        centoslike2(verbose=False)
        """ futil.selinuxstatus.sestatus1print()
        By calling centoslike2() we already get some commentary about
        selinux status
        """
    else:
        pass
    return


@verbose_forced_boolean
def nameserver_check(domain=None,verbose=False):
    """ Count nameserver entries and test a nameserver lookup """
    futil.dns.nameserver_count_excluding_print(futil.dns.GOOGLELIST)
    #futil.dns.nameserver_count_excluding_print('google')
    if centoslike_contract(verbose=False):
        pass
    elif verbose:
        futil.dns.resolvconf_dirs_more()
    else:
        pass
    futil.dns.nameserver_count_startswith('192.',True,True)
    futil.dns.drill_or_dig_domain(domain)
    spf_lines = futil.dns.drill_or_dig_spf('random',verbose,True)
    if len(spf_lines) > 0:
        pass
    elif domain is not None:
        futil.dns.drill_or_dig_txt(domain,verbose)
    return


@first_arg_forced_boolean
@verbose_forced_boolean
def bind_status(verbose=True):
    """ Current status of bind nameserver daemon plus precheck of rndc key """
    if verbose is True:
        fres = key_show_verbose()
    blines = bind.rn_status_print(verbose)
    return blines


def bind_config_summary():
    """ Print bind named summary - list 'enabled' named config lines """
    for line in futil.dnsbind.config_keylines_enabled():
        print(line)
    #print futil.dnsbind.config_keylines_enabled_linesep()
    return


def bind_config_summary_selective():
    """ Print bind named summary - selective config lines """
    lines = futil.dnsbind.config_summary(print_flag=True,line_prefix='filepath')
    return lines


def bind_config_keys_enabled(printable_only=True):
    """ Bind named is key value format. Here print the enabled key having value(s) """
    # Not to be confused with printing binds own internal key or key for talking with rndc
    if printable_only:
        klist = futil.dnsbind.config_keys_enabled_printable(print_flag=True)
    else:
        klist = futil.dnsbind.config_keys_enabled(print_flag=True)
    #for key in klist:
    #    print(key)
    return klist


def bind_configlines_enabled(verbose=True):
    #lines = futil.dnsbind.config_keylines_enabled_linesep()
    lines = futil.dnsbind.config_keylines_enabled()
    for line in lines:
        print(line.rstrip())
    return lines


def bind_configlines_disabled(verbose=True):
    lines = futil.dnsbind.config_keylines_disabled()
    for line in lines:
        print(line.rstrip())
    return lines


def rndc_key_show(keypath='/etc/rn*.key'):
    fres = futil.files_directories.dirpath_dlp(dirpath=keypath)
    return fres


@first_arg_forced_boolean
@verbose_forced_boolean
def key_show(verbose=False):
    """ Alias / thin wrapper around rndc_key_show """
    fres = rndc_key_show()
    lines = []
    if verbose:
        lines = futil.cmdbin.run_res_lines_stripped(fres)
    for line in lines:
        print(line)
    return fres


def key_show_verbose():
    """ Alias around key_show with verbose=True """
    fres = key_show(True)
    return fres


@first_arg_forced_boolean
@verbose_forced_boolean
def bind_status_keychecking(verbose=True):
    """ Current status of bind nameserver daemon plus precheck of rndc key """
    fres = key_show_verbose()
    blines = bind.rn_status_print(verbose)
    return blines


@first_arg_forced_boolean
@verbose_forced_boolean
def bind_listenon_line_show(verbose=True):
    """ Show the ipv4 relevant listen-on line from the active bind (named) config """
    lline = futil.dnsbind.listenon_line(verbose)
    return lline

@first_arg_forced_boolean
@verbose_forced_boolean
def bind_listenonany4_preview(verbose=True):
    """ Attempt to preview the change required to make bind listen on any ipv4 """
    pline = futil.dnsbind.config_any4_preview(verbose=True)
    if verbose is True:
        print(pline)
    return pline


def rndc_confgen_typical():
    """ For centos do a typical rndc_confgen """
    genlines = []
    if centoslike_contract(verbose=False):
        genlines = bind.rn_confgen_a()
    return genlines


def rndc_confgen_root_output(keysize=256):
    """ For centos do a rndc_confgen that saves output in /root/ """
    rlines = bind.rn_confgen_root_output(keysize)
    return rlines


@first_arg_forced_boolean
@verbose_forced_boolean
def rndc_querylog_toggle(verbose=True):
    """ Toggle the ON/OFF status of querylog via rndc """
    rlines = bind.rn_querylog_toggle(verbose)
    return rlines

@first_arg_forced_boolean
@verbose_forced_boolean
def querylog_toggle(verbose=True):
    """ Alias of rndc_querylog_toggle """
    return rndc_querylog_toggle(verbose)


