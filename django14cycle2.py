#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f django14cycle2.py -u root -H aa.bb.cc.dd poor_cycle1counts
# fab -a -f django14cycle2.py -u root -H aa.bb.cc.dd poor_cycle2counts
# fab -a -f django14cycle2.py -u root -H aa.bb.cc.dd poor_cycle5
# fab -a -f django14cycle2.py -u root -H aa.bb.cc.dd poor_cycle10
# fab -a -f django14cycle2.py -u root -H aa.bb.cc.dd sync_load_index1
# Above five examples are more for script testing, with next examples better for benchmarking
# fab -a -f django14cycle2.py -u root -H aa.bb.cc.dd poor_cycle1rc
# fab -a -f django14cycle2.py -u root -H aa.bb.cc.dd poor_cycle2rc
# fab -a -f django14cycle2.py -u root -H aa.bb.cc.dd poor_cycle4rc
# fab -a -f django14cycle2.py -u root -H aa.bb.cc.dd poor_cycle8rc
# fab -a -f django14cycle2.py -u root -H aa.bb.cc.dd sync_load_index1
# fab -a -f django14cycle2.py -u root -H aa.bb.cc.dd sync_load_index8888

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

import fabutil as futil

PRETTIFY_RC_PY='prettify/local-database/rc/py'


def poor_cycle1counts():
    """ 1 concurrent. Fetches counts page """
    return futil.web_wsgi.poor_cycle(target='counts',concurrent=1,totaltime=120,data='full',visible='internal')

def poor_cycle2counts():
    """ 2 concurrent. Fetches counts page """
    return futil.web_wsgi.poor_cycle(target='counts',concurrent=2,totaltime=180,data='full',visible='both')

def poor_cycle1rc():
    """ 1 concurrent """
    return futil.web_wsgi.poor_cycle(target=PRETTIFY_RC_PY,concurrent=1,totaltime=180,data='full',visible='both')

def poor_cycle2rc():
    """ 2 concurrent. Used for day1 test of server, before any detailed configuration """
    return futil.web_wsgi.poor_cycle(target=PRETTIFY_RC_PY,concurrent=2,totaltime=180,data='full',
                                     visible='both',app_name='local2dj14',dir_fab='django14cycle2.fab')

def poor_cycle4rc():
    """ 4 concurrent """
    return futil.web_wsgi.poor_cycle(target=PRETTIFY_RC_PY,concurrent=4,totaltime=180,data='full',
                                     visible='both',app_name='local2dj14',dir_fab='django14cycle2.fab')

def poor_cycle8rc():
    """ 8 concurrent """
    return futil.web_wsgi.poor_cycle(target=PRETTIFY_RC_PY,concurrent=8,totaltime=180,data='full',
                                     visible='both',app_name='local2dj14',dir_fab='django14cycle2.fab')

def poor_cycle5():
    """ Benchmark with a fixed short burst of http (5 seconds) """
    return futil.web_wsgi.poor_cycle(target=PRETTIFY_RC_PY,concurrent=2,totaltime=5,data='full',
                                     visible='internal')

def poor_cycle10():
    """ Benchmark with a fixed short burst of http (10 seconds) """
    return futil.web_wsgi.poor_cycle(target=PRETTIFY_RC_PY,concurrent=2,totaltime=10,data='full',
                                     visible='internal')

def sync_load_index1():
    """ Lightweight alternative that does sync, load, index and no http benchmarking """
    return futil.web_wsgi.sync_load_index14one(True)


def sync_load_index8888():
    """ Sync, load, index and no http benchmarking but using fixed port 8888 """
    return futil.web_wsgi.sync_load_index14one(True,8888)
