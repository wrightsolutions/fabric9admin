#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


# fab -a -f debian.py -u root -H aa.bb.cc.dd count
# fab -a -f debian.py -u root -H aa.bb.cc.dd count2
# fab -a -f debian.py -u root -H aa.bb.cc.dd count2:verbose=false
# fab -a -f debian.py -u root -H aa.bb.cc.dd size
# fab -a -f debian.py -u root -H aa.bb.cc.dd size:packagename=locales
# fab -a -f debian.py -u root -H aa.bb.cc.dd size:verbose=false
# fab -a -f debian.py -u root -H aa.bb.cc.dd size:alphabetical=true
# fab -a -f debian.py -u root -H aa.bb.cc.dd size:section=editors
# fab -a -f debian.py -u root -H aa.bb.cc.dd installed_size
# fab -a -f debian.py -u root -H aa.bb.cc.dd installed_size:packagename=locales
# fab -a -f debian.py -u root -H aa.bb.cc.dd installed_size:verbose=false
# fab -a -f debian.py -u root -H aa.bb.cc.dd installed_size:alphabetical=true
# fab -a -f debian.py -u root -H aa.bb.cc.dd installed_size:section=editors
# fab -a -f debian.py -u root -H aa.bb.cc.dd selections
# fab -a -f debian.py -u root -H aa.bb.cc.dd selections:save=true
# fab -a -f debian.py -u root -H aa.bb.cc.dd infolist
# fab -a -f debian.py -u root -H aa.bb.cc.dd infolist:save=true
# fab -a -f debian.py -u root -H aa.bb.cc.dd manual_list
# fab -a -f debian.py -u root -H aa.bb.cc.dd manual_list:save=true
# fab -a -f debian.py -u root -H aa.bb.cc.dd manual25
# fab -a -f debian.py -u root -H aa.bb.cc.dd manual50
# fab -a -f debian.py -u root -H aa.bb.cc.dd showauto_list
# fab -a -f debian.py -u root -H aa.bb.cc.dd showauto_list:save=true
# fab -a -f debian.py -u root -H aa.bb.cc.dd manual_salt
# fab -a -f debian.py -u root -H aa.bb.cc.dd manual_salt:save=true
# fab -a -f debian.py -u root -H aa.bb.cc.dd showauto_salt
# fab -a -f debian.py -u root -H aa.bb.cc.dd showauto_salt:save=true
# Shortcut to run both selections:save=true and infolist:save=true shown below
# fab -a -f debian.py -u root -H aa.bb.cc.dd to_file

from os import path
import fabutil as futil
from fabutil import conf_get_set as getset
# import pprint; pp2 = pprint.PrettyPrinter(indent=2)


def first_arg_false_to_false(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is True or first_arg in ['true','True','TRUE']:
                # Ensure we never interfere with any first arg intended as positive boolean
                pass
            elif first_arg is False:
                # This branch will likely never be encountered
                pass
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def first_arg_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if len(args) > 0:
            #print 'Wrapped function', function_to_decorate.__name__, 'has args'
            first_arg = args[0]
            if first_arg is True:
                # Ensure we never interfere with any first arg intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as True'
                pass
            elif first_arg is False:
                # Ensure we never interfere with any first arg intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as False'
                pass
            elif first_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to True'
                args_list = list(args)
                args_list[0] = True
                args = tuple(args_list)
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to False'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if 'verbose' in kwargs:
            #print "Wrapped function", function_to_decorate.__name__, "has kwarg 'verbose'"

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as True'
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as False'
                pass
            elif verbose_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to True'
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to False'
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


@first_arg_false_to_false
def print_preamble(backports_epel_report=True):
    """ Print preamble showing some short fingerprint style markers for the server.
    Even if we know we are Debian or RPM based, we might
    still distinguish servers based on availability of extra repositories.
    backports_epel_true=True (default) gives you some feedback about extra
    repositories.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print pretty_tuple
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    if not backports_epel_report:
        # Do not bother reporting about backports / epel
        pass
    elif pretty_tuple[1] == 'debian':
        print "Backports: {0}".format(futil.package_deb.has_backports())
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        print "EPEL: {0}".format(futil.package_rpm.has_epel())
        print futil.package_rpm.repolist_dict()
    return


@verbose_forced_boolean
def installed_size(packagename=None,verbose=True,alphabetical=False,section=None):
    if alphabetical in ['true','True','TRUE']:
        alphabetical = True
    else:
        alphabetical = False
    sizes_available = False
    sizes = futil.package_deb.dpkg_size(packagename,verbose,alphabetical,section)
    if len(sizes) > 0:
        sizes_available = True
        for line in sizes:
            print line
    return sizes_available


@verbose_forced_boolean
def size(packagename=None,verbose=True,alphabetical=False,section=None):
    """ Short name wrapper around installed_size()
    return installed_size(*args, **kwargs) """
    return installed_size(packagename,verbose,alphabetical,section)


def installed_count():
    count = futil.package_deb.dpkg_installed_count()
    if count < 5:
        print "Installed count of {0} is suspect, log on to server and check".format(count)
    else:
        print "Installed count is {0}".format(count)
    return count


@verbose_forced_boolean
def installed_count2(verbose=False):
    if verbose is True:
        print_preamble()
    futil.package_deb.dpkg_installed_counts(True)
    return count


@verbose_forced_boolean
def installed_counts(verbose=True):
    """ installed, configured, removed, purged and
    count of packages marked 'manual'
    """ 
    installed_count2()
    # Here would go extra information about configured, removed, purged
    manual_list(False)
    return


def count(*args, **kwargs):
    """ Short name wrapper around installed_count() """
    return installed_count(*args, **kwargs)


def count2(*args, **kwargs):
    """ Short name wrapper around installed_count2() """
    return installed_count2(*args, **kwargs)


def counts(*args, **kwargs):
    """ Short name wrapper around installed_counts() """
    return installed_counts(*args, **kwargs)


def manual25():
    manual_list = futil.package_state.info_long(print_flag=True,prefix=' ',count=25,manual_only=True)
    return manual_list


def manual50():
    manual_list = futil.package_state.info_long(print_flag=True,prefix=' ',count=50,manual_only=True)
    return manual_list


@first_arg_false_to_false
def selections(save=False):
    selections = []
    if save is False:
        selections = futil.package_deb.selections_raw()
        print "Selections count is {0}".format(len(selections))
    else:
        filepath = futil.package_deb.selections_to_file('auto')
        print "Selections written to {0}".format(filepath)
    return selections


def selections_save(filepath='auto'):
    filepath = futil.package_deb.selections_to_file(filepath)
    print "Selections written to {0}".format(filepath)
    return filepath


@first_arg_false_to_false
def infolist(save=False):
    installed_list = []
    if save is False:
        installed_list = futil.package_deb.info_installed()
        print "Installed (info) count is {0}".format(len(installed_list))
    else:
        filepath = futil.package_deb.info_to_file('auto')
        print "Installed (info) written to {0}".format(filepath)
    return installed_list


@first_arg_false_to_false
def manual_list(save=False):
    marked_list = []
    if save is False:
        marked_list = futil.package_state.manual_raw()
        print "Manual count is {0}".format(len(marked_list))
    else:
        filepath = futil.package_state.manual_to_file('auto')
        if filepath:
            print "Manual count of written to {0}".format(filepath)
        else:
            print "Manual count of {0} not written - unexpected".format(
                len(marked_list))
    return marked_list


@first_arg_false_to_false
def showauto_list(save=False):
    marked_list = []
    if save is False:
        marked_list = futil.package_state.showauto_raw()
        print "Showauto count is {0}".format(len(marked_list))
    else:
        filepath = futil.package_state.showauto_to_file('auto')
        if filepath:
            print "Showauto count written to {0}".format(filepath)
        else:
            print "Showauto count of {0} not written - unexpected".format(
                len(marked_list))
    return marked_list


@first_arg_false_to_false
def manual_salt(save=False,filepath='auto'):
    outform_list = []
    if save is False:
        outform_list = futil.package_state.manual_raw(None,'salt')
        filtered_list = futil.package_state.packages_outform_unsalt(outform_list)
        print "Manual count is {0}".format(len(filtered_list))
    else:
        filepath = futil.package_state.manual_to_file(filepath,'salt')
        if filepath:
            print "Manual count written to {0}".format(filepath)
        else:
            print "Manual count not written to {0} - unexpected".format(filepath)
    return outform_list


@first_arg_false_to_false
def showauto_salt(save=False):
    outform_list = []
    if save is False:
        outform_list = futil.package_state.showauto_raw(None,'salt')
        filtered_list = futil.package_state.packages_outform_unsalt(outform_list)
        print "Showauto count is {0}".format(len(filtered_list))
    else:
        filepath = futil.package_state.showauto_to_file('auto','salt')
        if filepath:
            print "Showauto count written to {0}".format(filepath)
        else:
            print "Showauto count not written to {0} - unexpected".format(filepath)
    return outform_list


def to_file():
    saved_filepath = None
    saved_filepath = selections_save()
    if saved_filepath is not None:
        try:
            filepath = path.splitext(saved_filepath)[0]
            infolist_filepath = futil.package_deb.info_to_file(filepath)
            print "Installed (info) written to {0}".format(infolist_filepath)
        except Exception:
            saved_filepath = None
    #infolist(True)
    if saved_filepath is not None:
        manual_salt(save=True,filepath=saved_filepath)
    return saved_filepath


