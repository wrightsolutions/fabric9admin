#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#from fabric.contrib.files import exists
import fabutil as futil
from fabutil import conf_get_set as getset
from fabutil import cmdbin
# import pprint; pp2 = pprint.PrettyPrinter(indent=2)

# fab -a -f sshctl.py -u root -H aa.bb.cc.dd sshd_summary_verbose
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd sshd_enabled
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd sshd_config
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd config
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd config_lines_commented
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd config_reset
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd loglevel_line_context
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd loglevel_line_context:verbose=True
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd loglevel_line
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd loglevel_setter_debug_verbose2
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd loglevel_setter_debug_remote
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd loglevel_setter_debug_remote_md5ignore
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd loglevel_setter_info_remote
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd loglevel_setter_info_remote_md5ignore
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd passwordauth_setter_no_remote
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd passwordauth_setter_no_remote_md5ignore
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd passwordauth_setter_yes_remote
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd passwordauth_setter_yes_remote_md5ignore
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd permitrootlogin_setter_prohibitpassword_remote
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd permitrootlogin_setter_prohibitpassword_remote_md5ignore
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd permitrootlogin_setter_forcedcommandsonly_remote
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd permitrootlogin_setter_forcedcommandsonly_remote_md5ignore
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd print_preamble
# fab -a -f sshctl.py -u root -H aa.bb.cc.dd print_preamble:backports_epel_report=True

""" Query sshd_config and report on key values
Update the remote sshd_config when requested via appropriate option (_remote_md5ignore?)

Error: File /etc/ssh/sshd_config encountered a <type 'exceptions.SystemExit'> exception
is probably explained better as 'bash: sudo: command not found' (appears further up in text)
Solution: install sudo
"""


def first_arg_false_to_false(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is True or first_arg in ['true','True','TRUE']:
                # Ensure we never interfere with any first arg intended as positive boolean
                pass
            elif first_arg is False:
                # This branch will likely never be encountered
                pass
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def first_arg_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if len(args) > 0:
            #print 'Wrapped function', function_to_decorate.__name__, 'has args'
            first_arg = args[0]
            if first_arg is True:
                # Ensure we never interfere with any first arg intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as True'
                pass
            elif first_arg is False:
                # Ensure we never interfere with any first arg intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as False'
                pass
            elif first_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to True'
                args_list = list(args)
                args_list[0] = True
                args = tuple(args_list)
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to False'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if 'verbose' in kwargs:
            #print "Wrapped function", function_to_decorate.__name__, "has kwarg 'verbose'"

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as True'
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as False'
                pass
            elif verbose_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to True'
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to False'
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def nsswitch_db():
    with getset.Keyedfile('/etc/nsswitch.conf') as conf:
        assert conf.keysep == ':'
        for line_db in conf.lines_enabled_contains(' db'):
            print line_db
    return


def nsswitch_files():
    """ Rather than use lines_enabled_contains(' files') we
    choose an alternative approach as in this program we are
    attempting to provide examples / illustrations of the api
    """
    with getset.Keyedfile('/etc/nsswitch.conf') as conf:
        # Using lines_enabled() instead of keylines_enabled() to drop line ending
        for line in conf.lines_enabled():
            if ' files' in line:
                print line
        assert conf.keysep == ':'
    return


@first_arg_forced_boolean
@verbose_forced_boolean
def centoslike_contract(verbose=False):
    """ Returns several indicators. Most useful where network delays are not a factor
    Do use the wrapper functions centoslike() or centoslike2() as appropriate.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    #print pretty_tuple
    rline = futil.cmdbin.release_line()
    #print rline
    #if futil.cmdbin.cpu64():
    #    print "CPU has 64 bit support"
    centos_or_centoslike = None
    debian_or_debianlike = False
    if pretty_tuple[1] in futil.cmdbin.DEBIAN_PLUS_DERIVATIVES2:
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif 'ebianlike' in rline:
        debian_or_debianlike = True
        centos_or_centoslike = False
    else:
        pass
    if pretty_tuple[1] == 'debian':
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif debian_or_debianlike:
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        centos_or_centoslike = True
        debian_or_debianlike = False
        if verbose is True:
            print "EPEL: {0}".format(futil.package_rpm.has_epel())
            print futil.package_rpm.repolist_dict()

    if centos_or_centoslike is None:
        # If inconclusive then set as False
        centos_or_centoslike = False

    return centos_or_centoslike


@first_arg_forced_boolean
@verbose_forced_boolean
def centoslike(verbose=False):
    """ Wrapper around centoslike_contract() to which you can add any
    optional behaviour you choose later
    """
    centos_or_centoslike = centoslike_contract(verbose)
    return centos_or_centoslike


def centoslike2(verbose=True):
    """ Wrapper around centoslike_contract with verbose forced True
    Because selinux config file might be considered sensitive we make 
    pulling that file locally a feature of this wrapper rather than
    being default behaviour of main centoslike() function
    Avoid adding time consuming things like 'repolist' as centoslike2() is
    intended to be callable from _fast() type wrappers and repolist is too slow
    """
    centos_or_centoslike = centoslike_contract(verbose)
    if centos_or_centoslike:
        futil.selinuxstatus.sestatus1print()
        keyed = futil.selinuxstatus.seconfig_keyed()
        #print "keyed has length {0}".format(len(keyed))
        #print keyed
        if 'SELINUX' in keyed:
            print "SELINUX set {0}".format(keyed['SELINUX'])
        if 'SELINUXTYPE' in keyed:
            print "SELINUXTYPE set {0}".format(keyed['SELINUXTYPE'])
        eline = futil.selinuxstatus.enforced1()
        if len(eline) > 0:
            print("SE Linux enforced? Current running system is set {0}".format(eline[0]))
    if not centos_or_centoslike:
        return centos_or_centoslike
    md5text = futil.cmdbin.md5text_or_sudo('/etc/ssh/sshd_config')
    print("sshd_config md5 is {0}".format(md5text))
    #from ordered2dict import KeyedOD,KeyedEqualsOD
    EXPRESSIONS=['sha512','sha256','md5sum']
    with getset.Keyedequals('/etc/sysconfig/authconfig') as conf:
        line = ''
	if 'PASSWDALGORITHM' in conf:
            #print(conf.keylines_typed(1,['PASSWDALGORITHM']))
            line = conf.keyline1rstripped('PASSWDALGORITHM')
        lines = []
	if line is not None and len(line) > 2:
            print(line)
        else:
            # Not enough printed yet so put in some more effort below
            lines = conf.lines_noncomment_containing_any(EXPRESSIONS)
        for line in lines:
            print(line)
    return centos_or_centoslike


@first_arg_false_to_false
def print_preamble(backports_epel_report=True):
    """ Print preamble showing some short fingerprint style markers for the server.
    Even if we know we are Debian or RPM based, we might
    still distinguish servers based on availability of extra repositories.
    backports_epel_true=True (default) gives you some feedback about extra
    repositories.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print pretty_tuple
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    if not backports_epel_report:
        # Do not bother reporting about backports / epel
        pass
    elif pretty_tuple[1] == 'debian':
        print "Backports: {0}".format(futil.package_deb.has_backports())
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        print "EPEL: {0}".format(futil.package_rpm.has_epel())
        print futil.package_rpm.repolist_dict()

    if backports_epel_report:
        """ Consider backports_epel_report similar to verbose=true
        way of requesting more info """
        futil.selinuxstatus.sestatus1print()
    return


@first_arg_false_to_false
@verbose_forced_boolean
def print_preamble_fast(verbosity=0):
    """ Print preamble showing some short fingerprint style markers for the server.
    This variant avoids checking centos for epel to avoid time consuming 'yum repolist'.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print(pretty_tuple)
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    if pretty_tuple[1] == 'debian':
        pass
        # fast so skip: print "Backports: {0}".format(futil.package_deb.has_backports())
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        # Best guess: We are centos or centoslike
        centoslike2(verbose=False)
        """ futil.selinuxstatus.sestatus1print()
        By calling centoslike2() we already get some commentary about
        selinux status
        """
    else:
        pass
    return


def sshd_summary_verbose():
    """ Print ssh summary - list 'enabled' ssh config lines """
    for line in futil.ssh_lokkit.sshd_config_keylines_enabled():
        print line
    #print futil.ssh_lokkit.sshd_config_keylines_enabled_linesep()
    print futil.ssh_lokkit.sshd_motd_yes_preview(verbose=True)
    return


@first_arg_forced_boolean
@verbose_forced_boolean
def lines_enabled(verbose=True):
    lines_enabled = futil.ssh_lokkit.sshd_config_keylines_enabled()
    if verbose is True:
        for line in lines_enabled:
            print(line)
    return lines_enabled


@first_arg_forced_boolean
@verbose_forced_boolean
def loglevel_line(verbose=False):
    """ Best attempt at the enabled / active LogLevel directive """
    line = futil.ssh_lokkit.loglevel_line()
    return line


@first_arg_forced_boolean
@verbose_forced_boolean
def loglevel_enabled_line(verbose=False):
    eline = futil.ssh_lokkit.loglevel_enabled_line(verbose)
    return eline


@first_arg_forced_boolean
@verbose_forced_boolean
def loglevel_line_context(verbose=False):
    """ Show both enabled and disabled LogLevel lines - together a 'context' """
    line = futil.ssh_lokkit.loglevel_line_context(verbose)
    return line


@verbose_forced_boolean
def loglevel_sedlines(loglevel='AUTHPRIV',verbose=False):
    sedlines = futil.ssh_lokkit.loglevel_sedlines(loglevel,verbose)
    return sedlines


def loglevel_setter_debug():
    """ Setter: LogLevel DEBUG """
    #futil.ssh_lokkit.config_context_update(keyname,new_value,verbosity=1,unchanged_okay=False)
    cup_res = futil.ssh_lokkit.loglevel_context_update('DEBUG',verbosity=0)
    #cup = futil.ssh_lokkit.config_context_update('LogLevel','DEBUG',verbosity=0)
    return cup_res


def loglevel_setter_debug_verbose2():
    """ verbosity=2 version of md5 ignoring Setter: LogLevel DEBUG """
    #futil.ssh_lokkit.config_context_update(keyname,new_value,verbosity=1,unchanged_okay=False)
    cup = futil.ssh_lokkit.config_context_update('LogLevel','DEBUG',verbosity=2)
    return cup


def loglevel_setter_debug_remote():
    """ verbosity=2 fixed because we are actually going to make the LogLevel
    change on the remote.
    Prefer the context_update_remote_ptype() calls to achieve our change
    """
    #cup = futil.ssh_lokkit.config_context_update_remote('LogLevel','DEBUG',verbosity=2)
    cup = futil.ssh_lokkit.loglevel_context_update_remote_ptype('DEBUG',verbosity=2)
    return cup


def loglevel_setter_debug_remote_md5ignore():
    """ verbosity=2 fixed because we are actually going to make the LogLevel change on the remote """
    #cup = futil.ssh_lokkit.config_context_update_remote('LogLevel','DEBUG',verbosity=2)
    cup = futil.ssh_lokkit.loglevel_context_update_remote('DEBUG',verbosity=2)
    return cup


def loglevel_setter_info_remote():
    """ verbosity=2 fixed because we are actually going to make the LogLevel
    change on the remote.
    Prefer the context_update_remote_ptype() calls to achieve our change
    """
    #cup = futil.ssh_lokkit.config_context_update_remote('LogLevel','INFO',verbosity=2)
    cup = futil.ssh_lokkit.loglevel_context_update_remote_ptype('INFO',verbosity=2)
    return cup


def loglevel_setter_info_remote_md5ignore():
    """ verbosity=2 fixed because we are actually going to make the LogLevel change on the remote """
    #cup = futil.ssh_lokkit.config_context_update_remote('LogLevel','INFO',verbosity=2)
    cup = futil.ssh_lokkit.loglevel_context_update_remote('INFO',verbosity=2)
    return cup


@first_arg_forced_boolean
@verbose_forced_boolean
def lines_disabled(verbose=True):
    lines = futil.ssh_lokkit.sshd_config_keylines_disabled(verbose)
    return lines


def sshd_config():
    levline = loglevel_line(False)
    if levline > 0:
        print("sshd_config() LogLevel shown below as reported by loglevel_line():")
        print(levline)
    else:
        print("sshd_config() LogLevel is currently difficult to detect :(")
    enabled_list = lines_enabled(True)
    return enabled_list


def config():
    """ Alias of sshd_config for convenience. """
    return sshd_config()


def lines_keylike_but_hashed():
    """ Alias of lines_disabled() for convenience. """
    lines = lines_disabled(verbose=True)
    return lines


def config_lines_commented():
    """ Alias of lines_disabled(). """
    lines = lines_disabled(verbose=True)
    return lines


def passwordauth_setter_no_remote():
    """ verbosity=2 fixed because we are actually going to make the PasswordAuthentication
    change on the remote.
    Prefer the context_update_remote_ptype() calls to achieve our change
    """
    #cup = futil.ssh_lokkit.config_context_update_remote('PasswordAuthentication','no',verbosity=2)
    cup = futil.ssh_lokkit.passwordauth_context_update_remote_ptype('no',verbosity=2)
    return cup


def passwordauth_setter_no_remote_md5ignore():
    """ verbosity=2 fixed because we are actually going to make the PasswordAuthentication
    change on the remote.
    Prefer the context_update_remote() calls to achieve our change as md5less
    """
    #cup = futil.ssh_lokkit.config_context_update_remote('PasswordAuthentication','no',verbosity=2)
    cup = futil.ssh_lokkit.passwordauth_context_update_remote('no',verbosity=2)
    return cup


def passwordauth_setter_yes_remote():
    """ verbosity=2 fixed because we are actually going to make the PasswordAuthentication
    change on the remote.
    Prefer the context_update_remote_ptype() calls to achieve our change
    """
    #cup = futil.ssh_lokkit.config_context_update_remote('PasswordAuthentication','yes',verbosity=2)
    cup = futil.ssh_lokkit.passwordauth_context_update_remote_ptype('yes',verbosity=2)
    return cup


def passwordauth_setter_yes_remote_md5ignore():
    """ verbosity=2 fixed because we are actually going to make the PasswordAuthentication
    change on the remote.
    Prefer the context_update_remote() calls to achieve our change as md5less
    """
    #cup = futil.ssh_lokkit.config_context_update_remote('PasswordAuthentication','yes',verbosity=2)
    cup = futil.ssh_lokkit.passwordauth_context_update_remote('yes',verbosity=2)
    return cup


def permitrootlogin_setter_prohibitpassword_remote():
    """ verbosity=2 fixed because we are actually going to make the PermitRootLogin
    change on the remote. Note: prohibit-password and without-password are interchangable and achieve same
    Prefer the context_update_remote_ptype() calls to achieve our change
    """
    #cup = futil.ssh_lokkit.config_context_update_remote('PermitRootLogin','prohibit-password',verbosity=2)
    cup = futil.ssh_lokkit.permitrootlogin_context_update_remote_ptype('prohibit-password',verbosity=2)
    return cup


def permitrootlogin_setter_prohibitpassword_remote_md5ignore():
    """ verbosity=2 fixed because we are actually going to make the PermitRootLogin
    change on the remote. Note: prohibit-password and without-password are interchangable and achieve same
    Prefer the context_update_remote() calls to achieve our change as md5less
    """
    #cup = futil.ssh_lokkit.config_context_update_remote('PermitRootLogin','prohibit-password',verbosity=2)
    cup = futil.ssh_lokkit.permitrootlogin_context_update_remote('prohibit-password',verbosity=2)
    return cup


def permitrootlogin_setter_forcedcommandsonly_remote():
    """ verbosity=2 fixed because we are actually going to make the PermitRootLogin
    change on the remote.
    Prefer the context_update_remote_ptype() calls to achieve our change
    """
    #cup = futil.ssh_lokkit.config_context_update_remote('PermitRootLogin','forced-commands-only',verbosity=2)
    cup = futil.ssh_lokkit.permitrootlogin_context_update_remote_ptype('forced-commands-only',verbosity=2)
    return cup


def permitrootlogin_setter_forcedcommandsonly_remote_md5ignore():
    """ verbosity=2 fixed because we are actually going to make the PermitRootLogin
    change on the remote.
    Prefer the context_update_remote() calls to achieve our change as md5less
    """
    #cup = futil.ssh_lokkit.config_context_update_remote('PermitRootLogin','forced-commands-only',verbosity=2)
    cup = futil.ssh_lokkit.permitrootlogin_context_update_remote('forced-commands-only',verbosity=2)
    return cup


@first_arg_forced_boolean
@verbose_forced_boolean
def config_reset(verbose=True):
    """ Reset the config [and binaries] using rpm -V so that default sshd_config """
    return futil.ssh_lokkit.sshd_config_reset(verbose)




