#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015,2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.8.5
# https://pypi.python.org/pypi/Fabric/1.8.5

""" Functions for querying system configuration settings including sysctl """

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
#from os import getuid,getgid,path
#from os import linesep as os_linesep
#import re

try:
    from . import cmdbin
except ValueError:
    import cmdbin
from . import conf_get_set as getset

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

CMD_FREE = "/usr/bin/free "
CMD_FREE_M_T = "/usr/bin/free -m -t "
CMD_NPROC = "/usr/bin/nproc "
CMD_SWAP_SUMMARY = "/sbin/swapon --summary;"
CMD_VM_SWAPPINESS = "/sbin/sysctl vm.swappiness;"
CMD_VM_DIRTY_RATIO = "/sbin/sysctl vm.dirty_ratio;"


def free_marked(print_flag=True):
    swap_total = None
    with hide('output'):
        pretty_res = cmdbin.run(CMD_FREE_M_T)
    free_lines = cmdbin.run_res_lines_stripped(pretty_res)
    print_lines = []
    if len(free_lines) > 0:
        for line in free_lines:
            if line.lower().startswith('swap'):
                swap_figures = line.split(chr(58))[1]
                swap_figures_array = swap_figures.strip().split()
                swap_total_string = swap_figures_array[0].strip()
                try:
                    swap_total = int(swap_total_string)
                except:
                    swap_total = 0
                if swap_total == 0:
                    print_lines.append("{0}   ZEROSWAP".format(line))
                elif swap_total > 0:
                    print_lines.append("{0}   SWAPAVAILABLE".format(line))
                else:
                    print_lines.append("{0}   SWAPUNKNOWN".format(line))
            elif line.lower().startswith('total') and chr(58) in line:
                proc_res = cmdbin.run(CMD_NPROC)
                proc_count = 0
                if proc_res.succeeded:
                    try:
                        proc_count = int(str(proc_res).strip())
                    except Exception:
                        proc_count = 0
                if proc_count > 0:
                    print_lines.append("{0}   PROCESSORS:{1}".format(line,proc_count))
                else:
                    print_lines.append(line)
            else:
                print_lines.append(line)
    if print_flag:
        for line in print_lines:
            print line
    return swap_total


def swap_summary(print_flag=False):
    with hide('output'):
        ros_res = cmdbin.run_or_sudo(CMD_SWAP_SUMMARY)
    swap_lines = cmdbin.run_res_lines_stripped(ros_res)
    if print_flag:
        for line in swap_lines:
            print line
    return swap_lines


def proc_sys_vm(print_flag=True,local=True):
    with hide('warnings'):
        swappiness_res = cmdbin.run_or_sudo(CMD_VM_SWAPPINESS)
        dirty_ratio_res = cmdbin.run_or_sudo(CMD_VM_DIRTY_RATIO)
    sysctl_lines = []
    if local is not False:
        sysctl_d_filepath = '/etc/sysctl.d/60-proc-sys-vm.conf'
        #exists60 = exists('/etc/sysctl.d/60-proc-sys-vm.conf',verbose=True)
        exists60 = exists(sysctl_d_filepath)
        if exists60:
            # hide warnings but show stdout
            with hide('stdout','warnings'):
                cat_line = "{0} {1}".format(cmdbin.CMD_CAT,sysctl_d_filepath)
                ros_res = cmdbin.run_or_sudo(cat_line)
                sysctl_lines = cmdbin.run_res_lines_stripped(ros_res)
    if len(sysctl_lines) > 0:
        if print_flag is not False:
            for line in sysctl_lines:
                print line
    return sysctl_lines
