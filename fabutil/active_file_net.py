#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2018,2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from collections import Counter,defaultdict,namedtuple,OrderedDict
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
#from fabric.contrib.files import exists,append
from os import path
import re
from string import ascii_lowercase,digits,printable,punctuation,whitespace
try:
    from . import cmdbin
except ValueError:
    import cmdbin
from . import package_deb as pdeb
from . import package_rpm as prpm

CMD_NDO2DB='/usr/sbin/ndo2db'

# Future: First line next might need changing to && instead of (|) once test cases available
CMD_LSOF_REG_CSV_POSTGRESQL_LOG="""lsof -F pntcs0 +D /var/log/postgresql/ | \
/bin/sed '{3}N; /^\(.*\)\\n\\1$/!P; D' | /bin/sed 's/^p/\\np/' | \
/bin/sed -e '/./,/^t/{0}H;{3}d;{1}' -e 'x;/tREG/!d' -e 's/tREG\\x0s/,REG,/g' {2}
/bin/sed 's/\(\\x0\)/\\n/' | /bin/sed -e 's/^p//g' -e 's/^c/,/g' | \
/bin/sed 's/^n/,/g' | \
/bin/sed -e ':a;N;{3}ba;s/\\n/ /g' -e 's/  /\\n/g' -e 's/ ,/,/g' -e 's/^[ ]*\([0-9]*\)\(.*\)/\\1\\2/g'
""".format(cmdbin.BRACES_OPEN,cmdbin.BRACES_CLOSE,cmdbin.PIPE_BACKSLASH,
           cmdbin.DOLLAR_EXCLAMATION,cmdbin.XZERO)

#CMD_LSOF_REG_CSV_DIRECTORY_TEMPLATE="""/usr/bin/lsof -F pntcs0 +D %s && \
#Above disabled to more cross version (centos/debian) but may need reimplemented different way
CMD_LSOF_REG_CSV_DIRECTORY_TEMPLATE="""lsof -F pntcs0 +D %s && \
/bin/sed '{3}N; /^\(.*\)\\n\\1$/!P; D' | /bin/sed 's/^p/\\np/' | \
/bin/sed -e '/./,/^t/{0}H;{3}d;{1}' -e 'x;/tREG/!d' -e 's/tREG\\x0s/,REG,/g' {2}
/bin/sed 's/\(\\x0\)/\\n/' | /bin/sed -e 's/^p//g' -e 's/^c/,/g' | \
/bin/sed 's/^n/,/g' | \
/bin/sed -e ':a;N;{3}ba;s/\\n/ /g' -e 's/  /\\n/g' -e 's/ ,/,/g' -e 's/^[ ]*\([0-9]*\)\(.*\)/\\1\\2/g'
""".format(cmdbin.BRACES_OPEN,cmdbin.BRACES_CLOSE,cmdbin.PIPE_BACKSLASH,
           cmdbin.DOLLAR_EXCLAMATION)

#CMD_LSOF_REG_CSV_DIRECTORY_TEMPLATE="""/usr/bin/lsof -F pntcs0 +D {1} | \
#/bin/sed '$!N; /^\(.*\)\n\1$/!P; D' | /bin/sed 's/^p/\np/' | \
#/bin/sed -e '/./,/^t/{H;$!d;}' -e 'x;/tREG/!d' -e 's/tREG{2}s/,REG,/g' |\ 
#/bin/sed 's/\({2}\)/\n/' | /bin/sed -e 's/^p//g' -e 's/^c/,/g' | \
#/bin/sed 's/^n/,/g' | \
#/bin/sed -e ':a;N;$!ba;s/\n/ /g' -e 's/  /\n/g' -e 's/ ,/,/g' -e 's/^[ ]*\([0-9]*\)\(.*\)/\1\2/g'
#""".format(cmdbin.PIPE_BACKSLASH,'','')
#CMD_LSOF_REG_CSV_DIRECTORY_TEMPLATE="""/usr/bin/lsof -F pntcs0 +D %s | \
#/bin/sed '{3}N; /^\(.*\)\\n\\1$/!P; D' | /bin/sed 's/^p/\\np/' | \
#/bin/sed -e '/./,/^t/{0}H;{3}d;{1}' {2}
#/bin/sed -e 'x;/tREG/!d' -e 's/tREG{4}s/,REG,/g' {2}
#/bin/sed 's/\({4}\)/\\n/' | /bin/sed -e 's/^p//g' -e 's/^c/,/g' | \
#/bin/sed 's/^n/,/g' | \
#/bin/sed -e ':a;N;{3}ba;s/\\n/ /g' -e 's/  /\\n/g' -e 's/ ,/,/g' -e 's/^[ ]*\([0-9]*\)\(.*\)/\\1\\2/g'
#""".format(cmdbin.BRACES_OPEN,cmdbin.BRACES_CLOSE,cmdbin.PIPE_BACKSLASH,
#           cmdbin.DOLLAR_EXCLAMATION,cmdbin.XZERO)




ActiveInDir = namedtuple('ActiveInDir', ['pid','comm','type','size','fullpath','linenum'])

def active_in_dir_unique(nt_list):
    # Given a list of ActiveInDir namedtuples, iterates over the list removing duplicate fpath
    stored_fpath = None
    nt_unique = []
    for nt in nt_list:
        if nt.fpath == stored_path:
            continue
        nt_unique.append(nt)
        stored_path = nt.fpath
    return nt_unique


def active_file_reg_from_dir_inner(dir_to_list='/var/log/postgresql',verbosity=0):
    """ Take output of lsof and return the the named tuple with following
    fields:
    1) returncode
    2) dictofsizes
    3) dictofsizes
    4) lines
    5) ntlist
    Where (2) is a dictionary of file sizes keyed on filename
    and (3) is a dictionary of lines keyed on filename and populated with FIRST OCCURENCE
    and (5) is a list of named tuples having fields
      pid,comm,type,size,fullpath
    """
    

    ActiveInner = namedtuple('ActiveInner', ['returncode','dictofsizes','dictoflines','lines','ntlist'])
    return_tuple = ActiveInner
    return_tuple.returncode = 0
    return_tuple.dictofsizes = {}
    return_tuple.dictoflines = {}
    return_tuple.lines = []
    return_tuple.ntlist = []

    installed_flag = False
    ptype = cmdbin.deb_or_rpm()
    if ptype is None and verbosity > 0:
        print('active_file_reg_from_dir_inner() unable to determine if deb or rpm like')

    ptype_message = None
    ptype_message_template = "ptype='{0}' detected in active_file_reg_from_dir_inner()"
    if 'deb' == ptype:
        if verbosity > 0:
            print(ptype_message_template.format(ptype))
        installed_flag = pdeb.dpkg_status_installed1('lsof')
    elif 'rpm' == ptype:
        if verbosity > 0:
            print(ptype_message_template.format(ptype))
        installed_flag = prpm.rpm_status_installed1('lsof')
    else:
        pass

    if installed_flag is not True:
        return_tuple.returncode = 130
        # abort('Unable to determine active files as package lsof not installed.')
        return return_tuple


    cmd_lsof = CMD_LSOF_REG_CSV_DIRECTORY_TEMPLATE % (dir_to_list)
    lines = []
    with settings(warn_only=False):
    #with settings():

        #ros_res = cmdbin.run_or_sudo(cmd_lsof)
        ros_res = run(cmd_lsof)
        if 0 == ros_res.return_code:
            if verbosity > 0:
                print("Success from command: {0}".format(cmd_lsof))
        elif 1 == ros_res.return_code:
            # Did the sed fail??
            # Difficult to distinguish which failed lsof or sed in current setup!!!
            #return_tuple.returncode = 140
            return_tuple.returncode = 0
            # abort("Unable to determine regular files for directory={0}".format(dir_to_list))
            #return return_tuple
        else:
            # Here was previously rather than an else: if ros_res.failed:
            return_tuple.returncode = 140
            # abort("Unable to determine regular files for directory={0}".format(dir_to_list))
            return return_tuple

        patterned = cmdbin.run_res_lines_stripped_searching(ros_res,dir_to_list)
        if len(patterned) > 0:
            lines = patterned
            return_tuple.lines = lines
            # ActiveInDir = namedtuple('ActiveInDir', ['pid','comm','type','size','fullpath','linenum'])
            dict_of_sizes = OrderedDict()
            dict_of_lines = OrderedDict()
            nt_list = []
            for idx,line in enumerate(lines):
                lsof_array = line.split(chr(44))
                # 0th is pid, 1th is comm
                ftype = lsof_array[2]
                fsize = lsof_array[3]
                fpath = lsof_array[4]
                dict_of_sizes[fpath] = fsize
                if fpath not in dict_of_lines:
                    dict_of_lines[fpath] = line
                nt_list.append(ActiveInDir(lsof_array[0],lsof_array[1],ftype,fsize,fpath,idx))
            return_tuple.dictofsizes = dict_of_sizes
            return_tuple.dictoflines = dict_of_lines
            return_tuple.ntlist = nt_list

    return return_tuple
    

def active_file_reg_from_dir(dir_to_list=None,return_requested='lines',
                             verbosity=0,print_flag=False):
    """ Wrapper around active_file_reg_from_dir_inner
    """

    if dir_to_list is None:
        dir_to_list = '/var/log/postgresql'
        print_flag = True

    inner = active_file_reg_from_dir_inner(dir_to_list,verbosity)
    if inner.returncode == 130:
        abort('Unable to determine active REG files as package lsof not installed.')
    elif inner.returncode == 140:
        abort("Unable to determine active files for directory={0}".format(dir_to_list))
    else:
        pass


    return_object = []
    if return_requested == 'lines':
        return_object = inner.lines
    elif return_requested == 'ntlist':
        return_object = inner.ntlist
    elif return_requested == 'unique':
        #print "unique processing activated over dict of length {0}".format(
        #len(inner.dictoflines))
        lines = []
        for line in inner.dictoflines:
            lines.append(line)
        return_object = lines
    elif return_requested == 'ntunique':
        return_object = active_in_dir_unique(inner.ntlist)
    else:
        abort("Unable to give return_requested={0}".format(return_requested))

    if print_flag is True and 'unique' in return_requested:
        if return_requested == 'ntunique':
            linenums = []
            for nt in return_object:
                linenums.append(nt.linenum)
            for idx,line in enumerate(lines):
                if idx not in linenums:
                    continue
                print line
        else:
            for line in return_object:
                print(line)
    else:
        for line in inner.lines:
            print(line)

    return return_object
