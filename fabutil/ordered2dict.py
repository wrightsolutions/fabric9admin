#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.8.5
#  https://pypi.python.org/pypi/Fabric/1.8.5

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import getuid,getgid,path
from os import linesep as os_linesep
from string import ascii_letters,ascii_lowercase,digits,letters,printable,punctuation,whitespace
import re
import time
from collections import Counter,defaultdict,OrderedDict

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

class KeyedOD(OrderedDict):

	PARENTHS_OPEN = chr(40)
	PARENTHS_CLOSE = chr(41)
	BRACES_OPEN = chr(123)
	BRACES_CLOSE = chr(125)
	PIPE_BACKSLASH = "{0} {1}".format(chr(124),chr(92))
	DOLLAR_EXCLAMATION = "{0}{1}".format(chr(36),chr(33))
	XZERO = chr(0x0)
	XONEBEE = chr(0x1b)
	XZERO8 = chr(0x08)
	SQBACKSLASHSQSQ = "{0}{1}{0}{0}".format(chr(39),chr(92))
	POSTGRES_CONF_TMP = "/tmp/postgresql.conf"
	RE_DIGIT_DOT_ONLY = re.compile('[0-9\.]*\Z')
	# Mid 120s chars are { then | then } so 123, 124, 125 rewritten pipe wrapped in curlies


	""" Class method supplied with output_lines directly
	"""
	@classmethod
	def dict_of_lines(cls,column_count,key1indexed,output_lines,lines_to_skip=1):
		""" Create a dict of lines from the results of some sql select output that
		is pipe delimited.

		output_lines probably wants to be fed result of run_res_lines_stripped(fabric_res)

		key1indexed is the column number (one indexed) that should be used for the key
		so we will deduct 1 from the supplied number when looking up in line_array.
		"""

		lol = []
		# Next is a dictionary of lines abbreviated to dolines
		dolines = {}

		if key1indexed < 1:
			return lol

		key0indexed = key1indexed - 1

		columns_uniform = -1

		loop_dict = {}
		for idx,line in enumerate(output_lines):
			if idx < lines_to_skip:
				continue
			columns_uniform = 1
			#print(line)
			line_array = line.split(chr(124))
			#print(line_array)
			if len(line_array) == column_count:
				confk = None
				if cls.XONEBEE in line_array[-1]:
					print("Line {0} does have required column count of {1} but special char!".format(
						idx,column_count,cls.XONEBEE))
					columns_uniform = 0
					break
				elif cls.XZERO8 in line_array[-1]:
					print("Line {0} does have required column count of {1} but special char!".format(
						idx,column_count,cls.XZERO8))
					columns_uniform = 0
					break
				else:
					confk = line_array[key0indexed]
				loop_dict[confk] = line
			elif len(line_array) == 0:
				pass
			elif cls.XONEBEE in line:
				print("Line {0} does not have required column count of {1} but contains".format(
					idx,column_count,cls.XONEBEE))
			else:
				print(line,line_array)
				print("Above Line {0} does not have required column count of {1}".format(idx,column_count))
				columns_uniform = 0
				break

		if columns_uniform > 0:
			dolines = loop_dict

		return dolines


	""" Class method supplied with output_lines directly
	"""
	@classmethod
	def list_of_lists(cls,column_count,key1indexed,output_lines,lines_to_skip=1):
		""" Create a list of lists from the results of some sql select output that
		is pipe delimited.

		output_lines probably wants to be fed result of run_res_lines_stripped(fabric_res)

		key1indexed is the column number (one indexed) that should be used for the key
		so we will deduct 1 from the supplied number when looking up in line_array.
		"""

		lol = []

		if key1indexed < 1:
			return lol

		key0indexed = key1indexed - 1

		columns_uniform = -1

		loop_list = []
		for idx,line in enumerate(output_lines):
			if idx < lines_to_skip:
				continue
			columns_uniform = 1
			#print(line)
			line_array = line.split(chr(124))
			#print(line_array)
			if len(line_array) == column_count:
				if cls.XONEBEE in line_array[-1]:
					print("Line {0} does have required column count of {1} but special char!".format(
						idx,column_count,cls.XONEBEE))
					columns_uniform = 0
					break
				elif cls.XZERO8 in line_array[-1]:
					print("Line {0} does have required column count of {1} but special char!".format(
						idx,column_count,cls.XZERO8))
					columns_uniform = 0
					break
				else:
					pass
				loop_list.append(line_array)
			elif len(line_array) == 0:
				pass
			elif cls.XONEBEE in line:
				print("Line {0} does not have required column count of {1} but contains".format(
					idx,column_count,cls.XONEBEE))
			else:
				print(line,line_array)
				print("Above Line {0} does not have required column count of {1}".format(idx,column_count))
				columns_uniform = 0
				break

		if columns_uniform > 0:
			lol = loop_list

		return lol


	""" Class method supplied with output_lines directly
	"""
	@classmethod
	def dict_of_lists(cls,column_count,key1indexed,output_lines,lines_to_skip=0):
		""" Create a dict of lists from the results of some sql select output that
		is pipe delimited.

		output_lines probably wants to be fed result of run_res_lines_stripped(fabric_res)

		key1indexed is the column number (one indexed) that should be used for the key
		so we will deduct 1 from the supplied number when looking up in line_array.
		"""

		dol = OrderedDict()

		if key1indexed < 1:
			return dol

		key0indexed = key1indexed - 1

		lol = cls.list_of_lists(column_count,key1indexed,output_lines,lines_to_skip)
		if len(lol) > 1:
			for line_array in lol:
				dol[line_array[key0indexed]] = line_array

		return dol


	""" Class method supplied with output_lines directly
	"""
	@classmethod
	def isocompact(cls):
		""" Class method to return isocompact format time with T separator
		"""
		from datetime import datetime as dt
		isocompact = dt.now().strftime('%Y%m%dT%H%M%S')
		return isocompact


	def __init__(self, *args, **kwargs):
		super(KeyedOD, self).__init__(*args, **kwargs)


	def dict_of_vals0(self,val0indexed):
		""" Return a dictionary of values
		where value is determined by the index supplied as parameter.
		Mid 120s chars are { then | then } so 123, 124, 125 rewritten pipe wrapped in curlies.
		This variant is zero indexed rather than being one indexed like dict_of_vals()
		"""

		dov = {}
		if val0indexed < 0:
			return dov

		for confk,line in self.items():
			line_array = line.split(chr(124))
			dov[confk] = line_array[val0indexed]
			
		return dov


	def dict_of_vals(self,val1indexed):
		""" Return a dictionary of values
		where value is determined by the index supplied as parameter.
		Mid 120s chars are { then | then } so 123, 124, 125 rewritten pipe wrapped in curlies.
		"""

		dov = {}
		val0indexed = val1indexed - 1
		if val0indexed < 0:
			return dov

		for confk,line in self.items():
			line_array = line.split(chr(124))
			dov[confk] = line_array[val0indexed]
			
		return dov


	def dict_filter_key_containing_match_from_list_simple(self,kdict,filter_list):
		""" Filter a dictionary (copy) based on key matches from given filter_list
		"""
		dict_filtered = {}
		for key_unfiltered,value_unfiltered in kdict.items():
			for fil in filter_list:
				if fil in key_unfiltered:
					dict_filtered[key_unfiltered] = value_unfiltered
		return dict_filtered


class KeyedEqualsOD(KeyedOD):

	KEY0INDEXED = 0
	VAL0INDEXED = 1
	TMP_HEADER = '# aaa Postgresql conf - '
	TMP_HEADER_SORT = 'sort -t# -k2 /tmp/postgresql.conf'

	""" Class method supplied with output_lines directly
	"""
	@classmethod
	def remote_conf(cls,column_count,key1indexed,val1indexed,output_lines,lines_to_skip=0):
		""" Create a dict of lists from the results of some sql select output that
		is pipe delimited.

		output_lines probably wants to be fed result of run_res_lines_stripped(fabric_res)

		key1indexed is the column number (one indexed) that should be used for the key
		so we will deduct 1 from the supplied number when looking up in line_array.

		Warning: This function will read than overwrite the filepath given as sqlconf
		"""

		dov = OrderedDict()

		if key1indexed < 1:
			return dov

		key0indexed = key1indexed - 1
		val0indexed = val1indexed - 1

		lol = self.list_of_lists(column_count,key1indexed,output_lines,lines_to_skip)
		if len(lol) > 1:
			for line_array in lol:
				dov[line_array[key0indexed]] = line_array[val0indexed]

		sqlconf = '~/postgresql.conf'
		run("printf 'log_parser_stats = off\n' > {0}".format(sqlconf))

		from . import conf_get_set as cmdbin_getset

		with cmdbin_getset.Keyedequals(sqlconf) as dbconf:

			for confk,value in dov.items():
				dbconf[confk] = value
			#dbconf.summary_print()

			"""
			dbconf_lines = []
			for k,v in dbconf.items():
			dbconf_lines.append("{0} = {1}".format(k,v))
			"""
			dbconf_lines = dbconf.lines_for_put_delimited()

			if len(dbconf_lines) > 0:
				put_using_named_temp_cmdbin(sqlconf,dbconf_lines,newline=chr(10))
			else:
				print("sqlconf was not generated on the remote as lines_for_put_delimited() unexpected.")

		return dov


	def __init__(self, column_count, key1indexed, output_lines):
		#output_lines = kwargs.pop('output_lines', [])
		super(KeyedEqualsOD, self).__init__()
		self._column_count = column_count
		self._key0indexed = int(key1indexed)-1
		self._key1indexed = key1indexed
		self._lines = output_lines
		self._repetition_count = 0
		self._dict_of_arrays = {}
		self._server_version = 0


	""" lines property requires no setter as set during initialisation.
	lines = property(get_lines, set_lines, del_lines, 'lines property.')
	"""
	@property
	def lines(self):
		return self._lines


	""" column_count property requires no setter as set during initialisation.
	"""
	@property
	def column_count(self):
		return self._column_count


	""" key0indexed property requires no setter as set during initialisation.
	"""
	@property
	def key0indexed(self):
		return self._key0indexed


	""" key1indexed property requires no setter as set during initialisation.
	"""
	@property
	def key1indexed(self):
		return self._key1indexed


	""" repetition_count property requires no setter as set during populate only.
	"""
	@property
	def repetition_count(self):
		return self._repetition_count


	""" dict_of_arrays property requires no setter as set during populate only.
	"""
	@property
	def dict_of_arrays(self):
		return self._dict_of_arrays


	""" server_version property requires no setter as set during populate only.
	"""
	@property
	def server_version(self):
		return self._server_version


	def tmp_header(self):
		""" header = '# aaa Postgresql conf - use following: sort -t# -k2 /tmp/postgresql.conf'
		TMP_HEADER = '# aaa Postgresql conf - '
		chr(35) is hash / octothorpe
		"""
		hline = "{0} header {1}".format(chr(35),self.server_version)
		#print(hline,hline)

		if self.server_version > 0:
			try:
				hline = "{0}{1} - {2} - use following: {3}".format(
					KeyedEqualsOD.TMP_HEADER,self.server_version,
					KeyedOD.isocompact(),KeyedEqualsOD.TMP_HEADER_SORT)
			except NameError:
				hline = "{0} NameError {1}".format(chr(35),self.server_version)
		else:
			try:
				hline = "{0}{1} - use following: {2}".format(
					KeyedEqualsOD.TMP_HEADER,KeyedOD.isocompact(),
					KeyedEqualsOD.TMP_HEADER_SORT)
			except NameError:
				hline = "{0} NameError {1}".format(chr(35),self.server_version)

		return hline


	def list_of_lists(self,lines_to_skip=1):
		""" Create a dict of lists from the results of some sql select output that
		is pipe delimited.

		output_lines probably wants to be fed result of run_res_lines_stripped(fabric_res)

		key1indexed is the column number (one indexed) that should be used for the key
		so we will deduct 1 from the supplied number when looking up in line_array.
		"""

		lol = []

		if self.key0indexed < 0:
			return lol

		column_count = self.column_count
		columns_uniform = -1

		loop_list = []
		for idx,line in enumerate(self.lines):
			if idx < lines_to_skip:
				continue
			columns_uniform = 1
			#print(line)
			line_array = line.split(chr(124))
			#print(line_array)
			if len(line_array) == column_count:
				if KeyedOD.XONEBEE in line_array[-1]:
					print("Line {0} does have required column count of {1} but special char!".format(
						idx,column_count,KeyedOD.XONEBEE))
					columns_uniform = 0
					break
				elif KeyedOD.XZERO8 in line_array[-1]:
					print("Line {0} does have required column count of {1} but special char!".format(
						idx,column_count,KeyedOD.XZERO8))
					columns_uniform = 0
					break
				else:
					pass
				loop_list.append(line_array)
			elif len(line_array) == 0:
				pass
			elif KeyedOD.XONEBEE in line:
				print("Line {0} does not have required column count of {1} but contains".format(
					idx,column_count,KeyedOD.XONEBEE))
			else:
				print(line,line_array)
				print("Above Line {0} does not have required column count of {1}".format(idx,column_count))
				columns_uniform = 0
				break

		if columns_uniform > 0:
			lol = loop_list

		return lol


	def dict_of_lists(self,lines_to_skip=0):
		""" Create a dict of lists from the results of some sql select output that
		is pipe delimited.

		output_lines probably wants to be fed result of run_res_lines_stripped(fabric_res)

		key1indexed is the column number (one indexed) that should be used for the key
		so we will deduct 1 from the supplied number when looking up in line_array.
		"""

		dol = OrderedDict()

		if self.key0indexed < 0:
			return dol

		key0indexed = self.key0indexed

		lol = self.list_of_lists(lines_to_skip)
		if len(lol) > 1:
			for line_array in lol:
				dol[line_array[key0indexed]] = line_array

		return dol


	def populate_from_lines(self,lines_to_skip=1):
		""" Important method used to populate the internal dictionary and
		also to set repetition_count

		Signature of called: dict_of_lines(cls,column_count,key1indexed,output_lines,lines_to_skip=1)
		"""

		self.clear()
		""" Above ensures we are starting fresh rather than combining items extracted from lines
		with the existing dictionary content
		"""
		self._server_version = 0
		self._repetition_count = 0

		dolines = KeyedOD.dict_of_lines(self.column_count,self.key1indexed,
										self.lines,lines_to_skip)
		""" Next is a dictionary of arrays which is not self but is stored for easy reference as
		a property of self
		"""
		doa = {}
		for confk,line in dolines.items():
			self[confk] = line
			line_array = line.split(chr(124))
			if confk == 'server_version':
				val = line_array[KeyedEqualsOD.VAL0INDEXED]
				if KeyedOD.RE_DIGIT_DOT_ONLY.match(val):
					#print(val)
					self._server_version = val
			doa[confk] = line_array

		if len(self.lines) == len(self):
			self._dict_of_arrays = doa
			return True
		elif len(self.lines) > len(self):
			self._dict_of_arrays = doa
			self._repetition_count = len(self.lines) - len(self)
			return True
		else:
			pass

		return len(self)


	def remote_conf(self,val1indexed,lines_to_skip=0):
		""" Create a dict of lists from the results of some sql select output that
		is pipe delimited.

		output_lines probably wants to be fed result of run_res_lines_stripped(fabric_res)

		key1indexed is the column number (one indexed) that should be used for the key
		so we will deduct 1 from the supplied number when looking up in line_array.

		Warning: This function will read than overwrite the filepath given as sqlconf
		"""

		dov = OrderedDict()

		if key0indexed < 0:
			return dov

		key0indexed = self.key0indexed
		val0indexed = val1indexed - 1

		lol = self.list_of_lists(lines_to_skip)
		if len(lol) > 1:
			for line_array in lol:
				dov[line_array[key0indexed]] = line_array[val0indexed]

		sqlconf = '~/postgresql.conf'
		run("printf 'log_parser_stats = off\n' > {0}".format(sqlconf))

		from . import conf_get_set as cmdbin_getset

		with cmdbin_getset.Keyedequals(sqlconf) as dbconf:

			for confk,value in dov.items():
				dbconf[confk] = value
			#dbconf.summary_print()

			"""
			dbconf_lines = []
			for k,v in dbconf.items():
			dbconf_lines.append("{0} = {1}".format(k,v))
			"""
			dbconf_lines = dbconf.lines_for_put_delimited()

			if len(dbconf_lines) > 0:
				put_using_named_temp_cmdbin(sqlconf,dbconf_lines,newline=chr(10))
			else:
				print("sqlconf was not generated on the remote as lines_for_put_delimited() unexpected.")

		return dov


	def list_of_tuples_to_outfile(self,outfile,list_of_tuples):
		""" Second argument is a list of key value tuple pairs
		probably generated from dict.items() or sorted(dict.items())
		chr(34) is double quote and chr(39) is single quote
		"""
		written_count = 0
		dict_of_arrays = self.dict_of_arrays
		header = None
		for confk,val in list_of_tuples:
			if header is None:
				header = self.tmp_header()
				outfile.write("{0}\n".format(header))
			vartype = dict_of_arrays[confk][3]
			unit = dict_of_arrays[confk][7]
			keyval = "{0} = {1}".format(confk,val)
			if vartype == 'integer':
				valint = int(val)
				if valint < 0:
					keyval = "{0} = {1:d}".format(confk,valint)
				elif unit == '8kB':
					keyval = "{0} = {1:d}kB".format(confk,(valint*8))
				elif unit == 'kB':
					keyval = "{0} = {1:d}{2}".format(confk,valint,unit)
				elif unit in ['min','ms','s']:
					keyval = "{0} = {1:d}{2}".format(confk,valint,unit)
				elif val.startswith('0') and not val == 0:
					keyval = "{0} = {1}".format(confk,val)
				else:
					keyval = "{0} = {1:d}".format(confk,valint)
			elif vartype == 'string':
				if val == '(disabled)':
					keyval = "{0} = {1}{1}".format(confk,chr(39),val)
				else:
					keyval = "{0} = {1}{2}{1}".format(confk,chr(39),val)
			elif vartype == 'enum' and len(set(val).intersection(letters)) > 0:
					keyval = "{0} = {1}{2}{1}".format(confk,chr(39),val)
			else:
				keyval = "{0} = {2}".format(confk,chr(39),val)

			#outfile.write("{0:<70}{1} {2}{3}".format(keyval,chr(35),context,os_linesep))
			outfile.write("{0}{1}".format(keyval,os_linesep))

			written_count += 1

		return written_count


	def list_of_tuples_to_outfile_with_context(self,outfile,list_of_tuples):
		""" Second argument is a list of key value tuple pairs
		probably generated from dict.items() or sorted(dict.items())
		chr(34) is double quote and chr(39) is single quote
		"""
		written_count = 0
		dict_of_arrays = self.dict_of_arrays
		header = None
		for confk,val in list_of_tuples:
			if header is None:
				header = self.tmp_header()
				outfile.write("{0}\n".format(header))
			context = dict_of_arrays[confk][2]
			vartype = dict_of_arrays[confk][3]
			unit = dict_of_arrays[confk][7]
			keyval = "{0} = {1}".format(confk,val)
			if vartype == 'integer':
				valint = int(val)
				if valint < 0:
					keyval = "{0} = {1:d}".format(confk,valint)
				elif unit == '8kB':
					keyval = "{0} = {1:d}kB".format(confk,(valint*8))
				elif unit == 'kB':
					keyval = "{0} = {1:d}{2}".format(confk,valint,unit)
				elif unit in ['min','ms','s']:
					keyval = "{0} = {1:d}{2}".format(confk,valint,unit)
				elif val.startswith('0') and not val == 0:
					keyval = "{0} = {1}".format(confk,val)
				else:
					keyval = "{0} = {1:d}".format(confk,valint)
			elif vartype == 'string':
				if val == '(disabled)':
					keyval = "{0} = {1}{1}".format(confk,chr(39),val)
				else:
					keyval = "{0} = {1}{2}{1}".format(confk,chr(39),val)
			elif vartype == 'enum' and len(set(val).intersection(letters)) > 0:
					keyval = "{0} = {1}{2}{1}".format(confk,chr(39),val)
			else:
				keyval = "{0} = {2}".format(confk,chr(39),val)
			outfile.write("{0:<70}{1} {2}{3}".format(
					keyval,chr(35),context,os_linesep))
			written_count += 1

		return written_count


	def list_of_tuples_to_outfile_with_context_confk_filtered(self,outfile,
															  list_of_tuples,confk_filter):
		""" Second argument is a list of key value tuple pairs
		probably generated from dict.items() or sorted(dict.items())
		"""
		written_count = 0
		dict_of_arrays = self.dict_of_arrays
		header = None
		for confk,val in list_of_tuples:
			if header is None:
				header = self.tmp_header()
				outfile.write("{0}\n".format(header))
			if confk_filter not in confk:
				continue
			context = dict_of_arrays[confk][2]
			vartype = dict_of_arrays[confk][3]
			unit = dict_of_arrays[confk][7]
			keyval = "{0} = {1}".format(confk,val)
			if vartype == 'integer':
				valint = int(val)
				if valint < 0:
					keyval = "{0} = {1:d}".format(confk,valint)
				elif unit == '8kB':
					keyval = "{0} = {1:d}kB".format(confk,(valint*8))
				elif unit == 'kB':
					keyval = "{0} = {1:d}{2}".format(confk,valint,unit)
				elif unit in ['min','ms','s']:
					keyval = "{0} = {1:d}{2}".format(confk,valint,unit)
				elif val.startswith('0') and not val == 0:
					keyval = "{0} = {1}".format(confk,val)
				else:
					keyval = "{0} = {1:d}".format(confk,valint)
			elif vartype == 'string':
				if val == '(disabled)':
					keyval = "{0} = {1}{1}".format(confk,chr(39),val)
				else:
					keyval = "{0} = {1}{2}{1}".format(confk,chr(39),val)
			elif vartype == 'enum' and len(set(val).intersection(letters)) > 0:
					keyval = "{0} = {1}{2}{1}".format(confk,chr(39),val)
			else:
				keyval = "{0} = {2}".format(confk,chr(39),val)
			outfile.write("{0:<70}{1} {2}{3}".format(
					keyval,chr(35),context,os_linesep))
			written_count += 1

		return written_count


	def to_local_tmp(self,sortkeys=False):
		""" 
		Replaces older: logging_dict = cmdbin.sql_pipe_delimited_to_remote_conf(7,1,2,logging_lines,1)
		Replaces older call involving val1indexed=2
		"""
		val1indexed = 2
		# Above is fixed and based on postgres typical output

		written_count = 0
		with open(KeyedOD.POSTGRES_CONF_TMP, 'wt') as outf:
			kdict = self.dict_of_vals(val1indexed)
			header = None
			if sortkeys is True:
				written_count = self.list_of_tuples_to_outfile(outf,sorted(kdict.items()))
			else:
				written_count = self.list_of_tuples_to_outfile(outf,kdict.items())
			# Text file automatically closes

		return written_count


	def to_local_tmp_log_only(self,sortkeys=False):
		""" Output key value format file to /tmp/
		restricting entries to any key that includes the string 'log'
		"""
		val1indexed = 2
		# Above is fixed and based on postgres typical output

		written_count = 0
		with open(KeyedOD.POSTGRES_CONF_TMP, 'wt') as outf:
			kdict = self.dict_of_vals(val1indexed)
			header = None
			if sortkeys is True:
				written_count = self.list_of_tuples_to_outfile_with_context_confk_filtered(
					outf,sorted(kdict.items()),'log')
			else:
				written_count = self.list_of_tuples_to_outfile_with_context_confk_filtered(
					outf,kdict.items(),'log')
			# Text file automatically closes

		return written_count


	def to_local_tmp_wal_only(self,sortkeys=False):
		""" WAL and related directives only will be sent to local outfile
		"""
		val1indexed = 2
		# Above is fixed and based on postgres typical output

		written_count = 0
		with open(KeyedOD.POSTGRES_CONF_TMP, 'wt') as outf:
			kdict = self.dict_of_vals(val1indexed)
			filter_list = ['checkpoint','commit','_sender','standby','trigger','wal_level']
			kdict = self.dict_filter_key_containing_match_from_list_simple(kdict,filter_list)
			header = None
			if sortkeys is True:
				written_count = self.list_of_tuples_to_outfile_with_context(
					outf,sorted(kdict.items()))
			else:
				#written_count = self.list_of_tuples_to_outfile(outf,kdict.items())
				written_count = self.list_of_tuples_to_outfile_with_context(
					outf,kdict.items())
			# Text file automatically closes

		return written_count
