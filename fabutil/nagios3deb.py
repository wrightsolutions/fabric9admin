#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from collections import Counter,defaultdict,OrderedDict
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
#from fabric.contrib.files import exists,append
from os import path
import re
from string import ascii_lowercase,digits,printable,punctuation,whitespace
try:
    from . import cmdbin
except ValueError:
    import cmdbin
from . import conf_get_set as getset
from . import package_deb as pdeb


CMD_NDO2DB='/usr/sbin/ndo2db'
CMD_NAGIOS_SYNTAX_CHECK='/usr/sbin/nagios3 -v /etc/nagios3/nagios.cfg'
NAGIOS_FPATH_INITD='/etc/init.d/nagios3'
NDOUTILS_PACKAGE='ndoutils-nagios3-mysql'
NDOUTILS_PACKAGE_COMMON='ndoutils-common'
NDOUTILS_FPATH_DEFAULT='/etc/default/ndoutils'
NDOUTILS_FPATH_INITD='/etc/init.d/ndoutils'
NAGIOS_CONF='/etc/nagios3/nagios.cfg'
NDOMOD_CONF='/etc/nagios3/ndomod.cfg'
NDO2DB_CONF='/etc/nagios3/ndo2db.cfg'
NDOMOD_MATCH = 'ndomod:'
NDOMOD_PORT = 5668
NAGIOS3LOG='/var/log/nagios3/nagios.log'
NAGIOS_HOST_PERFDATA='/var/cache/nagios3/host-perfdata'
NAGIOS_SERVICE_PERFDATA='/var/cache/nagios3/service-perfdata'
NAGIOS_NDOMOD_PRINTF = 'printf -- "--- ndomod feeds ndo2db via port or socket ---\n" '
NAGIOS_NDOMOD_BOTH_EGREP_UNHATTED = 'output=|output_type|socket_type=|tcp_port=\s?[0-9]{1,5}|--- ndomod feed'


def ndoutils_install(packagename=NDOUTILS_PACKAGE,recommends=False):
    """ Supply alternative packagename argument if you do not want the db supporting
    mysql version of ndoutils installed
    """
    installed_version = 0
    if recommends is True:
        requires_res = pdeb.requires_packages([packagename],True,False,False)
    else:
        # Default to noninteractive so not prompted for nagios admin if nagios install triggered
        requires_res = pdeb.requires_packages_noninteractive([packagename],True,False,False)
    if requires_res:
       installed_version = pdeb.dpkg_installed_version_numeric(packagename)
    return installed_version


def ndoutils_restart():

    restarted_flag = False

    with settings(warn_only=True):
        ndoutils_restart_res = sudo("{0} restart".format(NDOUTILS_FPATH_INITD))
        if ndoutils_restart_res.succeeded:
            restarted_flag = True

    return restarted_flag


def ndoutils_restart_verbose(linecount=50):
    """ Returns a list of lines extracted from tail of nagios log
    Empty list means error or no matching lines found
    """

    ndolines = []

    restarted_flag = ndoutils_restart()

    if restarted_flag is True:
        with hide('output'):
            tail_res = cmdbin.run_or_sudo("{0} -n {1} -- {2}".format(cmdbin.CMD_TAIL,NAGIOS3LOG))
            ndolines = cmdbin.run_res_lines_stripped_matching(tail_res,NDOMOD_MATCH)
        
    return ndolines


def detailed_extra(config_path,print_flag=True):
    lines = None
    with getset.Keyedequals(config_path) as conf:
        lines = conf.summary_detailed_extra(print_flag,config_path)
        """ Above we are giving config_path as second parameter purely as prefix text 
        Use anything you want for prefix when calling summary_detailed_extra(...,...)
        """
    return lines


def detailed_extra_ndomod(config_path=NDOMOD_CONF,print_flag=True):
    lines = None
    with getset.Keyedequals(config_path) as conf:
        lines = conf.summary_detailed_extra(print_flag,config_path)
        """ Above we are giving config_path as second parameter purely as prefix text 
        Use anything you want for prefix when calling summary_detailed_extra(...,...)
        """
    return lines


def detailed_extra_ndo2db(config_path=NDO2DB_CONF,print_flag=True):
    lines = None
    with getset.Keyedequals(config_path) as conf:
        lines = conf.summary_detailed_extra(print_flag,config_path)
        """ Above we are giving config_path as second parameter purely as prefix text 
        Use anything you want for prefix when calling summary_detailed_extra(...,...)
        """
    return lines


def detailed_enabled(config_path,print_flag=False):
    lines = []
    with getset.Keyedequals(config_path) as conf:
        lines = conf.lines_enabled()
    if print_flag:
        for line in lines:
            print line
    return lines


def detailed_enabled_ndomod(config_path=NDOMOD_CONF,print_flag=False):
    lines = []
    with getset.Keyedequals(config_path) as conf:
        lines = conf.lines_enabled()
    if print_flag:
        for line in lines:
            print line
    return lines


def lines_enabled_ndomod():
    return detailed_enabled_ndomod(print_flag=True)


def detailed_enabled_ndo2db(config_path=NDO2DB_CONF,print_flag=False):
    lines = []
    with getset.Keyedequals(config_path) as conf:
        lines = conf.lines_enabled()
    if print_flag:
        for line in lines:
            print line
    return lines


def lines_enabled_ndo2db():
    return detailed_enabled_ndo2db(print_flag=True)


def ndoutils_enable(verbose=False):
    """ Enable nagios data output (ndoutils)
    Reminder about the contract of conf_get_set.py and order or operations next...
    Do the queries first. Do use the reliable write features (when implemented)
    However you are discouraged from performing updates and then attempt to query things further.

    No contract is given here that detailed queries will be reliable once updates
    have taken place. Reiterating to clarify: Query, Update (optional), Write the changes.
    """
    ndoutils_return = False
    conf_filepath = NDOUTILS_FPATH_DEFAULT
    with getset.Keyedequals(conf_filepath) as conf:
        if verbose is True:
            print conf.lines_current_simple()
        if 'ENABLE_NDOUTILS' in conf:
            #print conf.toggle_safe('ENABLE_NDOUTILS','true',True,1)
            toggle_res = conf.toggle_safe('ENABLE_NDOUTILS','1',True)
            if toggle_res:
                print conf.lines_for_put_containing('ENABLE_NDOUTILS')
                sedlines = conf.sedlines(True)
                #sedres = getset.sed_remote_apply('a'*32,conf_filepath,sedlines,'medium')
                #sedres = getset.sed_remote_apply(None,conf_filepath,sedlines,'medium',True,0.75,True)
                #md5true = '72a09b2f30c4b1cab7e9a366ee50988c'
                # md5current is what we supply so md5current=md5false
                md5false = '48cd98810a1eaa12fb63c757daec8ca2'
                """ /etc/default/ndoutils is almost all comments so disable ratio of changes checking by
                supplying 1.0 instead of 0.75
                #sedres = getset.sed_remote_apply(md5false,conf_filepath,sedlines,'medium',True,0.75,True)
                #sedres = getset.sed_remote_apply(md5false,conf_filepath,sedlines,'medium',True,0,True)
                sedres = getset.sed_remote_apply(md5false,conf_filepath,sedlines,'medium',True,1.0,True)
                """
                sedres = getset.sed_remote_apply(md5false,conf_filepath,sedlines,'medium',True,1.0,True)
                if sedres is True:
                    ndoutils_return = True
                else:
                    print "sed_remote_apply returned {0}".format(sedres)
                if verbose is True:
                    print conf.lines_for_put_containing('ENABLE_NDOUTILS')
                    print conf.lines_current_containing('ENABLE_NDOUTILS')
                sedline = conf.sedlines(True)
                #print conf.lines_for_put_simple()
            else:
                print 'Is ndoutils already ENABLE_NDOUTILS=1? toggle_safe returned {0}'.format(toggle_res)
    return ndoutils_return


def nagios_syntax_check(verbose=False):
    """ nagios3 -v /etc/nagios3/nagios.cfg """

    ok_flag = False

    with settings(warn_only=True):
        if verbose is True:
            nagios_restart_res = sudo(CMD_NAGIOS_SYNTAX_CHECK)
        else:
            with hide('output'):
                nagios_restart_res = sudo(CMD_NAGIOS_SYNTAX_CHECK)

        if nagios_restart_res.succeeded:
            ok_flag = True

    return ok_flag


def nagios_restart():

    restarted_flag = False

    with settings(warn_only=True):
        nagios_restart_res = sudo("{0} restart".format(NAGIOS_FPATH_INITD))
        if nagios_restart_res.succeeded:
            restarted_flag = True

    return restarted_flag


def nagios_stop():

    stopped_flag = False

    with settings(warn_only=True):
        nagios_stop_res = sudo("{0} stop".format(NAGIOS_FPATH_INITD))
        if nagios_stop_res.succeeded:
            stopped_flag = True

    return stopped_flag


def nagios_start():

    started_flag = False

    with settings(warn_only=True):
        nagios_start_res = sudo("{0} start".format(NAGIOS_FPATH_INITD))
        if nagios_start_res.succeeded:
            started_flag = True

    return started_flag


def nagios_broker(verbosity=1,config_path=NAGIOS_CONF):
    """ Set nagios broker directive.
    Use context_update style seds via context_update()
    """
    updated_flag = False
    with getset.Keyedequals(config_path) as conf:
        if verbosity > 1:
            noncomment_directive = conf.lines_noncomment_containing_any(['broker_module'])
            # Above supplies a list to lines_noncomment_containing_any()
            print "Before change here are {0} lines / placeholders ...".format(len(noncomment_directive))
            print "... from master conf which contain master and may be relevant"
            for line in noncomment_directive:
                print line.rstrip()
        value2 = '/usr/lib/ndoutils/ndomod-mysql-3x.o config_file=/etc/nagios3/ndomod.cfg'
        update_res = conf.context_update('broker_module',value2,2)
        #update_res = conf.context_update_localhost('interface',2,True,cmdbin.LOOPBACK4)
        if len(update_res) < 1:
            if verbosity > 1:
                print "context_update() returned {0}".format(update_res)
        else:
            """ Context update completed so change recorded. Next apply it to remote """
            sedlines = conf.sedlines((verbosity>1))
            #apply_res = getset.sed_remote_apply('a'*32,config_path,sedlines,'medium')
            #apply_res = getset.sed_remote_apply('a'*32,config_path,sedlines,'medium',True,0,True)
            apply_res = getset.sed_remote_apply('a'*32,config_path,sedlines,'medium',True,0.75,True)
            if apply_res is True:
                updated_flag = True
            elif verbosity > 1:
                print "sed_remote_apply returned {0}".format(apply_res)

    if updated_flag is True:
        pass
    elif verbosity > 0:
        print "nagios_broker() did not complete as expected!"
        print "Was broker_module already set to desired value before your request?"
    return updated_flag


def nagios_cfg_default(backup=True):
    return pdeb.missing_conf_via_reinstall_middle(NAGIOS_CONF)


def host_perfdata_enable(verbosity=1,config_path=NAGIOS_CONF,perfdata_path=NAGIOS_HOST_PERFDATA):
    """ Enable performance reporting to host-perfdata
    Use context_update style seds via context_update()
    """
    updated_flag = False
    with getset.Keyedequals(config_path) as conf:
        if verbosity > 1:
            noncomment_directive = conf.lines_noncomment_containing_any(['host_perfdata_file'])
            # Above supplies a list to lines_noncomment_containing_any()
            print "Before change here are {0} lines / placeholders ...".format(len(noncomment_directive))
            print "... from conf which contain host_perfdata_file and may be relevant"
            for line in noncomment_directive:
                print line.rstrip()
        print(cmdbin.dotty_level(perfdata_path))
        if 0 == cmdbin.dotty_level(perfdata_path):
            update_res = conf.context_update('host_perfdata_file',perfdata_path,2)
            #update_res = conf.context_update_localhost('interface',2,True,cmdbin.LOOPBACK4)
            if len(update_res) < 1:
                if verbosity > 1:
                    print "sed_remote_apply returned {0}".format(update_res)
            else:
                """ Context update completed so change recorded. Next apply it to remote """
                sedlines = conf.sedlines((verbosity>1))
                #apply_res = getset.sed_remote_apply('a'*32,config_path,sedlines,'medium')
                #apply_res = getset.sed_remote_apply('a'*32,config_path,sedlines,'medium',True,0,True)
                apply_res = getset.sed_remote_apply('a'*32,config_path,sedlines,'medium',True,0.75,True)
                if apply_res is True:
                    updated_flag = True
                elif verbosity > 1:
                    print "sed_remote_apply returned {0}".format(apply_res)

    if updated_flag is True:
        pass
    elif verbosity > 0:
        print "host_perfdata_enable() did not complete as expected!"
        print "Was host_perfdata_file already set to desired value before your request?"
    return updated_flag


def service_perfdata_enable(verbosity=1,config_path=NAGIOS_CONF,perfdata_path=NAGIOS_SERVICE_PERFDATA):
    """ Enable performance reporting to host-perfdata
    Use context_update style seds via context_update()
    """
    updated_flag = False
    with getset.Keyedequals(config_path) as conf:
        if verbosity > 1:
            noncomment_directive = conf.lines_noncomment_containing_any(['service_perfdata_file'])
            # Above supplies a list to lines_noncomment_containing_any()
            print "Before change here are {0} lines / placeholders ...".format(len(noncomment_directive))
            print "... from conf which contain service_perfdata_file and may be relevant"
            for line in noncomment_directive:
                print line.rstrip()
        print(cmdbin.dotty_level(perfdata_path))
        if 0 == cmdbin.dotty_level(perfdata_path):
            update_res = conf.context_update('service_perfdata_file',perfdata_path,2)
            #update_res = conf.context_update_localhost('interface',2,True,cmdbin.LOOPBACK4)
            if len(update_res) < 1:
                if verbosity > 1:
                    print "sed_remote_apply returned {0}".format(update_res)
            else:
                """ Context update completed so change recorded. Next apply it to remote """
                sedlines = conf.sedlines((verbosity>1))
                #apply_res = getset.sed_remote_apply('a'*32,config_path,sedlines,'medium')
                #apply_res = getset.sed_remote_apply('a'*32,config_path,sedlines,'medium',True,0,True)
                apply_res = getset.sed_remote_apply('a'*32,config_path,sedlines,'medium',True,0.75,True)
                if apply_res is True:
                    updated_flag = True
                elif verbosity > 1:
                    print "sed_remote_apply returned {0}".format(apply_res)

    if updated_flag is True:
        pass
    elif verbosity > 0:
        print "service_perfdata_enable() did not complete as expected!"
        print "Was service_perfdata_file already set to desired value before your request?"
    return updated_flag


def perfdata_enable(verbosity=0,config_path=NAGIOS_CONF):
    """ Set directive process_performance_data to be 1
    This function deals with the process_performance_data directive only.
    Do use the perfdata_enable_all() wrapper if you want a more complete configuration
    change that enables and sets the file locations also
    """
    #process_performance_data=0
    keyname = 'process_performance_data'
    value_requested = '1'
    perfdata_return = False
    with getset.Keyedequals(config_path) as conf:
        if verbosity > 1:
            print conf.lines_current_containing('perfdata')
        if keyname in conf:
            #print conf.toggle_safe('ENABLE_NDOUTILS','true',True,1)
            toggle_res = conf.toggle_safe(keyname,value_requested,True)
            if toggle_res:
                if verbosity > 1:
                    print conf.lines_for_put_containing(keyname)
                sedlines = conf.sedlines(True)
                md5before = 'a'*32
                sedres = getset.sed_remote_apply(md5before,config_path,sedlines,'medium',True,0.75,True)
                if sedres is True:
                    perfdata_return = True
                else:
                    print "{0} sed_remote_apply returned {1}".format(keyname,sedres)
                if verbosity > 1:
                    print conf.lines_for_put_containing(keyname)
                    print conf.lines_current_containing(keyname)
                sedline = conf.sedlines(True)
                #print conf.lines_for_put_simple()
            else:
                print "Is {0}={1} already set before our change?.".format(keyname,value_requested)
                print 'toggle_safe of {0} returned {1}'.format(keyname,toggle_res)
    return perfdata_return


def perfdata_enable_all(verbose=False):
    if verbose is True:
        host_enabled = host_perfdata_enable(verbosity=2)
    else:
        host_enabled = host_perfdata_enable(verbosity=0)

    if verbose is True:
        service_enabled = service_perfdata_enable(verbosity=2)
    else:
        service_enabled = service_perfdata_enable(verbosity=0)

    if verbose is True:
        perfdata_enabled = perfdata_enable(verbosity=2)
    else:
        perfdata_enabled = perfdata_enable(verbosity=0)

    return


def nagios_ndo_sendsvia(verbosity=1,config_path=NDOMOD_CONF):
    """ Returns 'tcp' or 'filesocket' or None
    based on querying ndomod config file for output_type

    itype is an abbreviation for interface type as logically we
    are pairing two sides of an interface when connecting ndomod to ndo2db
    """
    if config_path is None:
        config_path = NDOMOD_CONF
    # Above we are using knowledge of the caller and dealing with None appropriately
    via = None
    keyname = 'output_type'
    itype = None
    with getset.Keyedequals(config_path) as conf:
        if verbosity > 1:
            print conf.lines_enabled_contains(keyname)
        if keyname in conf:
            itype = conf[keyname]
        else:
            itype = -1
        if verbosity > 1:
            print("{0} from config file {1}".format(itype,config_path))

    if itype is None:
        if verbosity > 1:
            print("Unable to determine {0}. Does file {1} exist?".format(
                    keyname,config_path))
    elif itype == -1:
        print("Unable to determine {0} !".format(keyname))
    elif len(itype) < 3:
        print("Unable to determine {0} !!".format(keyname))
    else:
        """ Should you standardise on naming what is in via following ndo convention?
        If ndo itself was consistent then it is a stronger argument,
        however that is not the case as you will see when nagios_ndo_receivesvia()
        """
        if itype.startswith('tcp'):
            via = 'tcp'
        elif 'unix' in itype:
            via = 'filesocket'

    return via


def nagios_ndo2db_receivesvia(verbosity=1,config_path=NDO2DB_CONF):
    """ Returns 'tcp' or 'filesocket' or None
    based on querying ndo2db config file for type
    of connection it uses to get nagios data

    itype is an abbreviation for interface type as logically we
    are pairing two sides of an interface when connecting ndomod to ndo2db
    """
    if config_path is None:
        config_path = NDO2DB_CONF
    # Above we are using knowledge of the caller and dealing with None appropriately
    via = None
    keyname = 'socket_type'
    """ keyname above is correctly 'socket_type' rather than 'output_type' as
    this is how it appears in the underlying config file
    """
    itype = None
    with getset.Keyedequals(config_path) as conf:
        if verbosity > 1:
            print conf.lines_enabled_contains(keyname)
        if keyname in conf:
            itype = conf[keyname]
        else:
            itype = -1
        if verbosity > 1:
            print("{0} from config file {1}".format(itype,config_path))

    if itype is None:
        if verbosity > 1:
            print("Unable to determine {0}. Does file {1} exist?".format(
                    keyname,config_path))
    elif itype == -1:
        print("Unable to determine {0} !".format(keyname))
    elif len(itype) < 3:
        print("Unable to determine {0} !!".format(keyname))
    else:
        """ Should you standardise on naming what is in via following ndo convention?
        If ndo itself was consistent then it is a stronger argument,
        however that is not the case as you will see when nagios_ndo_receivesvia()
        """
        if itype.startswith('tcp'):
            via = 'tcp'
        elif 'unix' in itype:
            via = 'filesocket'

    return via


def nagios_poller_type(verbosity=1,config_ndomod=NDOMOD_CONF,config_ndo2db=NDO2DB_CONF):

    via1 = nagios_ndo_sendsvia(2,config_ndomod)
    via2 = nagios_ndo2db_receivesvia(2,config_ndo2db)
    iface = None

    if verbosity > 1:
        print("iface={0} uninitialised yet using via1={1} and via2={2}".format(
                iface,via1,via2))
    if via1 == via2:
        iface = via1
    elif via1 in ['tcp','filesocket']:
        if verbosity > 1:
            print("Interfacing is {0} mismatched!".format(via1))
    else:
        if verbosity > 1:
            print("Interfacing is misunderstood {0}??".format(via1))
    if verbosity > 1:
        print("nagios_poller_type() will return iface={0}".format(iface))
    return iface


def nagios_poller_port(verbosity=1,config_path=NDOMOD_CONF):
    if config_path is None:
        config_path = NDOMOD_CONF
    # Above we are using knowledge of the caller and dealing with None appropriately
    port = None
    poller_type = nagios_poller_type(verbosity,config_path)
    if poller_type is None:
        return port
    elif poller_type != 'tcp':
        return port

    keyname = 'port'
    with getset.Keyedequals(config_path) as conf:
        if verbosity > 1:
            print conf.lines_enabled_contains(keyname)
        if keyname in conf:
            port = conf[keyname]
    
    return port


def nagios_poller_summary(verbosity=1,config_ndomod=NDOMOD_CONF,config_ndo2db=NDO2DB_CONF):
    """ Summary grep of the two files ndomod.cfg and ndo2db in that order.
    printf -- "--- ndomod feeds ndo2db via port or socket ---\n" | \
    egrep '^(output=|output_type|socket_type=|tcp_port=\s?[0-9]{1,5}|--- ndomod feed)' \
    /etc/nagios3/ndomod.cfg - /etc/nagios3/ndo2db.cfg
    """
    printf_grep3 = "{0} | {1} '^({2})' {3} - {4}".format(
        NAGIOS_NDOMOD_PRINTF,cmdbin.CMD_EGREP,NAGIOS_NDOMOD_BOTH_EGREP_UNHATTED,
        config_ndomod,config_ndo2db)
    print("verbosity={0}. Next will execute {1}".format(verbosity,printf_grep3))
    poller_lines = []
    grep_res = None
    if verbosity < 2:
        with hide('output'):
            grep_res = cmdbin.run_or_sudo(printf_grep3)
    else:
        print("being very verbose when executing the grep command.")
        grep_res = cmdbin.run_or_sudo(printf_grep3)

    if grep_res and grep_res.succeeded:
        poller_lines = cmdbin.run_res_lines_stripped_searching(grep_res,':')

    if verbosity > 0:
        for line in poller_lines:
            print(line)
        if verbosity > 2:
            print("Returning poller_lines list of length {0}".format(len(poller_lines)))

    return poller_lines


def nagios_ndomod_force_tcp(verbosity=1,config_path=NDOMOD_CONF,port=NDOMOD_PORT):
    """ Force ndo to send via tcp socket on specified port
    """
    if config_path is None:
        config_path = NDOMOD_CONF
    if port is None:
        port = NDOMOD_PORT
    # Above we are using knowledge of the caller and dealing with None appropriately
    made_tcp = False
    keyname = 'output_type'
    vianow = None
    val2 = 'tcpsocket'

    with getset.Keyedequals(config_path) as conf:
        if verbosity > 1:
            print conf.lines_enabled_contains(keyname)
        if keyname in conf:
            vianow = conf[keyname]
        if verbosity > 1:
            print("{0} from config file {1}".format(vianow,config_path))

        if vianow is None:
            return made_tcp

        if vianow == val2:
            print "Is output_type already {0}? No update actioned.".format(val2)
            return True

        update_res = conf.context_update(keyname,val2,2)
        #update_res = conf.context_update_localhost('interface',2,True,cmdbin.LOOPBACK4)
        if len(update_res) < 1:
            if verbosity > 1:
                print "context_update({0},...) returned {1}".format(keyname,update_res)
        else:
            if verbosity > 1:
                print conf.lines_for_put_containing(keyname)

            """ Next we should setup a second sed so that port=5668 is forced
            but this is yet to be implemented. Todo.
            """

            sedlines = conf.sedlines((verbosity>1))
            sedres = getset.sed_remote_apply(cmdbin.MD5A32,config_path,sedlines,
                                             'medium',True,0.75,True)
            if verbosity > 1:
                print("sedres={0}".format(sedres))
            if sedres is True:
                made_tcp = True
            else:
                print "sed_remote_apply returned {0}".format(sedres)
                print conf.lines_for_put_containing(keyname)
                print conf.lines_current_containing(keyname)
                # Make the change on the remote system

    return made_tcp


def nagios_ndo2db_force_tcp(verbosity=1,config_path=NDO2DB_CONF,port=NDOMOD_PORT):
    """ Force ndo2db to receive via tcp socket on specified port
    """
    if config_path is None:
        config_path = NDO2DB_CONF
    if port is None:
        port = NDOMOD_PORT
    # Above we are using knowledge of the caller and dealing with None appropriately
    made_tcp = False
    keyname = 'socket_type'
    """ keyname above is correctly 'socket_type' rather than 'output_type' as
    this is how it appears in the underlying config file
    """
    vianow = None
    val2 = 'tcp'
    # For ndomod we would set val2='tcpsocket', but here we should val2='tcp'

    with getset.Keyedequals(config_path) as conf:
        if verbosity > 1:
            print conf.lines_enabled_contains(keyname)
        if keyname in conf:
            vianow = conf[keyname]
        if verbosity > 1:
            print("{0} from config file {1}".format(vianow,config_path))

        if vianow is None:
            return made_tcp

        if vianow == val2:
            print "Is output_type already {0}? No update actioned.".format(val2)
            return True

        update_res = conf.context_update(keyname,val2,2)
        #update_res = conf.context_update_localhost('interface',2,True,cmdbin.LOOPBACK4)
        if len(update_res) < 1:
            if verbosity > 1:
                print "context_update({0},...) returned {1}".format(keyname,update_res)
        else:
            if verbosity > 1:
                print conf.lines_for_put_containing(keyname)

            """ Next we should setup a second sed so that port=5668 is forced
            but this is yet to be implemented. Todo.
            """

            sedlines = conf.sedlines((verbosity>1))
            sedres = getset.sed_remote_apply(cmdbin.MD5A32,config_path,sedlines,
                                             'medium',True,0.75,True)
            if verbosity > 1:
                print("sedres={0}".format(sedres))
            if sedres is True:
                made_tcp = True
            else:
                print "sed_remote_apply returned {0}".format(sedres)
                print conf.lines_for_put_containing(keyname)
                print conf.lines_current_containing(keyname)
                #Make the change on the remote system

    return made_tcp


def nagios_poller_both_force_tcp(verbosity=1,config_ndomod=NDOMOD_CONF,
                                 config_ndo2db=NDO2DB_CONF,port=NDOMOD_PORT):
    """ Before forcing anything, you should check that the nagios database
    exists and (optionally) run the following sql command:
    mysql nagios -u ndouser -p < /usr/share/dbconfig-common/data/ndoutils-mysql/install/mysql
    """
    made_tcp1 = False
    made_tcp2 = False
    made_both = False

    if config_ndomod is None:
        config_ndomod = NDOMOD_CONF
    made_tcp1 = nagios_ndomod_force_tcp(verbosity,config_path=config_ndomod)

    if config_ndo2db is None:
        config_ndo2db = NDO2DB_CONF
    if made_tcp1 is True:
        made_tcp2 = nagios_ndo2db_force_tcp(verbosity,config_path=config_ndo2db)

    if all([made_tcp1,made_tcp2]):
        made_both = True

    return made_both
