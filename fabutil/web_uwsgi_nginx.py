#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2016 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
from os import linesep as os_linesep
import time
#from collections import namedtuple,OrderedDict
from . import cmdbin
from . import files_directories as files_dirs
from . import package_deb as pdeb
#from . import sign_repo
#from . import sysctl_plus
from . import conf_get_set as getset

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

PORT_NGINX = '8080'
PORT_UWSGI = '9001'

GROUP_WRITE_EXT_LIST_WEB = ['bz2','gz','log','lz','tar','zip']
GROUP_WRITE_EXT_LIST_PYWEB = GROUP_WRITE_EXT_LIST_WEB[:]
GROUP_WRITE_EXT_LIST_PYWEB.extend(['db3','sqlite','sqlite3'])
# DIRSTEM_DATA_SHORT = '~/'
DIRSTEM_DATA_SHORT = '/var/local'
GROUP_WEBSERVER = 'www-data'

APPS_AVAILABLE_PATH = '/etc/uwsgi/apps-available'

def group_writable_web(dirpath,group_write_ext_list=None):
    if group_write_ext_list is None:
        group_write_ext_list = GROUP_WRITE_EXT_LIST_WEB
    return files_dirs.group_writable(dirpath,'/var/www/pub','/var/l',group_write_ext_list)

def group_writable_web_recursive(dirpath,ext_list=None):
    if ext_list is None:
        ext_list = GROUP_WRITE_EXT_LIST_WEB
    return files_dirs.group_writable_recursive(dirpath,path_startswith1='/var/www/pub',
                          path_startswith2=None,group_write_ext_list=ext_list)

def group_writable_pyweb(dirpath,group_write_ext_list=None):
    if group_write_ext_list is None:
        group_write_ext_list = GROUP_WRITE_EXT_LIST_PYWEB
    return files_dirs.group_writable(dirpath,'/var/www/pub','/var/l',group_write_ext_list)

def group_writable_pyweb_recursive(dirpath,ext_list=None):
    if ext_list is None:
        ext_list = GROUP_WRITE_EXT_LIST_PYWEB
    return files_dirs.group_writable_recursive(dirpath,path_startswith1='/var/www/pub',
                          path_startswith2=None,group_write_ext_list=ext_list)

def chgrp_or_sudo_web(dirpath,newgroup=None):
    if newgroup and len(newgroup) > 1:
        grp = newgroup
    else:
        grp = GROUP_WEBSERVER
    return files_dirs.chgrp_or_sudo(dirpath,grp)


def uwsgi_python():
    uwsgi_package_list = ['uwsgi','uwsgi-core','uwsgi-plugin-http','uwsgi-plugin-python']
    packages_res = pdeb.requires_packages(uwsgi_package_list,True,False,False)
    return


def nginx_uwsgi(visible='internal',app_name='collectd',port_uwsgi=None):

    nginx1app_uwsgi_tpl = """
upstream _{{ app }} {
    server unix:/run/uwsgi/app/{{ app }}/socket;
}

server {
    listen {{ port_host }};
    #listen [::]:{{ port_host }};
    server_name {{ server_name }};

    location / {
        try_files $uri @uwsgi;
    }

    location @uwsgi {
        include uwsgi_params;
        uwsgi_pass _{{ app }};
    }
}
"""

    if port_uwsgi is None:
        port_host = PORT_NGINX
    else:
        try:
            port_host = int(port_uwsgi)
            if port_uwsgi < 80:
                port_host = PORT_UWSGI
        except:
            port_host = PORT_UWSGI

    """
    template_dict = {
        'port_host': port_host,
        'docroot_slashless': dirpath_fab,
        'server_name': 'collectd.org www.collectd.org',
        'app': app_name,
    }
    """
    
    docroot_slashless= 'collectd'
    template_dict = {
        'port_host': port_host,
        'server_name': 'localhost',
        'app': app_name,
    }

    try:
        from jinja2 import Environment
    except ImportError:
        files_dirs.abort_with_message('Need jinja2 available on local machine. apt-get install python-jinja2.')
    
    nginx1app_uwsgi = Environment().from_string(nginx1app_uwsgi_tpl).render(template_dict)

    files_dirs.put_using_named_temp("/etc/nginx/sites-available/{0}".format(app_name),
                                    nginx1app_uwsgi,'nginx sites-available',put_sudo=True)

    url_filename = "http://commons.wikimedia.org/wiki/file:linux_foundation_logo.png"
    index_html = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<meta name="robots" content="noarchive,follow" />
<title>Under Construction</title>
</head>
<body>
<p><img src="{0}" alt="Under Construction Logo" /> coming soon...</p>
<p>
    <a href="http://validator.w3.org/check?uri=referer"><img
        src="http://www.w3.org/Icons/valid-xhtml10-blue"
        alt="Valid XHTML 1.0 Strict" height="31" width="88" /></a>
  </p>
</body>
</html>
""".format(url_filename)

    web_path = "/var/www/{0}".format(app_name)
    mkdir_res = files_dirs.mkdir_existing(web_path,2755,True,True,True)
    if mkdir_res:   
        index_path = "{0}/index.html".format(web_path)
        print("Next will put index.html")
        files_dirs.put_using_named_temp(index_path,index_html,
                                        'index.html construction',put_sudo=False)
        """
        with cd(dirpath_fab):
            files_dirs.put_using_named_temp(index_path,index_html,'index.html construction',False)
        """

    return


def uwsgi_app(app_name='collectd',app_socket=None,app_chdir=None,numthreads=5,uid='www-data',gid='www-data'):
    uwsgi_app_tpl = """
    [uwsgi]
    #master = true
    #plugins = cgi
    plugins = http, pythong
    #async = 20
    #ugreen = True
    threads = {{ num_of_threads }}
    socket = {{ socket_path }}
    uid = {{ uid }}
    gid = {{ gid }}
    #cgi = /usr/bin/somecgi
    chdir = {{ chdir }}
    module = {{ module_name }}
    """
    if app_chdir is None:
        chdir = "/var/www/{0}".format(app_name)
    else:
        chdir = app_chdir

    if app_socket is None:
        app_socket = "/srv/{0}/socket".format(app_name)
    else:
        pass
        
    template_dict = {
        'module_name': app_name,
        'socket_path': app_socket,
        'num_of_threads': numthreads,
        'uid': uid,
        'gid': gid,
        'chdir': app_chdir,
        'module_name': app_name,
    }

    try:
        from jinja2 import Environment
    except ImportError:
        files_dirs.abort_with_message('Need jinja2 available on local machine. apt-get install python-jinja2.')

    uwsgi_app = Environment().from_string(uwsgi_app_tpl).render(template_dict)

    #web_path = "/var/www/{0}".format(app_name)
    uwsgi_app_path = "{0}/{1}".format(APPS_AVAILABLE_PATH,app_name)
    files_dirs.put_using_named_temp(uwsgi_app_path,uwsgi_app,
                                    'apps-available app conf construction',
                                    put_sudo=False)
    return

