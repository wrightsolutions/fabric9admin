#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
from fabric.contrib.console import confirm
#from fabric.contrib.files import exists
from collections import OrderedDict
from os import path
import re
from string import ascii_lowercase,digits,printable,punctuation,whitespace
from datetime import datetime as dt
try:
    from . import cmdbin
except ValueError:
    import cmdbin
#import files_directories as files_dirs
import package_deb as pdeb


""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

CMD_APT_MARK_AUTO = "/usr/bin/apt-mark showauto "
CMD_APT_MARK_MANUAL = "/usr/bin/apt-mark showmanual "
CMD_DPKG = "/usr/bin/dpkg "
CMD_DPKG_SELECTIONS = "/usr/bin/dpkg --get-selections "
CMD_DPKG_STATUS = "/usr/bin/dpkg -s "
CMD_DPKG_SEARCH = "/usr/bin/dpkg --search "
CMD_INFO_EITHER = "/bin/vdir /var/lib/dpkg/info/*.{list,md5sums}"
CMD_INFO_LIST = "/bin/vdir /var/lib/dpkg/info/*.list"
CMD_INFO_LISTT = "/bin/vdir -t /var/lib/dpkg/info/*.list"


def tilde_expand(path_short,abort_on_fail=True):
    if path_short.startswith('~/'):
        """ path_expanded = path.expanduser(path_short)
        will not do as you are interested in the remote user
        not the user account on your local machine! """
        expandres = run("dirname ~/.")
        if expandres.failed:
            if abort_on_fail:
                abort("Failed during path expansion of {0}".format(path_short))
            path_expanded = path.short
        else:
            path_expanded = path_short.replace('~/',"{0}/".format(expandres),1)
        """ path_expanded = run("dirname ~/.") """
    else:
        return path_short
    return path_expanded


def first_arg_path_restricted(function_to_decorate):
    """ Decorator to ensure that if first argument is a valid path
    then it is either in home (~/) or /root/ or /tmp/ or it gets set None
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is None:
                pass
            elif type(first_arg) != type(''):
                pass
            else:
                try:
                    if chr(47) not in filepath:
                        #print 'Wrapped function', function_to_decorate.__name__, 'first arg set None'
                        args_list = list(args)
                        args_list[0] = None
                        args = tuple(args_list)
                    elif first_arg.startswith('/root/'):
                        pass
                    elif first_arg.startswith('~/'):
                        #print 'Wrapped function', function_to_decorate.__name__, 'first arg expanded'
                        args_list = list(args)
                        expanded = tilde_expand(first_arg,False)
                        if expanded == first_arg:
                            args_list[0] = expanded
                        else:
                            args_list[0] = None
                        args = tuple(args_list)
                    elif first_arg.startswith('~'):
                        #print 'Wrapped function', function_to_decorate.__name__, 'first arg expanded'
                        args_list = list(args)
                        expanded = tilde_expand(first_arg,False)
                        if expanded == first_arg:
                            args_list[0] = expanded
                        else:
                            args_list[0] = None
                        args = tuple(args_list)
                    else:
                        #print 'Wrapped function', function_to_decorate.__name__, 'first arg is now prefixed'
                        args_list = list(args)
                        args_list[0] = path.join('/root/',first_arg)
                        args = tuple(args_list)               
                except:
                    pass
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def packages_outform_salt(packages_list,flatten=False):
    if len(packages_list) < 1:
        return
    pkg_list = sorted(packages_list)
    # Above sorted() because we will be grouping (makes sense to have pre-sorted)
    firstchar_stored = 'a'
    package_name_first = pkg_list[0]
    group_dict = {'?':[]}
    if package_name_first:
        firstchar_stored = package_name_first[0]
        group_dict[firstchar_stored]=[]
    for pkg in pkg_list:
        if pkg:
            firstchar = pkg[0]
            if firstchar == firstchar_stored:
                group_dict[firstchar].append(pkg)
            else:
                group_dict[firstchar]=[pkg]
                firstchar_stored = firstchar
        else:
            group_dict['?'].append(pkg)

    formatted_lines = []
    if flatten is True:
        for key in sorted(group_dict.iterkeys()):
            val = group_dict[key]
            formatted_lines.append("{0}: {1}".format(key, ' '.join(val)))
    else:
        label_prefix = 'debstate'
        label_dict = {}
        """ Use an OrderedDict above instead of plain dict,
        if you find that '?' is not appearing last """
        for key in sorted(group_dict.iterkeys()):
            val = group_dict[key]
            if key == '?':
                """ '?' will be dealt with last """
                continue
            label = "{0}{1}".format(label_prefix,key.upper())
            label_dict[label] = group_dict[key]
        if '?' in group_dict and group_dict['?']:
            label = "{0}?".format(label_prefix)
            label_dict[label] = group_dict[key]
        for label in sorted(label_dict.iterkeys()):
            formatted_lines.append("{0}_install:".format(label))
            formatted_lines.append('  pkg.installed:')
            formatted_lines.append('    - pkgs:')
            for item in label_dict[label]:
                formatted_lines.append("      - {0}".format(item))
            # Next append() ensures a gap after each group
            formatted_lines.append(' ')
    return formatted_lines


def gener_charsmax(seq, chunk_size, charsmax, shift=0):
    """ Give charsmax=70 if you want the total concatenation
    (excluding any space separator) to not exceed 70
    apt base-files <-- 14 but would be 13 excluding the space. """
    len_seq = len(seq)
    chunk_size_possible = len_seq - shift
    if chunk_size_possible < chunk_size:
        chunk_size = chunk_size_possible
    charstotal = 0
    for pos in range(shift, shift + chunk_size):
        itemlen = len(seq[pos])
        if charstotal + itemlen > charsmax:
            raise StopIteration
        charstotal += itemlen
        yield pos


def filepath_suffixed(filepath,outform='plain'):
    suffix_replacement = None
    if outform in ['salt','sls']:
        suffix_replacement = 'sls'
    else:
        suffix_replacement = 'manual'
    if suffix_replacement is not None:
        # Have something we should change suffix to
        if '.selections' in filepath:
            suffix_to_replace = 'selections'
        elif '.infolist' in filepath:
            suffix_to_replace = 'infolist'
        else:
            suffix_to_replace = None
        if suffix_to_replace is not None:
            filepath_original = filepath
            try:
                filepath = filepath_original.replace(".{0}".format(suffix_to_replace),
                                                     ".{0}".format(suffix_replacement),1)
            except Exception:
                #print 'filepath = filepath_original'
                filepath = filepath_original
        #print ".{0}".format(suffix_to_replace),".{0}".format(suffix_replacement)
    return filepath


def res_lines_stripped(res_object,outform='plain'):
    res_lines = cmdbin.run_res_lines_stripped(res_object)
    outform_lines = res_lines
    if outform in ['salt','sls']:
        outform_lines = packages_outform_salt(res_lines,False)
    elif outform in ['apt']:
        pass
    else:
        pass
        #outform_lines = res_lines
    return outform_lines


def packages_outform_unsalt(salt_lines):
    filtered = []
    for line in salt_lines:
        if '- ' in line and chr(58) not in line and '# ' not in line:
            filtered.append(line)
    return filtered


@first_arg_path_restricted
def manual_raw(filepath=None,outform='plain'):
    """ Output of apt-mark showmanual
    can be automatically written to a file if you supply a valid filepath
    """
    marked_lines = []
    with settings(warn_only=True):
        with hide('output'):
            marked_res = run(CMD_APT_MARK_MANUAL)
            marked_lines = res_lines_stripped(marked_res,outform)
            if marked_res.return_code == 2:
                print "apt-mark return code {0} encountered.".format(marked_res.return_code)
                print "Early versions of apt-mark do not accept argument showmanual"
            if outform == 'plain':
                pass
            else:
                marked_count = len(cmdbin.run_res_lines_stripped(marked_res))
                isocompact = dt.now().strftime(cmdbin.ISOCOMPACT_STRFTIME)
                total_listed = "# {0} - Total of {1} 'manual' packages will be listed below\n".format(
                    isocompact,marked_count)
                marked_lines.insert(0,total_listed)
                hashed_line = "# {0}".format(cmdbin.release_line())
                marked_lines.insert(0,hashed_line)
    if len(marked_lines) > 0 and filepath:
        #filepath = filepath_suffixed(filepath,outform)
        put_flag = cmdbin.put_using_named_temp_cmdbin(filepath,marked_lines)
    return marked_lines


def manual_to_file(filepath,outform='plain'):
    """ Wrapper around manual_raw which handles 'auto' as argument
    and returns filepath """
    if filepath == 'auto':
        marked_list = manual_raw(outform=outform)
        if outform in ['salt','sls']:
            marked_count = len(packages_outform_unsalt(marked_list))
        else:
            marked_count = len(marked_list)
        if marked_count > 0:
            (pname, pretty_name_id, pretty_name_word, version_major) = cmdbin.pretty_name_tuple()
            """ pretty_name_id = 'unknown' ; pretty_name = 'Unknown!' ; pretty_name_word = 'zero'
            pretty_dict = {'name' : pname, 'id': pretty_name_id, 'release': pretty_name_word, } """
            isocompact = dt.now().strftime(cmdbin.ISOCOMPACT_STRFTIME)
            filename_suffix = 'manual'
            if outform in ['salt','sls']:
                filename_suffix = 'sls'
            filename_constructed = "{0}_{1}_{2}manual__{3}.{4}".format(
                pretty_name_id,pretty_name_word,marked_count,isocompact,filename_suffix)
            filepath = path.join(tilde_expand('~/',True),filename_constructed)
            put_flag = cmdbin.put_using_named_temp_cmdbin(filepath,marked_list)
            if not put_flag:
                filepath = None
        else:
            filepath = None
    else:
        filepath = filepath_suffixed(filepath,outform)
        manual_raw(filepath,outform)
    return filepath


def info_long(print_flag=False,prefix=' ',count=25,manual_only=True):
    installed_list = []
    with hide('output'):
        info_res = run(CMD_INFO_LISTT)
        #print "CMD_INFO_LISTT returned {0} results".format(len(info_res)) 
    if info_res.succeeded:
        pass
    else:
        if print_flag:
            print "Unable to analyse last {0} packages.".format(count)
        return installed_list
    marked_lines = []
    if manual_only is False:
        pass
    else:
        with settings(warn_only=True):
            with hide('output'):
                marked_res = run(CMD_APT_MARK_MANUAL)
                marked_lines = cmdbin.run_res_lines_stripped(marked_res)
                if marked_res.return_code == 2:
                    if print_flag:
                        print "apt-mark return code {0} encountered.".format(marked_res.return_code)
                        print "Early versions of apt-mark do not accept argument showmanual"
                    return installed_list
    for idx,line in enumerate(cmdbin.run_res_lines_stripped_searching(info_res,'.list')):
        if idx > 100:
            break
        try:
            #path_in_full = line.split()[-1]
            last_portion = path.split(line)[-1]
            pname = path.splitext(last_portion)[0]
        except Exception:
            pname = None
        if pname and manual_only is False:
            installed_list.append(pname)
            if len(installed_list) >= count:
                break
        elif pname and pname in marked_lines:
            installed_list.append(pname)
            if len(installed_list) >= count:
                break
        else:
            pname = None

    
    """ if manual_only:
        marked_lines = []
        with settings(warn_only=True):
            with hide('output'):
                marked_res = run(CMD_APT_MARK_MANUAL)
                marked_lines = cmdbin.run_res_lines_stripped(marked_res)
                if marked_res.return_code == 2:
                    print "apt-mark return code {0} encountered.".format(marked_res.return_code)
                    print "Early versions of apt-mark do not accept argument showmanual"
        # Redefine installed_list as original filtered by exists in marked_lines
        installed_list = [ pname for pname in installed_list if pname in marked_lines ]
    """

    if print_flag:
        if len(installed_list) < count:
            if manual_flag is False:
                print "Unable to analyse last {0} 'total' packages.".format(count)
            else:
                print "Unable to analyse last {0} 'manual' packages.".format(count)
        else:
            print "Last {0} 'manual' packages (most recent shown first):".format(count)
            for pname in installed_list:
                print "{0}{1}".format(prefix,pname)

    return installed_list


def info_manual(print_flag=False,prefix=' ',count=5):
    installed_list = []
    with hide('output'):
        info_res = run(CMD_INFO_LISTT)
        #print "CMD_INFO_LISTT returned {0} results".format(len(info_res)) 
    if info_res.failed:
        if print_flag:
            print "Unable to analyse last {0} packages.".format(count)
        return installed_list
    marked_lines = []
    with settings(warn_only=True):
        with hide('output'):
            marked_res = run(CMD_APT_MARK_MANUAL)
            marked_lines = cmdbin.run_res_lines_stripped(marked_res)
            if marked_res.return_code == 2:
                print "apt-mark return code {0} encountered.".format(marked_res.return_code)
                print "Early versions of apt-mark do not accept argument showmanual"
    for idx,line in enumerate(cmdbin.run_res_lines_stripped_searching(info_res,'.list')):
        if idx > 100:
            break
        try:
            #path_in_full = line.split()[-1]
            last_portion = path.split(line)[-1]
            pname = path.splitext(last_portion)[0]
        except Exception:
            pname = None
        if pname and pname in marked_lines:
            installed_list.append(pname)
            if len(installed_list) >= count:
                break
    if print_flag:
        if len(installed_list) < count:
            print "Unable to analyse last {0} 'manual' packages.".format(count)
        else:
            print "Last {0} 'manual' packages:".format(count)
            for pname in installed_list:
                print "{0}{1}".format(prefix,pname)
    return installed_list


def info_manual5print(prefix=' '):
    return info_manual(print_flag=True,prefix=prefix,count=5)


@first_arg_path_restricted
def showauto_raw(filepath=None,outform='plain'):
    """ Output of apt-mark showauto
    can be automatically written to a file if you supply a valid filepath
    """
    marked_lines = []
    with hide('output'):
        marked_res = run(CMD_APT_MARK_AUTO)
        marked_lines = res_lines_stripped(marked_res,outform)
        if marked_res.return_code == 2:
            pass
    if len(marked_lines) > 0 and filepath:
        put_flag = cmdbin.put_using_named_temp_cmdbin(filepath,marked_lines)
    return marked_lines


def showauto_to_file(filepath,outform='plain'):
    """ Wrapper around showauto_raw which handles 'auto' as argument
    and returns filepath """
    if filepath == 'auto':
        marked_list = showauto_raw(outform=outform)
        marked_count = len(marked_list)
        if marked_count > 0:
            (pname, pretty_name_id, pretty_name_word, version_major) = cmdbin.pretty_name_tuple()
            """ pretty_name_id = 'unknown' ; pretty_name = 'Unknown!' ; pretty_name_word = 'zero'
            pretty_dict = {'name' : pname, 'id': pretty_name_id, 'release': pretty_name_word, } """
            isocompact = dt.now().strftime(cmdbin.ISOCOMPACT_STRFTIME)
            filename_constructed = "{0}_{1}_{2}packages__{3}.showauto".format(
                pretty_name_id,pretty_name_word,marked_count,isocompact)
            filepath = path.join(tilde_expand('~/',True),filename_constructed)
            put_flag = cmdbin.put_using_named_temp_cmdbin(filepath,marked_list)
            if not put_flag:
                filepath = None
        else:
            filepath = None
    else:
        showauto_raw(filepath,outform)
    return filepath


def marked_to_file(filepath=None,marked='manual'):
    file_res = None
    if filepath:
        if marked == 'auto':
            file_res = auto_to_file(filepath)
        else:
            file_res = manual_to_file(filepath)
        # 'auto' as argument to both above refers to 'auto' file naming
    else:
        file_res = None
    return file_res
