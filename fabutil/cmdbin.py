#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2019,2015,2014,2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# 2015 Tested against fabric 1.8.5
# https://pypi.python.org/pypi/Fabric/1.8.5
# 2019 Targetted testing against fabric 1.13

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import getuid,getgid,path
from os import linesep as os_linesep
from string import ascii_letters,ascii_lowercase,digits,printable,punctuation,whitespace
from random import shuffle,SystemRandom 
import re
from subprocess import Popen, PIPE
import shlex
import time
from unicodedata import name as unicode_name
from collections import Counter,defaultdict,OrderedDict
from copy import deepcopy
from hashlib import md5

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

CMD_APACHE_MODS_ENABLED="/usr/sbin/apache2ctl -t -D DUMP_MODULES "
CMD_APACHE_VHOST_INFO="/usr/sbin/apache2ctl -S "
CMD_APACHE_MPM_GREPPED = "/usr/sbin/apache2 -V | /bin/fgrep -A2 'MPM:'"
CMD_APACHE_V = "/usr/sbin/apache2 -V "
CMD_HTTPD_MODS_ENABLED="/usr/sbin/httpd -t -D DUMP_MODULES "

CMD_CAT = "/bin/cat "
CMD_TAC = "/usr/bin/tac "
CMD_CP_NOCROSS_PRESERVETIME = "/bin/cp --one-file-system --preserve=timestamps "
CMD_CP_UPDATE = "/bin/cp --update "
CMD_CP_UPDATE_PRESERVETIME = "/bin/cp --update --preserve=timestamps "
CMD_CP_UPDATE_NOCROSS = "/bin/cp --update --one-file-system "
CMD_CP_UPDATE_NOCROSS_PRESERVETIME = "/bin/cp --update --one-file-system --preserve=timestamps "
CMD_CP_PRESERVETIME = "/bin/cp --preserve=timestamps "
CMD_DIFF="/usr/bin/diff "
CMD_DIFF_VERSION="/usr/bin/diff --version;"
CMD_DIFF_UNIFIED_ZERO="/usr/bin/diff -U 0 "
CMD_DIFF_SIDEBYSIDE_NOCOMMON="/usr/bin/diff --side-by-side --suppress-common-lines "
CMD_DIRNAME="/usr/bin/dirname "
CMD_EGREP = "/bin/egrep "
CMD_EGREPI = "/bin/egrep -I "
CMD_EGREP_CONTROL_PACKAGE = "/bin/egrep '^Package:' "
CMD_FGREP = "/bin/fgrep "
CMD_FGREPI = "/bin/fgrep -I "
CMD_FGREP_H_V = "/bin/fgrep -H -v "
CMD_FGREP_H_V_Z = "/bin/fgrep -H -v 'zZzZz!zZzZz' "
CMD_FGREP_L_GROUP = "/bin/fgrep -l 'group' "
CMD_WGET = "/usr/bin/wget --no-check-certificate "
CMD_WGET1 = "/usr/bin/wget --no-check-certificate --timeout=1 "
CMD_WGET5 = "/usr/bin/wget --no-check-certificate --timeout=5 "
CMD_GPG_RECV = "gpg --keyserver keys.gnupg.net --recv-keys "
CMD_GPG_VERIFY = "gpg --verify --no-tty --batch "
CMD_GPG_LIST = "gpg --list-key --no-tty --batch "
CMD_KERN_ALL = "/bin/uname -a "
CMD_KERN_RELEASE = "/bin/uname -r "
CMD_LOCKFILE_CREATE = "/usr/bin/lockfile-create "
CMD_LSOF_INTERNET = "/usr/bin/lsof -i "
CMD_LSOF_TPORT = "/usr/bin/lsof -i tcp:"
CMD_MERCURIAL_CLONE = "/usr/bin/hg clone "
CMD_GIT_CLONE = "/usr/bin/git clone "
CMD_ALTERNATIVES_DISPLAY = "/usr/sbin/update-alternatives --display "
CMD_FIND = "/usr/bin/find "
FIND_PYC_DELETE_TAIL = "-name '*.pyc' -o -name '*.pyo' -delete "
FIND_SETPYC_DELETE_TAIL = "-name 'settings*.pyc' -delete "
CMD_FIND_FILES_HOUR_TEMPLATE = "/usr/bin/find {0} -type f -mmin -60 "
CMD_FIND_FILES_DAY_TEMPLATE = "/usr/bin/find {0} -type f -mtime -1 "
CMD_FIND_FILES_WEEK_TEMPLATE = "/usr/bin/find {0} -type f -mtime -7 "
CMD_FIND_FILES_MONTH_TEMPLATE = "/usr/bin/find {0} -type f -mtime -31 "
CMD_FIND_PYC_HOUR_DEL_TEMPLATE = "/usr/bin/find {0} -type f -mmin -60 "+FIND_PYC_DELETE_TAIL
CMD_FIND_PYC_DAY_DEL_TEMPLATE = "/usr/bin/find {0} -type f -mtime -1 "+FIND_PYC_DELETE_TAIL
CMD_FIND_PYC_WEEK_DEL_TEMPLATE = "/usr/bin/find {0} -type f -mtime -7 "+FIND_PYC_DELETE_TAIL
CMD_FIND_SETPYC_HOUR_DEL_TEMPLATE = "/usr/bin/find {0} -type f -mmin -60 "+FIND_SETPYC_DELETE_TAIL
CMD_FIND_SETPYC_DAY_DEL_TEMPLATE = "/usr/bin/find {0} -type f -mtime -1 "+FIND_SETPYC_DELETE_TAIL
CMD_FIND_SETPYC_WEEK_DEL_TEMPLATE = "/usr/bin/find {0} -type f -mtime -7 "+FIND_SETPYC_DELETE_TAIL
CMD_WHICH = "/usr/bin/which "
CMD_RM_BIGI = "/bin/rm -I "
CMD_RM_BIGI_VERBOSE = "/bin/rm -I --verbose "
CMD_SED = "/bin/sed "
CMD_SEDE = "/bin/sed -e "
CMD_SEDF = "/bin/sed -f "
CMD_SEDN = "/bin/sed -n "
CMD_SEDNF = "/bin/sed -n -f "
CMD_SEDNR = "/bin/sed -n -r "
CMD_SEDR = "/bin/sed -r "
CMD_SED16 = "/bin/sed -n 'p;p;p;p;p;p;p;p;p;p;p;p;p;p;p;p' "
CMD_TAIL = "/usr/bin/tail "
CMD_MD5SUM = "/usr/bin/md5sum "
CMD_MD5SUM_TEXT = "/usr/bin/md5sum --text "
CMD_SHA256 = "/usr/bin/sha256sum "
CMD_WORDCOUNT = "/usr/bin/wc "
CMD_LINECOUNT = "/usr/bin/wc -l "
CMD_LINECOUNT_UNHASHED = "/bin/egrep -I -c -v '^#' "
CMD_LINECOUNT_UNHASHED_ALNUM = "/bin/egrep -I -c '^[^#]\s*[[:alnum:]]' "
CMD_UPTIME = "/usr/bin/uptime "
CMD_POSTGRES_PASSLINE = "/usr/bin/getent passwd postgres "
CMD_PROCESS_USER = "/bin/ps -o pgrp,pid,comm,user,rss,sz,size,cputime -u "
CMD_TAREXTRACT = "/bin/tar -xf "
CMD_TAREXTRACTVERB = "/bin/tar -xvf "
CMD_TAREXTRACT_TO_UNBUILT = "/bin/tar -C ./unbuilt --strip-components=1 -xf "
CMD_TAREXTRACT_TO_UNBUILTDEBIAN = "/bin/tar -C ./unbuilt/debian --strip-components=1 -xf "
CMD_TARZEXTRACT = "/bin/tar -zxf "
CMD_TARZEXTRACTVERB = "/bin/tar -zxvf "
CMD_TARZEXTRACT_TO_UNBUILT = "/bin/tar -C ./unbuilt --strip-components=1 -zxf "
CMD_TARZEXTRACT_TO_UNBUILTDEBIAN = "/bin/tar -C ./unbuilt/debian --strip-components=1 -zxf "
CMD_TAR_STRIPPED1 = "/bin/tar --strip-components=1 "
CMD_TOUCH = "/usr/bin/touch "
CMD_MKDIRVP755 = "/bin/mkdir -v -p -m 755 "


_list_dot_d_array = []

# DIRSTEM_DATA_SHORT = '~/'
DIRSTEM_DATA_SHORT = '/var/local'
DIR_UNBUILT_RELATIVE = './unbuilt'
DIR_UNBUILT_DEBIAN_RELATIVE = './unbuilt/debian'
DIR_UNBUILT_DEBIAN_RELATIVE_DEBIAN = './unbuilt/debian/DEBIAN'

MD5A32 = 'a'*32
MD5DEFAULTEXCLAIM = 'default!default!default!default!'

PARENTHS_OPEN = chr(40)
PARENTHS_CLOSE = chr(41)
BRACES_OPEN = chr(123)
BRACES_CLOSE = chr(125)
PIPE_BACKSLASH = "{0} {1}".format(chr(124),chr(92))
DOLLAR_EXCLAMATION = "{0}{1}".format(chr(36),chr(33))
XZERO = chr(0x0)
XONEBEE = chr(0x1b)
XZERO8 = chr(0x08)
DQBACKSLASHDQDQ= "{0}{1}{0}{0}".format(chr(34),chr(92))
# chr(34) is double quote (")     ; chr(39) is single quote (')
SQBACKSLASHSQSQ= "{0}{1}{0}{0}".format(chr(39),chr(92))

RE_FIRST_ALPHA = re.compile('[a-z]+')
RE_VAR_LIB_ALPHA = re.compile('/var/lib/[a-z][a-z]+')
RE_OPT_LOCAL_ALPHA = re.compile('/opt/local/[a-z][a-z]+')
RE_VAR_LOCAL_ALPHA = re.compile('/var/local/[a-z][a-z]+')
RE_STATUS_INSTALLED = re.compile('Status:.*installed')
RE_MD5SUM = re.compile(r'[a-fA-F\d]{32}')
RE_PREFIXED_STRING_TUPLE = re.compile(r'[A-Z]*\s*')

REGEX_OPTS_EGREP = '-regextype posix-egrep -regex'
REGEX_OPTS_EMACS = '-regextype emacs -regex'
EXEC_TAIL = '{} \;'
REGEX_SOURCES_LIST = '.*(/sources.list|/sources.list.d/.*\.list)'


NUM_AS_WORDS_TO_TWENTY=['zero','one','two','three','four','five','six','seven','eight','nine','ten']
NUM_AS_WORDS_TO_TWENTY.extend(['eleven','twelve','thirteen','fourteen','fifteen'])
NUM_AS_WORDS_TO_TWENTY.extend(['sixteen','seventeen','eighteen','nineteen','twenty'])

SET_DIGITS_AND_PERIOD = set(digits).union(chr(46))
SET_PRINTABLE = set(printable)
SET_PUNCTUATION = set(punctuation)
SET_LOWER_PLUS_DIGITS = set(ascii_lowercase).union(digits)
SET_WHITESPACE = set(whitespace)
SET_COLON_SPACE_EQUALS = set([chr(58),chr(32),chr(61)])
SET_PUNCTUATION_WHITESPACE = set(punctuation).union(whitespace)
SET_PRINTABLE_NOT_DIGITS = set(printable).difference(digits)
SET_PRINTABLE_NOT_DIGITS_NOT_PERIOD = (set(printable).difference(digits)).difference(set(chr(46)))
PUNCTUATION3CLASS = "[{0}{1}{2}{2}]".format(chr(34),chr(39),chr(92))
PUNCTUATION_FEWER = re.sub(PUNCTUATION3CLASS, '', punctuation)
DOUBLE_DOT=chr(46)*2

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'
""" Above is a compact form that does not include colon (:) and below
is a variant of the same that includes microseconds but does
not include dot / full stop
"""
ISOCOMPACTMICRO_STRFTIME='%Y%m%dT%H%M%S%f'
LOOPBACK4='127.0.0.1'
LOOPBACK6='::1'

DEBIAN_PLUS_DERIVATIVES1=['debian','trisquel']
DEBIAN_DERIVATIVES2=['gnewsense','mint','ubuntu']
DEBIAN_PLUS_DERIVATIVES2=DEBIAN_PLUS_DERIVATIVES1[:]
DEBIAN_PLUS_DERIVATIVES2.extend(DEBIAN_DERIVATIVES2)
""" Lists above could be extended by using the huge svg graph 'DebianFamilyTree' on public internet.
That DebianFamilyTree has two large sections hence the 1 and 2 suffixes to our lists """
RPM_BASED1=['redhat','centos']
RPM_BASED2=RPM_BASED1[:]
RPM_BASED2.extend(['fedora'])
ETC_SELINUX_CONFIG = '/etc/selinux/config'
ETC_SYSCONFIG_SELINUX = '/etc/sysconfig/selinux'


def run_or_sudo(run_cmd,run_always=False,quiet=False):
    """ run_always flag set True means
        that sudo will never be used. """
    if env.user == 'root' or run_always:
        if quiet:
            with hide('output'):
                res = run(run_cmd)
        else:
            res = run(run_cmd)
    elif quiet:
        with hide('output'):
            res = sudo(run_cmd)
    else:
        res = sudo(run_cmd)
    return res


def run_or_sudo_warningless(run_cmd,run_always=False):
    """ run_always flag set True means
    that sudo will never be used.
    The request for 'warningless' implies quiet so there is
    no third paramter 'quiet' unlike related function run_or_sudo()
    """
    if env.user == 'root' or run_always:
        with hide('stdout','warnings'):
            res = run(run_cmd)
    else:
        with hide('stdout','warnings'):
            res = sudo(run_cmd)
    return res


def tilde_expand(path_short,abort_on_fail=True):
    path_expanded = path_short
    if path_short is None or path_short.startswith('~/'):
        """ path_expanded = path.expanduser(path_short)
        will not do as you are interested in the remote user
        not the user account on your local machine! """
        expandres = run("{0} ~/.".format(CMD_DIRNAME))
        if expandres.failed:
            if abort_on_fail:
                abort("Path expansion failed using unexpanded path of {0}".format(path_short))
            path_expanded = path.short
        else:
			if path_short is None and len(expandres) > 1:
				path_expanded = "{0}/".format(expandres)
			else:
				path_expanded = path_short.replace('~/',"{0}/".format(expandres),1)
        """ path_expanded = run("dirname ~/.") """
    return path_expanded


def preservetime_or_sudo(filepath_source,filepath_destination,
                         update=True,cross=False,verbose=False):
    """ All copy commands that will be executed by this function
    use the copy option --preserve-timestamps and by using that option will
    NOT preserve ownership and permissions when copying.
    See planned function copy_or_sudo in files_directories.py for a fuller
    implementation of copy options.
    """
    destination_dir = path.dirname(filepath_destination)
    destination_base = path.basename(filepath_destination)
    if len(destination_dir) < 3 or set(destination_dir).issubset(SET_PUNCTUATION_WHITESPACE):
        """ Use cp directly if you need to copy files into root (/)
        as this function will only copy where above two conditions are not met.
        """
        return False
    if update:
        if cross is True:
            copy_cmd = CMD_CP_UPDATE_PRESERVETIME
        else:
            copy_cmd = CMD_CP_UPDATE_NOCROSS_PRESERVETIME
    elif cross is True:
        copy_cmd = CMD_CP_PRESERVETIME
    else:
        copy_cmd = CMD_CP_NOCROSS_PRESERVETIME

    run_cmd = "{0} {1} {2};".format(copy_cmd,filepath_source,filepath_destination)
    if verbose:
        run_cmd = "{0} --verbose {1} {2};".format(copy_cmd,filepath_source,filepath_destination)

    if env.user == 'root':
        if verbose:
            res = run(run_cmd)
        else:
            with hide('output'):
                res = run(run_cmd)
    elif verbose:
        res = sudo(run_cmd)
    else:
        with hide('output'):
            res = sudo(run_cmd)
    return res


def rm_bigi_or_sudo(file_to_remove,filedir=None,verbose=False):
    res = False
    to_remove_dir = path.dirname(file_to_remove)
    to_remove_base = path.basename(file_to_remove)
    rm_cmd = None
    if filedir is not None:
        if len(filedir) < 3 or set(filedir).issubset(SET_PUNCTUATION_WHITESPACE):
            return False
    elif len(to_remove_dir) < 3 or set(to_remove_dir).issubset(SET_PUNCTUATION_WHITESPACE):
        """ Use rm directly if you need to remove files from root (/)
        as this function will only remove where above two conditions are not met.
        """
        return False
    else:
        pass
    if verbose:
        rm_cmd = "{0} {1};".format(CMD_RM_BIGI_VERBOSE,file_to_remove)
    else:
        rm_cmd = "{0} {1};".format(CMD_RM_BIGI,file_to_remove)
    with settings(warn_only=True):
        if filedir is not None:
            """ Enclose the command in with cd """
            with cd(filedir):
                if env.user == 'root':
                    if verbose:
                        res = run(rm_cmd)
                    else:
                        with hide('output'):
                            res = run(rm_cmd)
                elif quiet:
                    with hide('output'):
                        res = sudo(rm_cmd)
                else:
                    res = sudo(rm_cmd)
        elif env.user == 'root':
            if verbose:
                res = run(rm_cmd)
            else:
                with hide('output'):
                    res = run(rm_cmd)
        elif verbose:
            res = sudo(rm_cmd)
        else:
            with hide('output'):
                res = sudo(rm_cmd)
    return res


def run_res_lines_stripped(fabric_res,linesep=None):
    """ Two conditions to pass before any lines will be added to list being returned:
    (1) fabric result object must have attribute succeeded set
    (2) fabric result object must have return_code set zero
    If your results are line separated with something other than
    the value of os.linesep then use the linesep parameter to supply that.
    """
    lines_stripped_as_list = []
    if fabric_res.succeeded:
        if fabric_res.return_code == 0:
            if linesep is None:
                linesep = os_linesep
            for line_unstripped in fabric_res.split(linesep):
                lines_stripped_as_list.append(line_unstripped.strip())
    return lines_stripped_as_list


def run_or_sudo_stripped(run_cmd,run_always=False,quiet=False,linesep=None):
    ros_res = run_or_sudo(run_cmd=run_cmd,run_always=run_always,quiet=quiet)
    return run_res_lines_stripped(ros_res,linesep=linesep)


def process_user(user='www-data',target=None):
    with settings(warn_only=True):
        """ Warning only now set, next hide """
        with hide('output','warnings'):
            pres = run_or_sudo("{0} {1}".format(CMD_PROCESS_USER,user))
            if target is None or len(target) < 3:
                plines = run_res_lines_stripped(pres)
            else:
                plines = run_res_lines_stripped_searching(pres,target)
    return plines


def printable_only(unfiltered='',escape_prefilter=False):
    """ Use re.sub and complement to filter out non-printables.
    Alternatives documented for comparison and lastly a single char replace example also.
    Strip control chars:     # filtered = re.sub('[\x00-\x1F\x7F]', '', unfiltered)
    r'[^[\x20-\x7E]]' # Match all non-printable
    r'[\x00-\x1F\x7F]' # Match control characters 
    # filtered = str.replace('\x1b','z',unfiltered)
    """
    filtered = ''
    if unfiltered is None or len(unfiltered) < 1:
        return filtered
    if escape_prefilter is True:
        unfiltered = re.sub("\033\[[0-9;]+m", '', unfiltered)
    filtered = re.sub("[^{0}]".format(printable), '', unfiltered)
    return filtered


def dotty_level(filepath_tainted,before_only=False,single_initial_okay=False):
    """ Returns 2 if there are one or more double dots in the path
    before the final path delimiter (/)
    Returns 1 if there are one or more single dots in the path
    before the final path delimiter (/)
    When before_only is set True we can still return 0 even if
    a double dot appears after the final path delimiter (/)
    
    The first parameter is named filepath_tainted as we make no assumption
    about the providence or safety of the path supplied. It may be program
    supplied or user supplied - we really do not know.

    Try where possible to give the highest value of 1 or 2 so if double dots and single dots
    then we aim to return dotty_level = 2
    """
    dir_tainted = path.dirname(filepath_tainted)
    dirlen = len(dir_tainted)
    dotty_level = 9
    before_okay = False
    if len(dir_tainted) < 1:
        before_okay = True
    elif len(dir_tainted) < 2:
        if dir_tainted.startswith(chr(46)) and single_initial_okay:
            dotty_level = 1
            before_okay = True
    else:
        if dir_tainted.startswith(chr(46)):
            if dir_tainted.startswith(DOUBLE_DOT):
                dotty_level = 2
            elif single_initial_okay:
                dotty_level = 1
                before_okay = True
            else:
                if DOUBLE_DOT in dir_tainted:
                    dotty_level = 2
                else:
                    before_okay = True
                    dotty_level = 1
        else:
            before_okay = True

    #print("dotty_level={0} and before_okay={1}".format(dotty_level,before_okay))

    if before_okay is False:
        # We have already decided to fail based on initial chars in dirname
        if DOUBLE_DOT in dir_tainted:
            dotty_level = 2
        return dotty_level

    if dotty_level > 2:
        # If we have not got dotty level of 1 or 2 then so far we must be in the clear
        dotty_level = 0

    # Nothing in the initial chars of dirname has yet convinced us to fail.
    if DOUBLE_DOT in dir_tainted:
        dotty_level = 2
    elif chr(46) in dir_tainted:
        dotty_level = 1

    if before_only is False and dotty_level != 2:
        # Examine the full path rather than just dirname portion
        if DOUBLE_DOT in filepath_tainted:
            dotty_level = 2
        elif chr(46) in dir_tainted:
            dotty_level = 1
        else:
            pass
    
    return dotty_level


def linecount_unhashed(filepath,error_action='raise'):
    """ For given file get a count by grepping for lines that
    do not begin hash and contain some alphanumeric characters

    If unable to get a count then return 0 or raise an error (default)
    depending on value of supplied parameter error_action.
    """
    #gen_prefix = 'linecount_unhashed:'
    error_prefix = 'linecount_unhashed Error:'
    if filepath is None:
        if error_action.lower().startswith('raise'):
            raise Exception("{0} no filepath supplied".format(error_prefix))
        else:
            return 0
    if chr(47) in filepath:
        """ Regular file including some path element (/)                                          
        Here forward slash is hardcoded. This software is intended for remote
        systems that are GNU/Linux or GNU variants. """
        pass
    else:
        if error_action.lower().startswith('raise'):
            raise Exception("{0} bad filepath supplied".format(error_prefix))
        else:
            return 0
    unhashed_count = None
    with settings(warn_only=True):
        """ Warning only now set, next hide """
        with hide('stdout','warnings'):
            ros_res = run_or_sudo("{0} {1};".format(CMD_LINECOUNT_UNHASHED,filepath))
            res_lines = run_res_lines_stripped(ros_res)
            unhashed_count = None
            if len(res_lines) == 1:
                try:
                    unhashed_count = int(res_lines[0].strip())
                except:
                    unhashed_count = None
    if unhashed_count is None:
        if error_action.lower().startswith('raise'):
            raise Exception("{0} unable to determine count".format(error_prefix))
        else:
            unhashed_count = 0
    return unhashed_count


def linecount_unhashed_alnum(filepath,error_action='raise'):
    """ For given file get a count by grepping for lines that
    do not begin hash and contain some alphanumeric characters

    If unable to get a count then return 0 or raise an error (default)
    depending on value of supplied parameter error_action.
    """
    #gen_prefix = 'linecount_unhashed_alnum:'
    error_prefix = 'linecount_unhashed_alnum Error:'
    if filepath is None:
        if error_action.lower().startswith('raise'):
            raise Exception("{0} no filepath supplied".format(error_prefix))
        else:
            return 0
    if chr(47) in filepath:
        """ Regular file including some path element (/)                                          
        Here forward slash is hardcoded. This software is intended for remote
        systems that are GNU/Linux or GNU variants. """
        pass
    else:
        if error_action.lower().startswith('raise'):
            raise Exception("{0} bad filepath supplied".format(error_prefix))
        else:
            return 0
    unhashed_count = None
    with settings(warn_only=True):
        """ Warning only now set, next hide """
        with hide('stdout','warnings'):
            ros_res = run_or_sudo("{0} {1};".format(CMD_LINECOUNT_UNHASHED_ALNUM,filepath))
            res_lines = run_res_lines_stripped(ros_res)
            unhashed_count = None
            if len(res_lines) == 1:
                try:
                    unhashed_count = int(res_lines[0].strip())
                except:
                    unhashed_count = None
    if unhashed_count is None:
        if error_action.lower().startswith('raise'):
            raise Exception("{0} unable to determine count".format(error_prefix))
        else:
            unhashed_count = 0
    return unhashed_count


def filepath_suffixed_isocompact(filepath,middle=None):
    from datetime import datetime as dt
    isocompact = dt.now().strftime(ISOCOMPACT_STRFTIME)
    if middle is None or len(middle) < 1:
        suffixed = "{0}.{1}".format(filepath,isocompact)
    else:
        suffixed = "{0}.{1}.{2}".format(filepath,middle,isocompact)
    return suffixed


def shifted_from_micros(filepath):
    """ Produce a pseudo random filepath insert that may be used as a relatively
    inexpensive way of avoiding filepath clashes. Better [more expensive]
    implementations are certainly available.
    """
    from datetime import datetime as dt
    # %f is Microsecond as a decimal number [0,999999], zero-padded on the left
    micros = dt.now().strftime('%f')
    assert len(micros) > 5
    UNRANDOM_LIST=['somerandomness','desirable','whenever','clashespossible']
    from random import randint
    idx = randint(0,(-1+len(UNRANDOM_LIST)))
    unrandom_word = UNRANDOM_LIST[idx]
    micros_repadded = micros.replace(str(idx),'0')
    unrandom_string = "{0}{1}{2}{3}".format(unrandom_word,micros,filepath,micros_repadded)
    try:
        hundredth = int(micros[-2:])
    except Exception:
        hundredth = max(99,len(filepath))
    mdf = md5()
    mdf.update(unrandom_string)
    for _ in range(0,hundredth):
        mdf.update(unrandom_string)
    md5sum = mdf.hexdigest()
    isocompact_micro = dt.now().strftime(ISOCOMPACTMICRO_STRFTIME)
    assert len(isocompact_micro) > 15
    eight_string = "{0}{1}".format(md5sum[:4],md5sum[-4:])
    idx = randint(2,6)
    shifted = "{0}{1}{2}".format(isocompact_micro,eight_string[idx:],eight_string[:idx])
    return shifted


def filepath_micro(filepath,shifted=False,verbose=False):
    """ Given a filepath include a microseconds portion into the supplied filepath.
    shifted=True involves additional processing.
    chr(46) is dot / full stop
    """
    filepath_returned = filepath
    split_array = ('','')
    """ splitext() returns a pair (two tuple is). Above we set a pair of empties """
    try:
        split_array = path.splitext(filepath)
    except AttributeError:
        split_array = None
    except:
        split_array = ('')
    if split_array is None:
        raise "filepath_micro() supplied with bad filepath: {0}".format(filepath)
    elif len(split_array) < 2:
        raise "filepath_micro() supplied with unworkable filepath: {0}".format(filepath)
    elif split_array[1] == '':
        if split_array[0] == '' or split_array[0].startswith(chr(46)):
            raise "filepath_micro() supplied with unusable filepath: {0}".format(filepath)
    if shifted is True:
        filepath_insert = shifted_from_micros(filepath)
    else:
        from datetime import datetime as dt
        isocompact_micro = dt.now().strftime(ISOCOMPACTMICRO_STRFTIME)
        assert len(isocompact_micro) > 15
        #filepath_insert = "{0}{1}".format(chr(46),isocompact_micro)
        filepath_insert = isocompact_micro
    if verbose:
        print "filepath_micro() makes filepath_insert={0}".format(filepath_insert)
    if filepath_insert is not None:
        unjoined_list = list(split_array[:-1])
        unjoined_list.append("{0}{1}".format(chr(46),filepath_insert))
        unjoined_list.append(split_array[-1])
        filepath_returned = ''.join(unjoined_list)
    return filepath_returned


def print_success_and_failed(fab_object,dirpath=None):
    succ_plus_failed = -999
    try:
        succ_plus_failed = int(fab_object.succeeded) + \
            int(fab_object.failed)
    except Exception:
        succ_plus_failed = -1

    if dirpath:
        print "{0} success and failed: {1} {2}".format(
            dirpath,fab_object.succeeded,fab_object.failed)
    else:
        print "Success and failed: {0} {1}".format(
            fab_object.succeeded,fab_object.failed)
    return succ_plus_failed


def secret_system_random(seed_string=None,count=50,minimum_quality=2):
    seed_fewer = "{0}{1}{2}".format(ascii_letters, digits, PUNCTUATION_FEWER)
    lower_digits_extra = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    if seed_string is None:
        seed_chars = seed_fewer
    elif len(seed_string) > 4 and seed_string.startswith('lower'):
        seed_chars = lower_digits_extra
    elif len(seed_string) < 26:
        seed_chars = seed_fewer
    else:
        pass
    secret_key = None
    quality = 0
    sysrand = SystemRandom()
    try:
        secret_key = ''.join(sysrand.choice(seed_chars) for i in range(count))
        quality = 3
    except:
        #print "quality=0 ... trying quality=2 next"
        try:
            secret_key = ''.join(sysrand.choice(ascii_letters) for i in range(count))
            quality = 2
        except:
            secret_key = shuffle(list(ascii_letters))
            quality = 1
    if quality < minimum_quality:
        quality = 0
        secret_key = None
    return (secret_key,quality)


def secret_crypto(seed_string=None,count=50):
    """ Placeholder for alternative implementation to SystemRandom.
    For now just a wrapper around secret_system_random() which insists on a minimum_quality

    from django.utils.crypto import get_random_string
    secret_key = get_random_string(50, seed_chars)

    If minimum_quality=2 cannot be achieved then secret_system_random() will return
    first element of tuple as None, hence this function will return None
    """
    min_quality = 2
    secret_key,quality = secret_system_random(seed_string,count,minimum_quality=min_quality)
    return secret_key


def md5valid(md5sum,validity_level=2,delimiter=None):
    """ Logic for non delimited md5sums is the focus of this function.
    Logic for delimiter separated md5sums is basic
    and will likely only catch problems if delimiter supplied
    is a single printable such as colon (:)
    """
    delimiter_len = 0
    min_length = 32
    counter = None
    if md5sum is None or len(md5sum) < min_length:
        return False
    elif delimiter is not None:
        delimiter_len = len(delimiter)
    md5sum_len = len(md5sum)
    if delimiter_len > 0:
        counter = Counter(md5sum)
        min_length = 32 + 15*delimiter_len
        if len(md5sum) < min_length:
            return False
    if validity_level < 1:
        return True
    """ If you have a delimiter then counter will be other than None
    so can use that in future logic
    """
    valid_flag = False
    if set(md5sum).issubset(SET_PRINTABLE):
        if counter is None:
            valid_flag = True
        elif delimiter_len == 1 and counter[delimiter] == 15:
            valid_flag = True
        else:
            pass
    """ If have valid_flag=True here then passed validity level 1
    Further checking only if validity_level > 1
    """
    if validity_level > 1:
        valid_flag = False
        if counter is None:
            if md5sum_len == 32 and RE_MD5SUM.match(md5sum):
                valid_flag = True
        else:
            # Some delimiter in there also.
            md5sum_compact = md5sum.replace(delimiter,'')
            if len(md5sum_compact) == 32 and RE_MD5SUM.match(md5sum_compact):
                valid_flag = True
    return valid_flag


def md5_from_iterable(iterable,md5sum_default=None):
    md5sum = md5sum_default
    mdf = md5()
    idx = 0
    for item_string in iterable:
        mdf.update(item_string)
        idx += 1
    if idx < 1:
        md5sum = md5sum_default
    else:
        md5sum = mdf.hexdigest()
    return md5sum


def md5dict_from_iterable(iterable,linesep=None):
    """ Testing helper. Returns an OrderedDict of the following:
    individual line md5s and counts (an md5 for each item in the iterable)
    followed by the counts which all have key values beginning 'count_'
    space then linefeed ' \n' is d784fa8b6d98d27699781bd9a7cf19f0
    bare linefeed is 68b329da9893e34099c7d8ad5cb9c940
    space alone is unlikely but anyway 7215ee9c7d9dc229d2921a40e899ec5f
    """
    md5dict_counts_zero = OrderedDict()
    md5dict_counts_zero['count_space_alone'] = 0
    md5dict_counts_zero['count_space_linesep'] = 0
    md5dict_counts_zero['count_spaces_linesep'] = 0
    md5dict_counts_zero['count_linesep_alone'] = 0
    md5dict_counts_zero['count_other'] = 0
    md5dict = md5dict_counts_zero.copy()
    if linesep is None:
        linesep = os_linesep
    SPACE_LINESEP = "{0}{1}".format(chr(32),linesep)
    idx = 0
    for item_string in iterable:
        try:
            mdf = md5(item_string).hexdigest()
            md5dict[idx] = mdf
            if item_string == SPACE_LINESEP:
                md5dict['count_space_linesep'] += 1
            elif item_string == chr(32):
                md5dict['count_space_alone'] += 1
            elif item_string == linesep:
                md5dict['count_linesep_alone'] += 1
            elif item_string.startswith(chr(32)) and item_string.lstrip() == linesep:
                md5dict['count_spaces_linesep'] += 1
            else:
                md5dict['count_other'] += 1
        except AttributeError:
            """ Zero all the counts on error """
            md5dict = md5dict_counts_zero.copy()
            break
        except Exception:
            """ Zero all the counts on error """
            md5dict = md5dict_counts_zero.copy()
            break
        idx += 1
    return md5dict


def md5list_from_iterable(iterable):
    """ Testing helper. Returns a list of md5s 
    (an md5 for each item in the iterable)
    space then linefeed ' \n' is d784fa8b6d98d27699781bd9a7cf19f0
    bare linefeed is 68b329da9893e34099c7d8ad5cb9c940
    space alone is unlikely but anyway 7215ee9c7d9dc229d2921a40e899ec5f
    """
    md5list = []
    idx = 0
    for item_string in iterable:
        try:
            md5list.append(md5(item_string).hexdigest())
        except AttributeError:
            md5list = []
            break
        except Exception:
            md5list = []
            break
        idx += 1
    return md5list


def iterable1iterable2compare(iterable1,iterable2,summary_numbers_only=True,
                              missing_count_exception=True):
    """ Compare values of keys beginning 'count_' in two lists / iterables.
    Useful for detecting small changes in formatting / spacing / line endings
    between a file and its sed result output.
    Designed for dealing with counts so non-integers should error.
    """
    iterable1dict = md5dict_from_iterable(iterable1)
    iterable2dict = md5dict_from_iterable(iterable2)

    counts1tuples = [(k, v) for k, v in iterable1dict.iteritems() if str(k).startswith('count_')]
    counts2tuples = [(k, v) for k, v in iterable2dict.iteritems() if str(k).startswith('count_')]
    counts_dict1 = OrderedDict(counts1tuples)
    counts_dict2 = OrderedDict(counts2tuples)
    counts_dict_combined = counts_dict1.copy()
    counts_dict_combined.update(counts_dict2)
    compared_dict = OrderedDict()
    for count_key in counts_dict_combined.iterkeys():
        if count_key in counts_dict1 and count_key in counts_dict2:
            count1 = 0
            count2 = 0
            try:
                count1 = int(counts_dict1[count_key])
                count2 = int(counts_dict2[count_key])
            except ValueError:
                raise Exception("...2compare() only possible for integers")
            except Exception:
                raise
            count_diff = count1-count2
            compared_dict[count_key] = count_diff
        elif missing_count_exception:
            raise Exception("{0} key not found in both iterable dicts!".format(count_key))
        else:
            pass

    compared_as_tuple = tuple(compared_dict.itervalues())

    any_result = any(compared_as_tuple)
    text_as_list = ['Exactly same. (All counts equal)']
    if any_result:
        text_as_list = ['Different. (Some difference in counts)']
    if summary_numbers_only is True:
        pass
    else:
        if 'count_other' in compared_dict:
            count_other = compared_dict['count_other']
            if count_other == 0:
                other = counts_dict1['count_other']
                """ Above assignment of 'other' could have been sourced from
                counts_dict1 or counts_dict2 as we have just been told they are same
                """
                text_as_list.append("count_other={0} (same in both)".format(other))
            else:
                text_as_list.append("count_other={0} (differs)".format(count_other))
        sum_result = sum(compared_as_tuple)
        if sum_result == 0:
            text_as_list.append('Tally may have been shifted')
            text_as_list.append('between counts as sum()==0 ')
    return (text_as_list,compared_as_tuple)


def kern64():
    with settings(warn_only=True):
        kern64line = "{0} | {1} 'x86_64'".format(CMD_KERN_ALL,CMD_FGREP)
        kern64res = run(kern64line)
    if kern64res.failed:
        return False
    return True


def splitlines_retaining(lines_unsplit,linesep=None):
    """ If your lines are line separated with something other than
    the value of os.linesep then use the linesep parameter to supply that.
    This will in some cases return a different result than
    plain splitlines(True) due to folding / loss of any blank lines.
    """
    lines = []
    if linesep is None:
        linesep = os_linesep
        for line in lines_unsplit.split(linesep):
            lines.append("{0}{1}".format(line,linesep))
    return lines


def tuple_from_string_tuple(string_tuple,simple=False):
    list_from_string_tup = []
    if string_tuple is None:
        return ()
    string_len = 0
    try:
        string_len = len(string_tuple)
    except:
        string_len = 0
    if string_len < 2:
        return ()
    simple = True
    if simple is True:
        for elem in string_tuple.strip().strip('()').strip().split(','):
            list_from_string_tup.append("{0}".format(re.sub(r"^'|'$", '', elem.strip())))
    else:
        # Future code which will be default when implemented
        pass
    return tuple(list_from_string_tup)


def tuple_from_string_tuple_prefixed(string_tuple,simple=False):
    if RE_PREFIXED_STRING_TUPLE.match(string_tuple):
        string_tuple = string_tuple.strip().split('=')[1]
    return tuple_from_string_tuple(string_tuple=string_tuple,simple=simple)


def run_res_lines_stripped_matching(fabric_res,expression,linesep=None):
    """ Wrapper around run_res_lines_stripped which retains only
    lines which MATCH the target.
    """
    lines_stripped_as_list = []
    try:
        if expression.startswith("{0}{1}".format(chr(94),chr(92))):
            re_matching = re.compile(expression)
        else:
            # Does not begin ^\
            re_matching = re.compile(re.escape(expression))
    except Exception:
        return lines_stripped_as_list
    for line in run_res_lines_stripped(fabric_res,linesep):
        matched = re_matching.match(line)
        if matched:
            lines_stripped_as_list.append(line)
    return lines_stripped_as_list


def run_res_lines_stripped_matching1(fabric_res,expression,linesep=None):
    """ Wrapper around run_res_lines_stripped which retains only
    line which MATCH the target.
    This variant returns a string always.
    Matching string if match successful
    Empty string is match not successful
    """
    matched_line = ''
    try:
        if expression.startswith("{0}{1}".format(chr(94),chr(92))):
            re_matching = re.compile(expression)
        else:
            # Does not begin ^\
            re_matching = re.compile(re.escape(expression))
    except Exception:
        return matched_line
    for line in run_res_lines_stripped(fabric_res,linesep):
        matched = re_matching.match(line)
        if matched:
            matched_line = line
    return matched_line


def run_res_lines_stripped_searching(fabric_res,expression,linesep=None):
    """ Wrapper around run_res_lines_stripped which retains only
    lines which SEARCH the target.
    """
    lines_stripped_as_list = []
    try:
        re_searching = re.compile(re.escape(expression))
    except Exception:
        return lines_stripped_as_list
    for line in run_res_lines_stripped(fabric_res,linesep):
        matched = re_searching.search(line)
        if matched:
            lines_stripped_as_list.append(line)
    return lines_stripped_as_list


def run_res_lines_stripped_searching1(fabric_res,expression,linesep=None):
    """ Wrapper around run_res_lines_stripped which retains only
    line which SEARCH the target.
    This variant returns a string always.
    Searching string if match successful
    Empty string is match not successful
    """
    searched_line = ''
    try:
        re_searching = re.compile(re.escape(expression))
    except Exception:
        return searched_line
    for line in run_res_lines_stripped(fabric_res,linesep):
        matched = re_searching.search(line)
        if matched:
            searched_line = line
    return searched_line


def run_res_lines_tolerant_searching1(fabric_res,expression,linesep=None):
    """ retains only lines which SEARCH the target.
    No check is made on fabric_res.succeeded
    This variant returns a string always.
    Searching string if match successful
    Empty string is match not successful
    """
    searched_line = ''
    try:
        re_searching = re.compile(re.escape(expression))
    except Exception:
        return searched_line

    if linesep is None:
        linesep = os_linesep

    for line_unstripped in fabric_res.split(linesep):
        line = line_unstripped.strip()
        matched = re_searching.search(line)
        if matched:
            searched_line = line

    return searched_line


def run_res_lines_stripped_excluding(fabric_res,expression,linesep=None):
    """ Wrapper around run_res_lines_stripped which retains only
    line for which SEARCH the target fails.
    This variant returns a string always.
    Searching string if match successful
    Empty string is match not successful
    """
    lines_stripped_as_list = []
    try:
        re_searching = re.compile(re.escape(expression))
    except Exception:
        return searched_line
    for line in run_res_lines_stripped(fabric_res,linesep):
        matched = re_searching.search(line)
        if not matched:
            """ We have passed the filter as excluding expression
            not found in the line
            """
            lines_stripped_as_list.append(line)
    return lines_stripped_as_list


def run_res_lines_stripped_excluding1(fabric_res,expression,linesep=None):
    """ Wrapper around run_res_lines_stripped which retains only
    line for which SEARCH the target fails.
    This variant returns a string always.
    Searching string if match successful
    Empty string is match not successful
    """
    searched_line = ''
    try:
        re_searching = re.compile(re.escape(expression))
    except Exception:
        return searched_line
    for line in run_res_lines_stripped(fabric_res,linesep):
        matched = re_searching.search(line)
        if not matched:
            """ We have passed the filter as excluding expression
            not found in the line
            """
            searched_line = line
    return searched_line


def load3tuple():
    tuple3 = (None,None,None)
    load_line = 0
    with settings(warn_only=True):
        with hide('output','warnings'):
            ros_res = run_or_sudo(CMD_UPTIME)
            load_line = run_res_lines_stripped_searching1(ros_res,'load average:')
    if len(load_line) > 0:
        # chr(44) is comma, chr(58) is colon (:)
        #print load_line
        try:
            string3 = load_line.split(chr(58))[-1]
            # Splitting on comma is fine, just make sure you only keep last 3 after split
            list3 = [ item.strip() for item in string3.split(chr(44)) ][-3:]
            #print list3
            tuple3=tuple(list3)
        except:
            tuple3 = (None,None,None)
    return tuple3


def load1string():
    load1 = ''
    try:
        load1item = load3tuple()[0]
    except:
        load1item = None
    if load1item is not None:
        load1 = str(load1item)
    return load1


def load3string():
    load3 = ''
    try:
        load3tup = load3tuple()
        load1item = load3tup[0]
    except:
        load3tup = None
    if load1item is not None:
        load3 = "{0},{1},{2}".format(*load3tup)
    return load3


def lavg1prefixed(line,separator=' '):
    load1 = load1string()
    if len(load1) > 0:
        line = "{0}{1}{2}".format(load1,separator,line)
    return line


def lavg_prefixed(line,separator=' '):
    load3 = load3string()
    if len(load3) > 0:
        line = "{0}{1}{2}".format(load3,separator,line)
    return line


def unicode_name_index(uname,unicode_input):
    idx = -1
    for idx,uni in enumerate(unicode_input):
        if unicode_name(uni) == uname:
            uname_index = idx
    return idx


def local_popen_wrapper(local_cmd,workdir=None,
                        linesep=None,allow_root=False,buffer_size=None):
    """ Popen for local commands where preservation of exact contents of
    stdout is important. Losing whitespace is not really an issue for most
    automation / sysadmin work, but if you want a faithful byte by byte
    representation of the output from a sed / awk then this function may help.
    Small amounts of effort have been put into the following:
    (.) Ensuring any root invocation really is what the user wants.
    (.) Simple blocks (easily bypassed) for move and remove
    Blocks on mv and rm are really just a convenient warning and the message
    suggests that user may want to use fabric own local() command.
    """
    if getuid() == 0:
        if allow_root == True:
            pass
        else:
            raise Exception("Set allow_root=True for root user invocation")
    if workdir is None:
        pass
    elif set(workdir).isdisjoint(SET_WHITESPACE):
        """ Catch misuse where linesep is given as workdir.
        Here we pass as seems usable """
        pass
    else:
        raise Exception("Whitespace issue workdir={0}".format(workdir))
    if local_cmd.lstrip().startswith('/bin/mv') or local_cmd.startswith('/bin/rm'):
        raise Exception("Move and Remove are blocked. Use fabric own local() facility")
    elif 'passwd' in local_cmd or 'rm -f' in local_cmd or 'rm -fR' in local_cmd:
        raise Exception("Blocked command. Use fabric own local() facility")
    else:
        pass
    if buffer_size is None or buffer_size < 8192:
        buffer_size = 16384
    #print shlex.split(local_cmd)
    ploc = Popen(shlex.split(local_cmd),bufsize=buffer_size,
                 stdout=PIPE,shell=False,cwd=workdir)
    ploc_stdout, ploc_stderr = ploc.communicate()
    ploc_rc = ploc.returncode
    #print ploc_rc,ploc_stdout
    return (ploc_rc,ploc_stdout,ploc_stderr)


def local_popen_split_retaining(local_cmd,workdir=None,
                                linesep=None,allow_root=False,buffer_size=None):
    ploc_rc, ploc_stdout, ploc_stderr = local_popen_wrapper(local_cmd,workdir,linesep,
                                                            allow_root,buffer_size)
    lines = []
    if ploc_rc == 0:
        lines = ploc_stdout.splitlines(True)
    #print ploc_rc
    return (ploc_rc,lines)


def moreish_etc_subdir(subdir,dirs_list,run_always=True):
    """ For some subdirectory of /etc/ act similar to /bin/more for
    non binary files you find in there.
    if you wanted something similar to a more of /etc/resolvconf/*.d/*
    then you might get close by using:
    subdir='resolvconf'
    dirs_list=['resolv.conf.d','update.d']
    run_always flag set True means that sudo will never be used.
    """
    more_res = False
    dirs = []
    for d in dirs_list:
        dirs.append("/etc/{0}/{1}/*".format(subdir,d))
    if len(dirs) < 1:
        return more_res
    ros_line = "{0} {1}".format(CMD_FGREP_H_V_Z,' '.join(dirs))
    with settings(warn_only=True):
        ros_res = run_or_sudo(ros_line,run_always,False)
        if ros_res.succeeded:
            more_res = True
    return more_res


def add_nonlabel_not_already(target_dict,label,line):
    added_count = 0
    if label not in target_dict:
        target_dict[label]=line.split(chr(58))[-1].strip()
        added_count += 1
    return added_count


def cpu_metrics(post_formatted=True,include_missing=True,flag_per_line=False,quiet=False,
                flag_list=['lm','vmx','svm','sse2','sse4_1','sse4_2','avx','xop','fma4']):
    """ Returns output as a list or empyty list if failure """
    with settings(warn_only=True):
        cpuinfo_line = "{0} -i 'mhz|mips|qemu' /proc/cpuinfo".format(CMD_EGREP)
        if flag_per_line:
            cpuinfo_line = "{0} -i 'mhz|mips|qemu|flags' /proc/cpuinfo".format(CMD_EGREP)
        cpuinfo_res = run_or_sudo(cpuinfo_line,False,quiet)
    cpuinfo_lines = run_res_lines_stripped(cpuinfo_res)
    cpuinfo_dict = {}
    if post_formatted:
        """ Design choice made in this section is that hosts with multiple
        cpu allocation should be processed in a way that does not repeat
        the specifics. So have used add_nonlabel_not_already() to help achieve this
        """
        for line_tabbed in cpuinfo_lines[:]:
            line = line_tabbed.replace(chr(9),'')
            if 'bogomips' in line:
                add_nonlabel_not_already(cpuinfo_dict,'bogomips',line)
            elif line.startswith('model') and 'qemu' in line.lower():
                add_nonlabel_not_already(cpuinfo_dict,'qemu',line)
            elif line.startswith('cpu') and 'mhz' in line.lower():
                add_nonlabel_not_already(cpuinfo_dict,'mhz',line)
            elif line.startswith('flags'):
                for flag in line.split(chr(58))[-1].split(chr(32)):
                    if flag in flag_list:
                        label = "flag_{0}".format(flag)
                        add_nonlabel_not_already(cpuinfo_dict,label,'yes')
            else:
                pass
        if include_missing:
            if 'qemu' not in cpuinfo_dict:
                cpuinfo_dict['qemu']='no'
    if len(cpuinfo_dict) > 0:
        cpuinfo_lines = []
        try:
            for label in sorted(cpuinfo_dict.iterkeys()):
                cpuinfo_lines.append("{0}:{1}".format(label,cpuinfo_dict[label]))
        except ValueError,e:
            raise
    return cpuinfo_lines
        

def cpu64():
    flag64 = False
    cpuinfo_lines = cpu_metrics(True,False,True,True)
    for line in cpuinfo_lines:
        if line.startswith('flag_lm'):
            flag64 = True
    return flag64


def pretty_name():
    pretty_tuple = pretty_name_tuple()
    return pretty_tuple[0]


def pretty_name_tuple():
    pretty_name_id = 'unknown'
    pretty_name = 'Unknown!'
    pretty_name_word = 'zero'
    version_major = 0
    with settings(warn_only=True):
        with hide('warnings'):
            pretty_cat = "{0} /etc/system-release".format(CMD_CAT)
            pretty_res = run(pretty_cat)
            if pretty_res.succeeded:
                """ Both types of quotes stripped below """
                pretty_name = pretty_res.strip('"\\\'')

    if pretty_name.endswith(chr(33)):
        """ Exclamation mark (!) is chr(33). We are unchanged from default 'Unknown!' so
        our attempt at catting /etc/system-release probably did not work.
        We are probably not Rpm based.
        """
        pretty_grep = "{0} '^PRETTY_NAME' /etc/os-release".format(CMD_EGREP)
        with settings(warn_only=True):
            with hide('warnings'):
                pretty_res = run(pretty_grep)

        if pretty_res.succeeded:
            pretty_last = pretty_res.split(chr(61))[-1]
            """ Both types of quotes stripped below """
            pretty_name = pretty_last.strip('"\\\'')

            """ /etc/os-release found and examined """
            pname_array = pretty_name.split()
            pretty_name_id = pname_array[0].lower()
            if 'GNU' in pretty_name:
                """ Last word """
                pretty_name_word = pname_array[-1].strip('()')
                pname_array1 = pname_array[:-1]
            else:
                """ 2nd word / middle word """
                pretty_name_word = pname_array[1]
                pname_array1 = pname_array[:]
                pname_array1.pop(1)
                
            for item in pname_array1:
                try:
                    version = float(item)
                    version_major = int(version)
                    break
                except ValueError, TypeError:
                    pass
        else:
            pass

    elif pretty_res.succeeded:
        """ /etc/system-release existed so we are Rpm based probably """
        pname_array = pretty_name.split()
        pretty_name_id = pname_array[0].lower()
        for item in pname_array:
            try:
                version = float(item)
                version_major = int(version)
                break
            except ValueError, TypeError:
                pass
        if version_major > 0:
            pretty_name_word = 'unindexed'
            if version_major >= 0 and version_major < 21:
                pretty_name_word = NUM_AS_WORDS_TO_TWENTY[version_major]
    else:
        pass

    """ Example1: ('Debian GNU/Linux 7 (wheezy)', 'debian', 'wheezy')
    Example2: ('CentOS release 6.5 (Final)', 'centos', 'six')
    Example3: ('Ubuntu precise (12.04.3 LTS)', 'ubuntu', 'precise')
    """
    return (pretty_name, pretty_name_id, pretty_name_word, version_major)


def release_line(prefix='## '):
    """ For system identification that is less rigourous
    (probably for simple printing / reporting)
    Instead use pretty_name_tuple() where you need something
    predictable and split conveniently. """
    #name_grep = "{0} 'NAME=' /etc/os-release".format(CMD_FGREP)
    #with settings(warn_only=True):
    #    name_res = run(name_grep)
    name_line = ''
    osrel_cat = "{0} /etc/os-release".format(CMD_CAT)
    with settings(warn_only=True):
        """ Warning only now set, next hide """
        with hide('output','warnings'):
            name_res = run(osrel_cat)
    name_lines = run_res_lines_stripped_searching(name_res,'NAME=')
    if len(name_lines) > 0:
        for line in name_lines:
            last_portion = line.strip().split('=')[-1]
            if set(last_portion).issubset(SET_PRINTABLE):
                if len(name_line) > 1:
                    name_line += ' ; '
                else:
                    name_line += prefix
                name_line += last_portion.strip()
    else:
        sysrel_cat = "{0} /etc/system-release".format(CMD_CAT)
        with settings(warn_only=True):
            """ Warning only now set, next hide """
            with hide('output','warnings'):
                sys_res = run(sysrel_cat)
            sys_lines = run_res_lines_stripped(sys_res)
            if len(sys_lines) > 0:
                for line in sys_lines:
                    if set(line).issubset(SET_PRINTABLE):
                        if len(name_line) > 1:
                            name_line += ' ; '
                        else:
                            name_line += prefix
                        name_line += line.strip()
            else:
                which_debquery = "{0} /usr/bin/dpkg-query".format(CMD_WHICH)
                with hide('output'):
                    debquery_res = run(which_debquery)
                if debquery_res.failed:
                    name_line = "{0}Unknown with kernel".format(prefix)
                else:
                    name_line = "{0}Debianlike with kernel".format(prefix)
                with hide('output'):
                    kern_res = run(CMD_KERN_ALL)
                    kern_lines = run_res_lines_stripped(kern_res)
                    if len(kern_lines) > 0:
                        for line in kern_lines:
                            if set(line).issubset(SET_PRINTABLE):
                                if name_line.endswith('with kernel'):
                                    pass
                                else:
                                    name_line += ' ;'
                                name_line += " {0}".format(line.strip())
    return name_line.strip()


def deb_or_rpm():
    package_type = None
    pretty_tuple = pretty_name_tuple()
    if pretty_tuple[1] == 'debian' or pretty_tuple[1] in DEBIAN_PLUS_DERIVATIVES2:
        package_type = 'deb'
    elif pretty_tuple[1] == 'centos' or pretty_tuple[1] in RPM_BASED2:
        package_type = 'rpm'
    else:
        package_type = None
    return package_type


def md5_or_sudo(filepath):
    md5line = "{0} {1};".format(CMD_MD5SUM,filepath)
    md5res = run_or_sudo(md5line)
    md5lines = run_res_lines_stripped(md5res)
    mdf = None
    for line in md5lines:
        line_array = line.split()
        if len(line_array) > 1 and line_array[1] == filepath:
            mdf = line_array[0]
    return mdf


def md5text_or_sudo(filepath):
    md5line = "{0} {1};".format(CMD_MD5SUM_TEXT,filepath)
    md5res = run_or_sudo(md5line)
    md5lines = run_res_lines_stripped(md5res)
    mdf = None
    for line in md5lines:
        line_array = line.split()
        if len(line_array) > 1 and line_array[1] == filepath:
            mdf = line_array[0]
    return mdf


def put_using_named_temp_sudo(remote_filepath,lines,newline=chr(10)):
    """ Variant of put_using_named_temp() which:
    (1) returns False on fail rather than using an abort and cleanup
    (2) Always uses sudo
    (3) Has three arguments rather than four arguments
    (4) Hands off to .write() appropriately for a string or iterable tuple/list
    (5) Lives here rather than files_directories so no circular dependency
    """
    """ remote_filepath should be a full path
    Example: '/etc/apache2/ports.conf.append'
    See the fabric put api for clarification.

    This software is intended for remote systems that are GNU/Linux or GNU variants
    so default for newline is chr(10).
    """
    import tempfile
    put_res = None
    put_return = True
    named_temp = tempfile.NamedTemporaryFile()
    try:
        if hasattr(lines, "__iter__") and type(lines) != type(''):
            for line in lines:
                named_temp.write("{0}{1}".format(line,newline))
        else:
            named_temp.write("{0}{1}".format(lines,newline))
        named_temp.flush()
        put_res = put(named_temp,remote_filepath,use_sudo=True)
    except Exception as e:
        raise
        put_return = False
    finally:
        #print named_temp.name
        named_temp.close()

    if put_res:
        if put_res.failed:
            put_return = False
        else:
            put_return = True
            """ Above else: for clarity rather than necessity """
    else:
        put_return = False
    """ This function calls 'put' with use_sudo=True which has pros and cons
    that should be considered in conjunction with the following fabric 1.4.3
    code extract:
        if use_sudo:
            target_path = remote_path
            hasher = hashlib.sha1()
            hasher.update(env.host_string)
            hasher.update(target_path)
            remote_path = hasher.hexdigest()
    """
    return put_return


def put_using_named_temp_cmdbin(remote_filepath,lines,newline=chr(10)):
    """ Variant of put_using_named_temp() which:
    (1) returns False on fail rather than using an abort and cleanup
    (2) Has three arguments rather than four arguments
    (3) Hands off to .write() appropriately for a string or iterable tuple/list
    (4) Lives here rather than files_directories so no circular dependency
    """
    """ remote_filepath should be a full path
    Example: '/etc/apache2/ports.conf.append'
    See the fabric put api for clarification.

    This software is intended for remote systems that are GNU/Linux or GNU variants
    so default for newline is chr(10).
    """
    import tempfile
    put_res = None
    put_return = True
    named_temp = tempfile.NamedTemporaryFile()
    try:
        if hasattr(lines, "__iter__") and type(lines) != type(''):
            for line in lines:
                named_temp.write("{0}{1}".format(line,newline))
        else:
            named_temp.write("{0}{1}".format(lines,newline))
        named_temp.flush()
        put_res = put(named_temp,remote_filepath,use_sudo=False)
    except Exception as e:
        raise
        put_return = False
    finally:
        #print named_temp.name
        named_temp.close()

    if put_res:
        if put_res.failed:
            put_return = False
        else:
            put_return = True
            """ Above else: for clarity rather than necessity """
    else:
        put_return = False
    return put_return


def untarred_name(string_tarred):
    string_untarred = ''
    string_list = string_tarred.split(chr(46))
    string_list_len = len(string_list)
    if string_list[-1] not in ['gz','tgz']:
        return string_untarred
    # Drop the last element next
    string_list.pop()
    if 'tar' == string_list[-1]:
        string_list.pop()
    if len(string_list) > 0:
        string_untarred = chr(46).join(string_list)
    return string_untarred


def untar(file_tarred):
    """ Extract files from a tar/tar.gz or similar.
    This could alternatively have been placed in files_directories,
    however having it in the main command file makes for less interdependencies.
    Return is a boolean.
    """
    untarred = False
    if file_tarred is None:
        return untarred
    elif file_tarred.endswith('gz'):
        extract_line = "{0} {1}".format(CMD_TARZEXTRACT,file_tarred)
    else:
        extract_line = "{0} {1}".format(CMD_TAREXTRACT,file_tarred)
    ros_res = run_or_sudo(extract_line)
    if ros_res.succeeded:
        untarred = True
    #failed_report = run_res_lines_stripped_searching1(ros_res,'xiting with failure status')
    return untarred


def untar_verbose(file_tarred):
    """ Extract files from a tar/tar.gz or similar.
    This could alternatively have been placed in files_directories,
    however having it in the main command file makes for less interdependencies.
    Return is list of verbose lines.
    """
    lines = []
    if file_tarred is None:
        return lines
    elif file_tarred.endswith('gz'):
        extract_line = "{0} {1}".format(CMD_TARZEXTRACTVERB,file_tarred)
    else:
        extract_line = "{0} {1}".format(CMD_TAREXTRACTVERB,file_tarred)
    ros_res = run_or_sudo(extract_line)
    if ros_res.succeeded:
        lines = run_res_lines_stripped_searching1(ros_res,chr(47))
    #failed_report = run_res_lines_stripped_searching1(ros_res,'xiting with failure status')
    return lines


def untar_returning_directory(file_tarred):
    untarred_dir = None
    untarred = untar(file_tarred)
    if untarred:
        untarred_dir = untarred_name(file_tarred)
    return untarred_dir


def untar_owner_group(file_tarred,owner=None,group=None):
    """ untar specifying owner and group if appropriate.
    Returns a boolean.
    """
    untarred = False

    if file_tarred is None:
        return untarred
    elif file_tarred.endswith('gz'):
        file_tarred_prefixed = "-zxf {0}".format(file_tarred)
    else:
        file_tarred_prefixed = "-xf {0}".format(file_tarred)

    if owner is None and group is None:
        if '.debian.' in file_tarred:
            extract_line = "{0} -C {1} {2}".format(CMD_TAR_STRIPPED1,
                                                   DIR_UNBUILT_DEBIAN_RELATIVE,
                                                   file_tarred_prefixed)
        else:
            extract_line = "{0} -C {1} {2}".format(CMD_TAR_STRIPPED1,
                                                   DIR_UNBUILT_RELATIVE,
                                                   file_tarred_prefixed)
    elif owner is None:
        if '.debian.' in file_tarred:
            extract_line = "{0} --group={1} -C {2} {3}".format(CMD_TAR_STRIPPED1,
                                                               group,
                                                               DIR_UNBUILT_DEBIAN_RELATIVE,
                                                               file_tarred_prefixed)
        else:
            extract_line = "{0} --group={1} -C {2} {3}".format(CMD_TAR_STRIPPED1,
                                                               group,
                                                               DIR_UNBUILT_RELATIVE,
                                                               file_tarred_prefixed)
    elif group is None:
        if '.debian.' in file_tarred:
            extract_line = "{0} --owner={1} -C {2} {3}".format(CMD_TAR_STRIPPED1,
                                                               owner,
                                                               DIR_UNBUILT_DEBIAN_RELATIVE,
                                                               file_tarred_prefixed)
        else:
            extract_line = "{0} --owner={1} -C {2} {3}".format(CMD_TAR_STRIPPED1,
                                                               owner,
                                                               DIR_UNBUILT_RELATIVE,
                                                               file_tarred_prefixed)
    else:
        # Both --owner and --group flags will be used
        extract_line = "{0} --owner={1} --group={2} ".format(CMD_TAR_STRIPPED1,
                                                             owner,group)
        if '.debian.' in file_tarred:
            extract_line = "{0} -C {1} {2}".format(extract_line,
                                                   DIR_UNBUILT_DEBIAN_RELATIVE,
                                                   file_tarred_prefixed)
        else:
            extract_line = "{0} -C {1} {2}".format(extract_line,
                                                   DIR_UNBUILT_RELATIVE,
                                                   file_tarred_prefixed)

    ros_res = run_or_sudo(extract_line)
    if ros_res.succeeded:
        untarred = True
    #failed_report = run_res_lines_stripped_searching1(ros_res,'xiting with failure status')
    return untarred


def untar_to_unbuilt(file_tarred,owner=None,group=None):
    """ Untar to unbuilt directory.
    Return is a boolean
    """
    untarred = False
    extract_line = None
    if file_tarred is None:
        return untarred
    mkdir_unbuilt = "{0} {1}".format(CMD_MKDIRVP755,DIR_UNBUILT_DEBIAN_RELATIVE)
    mkdir_res = run(mkdir_unbuilt)
    if mkdir_res.failed:
        return untarred
    untarred = untar_owner_group(file_tarred,owner,group)
    return untarred


def sql_pipe_delimited_to_list_of_lists(column_count,key1indexed,output_lines,lines_to_skip=1):
    """ Create a dict of lists from the results of some sql select output that
    is pipe delimited.

    output_lines probably wants to be fed result of run_res_lines_stripped(fabric_res)

    key1indexed is the column number (one indexed) that should be used for the key 
    so we will deduct 1 from the supplied number when looking up in line_array.
    """

    lol = []

    if key1indexed < 1:
        return lol

    key0indexed = key1indexed - 1

    columns_uniform = -1

    loop_list = []
    for idx,line in enumerate(output_lines):
        if idx < lines_to_skip:
            continue
        columns_uniform = 1
        #print(line)
        line_array = line.split(chr(124))
        #print(line_array)
        if len(line_array) == column_count:
            if XONEBEE in line_array[-1]:
                print("Line {0} does have required column count of {1} but special char!".format(
                        idx,column_count,XONEBEE))
                columns_uniform = 0
                break
            elif XZERO8 in line_array[-1]:
                print("Line {0} does have required column count of {1} but special char!".format(
                        idx,column_count,XZERO8))
                columns_uniform = 0
                break
            else:
                pass
            loop_list.append(line_array)
        elif len(line_array) == 0:
            pass
        elif XONEBEE in line:
            print("Line {0} does not have required column count of {1} but contains".format(
                    idx,column_count,XONEBEE))
        else:
            print(line,line_array)
            print("Above Line {0} does not have required column count of {1}".format(idx,column_count))
            columns_uniform = 0
            break

    if columns_uniform > 0:
        lol = loop_list

    return lol


def sql_pipe_delimited_to_dict_of_lists(column_count,key1indexed,output_lines,lines_to_skip=0):
    """ Create a dict of lists from the results of some sql select output that
    is pipe delimited.

    output_lines probably wants to be fed result of run_res_lines_stripped(fabric_res)

    key1indexed is the column number (one indexed) that should be used for the key
    so we will deduct 1 from the supplied number when looking up in line_array.
    """

    dol = OrderedDict()

    if key1indexed < 1:
        return dol

    key0indexed = key1indexed - 1

    lol = sql_pipe_delimited_to_list_of_lists(column_count,key1indexed,output_lines,lines_to_skip)
    if len(lol) > 1:
        for line_array in lol:
            dol[line_array[key0indexed]] = line_array

    return dol


def sql_pipe_delimited_to_remote_conf(column_count,key1indexed,val1indexed,output_lines,lines_to_skip=0):
    """ Create a dict of lists from the results of some sql select output that
    is pipe delimited.

    output_lines probably wants to be fed result of run_res_lines_stripped(fabric_res)

    key1indexed is the column number (one indexed) that should be used for the key 
    so we will deduct 1 from the supplied number when looking up in line_array.

    Warning: This function will read than overwrite the filepath given as sqlconf
    """

    dov = OrderedDict()

    if key1indexed < 1:
        return dov

    key0indexed = key1indexed - 1
    val0indexed = val1indexed - 1

    lol = sql_pipe_delimited_to_list_of_lists(column_count,key1indexed,output_lines,lines_to_skip)
    if len(lol) > 1:
        for line_array in lol:
            dov[line_array[key0indexed]] = line_array[val0indexed]

    sqlconf = '~/postgresql.conf'
    run("printf 'log_parser_stats = off\n' > {0}".format(sqlconf))

    from . import conf_get_set as cmdbin_getset

    with cmdbin_getset.Keyedequals(sqlconf) as dbconf:

        for confk,value in dov.items():
            dbconf[confk] = value
        #dbconf.summary_print()

        """
        dbconf_lines = []
        for k,v in dbconf.items():
            dbconf_lines.append("{0} = {1}".format(k,v))
        """
        dbconf_lines = dbconf.lines_for_put_delimited()

        if len(dbconf_lines) > 0:
            put_using_named_temp_cmdbin(sqlconf,dbconf_lines,newline=chr(10))
        else:
            print("sqlconf was not generated on the remote as lines_for_put_delimited() unexpected.")

    return dov


