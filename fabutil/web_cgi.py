#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.8.5
# https://pypi.python.org/pypi/Fabric/1.8.5

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
import re
from os import path
from os import linesep as os_linesep
import time
#from collections import namedtuple,OrderedDict
from . import cmdbin
from . import files_directories as files_dirs
from . import package_deb as pdeb
from . import conf_get_set as getset

GROUP_WRITE_EXT_LIST_WEB = ['bz2','gz','log','lz','tar','zip']
GROUP_WRITE_EXT_LIST_PYWEB = GROUP_WRITE_EXT_LIST_WEB[:]
GROUP_WRITE_EXT_LIST_PYWEB.extend(['db3','sqlite','sqlite3'])
# DIRSTEM_DATA_SHORT = '~/'
DIRSTEM_DATA_SHORT = '/var/local'
GROUP_WEBSERVER = 'www-data'
APACHE_MODS_AVAIL = '/etc/apache2/mods-available'
APACHE_MODS_ENABLED = '/etc/apache2/mods-enabled'
APACHE_SITES_AVAIL = '/etc/apache2/sites-available'
APACHE_SITES_ENABLED = '/etc/apache2/sites-enabled'

RE_PARENTHESIZED_WORD = re.compile(r'\(\w+\)')
RE_SYNTAX = re.compile(r'Syntax\s+')
RE_MISSPELLED = re.compile('perhaps misspelled')
RE_ACTION_FAILED = re.compile('ction.*failed')
RE_MORE_INFORMATION = re.compile('may have more information')

""" Above are prefixed RE_ as regular expressions.
Below are NOT prefixed RE_ as bare strings that can be
later used in regular expressions.
"""
EXECCGI_NONCOMMENT = '^(ExecCGI|[^#]*ExecCGI)'
EXECCGI_NONCOMMENT_NONMINUS = '^(ExecCGI|[^#]*[^-]ExecCGI)'
APACHE2_ENABLED_GLOB = '/etc/apache2/sites-enabled/*'
CMD_USR_LIB_CGI_ENABLED_COUNT = "egrep 'ScriptAlias[ ]*/cgi-bin/[ ]*/usr/lib/cgi-bin/' {0} | /usr/bin/wc -l".format(
	APACHE2_ENABLED_GLOB)


def mods_avail(search='available',fileext='load',filestem='*gi*',excluding=None):
    """ Report on mods-available (and mods_enabled) USING FIND
    Supplying excluding='proxy_' would apply a post filter to remove proxy_ lines
    """
    mod_lines = []

    filesearch = "{0}.{1}".format(filestem,fileext)
    if search == 'available':
        CMD_FIND = "/usr/bin/find {0} -name '{1}'".format(APACHE_MODS_AVAIL,filesearch)
    elif search == 'enabled':
        CMD_FIND = "/usr/bin/find {0} -name '{1}'".format(APACHE_MODS_ENABLED,filesearch)
    else:
        CMD_FIND = "/usr/bin/find {0} {1} -name '{2}'".format(APACHE_MODS_ENABLED,
                                                              APACHE_MODS_AVAIL,
                                                              filesearch)
    if excluding is None:
        with settings(warn_only=True):
            mres = cmdbin.run_or_sudo_warningless(CMD_FIND)
            if mres.succeeded:
                mod_lines = cmdbin.run_res_lines_stripped(mres)
    else:
        with settings(warn_only=True):
            mres = cmdbin.run_or_sudo_warningless(CMD_FIND)
            if mres.succeeded:
                mod_lines = cmdbin.run_res_lines_stripped_excluding(mres,excluding)
    return mod_lines


def extensions_enabled(exttype=None,verbose=False):
    """ Report on mods_enabled via feedback from the binary
    exttype is the extension type and if 'shared' or 'static' is
    supplied will be used as a post filter on the output of the command.

    Supplying exttype=None says that we do not want to restrict
    the extensions list to just either shared or static.
    """
    filtered_lines = []
    ptype = cmdbin.deb_or_rpm()
    if ptype == 'deb':
        CMD_MODS = cmdbin.CMD_APACHE_MODS_ENABLED
    elif ptype == 'rpm':
        CMD_MODS = cmdbin.CMD_HTTPD_MODS_ENABLED
    else:
        CMD_MODS = None
    if CMD_MODS and exttype is None:
        """ All extension types so no post filtering """
        mres = cmdbin.run_or_sudo_warningless(CMD_MODS)
        if mres.succeeded:
            enabled_lines = cmdbin.run_res_lines_stripped(mres)
    elif CMD_MODS:
        """ Some post filtering based on exttype """
        mres = cmdbin.run_or_sudo_warningless(CMD_MODS)
        if mres.succeeded:
            searcher = "({0})".format(exttype)
            enabled_lines = cmdbin.run_res_lines_stripped_searching(mres,searcher)
    else:
        pass
    if verbose is True:
        for line in enabled_lines:
            if RE_SYNTAX.match(line):
                filtered_lines.append(line)
            elif RE_MISSPELLED.search(line):
                filtered_lines.append(line)
            elif RE_ACTION_FAILED.search(line):
                filtered_lines.append(line)
            elif RE_MORE_INFORMATION.search(line):
                filtered_lines.append(line)
            elif RE_PARENTHESIZED_WORD.search(line):
                # As for non verbose processing we keep if () detected
                filtered_lines.append(line)
            else:
                pass
    else:
        for ext in enabled_lines:
            if RE_PARENTHESIZED_WORD.search(ext):
                filtered_lines.append(ext)
    return filtered_lines


def execcgi(minus=False,context=False):
    """ Grep files in sites-enabled for ExecCGI """
    grep_flag = 'l'
    if context is True:
        # Use -H instead of -l when grepping
        grep_flag = 'H'
    find_regex = EXECCGI_NONCOMMENT_NONMINUS
    if minus is True:
        find_regex = EXECCGI_NONCOMMENT
    find_tail = " ! -type d -exec egrep -{0} '{1}' ".format(grep_flag,find_regex)
    find_tail9 = '{} \;'
    CMD_FIND = "/usr/bin/find {0} {1} {2}".format(APACHE_SITES_ENABLED,
                                                  find_tail,
                                                  find_tail9)
    #find /etc/apache2/sites-enabled/ ! -type d -exec egrep -l '^(ExecCGI|[^#]*[^-]ExecCGI)' {} \;
    with settings(warn_only=True):
        fres = cmdbin.run_or_sudo_warningless(CMD_FIND)
        if fres.succeeded:
            find_lines = cmdbin.run_res_lines_stripped(fres)
    return find_lines
    

def sites_avail(search='enabled',excluding=None):
    """ Report on mods-available (and mods_enabled) USING FIND
    Supplying excluding='proxy_' would apply a post filter to remove proxy_ lines
    """
    mod_lines = []

    if search == 'available':
        CMD_FIND = "/usr/bin/find {0} ".format(APACHE_SITES_AVAIL)
    elif search == 'enabled':
        CMD_FIND = "/usr/bin/find {0} ".format(APACHE_SITES_ENABLED)
    else:
        CMD_FIND = "/usr/bin/find {0} {1} ".format(APACHE_SITES_ENABLED,
                                                   APACHE_SITES_AVAIL)
    if excluding is None:
        with settings(warn_only=True):
            mres = cmdbin.run_or_sudo_warningless(CMD_FIND)
            if mres.succeeded:
                mod_lines = cmdbin.run_res_lines_stripped(mres)
    else:
        with settings(warn_only=True):
            mres = cmdbin.run_or_sudo_warningless(CMD_FIND)
            if mres.succeeded:
                mod_lines = cmdbin.run_res_lines_stripped_excluding(mres,excluding)
    return mod_lines


def usr_lib_cgibin_count(error_action='raise'):
	"""
	If unable to get a count then return 0 or raise an error (default)
	depending on value of supplied parameter error_action.
	"""

	error_prefix = 'usr_lib_cgibin_count Error:'
	cgibin_count = -2
	with settings(warn_only=True):
		""" Warning only now set, next hide """
		with hide('stdout','warnings'):
			count_res = cmdbin.run_or_sudo(CMD_USR_LIB_CGI_ENABLED_COUNT)
			if count_res.succeeded:
				res_lines = cmdbin.run_res_lines_stripped(count_res)
				if len(res_lines) == 1:
					try:
						cgibin_count = int(res_lines[0].strip())
					except:
						cgibin_count = -1


	if cgibin_count < 0:
		if error_action.lower().startswith('raise'):
			if -2 == cgibin_count:
				exception_feedback = "{0} Unable to determine count as Grepping failed.".format(
					error_prefix)
			elif -1 == cgibin_count:
				exception_feedback = "{0} Unable to determine count when converting number.".format(
					error_prefix)
			elif cgibin_count < 0:
				exception_feedback = "{0} Unable to determine count. ( {1} )".format(
					error_prefix,cgibin_count)
			raise Exception(exception_feedback)
		else:
			cgibin_count = 0

	return cgibin_count

