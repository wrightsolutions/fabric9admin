#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2018,2015 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

""" Functions for querying cgroup binaries and settings """

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
import re
try:
    from . import cmdbin
except ValueError:
    import cmdbin
from . import conf_get_set as getset

CMD_SESTATUS="/usr/sbin/sestatus "
CMD_GETENFORCE="/usr/sbin/getenforce "


def sestatus_as_list(verbose=False):
    """ Run sestatus and return output.
    If sestatus not present then for Debian or Debianlike:
      apt-get --no-install-recommends install selinux-basics
      or apt-get install policycoreutils
    """
    sestatus_lines = []
    """ sestatus_lines begins as empty list, but if
    all looks good then later we will do sestatus_lines=status_lines
    """
    with settings(warn_only=True):
        with hide('warnings'):
            if verbose:
                ros_res = cmdbin.run_or_sudo(CMD_SESTATUS)
            else:
                with hide('output'):
                    ros_res = cmdbin.run_or_sudo(CMD_SESTATUS)
        if ros_res.succeeded:
            status_lines = cmdbin.run_res_lines_stripped(ros_res)
            for line in status_lines:
                if 'SELinux' in line:
                    sestatus_lines = status_lines
                    break
            if verbose:
                for line in status_lines:
                    print line
    return sestatus_lines


def enforced1(verbose=False):
    """ Run sestatus and return single indicator of 'SELinux status' line
    as a single element list """
    seenforce1line = ['']
    """ seenforce_lines begins as empty list, but if
    all looks good then later we will do seenforce_lines=enforce_lines
    """
    with settings(warn_only=True):
        with hide('warnings'):
            with hide('output'):
                ros_res = cmdbin.run_or_sudo(CMD_GETENFORCE)
        if ros_res.succeeded:
            enforce_lines = cmdbin.run_res_lines_stripped(ros_res)
            #print enforce_lines
            for line in enforce_lines:
                if line in ['Disabled','Enforcing','Permissive']:
                    seenforce1line = [line]
                    break
                else:
                    #print 'not found SELinux sta'
                    pass
            if verbose and len(seenforce1line) > 0:
                print seenforce1line[0]
    return seenforce1line


def enforced1print():
    """ Wrapper around sestatus1() forcing argument verbose=True """
    return enforced1(True)


def sestatus1(verbose=False):
    """ Run sestatus and return single indicator of 'SELinux status' line
    as a single element list """
    sestatus1line = ['']
    """ sestatus_lines begins as empty list, but if
    all looks good then later we will do sestatus_lines=status_lines
    """
    with settings(warn_only=True):
        with hide('warnings'):
            with hide('output'):
                ros_res = cmdbin.run_or_sudo(CMD_SESTATUS)
        if ros_res.succeeded:
            status_lines = cmdbin.run_res_lines_stripped(ros_res)
            #print status_lines
            for line in status_lines:
                if 'SELinux sta' in line:
                    sestatus1line = [line]
                    break
                else:
                    #print 'not found SELinux sta'
                    pass
            if verbose and len(sestatus1line) > 0:
                print sestatus1line[0]
    return sestatus1line


def sestatus1print():
    """ Wrapper around sestatus1() forcing argument verbose=True """
    return sestatus1(True)


def sestatus_present():
    """ Boolean result of whether could find sestatus binary """
    if len(sestatus1()) > 0:
        return True
    return False


def seconfig():
    if exists(cmdbin.ETC_SELINUX_CONFIG):
        return cmdbin.ETC_SELINUX_CONFIG
    elif exists(cmdbin.ETC_SYSCONFIG_SELINUX):
        return cmdbin.ETC_SYSCONFIG_SELINUX
    else:
        pass
    return None


def seconfig_selinux(seconfig,print_flag=False):
    """ Given a config file path, grep for SELINUX in that config """
    if not exists(seconfig):
        return False
    selinux_equals = None
    with settings(warn_only=True):
        grep_line = "{0} 'SELINUX=' {1}".format(cmdbin.CMD_FGREP,seconfig)
        with hide('warnings'):
            grep_res = cmdbin.run_or_sudo(grep_line)
            selinux_equals = cmdbin.run_res_lines_stripped(grep_res)
    if selinux_equals:
        if print_flag:
            print selinux_equals
        return True
    return False


def seconfig_keyed(verbose=False):
    conf_filepath = seconfig()
    if conf_filepath is None:
        return None
    keyed = None
    with getset.Keyedequals(conf_filepath) as conf:
        keyed = { k:v for k,v in conf.items() }
        """ Above has created a new dictionary based on key vals of conf
        Next do some printing if we have been set verbose=True """
        if verbose:
            #print conf.lines_enabled_contains('SELINUXTYPE')
            enabled = conf.keylines_enabled()
            for eline in enabled:
                print eline
    return keyed



