# fabutil test suite - files

```
#!shell
$ cd fabutil;python -m unittest discover
```
Input files and other files useful for supporting
testing / analysis.

## input files / example configuration files

The files in this directory borrow ideas from
actual configuration files, but are reworked / altered
to create particular test conditions.

The files themselves here are usually physical representations
of what is encoded in strings in the actual tests,
however they might be beneficial if you want to manually
simulate some results and need something to feed to sed.


## Files ending .sh and .dash

Mostly example results from sed_checksum() function.


