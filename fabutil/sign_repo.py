#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
#import re
try:
    from . import cmdbin
except ValueError:
    import cmdbin
from . import files_directories as files_dirs
from . import package_deb as pdeb

CMD_MERCURIAL_CLONE = "/usr/bin/hg clone "
CMD_GIT_CLONE = "/usr/bin/git clone "


def gpg_recv_and_verify(signed_file):
    gpgres = run(cmdbin.CMD_GPG_LIST + "CE0222D0EB8CFF0E")
    if gpgres.failed:
       gpgres = run(cmdbin.CMD_GPG_RECV + "CE0222D0EB8CFF0E")
       if not gpgres.succeeded:
       	  abort("Aborting as preparation for gpg verification failed.")
    gpgres = run(cmdbin.CMD_GPG_VERIFY + signed_file + ".sig")	 
    #gpgres = contains(tar,'02 Feb 2012 18:48:46 GMT using DSA key ID EB8C')	 
    return gpgres


def repo_clone(repo_url,revision=None):
    clone_success = True
    requires_res = pdeb.requires_binary_package('hg','mercurial',assumeyes=True)
    if revision:
        cloneres = run("{0} -r {1} {2}".format(CMD_MERCURIAL_CLONE,
                                               revision,repo_url))
    else:
        cloneres = run(CMD_MERCURIAL_CLONE)

    if cloneres.failed and not confirm("Repo clone failed. Continue anyway?"):
        clone_success = False
        files_dirs.abort_with_cleanup(dirpath_fab,"Aborting at user request.")

    return clone_success


def git_clone(repo_url,revision=None):
    clone_success = True
    requires_res = pdeb.requires_binary_package('git','git',assumeyes=True)
    if revision:
        #cloneres = run("{0} -r {1} {2}".format(CMD_GIT_CLONE,revision,repo_url))
        files_dirs.abort_with_cleanup(dirpath_fab,"Aborting as git revision unsupported.")
    else:
        cloneres = run("{0} {1}".format(CMD_GIT_CLONE,repo_url))

    if cloneres.failed and not confirm("Git clone failed. Continue anyway?"):
        clone_success = False
        files_dirs.abort_with_cleanup(dirpath_fab,"Aborting at user request.")

    return clone_success





