#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

CMD_UID = "/usr/bin/id -u "
CMD_USERADD = "/usr/sbin/useradd "
CMD_USERADD_DEFAULTS = "/usr/sbin/useradd --defaults "


def user_uid(user='syslog'):
    return_uid = -1
    uid_line = None
    if re_first_alpha.match(user):
        uid_line = "{0} {1}".format(CMD_UID,user)
    """ Easiest thing in the world to give a uid rather
    than user name to this function, so some attempt
    made to trap that. """
    if uid_line:
        with settings(warn_only=True):
            uid_res = run(uid_line)
            if uid_res.succeeded:
                return_uid = int(uid_res)
    return return_uid


def idnum_between(idnum=101,lower=None,upper=65535):
    int_lower = 0
    try:
        int_lower = int(lower)
    except:
        return False
    
    if not idnum or not lower or int_lower < 0:
        return False
    #print "Test {0} <= {1} <= {2}".format(lower,idnum,upper)
    if int_lower <= int(idnum) <= int(upper):
        return True
    #print "Failed test {0} <= {1} <= {2}".format(lower,idnum,upper)
    return False


def user_uid_between(user='syslog',lower=None,upper=65535):
    if not user or not lower or lower < 0:
        return False
    uid = user_uid(user)
    if lower <= uid <= upper:
        return True
    return False
