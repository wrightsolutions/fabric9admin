#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2019,2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# 2019 Tested against fabric 1.13
# 2015 Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz


from __future__ import with_statement
from fabric.api import abort, cd, env, get, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
from os import linesep as os_linesep
import time
#from collections import namedtuple,OrderedDict
from . import cmdbin
from . import files_directories as files_dirs
from . import package_deb as pdeb
from . import sign_repo
from . import sysctl_plus
from . import conf_get_set as getset

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

CMD_DJADMIN = "/usr/bin/django-admin "
# "/usr/bin/django-admin collectstatic --noinput --settings=settings --pythonpath="
QUALIFIED2 = "--settings=settings --pythonpath="
CMD_DJADMIN_COLLECTSTATIC_QUALIFIED = "/usr/bin/django-admin collectstatic --noinput {0}".format(QUALIFIED2)
CMD_DJADMIN_DIFFSETTINGS_QUALIFIED = "/usr/bin/django-admin diffsettings {0}".format(QUALIFIED2)
CMD_DJADMIN_SYNCDB_QUALIFIED = "/usr/bin/django-admin syncdb --noinput {0}".format(QUALIFIED2)

PORT_WSGI = '1780'
""" Above stem is 17 to represent Django 17 but change to 1980 when Django 19
Originally PORT_WSGI was 8080 but we are making an effort to avoid clashing with
an existing typical web server port assignment so benchmarking less likely to
interfere with exising setup
"""
GROUP_WRITE_EXT_LIST_WEB = ['bz2','gz','log','lz','tar','zip']
GROUP_WRITE_EXT_LIST_PYWEB = GROUP_WRITE_EXT_LIST_WEB[:]
GROUP_WRITE_EXT_LIST_PYWEB.extend(['db3','sqlite','sqlite3'])
# DIRSTEM_DATA_SHORT = '~/'
DIRSTEM_DATA_SHORT = '/var/local'
GROUP_WEBSERVER = 'www-data'

def group_writable_web(dirpath,group_write_ext_list=None):
    if group_write_ext_list is None:
        group_write_ext_list = GROUP_WRITE_EXT_LIST_WEB
    return files_dirs.group_writable(dirpath,'/var/www/pub','/var/l',group_write_ext_list)

def group_writable_web_recursive(dirpath,ext_list=None):
    if ext_list is None:
        ext_list = GROUP_WRITE_EXT_LIST_WEB
    return files_dirs.group_writable_recursive(dirpath,path_startswith1='/var/www/pub',
                          path_startswith2=None,group_write_ext_list=ext_list)

def group_writable_pyweb(dirpath,group_write_ext_list=None):
    if group_write_ext_list is None:
        group_write_ext_list = GROUP_WRITE_EXT_LIST_PYWEB
    return files_dirs.group_writable(dirpath,'/var/www/pub','/var/l',group_write_ext_list)

def group_writable_pyweb_recursive(dirpath,ext_list=None):
    if ext_list is None:
        ext_list = GROUP_WRITE_EXT_LIST_PYWEB
    return files_dirs.group_writable_recursive(dirpath,path_startswith1='/var/www/pub',
                          path_startswith2=None,group_write_ext_list=ext_list)

def chgrp_or_sudo_web(dirpath,newgroup=None):
    if newgroup and len(newgroup) > 1:
        grp = newgroup
    else:
        grp = GROUP_WEBSERVER
    return files_dirs.chgrp_or_sudo(dirpath,grp)


def apache2threads(apache2user='www-data',print_flag=False):
    threads = cmdbin.process_user(apache2user)
    if print_flag is not False:
        for tline in threads:
            print tline
    return threads


def apache2mpm_lines(print_flag=False):
    mpm_lines = []
    with settings(warn_only=True):
        mres = cmdbin.run_or_sudo_warningless(cmdbin.CMD_APACHE_MPM_GREPPED)
        mpm_lines = cmdbin.run_res_lines_stripped(mres)
    if print_flag is not False:
        for mline in mpm_lines:
            print mline
    return mpm_lines


def apache2mpm():
    mpm = None
    mpm_line = ''
    with settings(warn_only=True):
        mres = cmdbin.run_or_sudo_warningless(cmdbin.CMD_APACHE_V)
        if mres.succeeded:
            mpm = 'installed'
            mpm_line = cmdbin.run_res_lines_stripped_searching1(mres,'MPM:')
    if len(mpm_line) > 7:
        # chr(58) is colon (:)
        try:
            line_lower_array = mpm_line.lower().split(chr(58))
            mpm = line_lower_array[-1].strip()
        except:
            mpm = 'unknown'
    return mpm


def settings_debug(project_path=None,debug=False,verbosity=1,md5current=None,settings='settings_final.py'):
    """ Set the value DEBUG=False or DEBUG=True in settings_final.py or other local setting file.

    chr(47) is forward slash (/)

    verbosity: 0 - no printing, caller should consult return value to determine success
    1 - minimal messages including some feedback when DEBUG value already same as requested.
    2 - minimal+   ; 3 - verbose (debug)
    """
    locpy = 'settings_final.py'
    md5false = None
    md5true = None
    if project_path is None:
        conf_filepath = "/var/www/public_html/poor_cycle.fab/local2django17bench/{0}".format(locpy)
        if md5current is None:
            md5false = '1a16755167a1f84a66e37e8b6a3ffee9'
            md5true = '3950d58f378b6de010689061fd9b3ed2'
    elif project_path.endswith('.py'):
        conf_filepath = project_path
        if project_path == '/root/django14skeleton.fab/proj14/proj14/local_settings.py':
            pass
        elif project_path == '/var/www/public_html/poor_cycle.fab/local2django17bench/local_settings.py':
            md5false = '1a16755167a1f84a66e37e8b6a3ffee9'
            md5true = '3950d58f378b6de010689061fd9b3ed2'
    elif project_path.startswith(chr(47)):
        conf_filepath = "{0}{1}{2}".format(project_path,chr(47),locpy)
    else:
        conf_filepath = None
    md5before = 'default!default!default!default!'
    md5before2 = 'default!default!default!default!'
    toggled = False
    with getset.Keyedequals(conf_filepath) as conf:
        if verbosity > 2:
            #print conf.lines_enabled_contains('DEBUG')
            print conf.lines_noncomment_containing_any(['DEBUG =','DEBUG='])
            #print conf.lines_current_simple()
        if 'DEBUG' not in conf:
            if verbosity > 0:
                print "Django DEBUG keyword not found in {0}".format(conf_filepath)
        else:
            # Found 'DEBUG' in conf:
            """
            if debug is False or debug is True:
                debug_string = str(debug)
                if verbosity > 2:
                    print "Automatically str() of {0} completed".format(debug,debug_string)
            else:
                debug_string = debug
            """
            toggle_res = conf.toggle('DEBUG',debug,True,0)
            if toggle_res:
                if verbosity > 2:
                    print conf.sedlines_containing('DEBUG')
                    print conf.lines_for_put_containing('DEBUG')
                sedlines = conf.sedlines(True)
                if md5current is None:
                    if md5false is not None and (debug is True or debug in ['true','True']):
                        md5before = md5false
                        md5before2 = '16d7cd36a3a4b9eda98a0bb4d697268e'
                        # Will be True once sed has been run
                    elif md5true is not None and (debug is False or debug in ['false','False']):
                        md5before = md5true
                        # Will be False once sed has been run
                    else:
                        md5before = 'a'*32
                else:
                    md5before = md5current
                #sedres = getset.sed_remote_apply('a'*32,conf_filepath,sedlines,'medium')
                #sedres = getset.sed_remote_apply(None,conf_filepath,sedlines,'medium',True,0.75,True)
                """ local settings is sometimes all comments so disable ratio of changes checking by
                supplying 1.0 instead of 0.75
                #sedres = getset.sed_remote_apply(md5before,conf_filepath,sedlines,'medium',True,0.75,True)
                #sedres = getset.sed_remote_apply(md5before,conf_filepath,sedlines,'medium',True,0.5,True)
                #sedres = getset.sed_remote_apply(md5before,conf_filepath,sedlines,'medium',True,1.0,True)
                """
                sedres = getset.sed_remote_apply(md5before,conf_filepath,sedlines,'medium',True,0.75,True)
                if sedres is True:
                    toggled = True
                else:
                    if md5before == md5false and not md5before2.startswith('default'):
                        """ Try a second attempt where we are attempting to change to True
                        and file is known local_settings.py """
                        sedres = getset.sed_remote_apply(md5before2,conf_filepath,
                                                         sedlines,'medium',True,0.75,True)
                if sedres is not True and verbosity > 1:
                    print "sed_remote_apply for local settings returned {0}".format(sedres)
                if verbosity > 2:
                    print conf.lines_for_put_containing('DEBUG')
                    print conf.lines_current_containing('DEBUG')
                # Make the change on the remote system
            else:
                if verbosity > 0:
                    print "No change or fail. Is DEBUG={0} already set in local settings {1}?".format(
                        debug,conf_filepath)
                    print "No change or fail. toggle_safe returned {0}".format(toggle_res)
                    """ print conf.toggle_new('DEBUG') """
                else:
                    pass
    return toggled


def diffsettings_or_sudo(manage_dir,admin=True,linesep=None):
    """ First argument manage_dir you supply is the directory where the manage.py lives 
    ( alternatively you can think of this as dirproject - Directory of the project )
    Second argument is way of indicating whether django-admin or manage.py should be used.

    Function returns a list of lines which is the output produced by diffsettings (stripped)
    
    If function returns an empty list then something went wrong.
    """
    settings_diffed = []
    if manage_dir is None or files_dirs.slash_count(manage_dir) < 1:
        return settings_diffed
    elif admin is True:
        requires_django_res = pdeb.requires_binary_package('django-admin',
                                                           'python-django',assumeyes=True)
        admin_line = "{0}{1}".format(CMD_DJADMIN_DIFFSETTINGS_QUALIFIED,manage_dir)
        settings_diffed = cmdbin.run_or_sudo_stripped(admin_line,False,True,linesep)
    else:
        with cd(manage_dir):
            manage_line = "./manage.py diffsettings"
            settings_diffed = cmdbin.run_or_sudo_stripped(manage_line,False,True,linesep)
    return settings_diffed


def diffsettings6(manage_dir,admin=True,linesep=None,simple=True):
    """ Returns a list of tuples. 6 tuples in total. Each tuple represents a
    diff setting result ( Example INSTALLED_APPS )
    INSTALLED_APPS ; MIDDLEWARE_CLASSES
    TEMPLATE_CONTEXT_PROCESSORS ; TEMPLATE_LOADERS
    STATICFILES_DIRS ; STATICFILES_FINDERS

    Remember: What you are getting as results here is based on what diffsetting
    returns rather than being a definitive answer for INSTALLED_APPS

    To clarify: In INSTALLED_APPS had django something be default, then diffsettings
    would show additional / difference to the default so output of diffsettings is
    only definitive when have knowledge of defaults also.
    """
    installed_apps = ()
    middleware_classes = ()
    template_context_processors = ()
    template_loaders = ()
    staticfiles_dirs = ()
    staticfiles_finders = ()
    for line in diffsettings_or_sudo(manage_dir=manage_dir,admin=admin,linesep=linesep):
        if line.startswith('INSTALLED_APPS'):
            installed_apps = cmdbin.tuple_from_string_tuple_prefixed(line,simple=simple)
        elif line.startswith('MIDDLEWARE_CLASSES'):
            middleware_classes = cmdbin.tuple_from_string_tuple_prefixed(line,simple=simple)
        elif line.startswith('TEMPLATE_CONTEXT_PROCESSORS'):
            template_context_processors = cmdbin.tuple_from_string_tuple_prefixed(line,simple=simple)
        elif line.startswith('TEMPLATE_LOADERS'):
            template_loaders = cmdbin.tuple_from_string_tuple_prefixed(line,simple=simple)
        elif line.startswith('STATICFILES_DIRS'):
            staticfiles_dirs = cmdbin.tuple_from_string_tuple_prefixed(line,simple=simple)
        elif line.startswith('STATICFILES_FINDERS'):
            staticfiles_finders = cmdbin.tuple_from_string_tuple_prefixed(line,simple=simple)
        else:
            pass
    return [installed_apps,middleware_classes,
            template_context_processors,template_loaders,
            staticfiles_dirs,staticfiles_finders]


def diffsettings6print(manage_dir,empty_included=False):
    list6 = diffsettings6(manage_dir)
    list6titles = ['INSTALLED_APPS','MIDDLEWARE_CLASSES',
    'TEMPLATE_CONTEXT_PROCESSORS','TEMPLATE_LOADERS',
    'STATICFILES_DIRS','STATICFILES_FINDERS']
    tuple_non_empty_count = 0
    if empty_included is True:
        for idx,tup in enumerate(list6):
            print "{0}:".format(list6titles[idx])
            if len(tup) > 1:
                tuple_non_empty_count += 1
            print tup
    else:
        for idx,tup in enumerate(list6):
            if len(tup) < 1:
                continue
            tuple_non_empty_count += 1
            print "{0}:".format(list6titles[idx])
            print tup
    return tuple_non_empty_count


def diffsettings_installed_apps_contains(manage_dir,target='django.contrib.staticfiles',
                                         print_flag=False):
    """ 
    Remember: What you are getting as results here is based on what diffsetting
    returns rather than being a definitive answer for INSTALLED_APPS
    """
    contains = False
    list6 = diffsettings6(manage_dir)
    diffsettings_installed_apps = list6[0]
    if len(diffsettings_installed_apps) > 0 and target in diffsettings_installed_apps:
        contains = True
    if contains is True and print_flag is True:
        print "target: {0} found in:".format(target)
        print diffsettings_installed_apps
    return contains


def manage_py_or_sudo(manage_command='syncdb',dirproject=None):
    managed_it_flag = False
    manage_dir = None
    if dirproject:
        manage_dir = dirproject
    else:
        try:
            if files_dirs.slash_count(dirpath_django_project) > 0:
                manage_dir = dirpath_django_project
        except Exception:
            manage_dir = None

    if manage_dir and files_dirs.slash_count(manage_dir) > 0:
        with cd(manage_dir):
            manage_line = "./manage.py {0}".format(manage_command)
            if cmdbin.run_or_sudo(manage_line):
                managed_it_flag = True
    return managed_it_flag


def settings_append_sed(dirpath):
    """ Put a helper .sed file to the remote server.
    Supplied dirpath should be a directory which we will append
    the filename to before doing the put.
    chr(47) is forward slash (/) ; chr(92) is backward slash
    """
    put_flag = False
    if dirpath is None:
        return put_flag
    elif dirpath.startswith(chr(47)):
        pass
    else:
        return put_flag
    local_filename_in_sed = 'local_settings.py'
    sed = """
$a{0}
# Appended as a helper for local settings{0}
try:{0}
	from local_settings import *{0}
except ImportError:{0}
	print '{1} deployment settings missing?:'{0}
	# By manually setting False on next two lines, we ensure{0}
	# that debug can only ever be enabled by a present and{0}
	# correct local_settings.py{0}
	DEBUG = False{0}
	TEMPLATE_DEBUG = DEBUG
""".format(chr(92),local_filename_in_sed)

    put_res = cmdbin.put_using_named_temp_cmdbin(
            "{0}{1}settings_append.sed".format(dirpath,chr(47)),sed)
    return put_res


def sync_load_index(dirpath_project,dirpath_log,dirpath_djapian,print_flag=False):

    sli_elapsed = 0
    t1 = time.time()
    sync_res = manage_py_or_sudo('syncdb',dirpath_project)

    load_res = manage_py_or_sudo('loaddata local_hyphen_source_def',dirpath_project)

    perm_res = files_dirs.group_writable(dirpath_log,'/var/www/pub','/var/l')

    index_res = manage_py_or_sudo('index --rebuild',dirpath_project)
    """ Multiple runs of --rebuild might produce spurious errors as it is
    only supposed to be used the first time the index is built. 
    This is why there is a directory removal earlier in the script to try
    and avoid such issue where multiple runs / re-run of this benchmark.
    """

    group_write_res = group_writable_pyweb_recursive(dirpath_djapian)
    t2 = time.time()
    """ Above gives appropriate access on Djapian subdirectories which
    helps to avoid DatabaseLockError: Unable to get write lock on ...
    """
    if not group_write_res:
        files_dirs.abort_with_message('Aborting during Djapian index permission setting.')

    sli_elapsed = t2-t1

    if print_flag:
        time_line = "sync, load, index timing: {0:.3f} seconds".format(sli_elapsed)
        print cmdbin.lavg_prefixed(time_line)

    return sli_elapsed


def dump_repeat_hash(dirpath_bin,dirpath_project):
    """ Benchmark item that does a dump of data
    Path [stem] of script is supplied as first arg
    """
    manage_line = "{0}/manage_dumpdata_million_repeat_hash.sh {1}".format(
        dirpath_bin,dirpath_project)
    drh_elapsed = 0
    drh1 = time.time()
    drh_res = cmdbin.run_or_sudo(manage_line)
    if drh_res:
        drh2 = time.time()
        drh_elapsed = drh2-drh1
    int_drh_elapsed = int(drh_elapsed)
    return int_drh_elapsed


def poor_cycle_setup14(target='/rc/py',concurrent=2,totaltime=180,
                       data='initial',visible='internal',
                       app_name='local2dj14',dir_fab='poor_cycle14.fab',port_wsgi=None):
    path3 = none3 = (None,None,None)

    (pname, pretty_name_id, pretty_name_word, version_major) = cmdbin.pretty_name_tuple()

    print pretty_name_id
    print pretty_name_word

    """ The next code block is just a helper to allow shortened rc/py
    as argument. """
    if not target:
        pass
    elif target.startswith('prettify/'):
        pass
    elif target == 'rc/py/':
        target = 'prettify/local-database/rc/py/'
    elif target == 'rc/py':
        target = 'prettify/local-database/rc/py'
    elif target.endswith('/rc/py/'):
        target = 'prettify/local-database/rc/py/'
    elif target.endswith('/rc/py'):
        target = 'prettify/local-database/rc/py'

    dirstem_data = files_dirs.tilde_expand(DIRSTEM_DATA_SHORT)
    dirpath_data = "{0}/django14data".format(dirstem_data)
    mkdir_res = files_dirs.mkdir_existing("{0}/django14data".format(DIRSTEM_DATA_SHORT),
                               2771,False)
    """ mkdir_res = run("mkdir -m 2771 {0}".format(
            "{0}/django14data".format(dirstem_data)))
    """
    if mkdir_res:
        files_dirs.dirpath_dlp(dirpath_data)
        #print_success_and_failed(mkdir_res,dirpath_data)
    else:
        pass
        """ abort_with_cleanup(dirpath_fab,'Aborting during directory initialisation.') """
        # abort('Aborted during initialisation of directory django14data.')

    chown_line = "{0} root:www-data {1}".format(files_dirs.CMD_CHOWN,dirpath_data)
    chown_res = cmdbin.run_or_sudo(chown_line)
    cmdbin.print_success_and_failed(chown_res,dirpath_data)
    if chown_res.failed:
        abort('Aborted during initialisation of directory django14data ownership.')


    dirpath_djapian_app = "{0}/djapian/{1}".format(dirstem_data,app_name)
    rmdir_res = files_dirs.rmdir_existing(dirpath_djapian_app,True)
    if not rmdir_res:
        pass
        """ abort_with_cleanup(dirpath_fab,'Aborting during directory initialisation.') """
        #abort('Aborting during remove of djapian indexes for app.')

    mkdir_res = files_dirs.mkdir_existing("{0}/djapian".format(DIRSTEM_DATA_SHORT),2771,True,True,True)
    if not mkdir_res:
        """ abort_with_cleanup(dirpath_fab,'Aborting during directory initialisation.') """
        abort('Aborting during initialisation of directory djapian.')

    """ Read and understand function mkdir_existing and safety features to see why generally
    it is preferred to send any tilde or short form of directory to that function.
    That knowledge helps explain why dirpath_djapian is defined here rather than being
    used in the call to mkdir_existing() above.
    """
    dirpath_djapian = "{0}/djapian".format(dirstem_data)

    """ Now above we gave group write via 2771, but here we need to set the group to which
    that group write privilege will belong. """
    chgrp_or_sudo_web(dirpath_djapian)

    if env.user == 'root':
        files_dirs.set_dirpath_fab_from_short("/var/www/public_html/{0}".format(dir_fab))
    else:
        files_dirs.set_dirpath_fab_from_short("~/public_html/{0}".format(dir_fab))

    externally_visible_flag = False
    if 'both' in visible:
        externally_visible_flag = True

    repo_url_django_name = 'local2django17bench'
    repo_url_django = "https://bitbucket.org/wrightsolutions/{0}".format(
        repo_url_django_name)
    dir_ubin = "ubin9dirappendsu"
    repo_url_ubin = "https://bitbucket.org/wrightsolutions/{0}".format(dir_ubin)
    # Above two lines together define full url for the remote repo. Next two same but for webhelp
    dir_webhelp = "web24helper"
    repo_url_webhelp = "https://bitbucket.org/wrightsolutions/{0}".format(dir_webhelp)

    dirpath_fab = files_dirs.get_dirpath_fab()
    """ Make temporary directory in user home to hold cloned repo """
    mkdir_res = files_dirs.mkdir_existing(dirpath_fab,2755,True,True,True)
    if mkdir_res:
        print "Created directory {0}".format(dirpath_fab)
    else:
        abort('Aborted during initialisation of .fab directory.')

    print "Cloning into directory {0}".format(dirpath_fab)
    with cd(dirpath_fab):
        sign_repo.git_clone(repo_url_django)
        sign_repo.git_clone(repo_url_webhelp)
        sign_repo.git_clone(repo_url_ubin)

    """
    print "Hg cloning into directory {0}".format(dirpath_fab)
    with cd(dirpath_fab):
        sign_repo.repo_clone(repo_url_localbin,'dcbd2b2057cdce99fb19fee5b711e7f24ed52923')
        sign_repo.repo_clone(repo_url_localwebserver,'8647d7b821646a52c526e6096ccdf12588ef0280')
    """

    requires_django_res = pdeb.requires_binary_package('django-admin',
                                                       'python-django',assumeyes=True)
    if not requires_django_res:
        # None or False indicates we could NOT find django-admin
        print("Enabling modules will be a problem as django-admin NOT available!")

    requires_a2enmod_res = pdeb.requires_binary_package('a2enmod',
                                                        'apache2',assumeyes=True)
    if requires_a2enmod_res:
        which_a2enmod = requires_a2enmod_res
	#print(which_a2enmod,which_a2enmod,which_a2enmod)
    else:
        # None or False indicates we could NOT find a2enmod
        print("Enabling modules will be a problem as a2enmod NOT available!")
        
    """ Although we do not use a2enmod directly (just yet) we do need to ensure, ahead of time,
    that common /etc/apache2 directories will exist. Pushing a file to mods-available should
    then not create an issue.
    Earlier versions packaged a2enmod in a package named '-common', now it is in main apache2 package
    """

    dirstem_django = dirpath_fab
    dirpath_django_project = "{0}/{1}".format(dirstem_django,
                                              repo_url_django_name)
    dirpath_django_app = "{0}/{1}".format(dirpath_django_project,app_name)

    mpm = apache2mpm()
    print "Current apache2mpm is {0}".format(mpm)
    if mpm == 'worker':
        worker_better = 'which might result in better benchmark results'
        if not confirm("Worker MPM setup in place {0}. Continue anyway?".format(worker_better)):
            return none3

    """ Ensure apache2 binary is available as otherwise our reload / restart will fail later """
    requires_apache2_res = pdeb.requires_binary_package('apache2',
                                                        'apache2-mpm-prefork',assumeyes=True)

    requires_wsgi_res = pdeb.requires_dpkg_package('libapache2-mod-wsgi',True)
    """ Above requires should come before we send any templated wsgi related conf to the server """

    path_line = """
<IfModule mod_wsgi.c>
WSGIPythonPath {0}:{1}
</IfModule>""".format(dirstem_django,dirpath_django_project)
    # .format(dirstem_django,repo_url_django_name)
    if env.user == 'root':
        append('/etc/apache2/mods-available/wsgi.conf',path_line)
    else:
        append('/etc/apache2/mods-available/wsgi.conf',path_line,use_sudo=True)

    try:
        from jinja2 import Environment
    except ImportError:
        files_dirs.abort_with_message('Need jinja2 available on local machine. apt-get install python-jinja2.')

    local_settings_tpl = """
# Django local_settings for {{ project }} project.
DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '{{ dbpath }}/{{ dbname }}.sqlite', # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'log_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filename': '{{ djangolog }}',
            'maxBytes': 4194304, # 4 * 1048576
            'backupCount': 5,
        },
    },
    'loggers': {
        '': {
            'handlers': ['log_file'],
            'level': 'DEBUG',
            'propagate': True
        },
        'django.request': {
            'handlers': ['log_file'],
            'level': 'WARNING',
            'propagate': False,
        },
    }
}

DJAPIAN_DATABASE_PATH = '{{ dirdjapian }}'
"""
    """
    Now that definition of local_settings_tpl completed on previous line we
    will populated it using a render(). Note: filename on system may be
    different than local_settings.py and in current variant is settings_final.py
    """

    dirpath_django_log = "{0}/local2.log".format(dirpath_data)

    path3 = (dirpath_django_project,dirpath_django_log,dirpath_djapian)

    template_dict = {
        'dbpath': dirpath_data,
        'dbname': repo_url_django_name,
        'djangolog': dirpath_django_log,
        'dirdjapian': dirpath_djapian,
        'project': repo_url_django_name,
        }
    settings_rendered = Environment().from_string(local_settings_tpl).render(template_dict)

    files_dirs.put_using_named_temp("{0}/settings_final.py".format(dirpath_django_project),
                         settings_rendered,'settings_final construction',False)

    wsgi_py = """
# WSGI config for {0} project.
from os import environ

environ.setdefault("DJANGO_SETTINGS_MODULE", "{1}")

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.

from django.core.wsgi import get_wsgi_application
try:
   application = get_wsgi_application()
except Exception:
   print('wsgi and django initialisation issue...quitting')

""".format(repo_url_django_name,'settings')

    files_dirs.put_using_named_temp("{0}/wsgi.py".format(dirpath_django_app),
                         wsgi_py,'wsgi.py construction',False)

    requires_djapian_res = pdeb.requires_dpkg_package('python-django-djapian',
                                                      True,'universe')
    if not requires_djapian_res:
        files_dirs.abort_with_message('Aborting during djapian availability check.')

    requires_yaml_res = pdeb.requires_dpkg_package('python-yaml',True)
    if not requires_yaml_res:
        files_dirs.abort_with_message('Aborting during yaml availability check.')

    requires_pygments_res = pdeb.requires_dpkg_package('python-pygments',True)
    if not requires_pygments_res:
        files_dirs.abort_with_message('Aborting during pygments availability check.')

    apache2conf_tpl = """
<VirtualHost 127.0.0.1:{{ port_host }}>

DocumentRoot {{ docroot_slashless }}

<Directory />
   Options -Indexes
   AllowOverride None
   Require all granted
</Directory>

<Directory "{{ docroot_slashless }}/">
   Options -Indexes
   AllowOverride None
   Require all granted
</Directory>

   WSGIDaemonProcess {{ app }} user=www-data group=www-data processes=1 threads=5
   WSGIScriptAlias / {{ docroot_slashless }}/{{ project }}/{{ app }}/wsgi.py
   #WSGIPythonPath /opt/local/webdata/local2bottle/wsgi/# wsgi.conf

   Alias /static {{ docroot_slashless }}/{{ project }}/{{ app }}/static
   #Alias /static {{ docroot_slashless }}/local2django17/local2dj14/static
</VirtualHost>
"""

    if port_wsgi is None:
        port_wsgi = PORT_WSGI
    else:
        try:
            port_wsgi = int(port_wsgi)
            if port_wsgi < 80:
                port_wsgi = PORT_WSGI
        except:
            port_wsgi = PORT_WSGI

    template_dict = {
        'port_host': port_wsgi,
        'docroot_slashless': dirpath_fab,
        'project': repo_url_django_name,
        'app': app_name,
        }

    apache2conf = Environment().from_string(apache2conf_tpl).render(template_dict)
    # docroot_slashless=dirstem_data + '/django14')

    if port_wsgi in [80,8080]:
        continue_add_port = 'Continue and add port'
        if not confirm("Apache will listen additionally on port {0}. {1}?".format(
                port_wsgi,continue_add_port)):
            return none3

    apache2conf_res = None
    import tempfile
    named_temp = tempfile.NamedTemporaryFile()
    try:
        named_temp.write(apache2conf)
        named_temp.flush()
        apache2conf_res = put(named_temp,"/etc/apache2/sites-enabled/{0}.conf".format(app_name),
                              use_sudo=True)
        # Apache 2.4 and newer require .conf extension to be properly found by IncludeOptional
    finally:
        named_temp.close()

    if externally_visible_flag:
        listen_line = "Listen {0}".format(port_wsgi)
    else:
        listen_line = "Listen 127.0.0.1:{0}".format(port_wsgi)

    port_lines = """
<IfModule mod_wsgi.c>
# NameVirtualHost *:{0}
{1}
</IfModule>
""".format(port_wsgi,listen_line)

    named_temp = tempfile.NamedTemporaryFile()
    try:
        named_temp.write(port_lines)
        named_temp.flush()
        apache2port_res = put(named_temp,'/etc/apache2/ports.conf.append',use_sudo=True)
    finally:
        named_temp.close()

    if apache2port_res:
        if apache2conf_res.failed:
            files_dirs.abort_with_message('Aborting during apache2 port put.')
    else:
            files_dirs.abort_with_message('Aborting during apache2 port.')

    append_bin = "{0}/{1}/appender/append_not_already_dot_append.sh".format(dirpath_fab,dir_ubin)
    append_run = "{0} /etc/apache2/ports.conf 'Listen.*{1}'".format(append_bin,port_wsgi)
    append_res = run(append_run)
    if append_res.failed:
        files_dirs.abort_with_message('Aborting during apache2 port addition.')

    if apache2conf_res:
        if apache2conf_res.failed:
            files_dirs.abort_with_message('Aborting during apache2 configuration put.')
    else:
            files_dirs.abort_with_message('Aborting during apache2 configuration.')

    """ apache2re_res = sudo("/etc/init.d/apache2 reload",group='adm') """
    apache2re_res = sudo("/etc/init.d/apache2 reload")
    if apache2re_res.failed:
        files_dirs.abort_with_message('Aborting during apache2 reload.')

    if 'root' in env.user:
        pass
    else:
        #apache2enmod_res = sudo("/usr/sbin/a2enmod userdir")
        apache2enmod_res = sudo("{0} userdir".format(which_a2enmod))
        if apache2enmod_res.failed:
            files_dirs.abort_with_message('Aborting during apache2 Enable Module userdir.')

    apache2enmod_res = sudo("{0} wsgi".format(which_a2enmod))
    if apache2enmod_res.failed:
        files_dirs.abort_with_message('Aborting during apache2 Enable Module wsgi python module.')

    """ apache2re_res = sudo("/etc/init.d/apache2 reload") """
    restart_bin = "{0}/{1}/apache2/helper/apache2stop5startconditional.sh".format(
        dirpath_fab,dir_webhelp)
    apache2re_res = sudo(restart_bin)
    if apache2re_res.failed:
        files_dirs.abort_with_message('Aborting during apache2 stop 5 start.')

    with settings(warn_only=True):
        apache2port_res = sudo("{0}{1}".format(cmdbin.CMD_LSOF_TPORT,port_wsgi))
        if apache2port_res.failed:
            files_dirs.abort_with_message('Aborting during apache2 [additional] port test.')

    with settings(warn_only=True):
        url_cdn = 'http://c5871e0b1f1c144e2f0e-7fd212704b537f6e17de67217009ee51.r73.cf3.rackcdn.com'
        url_filename = 'constructionMStankieA14znak20111228164215derivativePublicDomainPx125.png'
        with cd(dirpath_fab):
            run(cmdbin.CMD_WGET + "{0}/{1}".format(url_cdn,url_filename))
            # if png_res.failed:

    index_html = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<meta name="robots" content="noarchive,follow" />
<title>Under Construction</title>
</head>
<body>
<p><img src="{0}" alt="Under Construction Logo" /> coming soon...</p>
<p>
    <a href="http://validator.w3.org/check?uri=referer"><img
        src="http://www.w3.org/Icons/valid-xhtml10-blue"
        alt="Valid XHTML 1.0 Strict" height="31" width="88" /></a>
  </p>
</body>
</html>
""".format(url_filename)

    index_path = "{0}/index.html".format(dirpath_fab)
    with cd(dirpath_fab):
        files_dirs.put_using_named_temp(index_path,index_html,'index.html construction',False)

    return path3


def poor_cycle_load14(target='/rc/py',concurrent=2,totaltime=10,dir_fab='poor_cycle14.fab',
                      dirpath_django_project=None,dirpath_django_log=None,
                      dirpath_djapian=None,port_wsgi=None):
    """ If you want fabric command line interaction with an argument
    then expect it to be a string. May need to int() what you get """
    ab_concurrent = int(concurrent)
    ab_totaltime = int(totaltime)
    tuple5 = zero5 = (0,0,0,0,0)
    if ab_totaltime < 1:
        return zero5

    requires_ab_res = None
    requires_ab_which = cmdbin.CMD_UPTIME
    if int(totaltime) > 0:
        requires_ab_res = pdeb.requires_binary_package('ab','apache2-utils',assumeyes=True)
        if requires_ab_res:
            requires_ab_which = requires_ab_res

    import time
    c1 = time.time()
    time_line_dict = {}
    """ seconds is not necessarily unique and so seconds is the VALUE not the key """
    seconds1 = sync_load_index(dirpath_django_project,dirpath_django_log,dirpath_djapian,True)
    time_line1 = "(1) sync, load, index timing: {0:.3f} seconds".format(seconds1)
    time_line_dict[time_line1]=seconds1

    seconds2 = sync_load_index(dirpath_django_project,dirpath_django_log,dirpath_djapian,True)
    time_line2 = "(2) sync, load, index timing: {0:.3f} seconds".format(seconds2)
    time_line_dict[time_line2]=seconds2

    seconds3 = sync_load_index(dirpath_django_project,dirpath_django_log,dirpath_djapian,True)
    time_line3 = "(3) sync, load, index timing: {0:.3f} seconds".format(seconds3)
    time_line_dict[time_line3]=seconds3

    seconds4 = sync_load_index(dirpath_django_project,dirpath_django_log,dirpath_djapian,True)
    time_line4 = "(4) sync, load, index timing: {0:.3f} seconds".format(seconds4)
    time_line_dict[time_line4]=seconds4
    #print cmdbin.lavg1prefixed(time_line4)

    print 'Sync, load, index output text repeated but in ascending order (of seconds) next'
    """ time_key next will be the time_line (text) as documented earlier """
    for time_key,time_value in sorted(time_line_dict.items(), key=lambda t: t[1]):
        print time_key
    print 'Sync, load, index output text (ordered) that shows (1), (2), (3), (4) might indicate degradation?'

    if ab_totaltime > 5:
        #if env.user == 'root':
        #    files_dirs.set_dirpath_fab_from_short("/var/www/public_html/{0}".format(dir_fab))
        #else:
        #    files_dirs.set_dirpath_fab_from_short("~/public_html/{0}".format(dir_fab))

        dirpath_fab = files_dirs.get_dirpath_fab()
        if dirpath_fab is None:
            print 'directory=None ? Did benchmark setup (poor_cycle_setup14 or poor_cycle_setup16) complete?'
            return zero5


        #dir_webhelp = "web24helper"   # Defined outside of the function so unavailable
        path_webhelp_script = "{0}/web24helper/django".format(dirpath_fab)
        dump_elapsed = dump_repeat_hash(path_webhelp_script,dirpath_django_project)
        if dump_elapsed < 1:
            print 'Benchmark may be suspect - dump_repeat_hash() completed in less < 1 sec'
    else:
        print "Benchmark step dump_repeat_hash() skipped for requested totaltime={0}".format(ab_totaltime)

    # By making final paramater True instead of False below, you can delegate the print instead
    sync_load_index(dirpath_django_project,dirpath_django_log,dirpath_djapian,False)
    time_line5 = "(1) sync, load, index timing: {0:.3f} seconds".format(seconds1)
    print cmdbin.lavg_prefixed(time_line5)

    c2 = time.time()

    cycle_elapsed = c2-c1
    time_complement = 0
    try:
        int_cycle_elapsed = int(cycle_elapsed)
        print "Balance calculation (seconds) will involve ({0} - {1})".format(
            ab_totaltime,int_cycle_elapsed)
        time_complement = ab_totaltime - int_cycle_elapsed
        if time_complement < 5:
            time_complement = 5
            print "Balance calculation (seconds) has prompted an adjustment time_complement=".format(
                time_complement)
    except Exception,e:
        print "Balance calculation (seconds) not completed! {0}".format(e)
        time_complement = 0

    if port_wsgi is None:
        port_wsgi = PORT_WSGI
    else:
        try:
            port_wsgi = int(port_wsgi)
            if port_wsgi < 80:
                port_wsgi = PORT_WSGI
        except:
            port_wsgi = PORT_WSGI

    removed = files_dirs.py_compiled_removal(dirpath_django_project,period='week')
    if not removed:
        #files_dirs.abort_with_message('Encounted a problem during pyc removal.')
        print('Encounted a problem during pyc removal. Fetch of /local2/ may fail!')
    # Above we are removing pyc compiled python to setup web server read correctly

    #settings_debug(None,debug=False,verbosity=1,md5current=None)
    settings_debug(dirpath_django_project,True,0,None)
    # Above we make an edit to settings_final to force it current for webserver

    # Before starting the wget priming against urls, confirm what apache thinks about vhosts
    run(cmdbin.CMD_APACHE_VHOST_INFO)

    url_counts = "http://localhost:{0}/local2/counts/".format(port_wsgi)
    url_benchmark = "http://127.0.0.1:{0}/local2/{1}".format(port_wsgi,target)

    if requires_ab_res and ab_totaltime > 1:
        """ To get things primed, do a single fetch first then short sleep. """
        run("{0} {1}".format(cmdbin.CMD_WGET5,url_counts))
        if time_complement > 0:
            time.sleep(2)
            print "Using time_complement={0} to complete next stage /usr/bin/ab".format(
                time_complement)
            """ Benchmark the url """
            """ run("/usr/bin/ab -kc {0:d} -t {1:d}".format(ab_concurrent,time_complement)) """
            run("{0} -kc {1:d} -t {2:d} {3}/".format(requires_ab_which,ab_concurrent,
                                                     time_complement,url_benchmark))
    elif ab_totaltime == 1:
        """ In this context we use timeout 1 for our single fetch """
        print 'Fetch: In this context we use timeout 1 for our single fetch'
        run("{0} {1}".format(cmdbin.CMD_WGET1,url_benchmark))
    else:
        """ No --timeout flag given to wget here """
        print 'Fetch: In this context we use no timeout for our single fetch'
        run("{0} {1}".format(cmdbin.CMD_WGET,url_benchmark))
    load3 = cmdbin.load3tuple()
    if load3[0] is not None:
        tuple5 = ((ab_totaltime - int_cycle_elapsed),time_complement)
        tuple5 += load3
    else:
        # If you are happy with tail being None,None,None on fail then no
        # need for special if / then enclosure - simple += append would do.
        tuple5 = ((ab_totaltime - int_cycle_elapsed),time_complement,0,0,0)
    assert len(tuple5) == 5
    return tuple5


def poor_cycle(target='/rc/py',concurrent=2,totaltime=10,
               data='initial',visible='internal',
               app_name='local2dj14',dir_fab='poor_cycle.fab',port_wsgi=None):
    """ See two functions setup and load for full function signature similar
    to target='/rc/py',concurrent=2,totaltime=180,data='initial',
               visible='internal',app_name='local2dj14',dir_fab='poor_cycle.fab'

    Poor config to deliberately stress a system for the purposes of
    benchmarking. Having 2 or more concurrent users requesting pygments
    prettified output (non cached) is usually enough to run a test benchmark
    against a low budget cloud server.
    A properly configured Apache mod_wsgi can work well, but in order to
    stress our system here, and to cause least disruption to any existing setup,
    we employ the minimum of Apache configuration effort
    An Apache configuration which has not been tailored to local conditions on
    the server is probably a poor configuration.
    Such configurations can introduce system stress and make a taxing benchmark.

    django14cycle differs from django14snail in that the concept of a basket or cycle of
    tasks is introduced. This is intended to introduce a single thread / non Apache measure
    into the final mark.
    Apache bench is still used, but as a contribution to, rather than the entire benchmark.

    Returns result from .._load() function which is a 5 tuple:
        tuple5 = ((ab_totaltime - int_cycle_elapsed),time_complement,0,0,0)
    Last three zeros would normally contain load averages rather than zeros 
    """
    tuple5 = none5 = (None,None,None,None,None)
    swap_total = sysctl_plus.free_marked(True)
    """ Having reported small amount of summary information about the server,
    next we have two steps to complete: 'setup' then 'load'
    """
    dirpath_django_project,dirpath_django_log,dirpath_djapian = poor_cycle_setup14(
        target,concurrent,totaltime,data,visible,app_name,dir_fab,port_wsgi)

    if dirpath_django_project is not None:
        tuple5 = poor_cycle_load14(target,concurrent,totaltime,dir_fab,
                                   dirpath_django_project,dirpath_django_log,
                                   dirpath_djapian,port_wsgi)
        if any(tuple5):
            pass
        else:
            tuple5 = none5
    return tuple5


def sync_load_index14one(print_flag=False,port_wsgi=None):
    """ Perform a single sync, load, index and optionally print the result.

    How destructive / intrusive is running this routine?
    We certainly install apache2 if it is not already installed.
    We do not change the existing MPM - it stays as it is when apache2 already present
    We do change apache2 to listen additionally on port 1480

    Could you run this routine on a live server? Probably.
    Worried about an existing apache2 setup? Take a recursive cp of /etc/apache2
    before running and revert the opening of 1480 port manually once output has completed.

    Rather than use poor_cycle_setup14, we could in future create a variant that
    does not add apache2 ports whilst doing the Django setup. However for now we are
    reusing existing code that is already in place.
    """
    dirpath_django_project,dirpath_django_log,dirpath_djapian = poor_cycle_setup14(
        '/rc/py',concurrent=1,totaltime=5,data='initial',visible='internal',
        app_name='local2dj14',dir_fab='sync_load_index.fab',port_wsgi=port_wsgi)

    seconds = 0
    if dirpath_django_project is not None:
        seconds = sync_load_index(dirpath_django_project,dirpath_django_log,dirpath_djapian,print_flag)
    return seconds

