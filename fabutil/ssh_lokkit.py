#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2018,2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
import re
from . import cmdbin
from . import conf_get_set as getset

""" Design decision to use Keyedspace2 rather than less specialist Keyedfile class
when processing /etc/ssh/sshd_config file

Keyedfile() works fine, but there might be occassion where we
are interested in the extra work that Keyedspace2 puts into identifying disabled keys

From notes of Keyedspace2() class
    Use Keyedspace2 subclass where you wish to:
    (1) expend much effort ensuring the last item in tuple6 and tuple14 (target) is space preserving
    [ target always include spacing around key (left of value) ]
    (2) Make extra effort to analyse what on face of things appear to be comment lines, to treat
    the contents as disabled key value where appropriate.
"""

SSHD_CONF = '/etc/ssh/sshd_config'
MD5SSHD_CENTOS7 = '40d961cd3154f0439fcac1a50bd77b96'
MD5SSHD_DEBIAN9 = 'df8447ce600dd3d6bc4048ccc2faa536'
"""
40d961cd3154f0439fcac1a50bd77b96  centos7sshd_config.conf
9fdf493802c70a296112e4643d550b80  centos7sshd_config_forcedcommandonly.conf
#e1d8dd6efd53dcbca60527cc06fdcbd5  centos7sshd_config_forcedcommandonly.conf
671240081310fdc24752a231f48f637f  centos7sshd_config_prohibitpassword.conf
#304b3c1622b8e10e418327421c38eb77  centos7sshd_config_prohibitpassword.conf
"""
#cmdbin.MD5A32			# 'a'*32
#cmdbin.MD5DEFAULTEXCLAIM	# 'default!default!default!default!'


def max3(auth_tries=6,sessions=10,startups='10:30:100'):
    auth_tries_changed = False
    sessions_changed = False
    startups_changed = False
    return (auth_tries_changed,sessions_changed,startups_changed)


def max3return():
    with getset.Keyedspace2(SSHD_CONF) as conf:
        #print conf.keylines_typed(2,['MaxAuthTries','MaxSessions','MaxStartups'])
        print conf.linecount
        print conf.md5sum
        print conf.lines
        return conf.keylines(['MaxAuthTries','MaxSessions','MaxStartups'])


def max3return2():
    with getset.Keyedspace2(SSHD_CONF) as conf:
        #print conf.keylines(['MaxAuthTries','MaxSessions','MaxStartups'])
        return conf.keylines_typed(2,['MaxAuthTries','MaxSessions','MaxStartups'])


def sshd_config_summary(print_flag=False,line_prefix=None):
    """ Summary of the sshd config directives that are most important
    ( defined in EXPRESSIONS in the function )
    Function amended to use Keyedspace2() rather than Keyedspace() for consistency.
    """
    EXPRESSIONS=['AcceptEnv','Allow','HostKey ','KeepAlive','Forwarding','^Max','^#Max',
                'PasswordAuthentication','Permit','PermitRootLogin','Port ','Protocol']
    lines = []
    with getset.Keyedspace2(SSHD_CONF) as conf:
        lines = conf.lines_noncomment_containing_any(EXPRESSIONS)
    if print_flag is True and line_prefix is not None:
        if line_prefix.lower() == 'filepath':
            for line in lines:
                print "{0}: {1}".format(conf_filepath,line.rstrip())
        elif line_prefix.lower() == 'filename':
            filename = path.basename(conf_filepath)
            for line in lines:
                print "{0}: {1}".format(filename,line.rstrip())
        else:
            for line in lines:
                print "{0}{1}".format(line_prefix,line.rstrip())
    elif print_flag is True:
        for line in lines:
            print line.rstrip()
    return lines


def sshd_config_keylines_enabled(linesep_remove=False):
    """ Returns important content from /etc/ssh/sshd_config - namely those lines
    of the form key: val and excludes #key: val 
    """
    enabled = []
    with getset.Keyedspace2(SSHD_CONF) as conf:
        enabled = conf.keylines_enabled_resplit()
    return enabled

    
def sshd_config_keylines_enabled_linesep(linesep_remove=False):
    """ Returns important content from /etc/ssh/sshd_config - namely those lines
    of the form key: val and excludes #key: val 
    """
    with getset.Keyedspace2(SSHD_CONF) as conf:
        enabled = conf.keylines_enabled()
        if linesep_remove:
            unsplit = ''.join(enabled)
            #print "conf_linesep:{0} {0} {0}".format(conf.conf_linesep)
            returned = unsplit.split(conf.conf_linesep)
        else:
            returned = enabled
    return returned


def sshd_config_keylines_disabled(verbose=False):
    """ Returns commented / hashed content from /etc/ssh/sshd_config -
    namely those lines that are useful comments but not directives actioned
    """
    dlines = []
    with getset.Keyedspace2(SSHD_CONF) as conf:
        dlines = conf.keylines_disabled()
    if verbose is True:
        for line in dlines:
            print(line.rstrip())
    return dlines


def sshd_config_reset(verbose=True):
    """ Reset the config [and binaries] using rpm -V so that default sshd_config """
    str_rpmv = 'md5sum /etc/ssh/sshd_config && rpm -V openssh-server;'
    str_mv_yum = 'mv /etc/ssh/sshd_config /tmp/ && yum --assumeyes reinstall openssh-server'
    str_combined = "{0} {1}".format(str_rpmv,str_mv_yum)
    reset_res = run(str_combined)
    if reset_res.succeeded:
        print('Sshd config restored to repo default')
    md5text = cmdbin.md5text_or_sudo('/etc/ssh/sshd_config')
    if verbose is True:
        print("After completion of ..._reset() have md5={0}".format(md5text))
    return reset_res


def sshd_motd_yes_preview(verbose=False):
    preview_line = ''
    # chr(35) is hash / octothorpe
    phashed = "{0}PrintMotd".format(chr(35))
    with getset.Keyedspace2(SSHD_CONF) as conf:
        if conf.lines_enabled_contains('PrintMotd'):
            preview_line = conf.sedline1('PrintMotd','yes')
	elif phashed in conf.keys_disabled_printable(True,False):
            if verbose is True:
                print('Note: PrintMotd is NOT currently enabled')
                #print "{0} keyname {1} printable but disabled.".format(gen_prefix,phashed)
            unhash_res = conf.context_unhash(phashed,verbosity=(verbose==True))
            if unhash_res and verbose is True:
                print('Note: PrintMotd could be unhashed to give an uncommented entry')
            # Next show the preview in two parts (i) uncommenting , (ii) setting yes
            if unhash_res:
		preview_line = conf.context_unhash_constructed(phashed,verbosity=(verbose==True))
            else:
                preview_line = conf.sedline1('PrintMotd','yes')
        else:
            pass
    return preview_line


def loglevel_line(verbose=False):
    """ return [and print] the loglevel line that is currently enabled
    also print when verbose=True
    returns loglevel line when LogLevel is hashed / commented rather than enabled
    returns '' when LogLevel is absent altogether
    """
    loglevel_line = ''
    # chr(35) is hash / octothorpe
    lhashed = "{0}LogLevel".format(chr(35))
    with getset.Keyedspace2(SSHD_CONF) as conf:
	lines_enabled_matching = conf.lines_enabled_contains('LogLevel')
        #if conf.lines_enabled_contains('LogLevel'):
        if lines_enabled_matching:
            if verbose:
                for line in lines_enabled_matching:
                    print(line)
            loglevel_line = lines_enabled_matching[-1]
	elif lhashed in conf.keys_disabled_printable(True,False):
            if verbose is True:
                print('Note: LogLevel is NOT currently enabled')
                #print "{0} keyname {1} printable but disabled.".format(gen_prefix,lhashed)
            unhash_res = conf.context_unhash(lhashed,verbosity=(verbose==True))
            if unhash_res and verbose is True:
                print('Note: LogLevel could be unhashed to give an uncommented entry')
            #loglevel_line = conf[lhashed]
            loglevel_line = conf.keyline1rstripped(lhashed)
        else:
            pass
    return loglevel_line


def loglevel_enabled_line(verbose=False):
    """ return [and print] the loglevel line that is currently enabled
    also print when verbose=True
    returns 0 when LogLevel is hashed / commented rather than enabled
    returns -1 when LogLevel is absent altogether
    """
    loglevel_line = -1
    # chr(35) is hash / octothorpe
    lhashed = "{0}LogLevel".format(chr(35))
    with getset.Keyedspace2(SSHD_CONF) as conf:
	lines_enabled_matching = conf.lines_enabled_contains('LogLevel')
        #if conf.lines_enabled_contains('LogLevel'):
        if lines_enabled_matching:
            if verbose:
                for line in lines_enabled_matching:
                    print(line)
            loglevel_line = lines_enabled_matching[-1]
	elif lhashed in conf.keys_disabled_printable(True,False):
            if verbose is True:
                print('Note: LogLevel is NOT currently enabled')
                #print "{0} keyname {1} printable but disabled.".format(gen_prefix,lhashed)
            unhash_res = conf.context_unhash(lhashed,verbosity=(verbose==True))
            if unhash_res and verbose is True:
                print('Note: LogLevel could be unhashed to give an uncommented entry')
            #loglevel_line = conf[lhashed]
            loglevel_line = 0
        else:
            loglevel_line = -1
    return loglevel_line


def loglevel_line_context(verbose=False):
    """ return [and print] the loglevel line that is currently enabled
    also print 'Context Note' helper messages and result when verbose=True
    returns a 'not defined' type message string when LogLevel is absent altogether
    Reminder of the type codes in conf_get_set
    0	Active / enabled
    1   Hashed / Commented
    2   Both types 0 and 1
    """
    loglevel_line = ''
    # chr(35) is hash / octothorpe
    lhashed = "{0}LogLevel".format(chr(35))
    with getset.Keyedspace2(SSHD_CONF) as conf:
        if 'LogLevel' in conf:
            if verbose is True:
                print('Context Note: LogLevel is configured directly (as below)')
            loglevel_line = conf.keylines_typed(0,['LogLevel'])
            """ Above we fetch with linetype=0 (commented/hashed)
            #loglevel_line = conf['LogLevel']
            """
        elif lhashed in conf.keys_disabled_printable(True,False):
            if verbose is True:
                print('Context Note: LogLevel is NOT currently enabled')
                #print "{0} keyname {1} printable but disabled.".format(gen_prefix,lhashed)
            """unhash_res = conf.context_unhash(lhashed,verbosity=(verbose==True))
            if unhash_res and verbose is True:
                print('Note: LogLevel could be unhashed to give an uncommented entry')
            loglevel_line = conf[lhashed]
            loglevel_line = conf.keylines_typed(1,[lhashed])   # <-- same result but prettier if 1rstripped()
            """
            loglevel_line = conf.keyline1rstripped(lhashed)
        else:
            loglevel_line = "{0} LogLevel is not defined in the conf!".format(chr(35))

        if verbose is True:
            print(loglevel_line)

    return loglevel_line


def loglevel_sedlines(loglevel='AUTHPRIV',verbose=False):
    """ Returns a list of sedlines that would be required to
    make the LogLevel into the supplied LogLevel (and enabled)
    """
    sedlines = []
    return sedlines


def config_context_update(keyname,new_value,verbosity=0,unchanged_okay=False):
    """ """
    with getset.Keyedspace2(SSHD_CONF) as conf:
        cup_res = conf.context_update(keyname,new_value,verbosity,unchanged_okay)
    return cup_res


def loglevel_context_update(new_value,verbosity=0,unchanged_okay=False):
    """ Wrapper around config_context_update that supplies fixed keyname """
    return config_context_update('LogLevel',new_value,verbosity,unchanged_okay)


def config_context_update_remote(keyname,new_value,verbosity=0,unchanged_okay=False):
    """ The _remote suffix added as we do extra work than plain config_context_update
    in that we attempt to apply the [sed] changes to the remote config
    Similar to _context_update_remote_ptype but involves a default 'a'*32 first
    argument to sed_remote_apply() so you are giving a dummy answer to skip
    any checksum checking prior to applying the change [on the remote]
    Might be preferred in an emergency or where the config file has been subject to
    many incremental changes and is too dynamic from any knowable checksum.
    """
    updated_flag = False
    apply_res = False
    with getset.Keyedspace2(SSHD_CONF) as conf:
        cup_res = conf.context_update(keyname,new_value,verbosity,unchanged_okay)
        if len(cup_res) > 0:
            sedlines = conf.sedlines((verbosity>1))
            apply_res = getset.sed_remote_apply(cmdbin.MD5A32,SSHD_CONF,sedlines,
							'medium',True,0.75,True)
        if apply_res is True:
            updated_flag = True
        elif verbosity > 1:
            amsg1 = "_update_remote() sed_remote_apply returned {0}.".format(apply_res)
            print("{0} - likely that change !NOT APPLIED! on remote".format(amsg1))
        else:
            pass
    return updated_flag


def config_context_update_remote_ptype(keyname,new_value,verbosity=0,unchanged_okay=False):
    """ The _remote suffix added as we do extra work than plain config_context_update
    in that we attempt to apply the [sed] changes to the remote config
    Use cmdbin.deb_or_rpm() to obtain package type (ptype) so can use suitable md5before
    config_context_update_remote_ptype() is preferred for safety whenever you can provide
    an accurate md5sum of the current file state.
    Often that current file state will be the default conf [checksum] but not always.
    More likely to fail than config_context_update_remote() because you need to
    supply an accurate [current] checksum for the change to authorise [and apply on remote]
    Better for known state situations where you know the remote config state as it can
    check it [checksum] agrees before applying the requested change.
    """
    ptype = cmdbin.deb_or_rpm()
    if ptype is None:
        if verbosity > 0:
            print('..._remote_ptype() unable to determine if deb or rpm like')
        updated_flag = config_context_update_remote(keyname,new_value,verbosity,unchanged_okay)
        return updated_flag

    md5before = None
    ptype_message = None
    if 'deb' == ptype:
        md5before = MD5SSHD_DEBIAN9
    elif 'rpm' == ptype:
        md5before = MD5SSHD_CENTOS7
    else:
        pass

    if md5before is not None:
        ptype_message = "ptype='{0}' detected in ..._remote_ptype()".format(ptype)

    if verbosity > 0 and ptype_message is not None:
        print(ptype_message)

    if md5before is None:
        md5before = cmdbin.MD5A32

    updated_flag = False
    apply_res = False
    with getset.Keyedspace2(SSHD_CONF) as conf:
        cup_res = conf.context_update(keyname,new_value,verbosity,unchanged_okay)
        if len(cup_res) > 0:
            md5text = cmdbin.md5text_or_sudo('/etc/ssh/sshd_config')
            if md5before == md5text:
                # Likely outcome if config file in default state
                pass
            elif 'rpm' == ptype:
                sum_whitelist = ['9fdf493802c70a296112e4643d550b80','671240081310fdc24752a231f48f637f']
                sum_whitelist.append('304b3c1622b8e10e418327421c38eb77')
                sum_whitelist.append('e1d8dd6efd53dcbca60527cc06fdcbd5')
                if md5text in sum_whitelist:
                    # Although file is not in default state, it is in a recognisable state
                    # So make the expectation be the value matched in the whitelist
                    md5before = md5text
            else:
                pass
            sedlines = conf.sedlines((verbosity>1))
            rever = True
            if 'PermitRootLogin' == keyname:
                # Ensure reverse=False when later calling getset.sed_remote_apply()
                rever = False
            apply_res = getset.sed_remote_apply(md5before,SSHD_CONF,sedlines,'medium',rever,0.75,True)
        if apply_res is True:
            updated_flag = True
        elif verbosity > 1:
            amsg1 = "_update_remote_ptype() sed_remote_apply returned {0}.".format(apply_res)
            print("{0} - change !NOT APPLIED! on remote - ?md5before wrong?".format(amsg1))
        else:
            pass
    return updated_flag


def loglevel_context_update_remote(new_value,verbosity=0,unchanged_okay=False):
    """ Wrapper of config_context_update_remote supplies fixed keyname """
    return config_context_update_remote('LogLevel',new_value,verbosity,unchanged_okay)


def loglevel_context_update_remote_ptype(new_value,verbosity=0,unchanged_okay=False):
    """ Wrapper of config_context_update_remote_ptype supplies fixed keyname """
    return config_context_update_remote_ptype('LogLevel',new_value,verbosity,unchanged_okay)


def passwordauth_context_update_remote(new_value,verbosity=0,unchanged_okay=False):
    """ Wrapper of config_context_update_remote supplies fixed keyname """
    return config_context_update_remote('PasswordAuthentication',new_value,verbosity,unchanged_okay)


def passwordauth_context_update_remote_ptype(new_value,verbosity=0,unchanged_okay=False):
    """ Wrapper of config_context_update_remote_ptype supplies fixed keyname """
    return config_context_update_remote_ptype('PasswordAuthentication',new_value,verbosity,unchanged_okay)


def permitrootlogin_context_update_remote(new_value,verbosity=0,unchanged_okay=False):
    """ Wrapper of config_context_update_remote supplies fixed keyname """
    return config_context_update_remote('PermitRootLogin',new_value,verbosity,unchanged_okay)


def permitrootlogin_context_update_remote_ptype(new_value,verbosity=0,unchanged_okay=False):
    """ Wrapper of config_context_update_remote_ptype supplies fixed keyname """
    return config_context_update_remote_ptype('PermitRootLogin',new_value,verbosity,unchanged_okay)


""" Openssh 5.3 and newer defaults for Max type settings:
MaxAuthTries
   Specifies the maximum number of authentication attempts permitted per connection.  Once the
   number of failures reaches half this value, additional failures are logged.  The default is 6.

MaxSessions
   Specifies the maximum number of open sessions permitted per network connection.  The default is 10.

MaxStartups
   Specifies the maximum number of concurrent unauthenticated connections to the SSH daemon.
   Additional connections will be dropped until authentication succeeds or the LoginGraceTime
   expires for a connection.  The default is 10:30:100.

   Alternatively, random early drop can be enabled by specifying the three colon separated values
   “start:rate:full” (e.g. "10:30:60").  sshd(8) will refuse connection attempts with a
   probability of “rate/100” (30%) if there are currently “start” (10) unauthenticated connections.
   The probability increases linearly and all connection attempts are refused if
   the number of unauthenticated connections reaches “full” (60).
"""
                #sedres = getset.sed_remote_apply(md5before,conf_filepath,sedlines,'medium',True,0.75,True)
                #sedres = getset.sed_remote_apply(md5before,conf_filepath,sedlines,'medium',True,0.5,True)
                #sedres = getset.sed_remote_apply(md5before,conf_filepath,sedlines,'medium',True,1.0,True)
#                sedres = getset.sed_remote_apply(md5before,conf_filepath,sedlines,'medium',True,0.75,True)
