#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
import re
try:
    from . import cmdbin
except ValueError:
    import cmdbin

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

CMD_CAT = "/usr/bin/cat "
CATNET_TEMPLATE = "/usr/bin/cat /proc/sys/net/{0}"
# Next /sbin/ is portable, versus centos only /usr/sbin/sysctl
CMD_SYSCTL = "/sbin/sysctl "
CMD_SYSCTL_FORWARDING4_ALL = "/sbin/sysctl -a --pattern net.ipv4.conf.all.forwarding"
CMD_SYSCTL_FORWARDING4_PERINT = "/sbin/sysctl -a --pattern '^net.ipv4.conf.*.forwarding'"
CMD_SYSCTL_FORWARDING4_IP_FORWARD = "/sbin/sysctl -a --pattern net.ipv4.ip_forward"
CMD_SYSCTL_FORWARDING6_ALL = "/sbin/sysctl -a --pattern net.ipv6.conf.all.forwarding"
CMD_SYSCTL_FORWARDING6_PERINT = "/sbin/sysctl -a --pattern '^net.ipv6.conf.*.forwarding'"
CMD_SYSCTL_FORWARDING_PERINT = "/sbin/sysctl -a --pattern '^net.ipv(4|6).conf.*.forwarding'"
CMD_SYSCTL_FORWARDING_NONPERINT = "/sbin/sysctl -a --pattern '^net.ipv(4|6).(ip_forward$|conf.all.forwarding)'"
# Never find a result from ipv6 following: /sbin/sysctl sysctl -a --pattern net.ipv6.ip_forward

# Next coreutils is where you will find 'cat' on rpm based systems
CMD_RPM_QUERY_COREUTILS = "/bin/rpm --query coreutils"


def sysctl_readable(sysctl_cmd,verbosity=0,run_always=False):
    """ Executes the sysctl (optionally using sudo) and returns
    a list of lines excluding those that indicate a problem 'reading key'
    With run_always set True, even non-root users will avoid trying to 'sudo' it
    """
    sres = None
    list_of_lines = []
    if sysctl_cmd is None:
	return list_of_lines
    elif 'sysctl' not in sysctl_cmd:
	return list_of_lines

    #with hide('output'):
    sres = cmdbin.run_or_sudo(sysctl_cmd,run_always,quiet=False)

    #stripped_lines = cmdbin.run_res_lines_stripped_searching(sres,'.')
    stripped_lines = cmdbin.run_res_lines_stripped(sres)

    unreadable_count = 0
    for line in stripped_lines:
        #line = sline.strip()
        if len(line) < 3:
            continue
        if line.startswith('sysctl: reading key'):
            unreadable_count += 1
            continue

        if chr(61) in line:
            try:
                line_array = line.split(chr(61))
                label = line_array[0]
                #sdict[label]=line_array[1]
            except Exception:
                raise
            list_of_lines.append(line)
        else:
            pass

    return list_of_lines


def net_forwarding_summary(all_or_perint='all',print_flag=False,run_always=False):
    """ Print a summary of the sysctl returned settings relevant to ip forwarding
    Default 'all' ignores perint

    Supplying anything other than 'all' in first arg will give you the
    obscure and auxilary things (special packets / various flags)
    which are truly controlled at a 'per interface' level.

    Remember that having 0 in a 'per interface' setting give NO INDICATION
    of whether that interface is set enabled for [general] ip forwarding
    """
    if 'all' == all_or_perint:
        sysctl_cmd = CMD_SYSCTL_FORWARDING_NONPERINT
    else:
        sysctl_cmd = CMD_SYSCTL_FORWARDING_PERINT
    lines = sysctl_readable(sysctl_cmd,verbosity=0,run_always=run_always)
    if print_flag:
        for line in lines:
            print(line)
    return lines


