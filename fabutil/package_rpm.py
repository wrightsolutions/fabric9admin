#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2018,2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
import re
try:
    from . import cmdbin
except ValueError:
    import cmdbin

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

CMD_RPM = "/bin/rpm "
CMD_RPM_INSTALL = "/bin/rpm -ivh "
CMD_RPM_TEST = "/bin/rpm --test "
CMD_RPM_QUERY = "/bin/rpm --query "
CMD_YUM = "/usr/bin/yum "
CMD_YUM_ASSUMEYES = "/usr/bin/yum --assumeyes "
CMD_YUM_ASSUMEYES_INSTALL = "/usr/bin/yum --assumeyes install "
CMD_YUM_CLEAN_ALL = "/usr/bin/yum clean all "
CMD_YUM_REPOLIST = "/usr/bin/yum repolist "

def repolist_dict(reponame=None):
    if reponame is not None:
        repolist_res = run("{0} {1}".format(CMD_YUM_REPOLIST,reponame))
    else:
        with hide('output'):
            """ If no reponame given then yum can be quite chatty so hide() """
            run(CMD_YUM_CLEAN_ALL)
            repolist_res = run(CMD_YUM_REPOLIST)
    repolist_lines = cmdbin.run_res_lines_stripped(repolist_res)
    repo_dict = {}
    for line_unstripped in repolist_lines:
        line = line_unstripped.strip()
        if line.startswith('Load') or line.startswith('Determining'):
            pass
        elif line.startswith('repolist') or line.startswith('repo id'):
            pass
        elif line.lstrip().startswith('*'):
            if 'base:' in line:
                print line
            else:
                pass
        else:
            try:
                repo_line_array = line.split()
                label = repo_line_array[0]
                repo_dict[label]=' '.join(repo_line_array[1:])
            except Exception:
                raise
    return repo_dict


def has_epel():
    has_flag = False
    repolist = repolist_dict()
    if len(repolist) > 0 and 'epel' in repolist:
        has_flag = True
    return has_flag


def rpm_status_installed1(packagename):
    """ Return a simple True or False - does the package
    appear to be ok installed?
    TODO: this is NOT IMPLEMENTED yet as code to check return code needs adding!!!
    """
    if packagename is None or len(packagename) < 2:
        return False
    installed = False
    query_line = "{0} {1}".format(CMD_RPM_QUERY,packagename)
    with settings(warn_only=True):
        """ Warning only now set. Next hide """
        with hide('stdout','warnings'):
            status_res = run(query_line)
            if 0 == status_res.return_code:
                installed = True
    return installed


def rpm_status_installed1searching(packagename):
    """ Return a simple True or False - does the package
    appear to be ok installed?
    Implemented as a non negative in that NOT finding 'not installed' is a positive
    Needs some coded tests added to tests directory and further testing!
    """
    if packagename is None or len(packagename) < 2:
        return False
    installed = False
    query_line = "{0} {1}".format(CMD_RPM_QUERY,packagename)
    with settings(warn_only=True):
        """ Warning only now set. Next hide """
        with hide('stdout','warnings'):
            status_res = run(query_line)
            installed = True
            for line in cmdbin.run_res_lines_stripped_searching(status_res,' not installed'):
                installed = False
                break
    return installed


def requires_binary_package(binary_unpathed=None,packagename_suggestion=None,
                            assumeyes=False):
    """ The word 'binary' here refers to the fact that you supply a binary
    name as the first argument. Two examples follow:
    requires_res = requires_binary_package('cat','coreutils')
    requires_binary_package('firewall-cmd','firewalld',assumeyes=True)
    """
    which_res = None
    requires_which = cmdbin.CMD_KERN_RELEASE
    with settings(warn_only=True):
        which_res = run("{0} {1}".format(cmdbin.CMD_WHICH,binary_unpathed))

    #cmdbin.print_success_and_failed(which_res)
    if which_res and which_res.succeeded:
        """ .succeeded is supported in Fabric 1.0 and newer """
        requires_which = which_res
        print(which_res)
        return requires_which
    elif which_res is not None:
        if assumeyes:
            cmdbin.run_or_sudo("{0} {1}".format(
                    CMD_YUM_ASSUMEYES_INSTALL,packagename_suggestion))
        else:
            cmdbin.run_or_sudo("{0} {1}".format(CMD_RPM_INSTALL,
                    packagename_suggestion))

        with settings(warn_only=True):
            which_res = run("{0} {1}".format(cmdbin.CMD_WHICH,binary_unpathed))

        if which_res and which_res.succeeded:
            requires_which = which_res
            print(which_res)
            return requires_which
    else:
        return which_res
    return False


