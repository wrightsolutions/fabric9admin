#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

""" Functions for querying cgroup binaries and settings """

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
import re
try:
    from . import cmdbin
except ValueError:
    import cmdbin
from . import conf_get_set as getset

CMD_CGSET="/usr/bin/cgget "
CMD_CGSET_H="/usr/bin/cgget -h "
CMD_CGSETRED="/bin/cgget "
CMD_CGSETRED_H="/bin/cgget -h "
CMD_LSCGROUP="/usr/bin/lscgroup "
CMD_LSCGROUP_H="/usr/bin/lscgroup -h "
CMD_CGROUPSMOUNT1="/bin/cgroups-mount "
CMD_CGROUPSMOUNT2="/usr/bin/cgroups-mount "
CGCONFIG_CONF='/etc/cgconfig.conf'
CGRULES_CONF='/etc/cgrules.conf'



def cgroup_binary():
    binary_dict = {}
    with settings(warn_only=True):
        with hide('output'):
            bin_res = cmdbin.run(CMD_LSCGROUP_H)
            if bin_res.succeeded:
                binary_dict['lscgroup']=CMD_LSCGROUP_H.split()[0]
        with hide('output'):
            bin_res = cmdbin.run(CMD_CGSET_H)
            if bin_res.succeeded:
                binary_dict['cgset']=CMD_CGSET_H.split()[0]
            else:
                bin_res = cmdbin.run(CMD_CGSETRED_H)
                if bin_res.succeeded:
                    binary_dict['cgset']=CMD_CGSETRED_H.split()[0]
        if len(binary_dict) < 1:
            """ Extra checking for cgroups-lite """
        with hide('output'):
            """ cgroups-mount returns 0 if already mounted """
            bin_res = cmdbin.run(CMD_CGROUPSMOUNT1)
            if bin_res.succeeded:
                binary_dict['cgroups-mount']=CMD_CGROUPSMOUNT1
    return binary_dict


def cgroup_binaries_present():
    if len(cgroup_binary()) > 0:
        return True
    return False


def cgroup_config():
    config_dict = {}
    with settings(warn_only=True):
        config_line = "{0} {1};".format(cmdbin.CMD_FGREP_L_GROUP,CGCONFIG_CONF)
        with hide('output'):
            config_res = cmdbin.run(config_line)
        if config_res.succeeded:
            config_dict['cgconfig']=CGCONFIG_CONF
        config_line = "{0} {1};".format(cmdbin.CMD_FGREP_L_GROUP,CGRULES_CONF)
        with hide('output'):
            config_res = cmdbin.run(config_line)
        if config_res.succeeded:
            config_dict['cgrules']=CGRULES_CONF
    return config_dict


def cgroup_config_present():
    if len(cgroup_config()) > 0:
        return True
    return False


def cgroup_mounts():
    mounts = []
    with settings(warn_only=True):
        grep_line = "{0} 'cgroup' /proc/mounts".format(cmdbin.CMD_FGREP)
        with hide('warnings'):
            grep_res = cmdbin.run_or_sudo(grep_line)
            mounts = cmdbin.run_res_lines_stripped(grep_res)
    return mounts


def cgroup_mountpoints():
    mounts = []
    if exists('/mnt/cgroups'):
        mounts.append('/mnt/cgroups')
    if exists('/sys/fs/cgroup'):
        mounts.append('/sys/fs/cgroup')
    if exists('/cgroups'):
        mounts.append('/cgroups')
    return mounts


def cgroup_mounted():
    """ cgroups_mounted and cgroup_mounted perform same """
    if len(cgroup_mounts()) > 0:
        return True
    return False


def cgroups_mounted(*args, **kwargs):
    """ cgroups_mounted alias of cgroup_mounted """
    return cgroup_mounted(*args, **kwargs)


def cgroup_summary(print_flag=False):
    cgroup_flag = False
    config_dict = {}
    binary_dict = {}
    #if cgroup_binaries_present():                                                                   
    if cgroups_mounted():
        config_dict = cgroup_config()
        if print_flag:
            print config_dict
	pretty_tuple = cmdbin.pretty_name_tuple()
        if pretty_tuple[1] in cmdbin.DEBIAN_DERIVATIVES2:
            """ Ubuntulike so might use cgroups-lite """
        elif pretty_tuple[1] in cmdbin.RPM_BASED2:
            pass
        else:
            binary_dict = cgroup_binary()
            if print_flag:
                print binary_dict
    if len(config_dict) > 0 and len(binary_dict) > 0:
        cgroup_flag = True
    return cgroup_flag

