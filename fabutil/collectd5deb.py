#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from collections import Counter,defaultdict,OrderedDict
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
#from fabric.contrib.files import exists,append
from os import path
import re
from string import ascii_lowercase,digits,printable,punctuation,whitespace
try:
	from . import cmdbin
except ValueError:
	import cmdbin
from . import conf_get_set as getset
from . import files_directories as files_dirs
from . import package_deb as pdeb
from . import web_cgi as webcgi


CMD_COLLECTD = '/usr/sbin/collectd'
CMD_COLLECTD_SYNTAX_CHECK = '/usr/sbin/collectd -t'
CMD_COLLECTD_PLUGINS_CHECK = '/usr/sbin/collectd -T'
NAGIOS_FPATH_INITD = '/etc/init.d/nagios3'
#COLLECTD_PACKAGE = 'collectd'
# Above is too heavy for most servers and collectd-core 
COLLECTD_PACKAGE_CORE = 'collectd-core'
COLLECTD_PACKAGE = 'collectd'
""" collectd-core includes the following two files:
/usr/share/doc/collectd-core/examples/collectd.conf
/usr/share/doc/collectd-core/examples/collection.cgi
"""
COLLECTD_APIPAGE_PACKAGE_ENTITIES = 'libhtml-parser-perl'
#COLLECTD_APIPAGE_PACKAGE_PERLCGI = ''
COLLECTD_FPATH_DEFAULT = '/etc/default/collectd'
COLLECTD_FPATH_INITD = '/etc/init.d/collectd'
COLLECTD_CONF = '/etc/collectd/collectd.conf'
COLLECTD_CONF_D = '/etc/collectd/collectd.conf.d'
COLLECTD_CONF_TEMPLATE = '/usr/share/doc/collectd-core/examples/collectd.conf'
COLLECTD_DEFAULT_54 = 'files/collectd54default.conf'
COLLECTD_LOG='/var/log/collectd.log'

GRAPHITE_WHISPER_FPATH = '/var/lib/graphite/whisper'

conf1lines = '''
# Config file for collectd(1).
#
# Some plugins need additional configuration and are disabled by default.
# Please read collectd.conf(5) for details.
#
# You should also read /usr/share/doc/collectd-core/README.Debian.plugins
# before enabling any more plugins.

#IncludeDir "/my/include/path"
<Include "/etc/collectd/collectd.conf.d">
        Filter "*.conf"
</Include>
'''
conf2lines = '''
# Config file for collectd(1).
#
# Some plugins need additional configuration and are disabled by default.
# Please read collectd.conf(5) for details.
#
# You should also read /usr/share/doc/collectd-core/README.Debian.plugins
# before enabling any more plugins.

##############################################################################
# Global                                                                     #
#----------------------------------------------------------------------------#
# Global settings for the daemon.                                            #
##############################################################################

#Hostname "localhost"
FQDNLookup true
#BaseDir "/var/lib/collectd"
#PluginDir "/usr/lib/collectd"
#TypesDB "/usr/share/collectd/types.db" "/etc/collectd/my_types.db"

#----------------------------------------------------------------------------#
# When enabled, plugins are loaded automatically with the default options    #
# when an appropriate <Plugin ...> block is encountered.                     #
# Disabled by default.                                                       #
#----------------------------------------------------------------------------#
#AutoLoadPlugin false

#----------------------------------------------------------------------------#
# Interval at which to query values. This may be overwritten on a per-plugin #
# base by using the Interval option of the LoadPlugin block:                 #
#   <LoadPlugin foo>                                                         #
#       Interval 60                                                          #
#   </LoadPlugin>                                                            #
#----------------------------------------------------------------------------#
#Interval 10
Interval 30

#Timeout 2
Timeout 2
#ReadThreads 5
ReadThreads 6
#WriteThreads 5
WriteThreads 3

# Limit the size of the write queue. Default is no limit. Setting up a limit
# is recommended for servers handling a high volume of traffic.
WriteQueueLimitHigh 1000000
WriteQueueLimitLow   800000

##############################################################################
# Logging                                                                    #
#----------------------------------------------------------------------------#
# Plugins which provide logging functions should be loaded first, so log     #
# messages generated when loading or configuring other plugins can be        #
# accessed.                                                                  #
##############################################################################

LoadPlugin logfile
#LoadPlugin syslog

<Plugin logfile>
	LogLevel "info"
#	File STDOUT
#	File "/var/log/collectd.log"
	Timestamp true
	PrintSeverity false
</Plugin>

#<Plugin syslog>
#	LogLevel info
#</Plugin>

#IncludeDir "/my/include/path"
<Include "/etc/collectd/collectd.conf.d">
        Filter "*.conf"
</Include>
'''

write_graphite_lines = '''
<Plugin write_graphite>
	<Node "example">
		Host "localhost"
		Port "2003"
		Protocol "tcp"
		LogSendErrors true
		Prefix "collectd"
		Postfix "collectd"
		StoreRates true
		AlwaysAppendDS false
		EscapeCharacter "_"
		</Node>
</Plugin>
'''


def collectd_install(packagename=None,extended_list=False,recommends=False):
	""" Supply alternative packagename argument if you do not want the db supporting
	mysql version of ndoutils installed
	"""
	installed_version = 0
	rrdplus_list = ['librrd4','librrds-perl']
	if extended_list is True:
		rrdplus_list = ['librrd4','librrds-perl','libxml2','libcurl3-gnutls','iptables']

	requires_res = pdeb.requires_packages_noninteractive6(rrdplus_list,True,recommends,
                                                              False,True,False)
	if requires_res:
                (pname, pretty_name_id, pretty_name_word, version_major) = cmdbin.pretty_name_tuple()
		# apt-get --no-install-recommends install collectd-core librrd4 librrds-perl
		if packagename is None:
                        if version_major < 6:
                                packagename = COLLECTD_PACKAGE_CORE
                        else:
                                # Debian >= 6 package name is 'collectd' which pulls in 'collectd-core' also
                                packagename = COLLECTD_PACKAGE
		# Default to noninteractive so not prompted at all
		requires_res = pdeb.requires_packages_noninteractive([packagename],True,False,False)
	if requires_res:
		installed_version = pdeb.dpkg_installed_version_numeric(packagename)
	return installed_version


def collectd_restart():

    actioned_flag = False

    with settings(warn_only=True):
        service_res = sudo("{0} restart".format(COLLECTD_FPATH_INITD))
        if service_res.succeeded:
            actioned_flag = True

    return actioned_flag


def collectd_start():

    actioned_flag = False

    with settings(warn_only=True):
        service_res = sudo("{0} start".format(COLLECTD_FPATH_INITD))
        if service_res.succeeded:
            actioned_flag = True

    return actioned_flag


def collectd_stop():

    actioned_flag = False

    with settings(warn_only=True):
        service_res = sudo("{0} stop".format(COLLECTD_FPATH_INITD))
        if service_res.succeeded:
            actioned_flag = True

    return actioned_flag


def collectd_status():

    actioned_flag = False

    with settings(warn_only=True):
        service_res = sudo("{0} status".format(COLLECTD_FPATH_INITD))
        if service_res.succeeded:
            actioned_flag = True

    return actioned_flag


def detailed_extra(config_path,print_flag=True):
    lines = None
    with getset.Keyedspace(config_path) as conf:
        lines = conf.summary_detailed_extra(print_flag,config_path)
        """ Above we are giving config_path as second parameter purely as prefix text 
        Use anything you want for prefix when calling summary_detailed_extra(...,...)
        """
    return lines


def detailed_extra_collectd(config_path=COLLECTD_CONF,print_flag=True):
    lines = None
    with getset.Keyedspace(config_path) as conf:
        lines = conf.summary_detailed_extra(print_flag,config_path)
        """ Above we are giving config_path as second parameter purely as prefix text 
        Use anything you want for prefix when calling summary_detailed_extra(...,...)
        """
    return lines


def detailed_enabled(config_path,print_flag=False):
    lines = []
    with getset.Keyedspace(config_path) as conf:
        lines = conf.lines_enabled()
    if print_flag:
        for line in lines:
            print line
    return lines


def detailed_enabled_collectd(config_path=COLLECTD_CONF,print_flag=False):
    lines = []
    with getset.Keyedspace(config_path) as conf:
        lines = conf.lines_enabled()
    if print_flag:
        for line in lines:
            print line
    return lines


def lines_enabled_collectd():
    return detailed_enabled_collectd(print_flag=True)


def collectd_syntax_check(verbose=False):
    """ /usr/sbin/collectd -t """

    ok_flag = False

    with settings(warn_only=True):
        if verbose is True:
            collectd_res = sudo(CMD_COLLECTD_SYNTAX_CHECK)
        else:
            with hide('output'):
                collectd_res = sudo(CMD_COLLECTD_SYNTAX_CHECK)

        if collectd_res.succeeded:
            ok_flag = True

    return ok_flag


def collectd_plugins_check(verbose=True):
    """ /usr/sbin/collectd -T
    capital T means plugins
    """

    ok_flag = False

    with settings(warn_only=True):
        if verbose is True:
            collectd_res = sudo(CMD_COLLECTD_PLUGINS_CHECK)
        else:
            with hide('output'):
                collectd_res = sudo(CMD_COLLECTD_PLUGINS_CHECK)

        if collectd_res.succeeded:
            ok_flag = True
	else:
            print('Testing using -T failed. Does /etc/collectd/collectd.conf exist?')

    return ok_flag


def collectd_interval(secs=None):
	""" Force ndo to send via tcp socket on specified port
	"""
	interval_secs = None
	if secs is None:
		#lines = def detailed_enabled_collectd(COLLECTD_CONF,False)
		with getset.Keyedspace(COLLECTD_CONF) as conf:
			if 'interval' in conf:
				interval_secs = conf['interval']
	"""
    with getset.Keyedspace(COLLECTD_CONF) as conf:
        if verbosity > 1:
            print conf.lines_enabled_contains(keyname)
        if keyname in conf:
            vianow = conf[keyname]
        if verbosity > 1:
            print("{0} from config file {1}".format(vianow,config_path))

        if vianow is None:
            return made_tcp

        if vianow == val2:
            print "Is output_type already {0}? No update actioned.".format(val2)
            return True

        update_res = conf.context_update(keyname,val2,2)
        #update_res = conf.context_update_localhost('interface',2,True,cmdbin.LOOPBACK4)
        if len(update_res) < 1:
            if verbosity > 1:
                print "context_update({0},...) returned {1}".format(keyname,update_res)
        else:
            if verbosity > 1:
                print conf.lines_for_put_containing(keyname)

            sedlines = conf.sedlines((verbosity>1))
            sedres = getset.sed_remote_apply(cmdbin.MD5A32,config_path,sedlines,
                                             'medium',True,0.75,True)
            if verbosity > 1:
                print("sedres={0}".format(sedres))
            if sedres is True:
                made_tcp = True
            else:
                print "sed_remote_apply returned {0}".format(sedres)
                print conf.lines_for_put_containing(keyname)
                print conf.lines_current_containing(keyname)
                # Make the change on the remote system
"""
	return interval_secs


def collectd_configure_default(filename_default='collectd_default.conf'):
    copy_cmd = "{0} --verbose {1} /etc/collectd/{2}".format(
        cmdbin.CMD_CP_NOCROSS_PRESERVETIME,
        COLLECTD_CONF_TEMPLATE,filename_default)
    ros_res = cmdbin.run_or_sudo(copy_cmd)
    return (ros_res.succeeded is True)


def collectd_configure_split(confnum=2):
	conf_lines = conf1lines
	if confnum > 1:
		conf_lines = conf2lines
	put_flag = cmdbin.put_using_named_temp_sudo(COLLECTD_CONF,conf_lines)
	if put_flag:
		dir_flag = files_dirs.mkdir_existing(COLLECTD_CONF_D,2755,False,True,False)
	return (put_flag and dir_flag)


def collection_page_support(verbose=True):
	""" Return (and print feedback) about the site-enabled and in particular
	the presence of /usr/lib/cgi-bin
	Signature of called requires_dpkg_package4(packagename,assumeyes,recommends,verbose=False)
	called requires_dpkg_package4 will return -1 if libhtml-parser-perl package not installed.
	"""

	supported_flag = False
	installed_version = pdeb.requires_dpkg_package4(COLLECTD_APIPAGE_PACKAGE_ENTITIES,True,False,verbose)

	if installed_version >= 0:
		cgibin_enabled_count = webcgi.usr_lib_cgibin_count('raise')
		if cgibin_enabled_count > 0:
			supported_flag = True

	if verbose:
		if supported_flag is True:
			print("Grepping of sites-enabled indicates /usr/lib/cgi-bin may be enabled")
		else:
			print("Grepping of sites-enabled indicates /usr/lib/cgi-bin is likely NOT enabled")
				
	return supported_flag


def collectd_plugin_write_graphite(enable=True,verbosity=1):
        """ Typically a process using port 2003 and including following switches:
        --pidfile=/var/run/carbon-cache.pid --logdir=/var/log/carbon/
        """
        enabled = False
        #collectd_configure_default()
        write_graphite_fpath = "{0}/write_graphite.conf".format(COLLECTD_CONF_D)
        dir_flag = files_dirs.mkdir_existing(COLLECTD_CONF_D,2755,False,True,False)
        with cd(COLLECTD_CONF_D):
                put_flag = cmdbin.put_using_named_temp_sudo(write_graphite_fpath,write_graphite_lines)
                if put_flag:
                        enabled = True

        if verbosity > 0:
                find_cmd = "find {0} -mindepth 1 -maxdepth 1 -type d ! -name 'carbon'".format(GRAPHITE_WHISPER_FPATH)
                ros_res = cmdbin.run_or_sudo(find_cmd)
                graphite_dirs = cmdbin.run_res_lines_stripped_searching1(ros_res,'/graphite/')
                for gdir in graphite_dirs:
                        print("Graphite dir: {0}".format(gdir))

        plugin = None
        keyname = "LoadPlugin write_graphite"
        gen_prefix = 'write_graphite'
        # The choice of Keyedequals2 is very deliberate next so retest thoroughly before considering a change of class
        with getset.Keyedequals2(COLLECTD_CONF) as conf:
                if verbosity > 1:
                        print conf.lines_enabled_contains(keyname)

                keyname_hashed = "{0}{1}".format(chr(35),keyname)
                # Signature: keys_disabled_printable(self,safe=True,hashless=False)
                if keyname_hashed in conf.keys_disabled_printable(True,False):
                        plugin = keyname
                if verbosity > 0:
                        print "{0} keyname {1} printable but disabled.".format(gen_prefix,keyname_hashed)
                        
                """ unhash_res = conf.context_unhash(keyname_hashed,verbosity,False)
                unhash_res = conf.context_unhash(keyname,verbosity,True)
                Preferred: unhash_res = conf.context_unhash(keyname,verbosity,False,True)
                """
                unhash_res = conf.context_unhash(keyname,verbosity,False,True)
                if unhash_res is True:
                        """ Use 1.0 rather than 0.75 as often /etc/locale.gen is all commented
                        out and our change would be the first enabled key
                        sedres = getset.sed_remote_apply(None,conf_filepath,conf.sedlines(),
                           'medium',True,0.75,True)  <-- Use 1.0 instead
                        """
                        sedres = getset.sed_remote_apply(None,COLLECTD_CONF,conf.sedlines(),
                                                         'medium',True,1.0,True)
                        if sedres is not True:
                                plugin = None
                elif verbosity > 1:
                        print(unhash_res)
                        print(conf.keys_disabled(True))
                        print(conf.keys_disabled(False))
                else:
                        pass

                keys_enabled_printable = conf.keys_enabled_printable()
                if len(keys_enabled_printable) < 1:
                        conf.keys_disabled5print()
                if keyname in keys_enabled_printable:
                        plugin = keyname
                        if verbosity > 1:
                                print "{0} keyname {1} already enabled. Simply return True".format(gen_prefix,keyname)
                        elif keyname in conf.keys_disabled_printable():
                                plugin = keyname
                                if verbosity > 0:
                                        print "{0} keyname {1} printable but disabled.".format(gen_prefix,keyname)
                                        unhash_res = conf.context_unhash(keyname_hashed,verbosity)
        
        return (plugin == keyname)

