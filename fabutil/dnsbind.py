#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

""" DNS specifically and anything related to bind dns

Note: Many of the configuration files related to dns are very specific
formats here are two things which mark them out as variants or special:
(.) options allows multiple values whereas generic Keyedfile is really one key -> one value
(.) options allows values that are themselves delimited Example: options timeout:2
See augeas for a pre templated solution to parsing non ini configuration files
See Keyedcolon() subclass of conf_get_set for a key value parser that forces colon (:)
as separator.

For the nameserver functions we use keylines_typed() rather than
straight dictionary lookups because the underlying OrderedDict is
designed to keep non-duplicates (in the dict) and we know
and expect [enabled] duplicates in our resolv.conf (Example: two nameserver entries)

To access two [enabled] lines shown below via a "duplicates replace" key value api
we use query the underlying data using keylines_typed(1,...) instead of straight 'in':
domain cable.buried.net
search cable.buried.net buried.net
nameserver 8.8.8.8
nameserver 8.8.4.4
"""



from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
import re
from . import cmdbin
from . import conf_get_set as getset
from . import files_directories as files_dirs

CMD_RNDC = "/usr/sbin/rndc "
CMD_RNDC_STATUS = "/usr/sbin/rndc status"
CMD_RNDC_QUERYLOG = "/usr/sbin/rndc querylog"
CMD_RNDC_STATS = "/usr/sbin/rndc stats"
# Next rndc-confgen binary is included in main 'bind' on Centos, but Debian in 'bind9utils'
CMD_RNDC_CONFGEN_A = "/usr/sbin/rndc-confgen -a -r /dev/urandom"
CMD_RNDC_CONFGEN_ROOT_OUTPUT = "/usr/sbin/rndc-confgen -r /dev/urandom > /root/rndc.conf"
CMD_RNDC_CONFGEN_ROOT_OUTPUT_TEMPLATE = "/usr/sbin/rndc-confgen -r /dev/urandom -b {0} > /root/rndc.conf"
# Dig is in a utils package, Centos 'bind-utils' whereas Debian provides dig in 'dnsutils'
CMD_DIG = "/usr/bin/dig "
CMD_DIG_TXT = "/usr/bin/dig -t txt "
CMD_DIG_VERSION = "/usr/bin/dig -v;"

RE_IN_TXT_SPF = re.compile(r'.*\d+.*IN\WTXT\W')

RESCONF = '/etc/resolv.conf'

GOOGLE4LIST = ['8.8.8.8','8.8.4.4']
GOOGLE6LIST = ['2001:4860:4860::8888','2001:4860:4860::8844']
GOOGLELIST = GOOGLE4LIST[:]
GOOGLELIST.extend(GOOGLE6LIST)


def ptype_lookup(verbose=False):
	""" Returns 'deb' or 'rpm' or None if unable to determine which.
	if calling this from a ...context_update_ptype() or remote_ptype() then
	you probably want to supply verbose=True so have positive confirmation
	in the output as to which ptype was determined.
	"""
	ptype = cmdbin.deb_or_rpm()
	if ptype is None:
		print('ptype_lookup() function unable to determine if deb or rpm like')
	ptype_message = None
	if 'deb' == ptype:
		pass
	elif 'rpm' == ptype:
		pass
		#md5before = MD5BINDCONF_CENTOS7
	else:
		pass
	ptype_message = "ptype='{0}' detected in ..._remote_ptype()".format(ptype)
	if verbose is True and ptype_message is not None:
		print(ptype_message)
	return ptype


def bind_conf_filepath(onerror='/dev/null'):
	""" Returns an appropriate filepath based on the distibution type """
	ptype = ptype_lookup()
	if 'deb' == ptype:
		conf_filepath = '/etc/bind/named.conf'
	elif 'rpm' == ptype:
		conf_filepath = '/etc/named.conf'
	else:
		conf_filepath = onerror
	return conf_filepath


def rn_status(verbosity=0):
	""" rndc status and do some minor testing of return code / output """
	if verbosity < 1:
		rlines = cmdbin.run_or_sudo(CMD_RNDC_STATUS)
	else:
		with hide('output'):
			rres = cmdbin.run_or_sudo(CMD_RNDC_STATUS)
        rlines  = cmdbin.run_res_lines_stripped(rres)
	if rres.succeeded:
		lines = rlines
	elif len(rlines) > 0:
		# Failed and something useful to report
		if verbosity > 0:
			print("rndc issue detailed")
		lines = []
	else:
		# Failed but no feedback to analyse
		if verbosity > 0:
			print("rndc issue (non-specific)")
		lines = []

	if len(lines) < 1:
		return lines

	for line in rlines:
		if 'could not load' in line or 'connection refused' in line:
			if verbosity > 0:
				print("rndc issue: {0}".format(line))
			lines = []
	return lines


def rn_status_print(verbose=True):
	""" Wrapper around rn_status which prints returned lines 
	verbosity supplied to rn_status is 1 or 2
	"""
	verbosity = 1+(verbose is True)
	lines = rn_status(verbosity)
	if verbose is True:
		for line in lines:
			if 'query logging is O' in line:
				lines.append("# Note: {0}".format(line))
	return lines


def rn_confgen_a_lines():
	""" confgen for rndc suitable for typical (low effort) setup on Centos
	-a is used in the command hence the _a_ in the function name
	"""
	genres = cmdbin.run_or_sudo(CMD_RNDC_CONFGEN_A)
	lines = []
        rlines  = cmdbin.run_res_lines_stripped(genres)
	if genres.succeeded:
		lines = rlines
	return lines


def rn_confgen_a():
	""" confgen for rndc suitable for typical (low effort) setup on Centos """
	lines = rn_confgen_a_lines()
        print('Having completed the generation, we now show typical key path (ls style)')
        print('...if no output showing rndc.key is shown then we encountered an issue.')
	if len(lines) > 0:
		files_dirs.dirpath_dlp(dirpath='/etc/rn*.key')
	for line in lines:
		print(line)
	return


def rn_conf_remove(copy_dest='/root/'):
	""" Removal of /etc/rndc.conf with optional copy keeping """
	# Not currently implemented
	return


def rn_confgen_root_output(keysize=256):
	if 256 == keysize:
		genres = cmdbin.run_or_sudo(CMD_RNDC_CONFGEN_ROOT_OUTPUT)
	else:
		cmd_rndc_confgen = CMD_RNDC_CONFGEN_ROOT_OUTPUT_TEMPLATE.format(keysize)
		genres = cmdbin.run_or_sudo(cmd_rndc_confgen)
	lines = []
        rlines  = cmdbin.run_res_lines_stripped(genres)
	if genres.succeeded:
		lines = rlines
		files_dirs.dirpath_dlp(dirpath='/root/rndc.conf')
	return lines


def rn_querylog_toggle(verbose=False):
	""" Toggle querylog via rndc command
	if verbose, then the current value is printed before the toggle is done
	"""
	if verbose is True:
		rlines = rn_status(verbosity=1)
		for rline in rlines:
			if 'query logging is O' in rline:
				print(rline.replace(' is ',' was '))
	togres = cmdbin.run_or_sudo(CMD_RNDC_QUERYLOG)
	if togres.succeeded and verbose:
		print('Toggle of query logging completed - new state in effect.')
        rlines = cmdbin.run_res_lines_stripped(togres)
	return rlines


def config_summary(print_flag=False,line_prefix=None):
	""" Summary of the sshd config directives that are most important
	( defined in EXPRESSIONS in the function )
	Function amended to use Keyedspace2() rather than Keyedspace() for consistency.
	"""
	conf_filepath = bind_conf_filepath()
	EXPRESSIONS=['^include','listen-on',
				'inet']
	lines = []
	with getset.Keyedspace2(conf_filepath) as conf:
		lines = conf.lines_noncomment_containing_any(EXPRESSIONS)
	if print_flag is True and line_prefix is not None:
		if line_prefix.lower() == 'filepath':
			for line in lines:
				print "{0}: {1}".format(conf_filepath,line.rstrip())
		elif line_prefix.lower() == 'filename':
			filename = path.basename(conf_filepath)
			for line in lines:
				print "{0}: {1}".format(filename,line.rstrip())
		else:
			for line in lines:
				print "{0}{1}".format(line_prefix,line.rstrip())
	elif print_flag is True:
		for line in lines:
			print line.rstrip()
	return lines


def config_keys_enabled_printable(print_flag=False):
	""" Returns from main bind config key value format, the keys that are enabled
	and printable
	"""
	klist = []
	with getset.Keyedspace2(bind_conf_filepath()) as conf:
		klist = conf.keys_enabled_printable()
	if print_flag is True:
		for key in klist:
			print(key)
        return klist


def config_keys_enabled(print_flag=False):
	""" Returns from main bind config key value format, the keys that are enabled.
	supply print_flag True if you want this function to iterate over results and print.
	"""
	klist = []
	with getset.Keyedspace2(bind_conf_filepath()) as conf:
		klist = conf.keys_enabled()
	if print_flag is True:
		for key in klist:
			print(key)
        return klist


def config_keylines_enabled(linesep_remove=False):
	""" Returns important content from the main bind conf - namely those lines
	of the form key: val and excludes #key: val
	"""
	enabled = []
	with getset.Keyedspace2(bind_conf_filepath()) as conf:
		enabled = conf.keylines_enabled_resplit()
	return enabled


def config_keylines_enabled_linesep(linesep_remove=False):
	""" Returns important content from the main bind conf - namely those lines
	of the form key: val and excludes #key: val
	"""
	with getset.Keyedspace2(bind_conf_filepath()) as conf:
		enabled = conf.keylines_enabled()
		if linesep_remove:
			unsplit = ''.join(enabled)
			#print "conf_linesep:{0} {0} {0}".format(conf.conf_linesep)
			returned = unsplit.split(conf.conf_linesep)
		else:
			returned = enabled
	return returned


def config_keylines_disabled(verbose=False):
	""" Returns commented / hashed content from the main bind conf -
	namely those lines that are useful comments but not directives actioned
	"""
	dlines = []
	with getset.Keyedspace2(bind_conf_filepath()) as conf:
		dlines = conf.keylines_disabled()
	if verbose is True:
		for line in dlines:
			print(line.rstrip())
	return dlines


def config_reset(verbose=True):
	""" Reset the config [and binaries] using rpm -V so that default sshd_config """
	conf_filepath = bind_conf_filepath(onerror=False)
	if conf_filepath:
		str_rpmv = "md5sum {0} && rpm -V bind;".format(conf_filepath)
		str_mv = "mv --target-directory=/tmp/ {0}".format(conf_filepath)
		str_yum = "yum --assumeyes reinstall bind"
		str_combined = "{0} {1} && {2}".format(str_rpmv,str_mv,str_yum)
		reset_res = run(str_combined)
		if reset_res.succeeded:
			print('Bind config (named.conf) restored to repo default')
		md5text = cmdbin.md5text_or_sudo(conf_filepath)
		if verbose is True:
			print("After completion of ..._reset() have md5={0}".format(md5text))
			print("...see /tmp/ for a copy of previous conf before the reset.")
	else:
		# Unable to set a valid conf_filepath, likely due to ptype None
		# So run ptype_lookup() again verbose to give user more feedback on why/how
		ptype_lookup(verbose=True)
	return reset_res


def listenon_line(verbose=False):
	""" return [and print] the listen-on line that is currently enabled
	also print when verbose=True
	returns listenon line when listenon is hashed / commented rather than enabled
	returns '' when listenon is absent altogether
	"""
	listenon4line = ''
	# chr(35) is hash / octothorpe
	lhashed = "{0}listen-on".format(chr(35))
	with getset.Keyedspace2(bind_conf_filepath()) as conf:
		lines_enabled_matching = conf.lines_enabled_contains('listen-on')
		llines = []
		for line in lines_enabled_matching:
			if 'listen-on-v6' in line:
				continue
			llines.append(line)
			if verbose:
				print(line)
		if len(llines) > 0:
			listenon4line = llines[-1]
		elif lhashed in conf.keys_disabled_printable(True,False):
			if verbose is True:
				print('Note: listen-on is NOT currently enabled')
				#print "{0} keyname {1} printable but disabled.".format(gen_prefix,lhashed)
			unhash_res = conf.context_unhash(lhashed,verbosity=(verbose==True))
			if unhash_res and verbose is True:
				print('Note: listen-on could be unhashed to give an uncommented entry')
			#listenon4line = conf[lhashed]
			listenon4line = conf.keyline1rstripped(lhashed)
		else:
			pass
	return listenon4line


def listenon_enabled_line(verbose=False):
	""" return [and print] the listen-on line that is currently enabled
	also print when verbose=True
	returns 0 when listenon is hashed / commented rather than enabled
	returns -1 when listenon is absent altogether
	"""
	listenon4line = -1
	# chr(35) is hash / octothorpe
	lhashed = "{0}listen-on".format(chr(35))
	with getset.Keyedspace2(bind_conf_filepath()) as conf:
		lines_enabled_matching = conf.lines_enabled_contains('listen-on')
		llines = []
		for line in lines_enabled_matching:
			if 'listen-on-v6' in line:
				continue
			llines.append(line)
			if verbose:
				print(line)
		if len(llines) > 0:
			listenon4line = llines[-1]
		elif lhashed in conf.keys_disabled_printable(True,False):
			if verbose is True:
				print('Note: listen-on is NOT currently enabled')
				#print "{0} keyname {1} printable but disabled.".format(gen_prefix,lhashed)
			unhash_res = conf.context_unhash(lhashed,verbosity=(verbose==True))
			if unhash_res and verbose is True:
				print('Note: listen-on could be unhashed to give an uncommented entry')
			#listenon4line = conf[lhashed]
			#listenon4line = conf.keyline1rstripped(lhashed)
			listenon4line = 0
		else:
			listenon4line = -1

	return listenon4line


def config_any4_preview(verbose=False):
	preview_line = ''
	listenon4line = listenon_enabled_line(verbose=False)
	if listenon4line < 2:
		print("Not possible to preview listen-on 'any' as action is only to amend/uncomment (not possible)")
		return preview_line
	port53any = 'port 53 { any; };'
	# chr(35) is hash / octothorpe
	lhashed = "{0}listen-on".format(chr(35))
	with getset.Keyedspace2(bind_conf_filepath()) as conf:
		lines_enabled_matching = conf.lines_enabled_contains('listen-on')
		llines = []
		for line in lines_enabled_matching:
			if 'listen-on-v6' in line:
				continue
			llines.append(line)
			if verbose:
				print(line)
		if len(llines) > 0:
			listenon4line = llines[-1]
			cup_verbosity=0+(verbose is True)
			#conf.context_update(self,keyname,new_value,verbosity=0,unchanged_okay=False):
			#cup_res = conf.context_update(keyname,port53any,verbosity,unchanged_okay)
			cup_res = conf.context_update('listen-on',port53any,cup_verbosity,False)
			if cup_res:
				#preview_line = cup_res
				preview_line = conf.sedline1('listen-on',port53any)
		elif lhashed in conf.keys_disabled_printable(True,False):
			if verbose is True:
				print('Note: listen-on is NOT currently enabled')
				#print "{0} keyname {1} printable but disabled.".format(gen_prefix,lhashed)
			unhash_res = conf.context_unhash(lhashed,verbosity=(verbose==True))
			if unhash_res and verbose is True:
				print('Note: listen-on could be unhashed to give an uncommented entry')
			# Next show the preview in two parts (i) uncommenting , (ii) setting yes
			if unhash_res:
				preview_line = conf.context_unhash_constructed(lhashed,verbosity=(verbose==True))
			else:
				preview_line = conf.sedline1('listen-on',port53any)
		else:
			pass
	return preview_line
