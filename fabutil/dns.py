#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

""" DNS specifically and anything related to lookups / resolutions.

Note: Many of the configuration files related to dns are very specific
formats here are two things which mark them out as variants or special:
(.) options allows multiple values whereas generic Keyedfile is really one key -> one value
(.) options allows values that are themselves delimited Example: options timeout:2
See augeas for a pre templated solution to parsing non ini configuration files
See Keyedcolon() subclass of conf_get_set for a key value parser that forces colon (:)
as separator.

For the nameserver functions we use keylines_typed() rather than
straight dictionary lookups because the underlying OrderedDict is
designed to keep non-duplicates (in the dict) and we know
and expect [enabled] duplicates in our resolv.conf (Example: two nameserver entries)

To access two [enabled] lines shown below via a "duplicates replace" key value api
we use query the underlying data using keylines_typed(1,...) instead of straight 'in':
domain cable.buried.net
search cable.buried.net buried.net
nameserver 8.8.8.8
nameserver 8.8.4.4
options attempts:3
options timeout:2
options rotate
options inet6
#options single-request
options single-request-reopen
# From manpage of resolv.conf:
# The keyword and value must appear on a single line,
# and the keyword (e.g., nameserver) must start the line.
# The value follows the keyword, separated by white space.
# Any problems with 'options rotate' see following test
# python -c 'from socket import gethostbyname;print [ "Y" if gethostbyname("debian.org") else "." for x in range(10) ]'
"""



from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
import re
from . import cmdbin
from . import conf_get_set as getset

CMD_RESCONF = "/sbin/resolvconf "
CMD_RESCONF_UPDATE = "/sbin/resolvconf -u "
CMD_DRILL = "/usr/bin/drill "
CMD_DRILL4 = "/usr/bin/drill -4 "
CMD_DRILL6 = "/usr/bin/drill -6 "
CMD_DRILLTCP4 = "/usr/bin/drill -t -4 "
CMD_DRILLTCP6 = "/usr/bin/drill -t -6 "
#CMD_DRILL_TXT = "/usr/bin/drill example.com txt "
CMD_DRILL_VERSION = "/usr/bin/drill -v;"
CMD_DIG = "/usr/bin/dig "
CMD_DIG_TXT = "/usr/bin/dig -t txt "
CMD_DIG_VERSION = "/usr/bin/dig -v;"

RE_IN_TXT_SPF = re.compile(r'.*\d+.*IN\WTXT\W')

RESCONF = '/etc/resolv.conf'
RESCONF_DIR = '/etc/resolvconf'

GOOGLE4LIST = ['8.8.8.8','8.8.4.4']
GOOGLE6LIST = ['2001:4860:4860::8888','2001:4860:4860::8844']
GOOGLELIST = GOOGLE4LIST[:]
GOOGLELIST.extend(GOOGLE6LIST)


def drill_or_dig_domain(domain=None):
    domain_res = False
    if domain is None or len(domain) < 3:
        return False
    if False and exists(CMD_DRILL):
        lookup_res = run("{0} {1}".format(CMD_DRILL,domain))
        if lookup_res.failed:
            lookup_res = run("{0} {1}".format(CMD_DRILL4,domain))
            if lookup_res.failed:
                lookup_res = run("{0} {1}".format(CMD_DRILLTCP4,domain))
                if lookup_res.succeeded:
                    domain_res = True
            else:
                domain_res = True
        else:
            domain_res = True
    elif exists(CMD_DIG):
        lookup_res = run("{0} {1}".format(CMD_DIG,domain))
        if lookup_res.succeeded:
            domain_res = True
    else:
        domain_res = False
    return domain_res


def drill_or_dig_txt(domain=None,verbose=True):
    lookup_line = None
    if exists(CMD_DRILL):
        lookup_line = "{0} {1} txt".format(CMD_DRILL,domain)
    elif exists(CMD_DIG):
        lookup_line = run("{0} {1}".format(CMD_DIG_TXT,domain))
    if lookup_line is not None:
        lookup_res = run(lookup_line)
        if lookup_res.succeeded:
            domain_res = True
    return domain_res


def drill_or_dig_spf(domain=None,verbose=False,print_flag=False):
    """ Convenient helper for getting just spf related entries from
    txt query. Returns a list (empty if nothing found).
    Use drill_or_dig_txt() to analyse further if an empty list returned
    and you really think the domain has spf details entered.
    """
    if domain is None or len(domain) < 3:
        return False
    elif domain == 'random':
        from random import randint
        DOMAIN_LIST = ['codeenigma.com','dandomain.dk','dpdhost.co.uk','instiller.co.uk']
        idx = randint(0,(-1+len(DOMAIN_LIST)))
        #print "idx={0}".format(idx)
        domain = DOMAIN_LIST[idx]
    else:
        pass

    lookup_line = None
    return_lines = []
    if exists(CMD_DRILL):
        lookup_line = "{0} {1} txt".format(CMD_DRILL,domain)
    elif exists(CMD_DIG):
        lookup_line = "{0} {1}".format(CMD_DIG_TXT,domain)

    if lookup_line is not None:
        if verbose is True:
            lookup_res = run(lookup_line)
        else:
            with hide('output'):
                lookup_res = run(lookup_line)
        lookup_lines  = cmdbin.run_res_lines_stripped(lookup_res)
        for line in lookup_lines:
            if RE_IN_TXT_SPF.match(line):
                return_lines.append(line)

    if print_flag:
        for line in return_lines:
            print line
    return return_lines


def nameserver_count():
    count = 0
    with getset.Keyedfile(RESCONF) as conf:
        for line in conf.keylines_typed(1,['nameserver']):
            count += 1
    return count


def nameserver_count_excluding(exclude_list,verbosity=0):
    """ Count nameservers in RESCONF excluding those specified
    in exclude list. Supplying verbosity=1 prints the count.
    Supplying verbosity=2 is best if you want to see the full
    results of inclusion/exclusion.
    """
    count = 0
    COUNT_PREFIX = 'Nameserver Count (excluding requested ?non-routables?)'
    if hasattr(exclude_list, "__iter__") and type(exclude_list) != type(''):
        pass
    elif exclude_list == 'google':
        exclude_list = GOOGLELIST[:]
    elif print_flag == True:
        print "{0} is {1} (counting terminated early)".format(COUNT_PREFIX,count)
        return count
    else:
        return count
    with getset.Keyedfile(RESCONF) as conf:
        for line in conf.keylines_typed(1,['nameserver']):
            line_array = line.strip().split()
            if line_array[1] in exclude_list:
                pass
            else:
                if verbosity > 1:
                    print("non excluded nameserver line: {0}".format(line.rstrip()))
                count += 1
    if verbosity > 0:
        print "{0} is {1}".format(COUNT_PREFIX,count)
    return count


def nameserver_count_excluding_print(exclude_list,verbosity=2):
    """ Wrapper around nameserver_count_excluding that sets
    verbosity at appropriate level for full printing
    """
    count = nameserver_count_excluding(exclude_list,verbosity)
    return count


def nameserver_count_startswith(starts,print_flag=False,verbose=False):
    count = 0
    COUNT_PREFIX = 'Nameserver Count (startswith)'
    with getset.Keyedfile(RESCONF) as conf:
        for line in conf.keylines_typed(1,['nameserver']):
            line_array = line.strip().split()
            if line_array[1].startswith(starts):
                count += 1
            else:
                pass
    if print_flag == True:
        if count > 0 or verbose == True:
            print "{0} is {1}".format(COUNT_PREFIX,count)
    return count
    

def resolvconf_dirs_more(main_itself=False):
    more_res = False
    #which_res = run("{0} {1}".format(cmdbin.CMD_WHICH,'resolvconf'))
    exists_resconf = exists(CMD_RESCONF,verbose=True)
    if exists_resconf:
        ros_line = "{0} {1}/{2}/* {1}/{3}/*".format(cmdbin.CMD_FGREP_H_V_Z,
                                                    RESCONF_DIR,
                                                    'resolv.conf.d',
                                                    'update.d')
        ros_res = cmdbin.run_or_sudo(ros_line)
        if ros_res.succeeded:
            more_res = True
    return more_res

