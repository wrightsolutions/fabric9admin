#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from collections import OrderedDict
from os import path
import re
from string import ascii_lowercase,digits,printable,punctuation,whitespace
try:
    from . import cmdbin
except ValueError:
    import cmdbin
#import files_directories as files_dirs


""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

CMD_APT = "/usr/bin/apt-get "
CMD_APT_NORECOMMENDS = "/usr/bin/apt-get --no-install-recommends "
CMD_APT_NONINTERACTIVE = "DEBIAN_FRONTEND=noninteractive /usr/bin/apt-get "
CMD_APT_NONINTERACTIVE_NORECOMMENDS = "DEBIAN_FRONTEND=noninteractive /usr/bin/apt-get --no-install-recommends "
CMD_APT_ASSUMEYES = "/usr/bin/apt-get --assume-yes "
CMD_APT_ASSUMEYES_NORECOMMENDS = "/usr/bin/apt-get --no-install-recommends -y "
CMD_APT_ASSUMEYES_NORECOMMENDS_CONFOLD = "/usr/bin/apt-get \
 -o DPkg::Options::=--force-confold --no-install-recommends -y "
CMD_APT_ASSUMEYES_NORECOMMENDS_CONFMISS = "/usr/bin/apt-get \
 -o Dpkg::Options::=--force-confmiss --reinstall --no-install-recommends -y "
CMD_APT_UPDATE = "/usr/bin/apt-get update "
CMD_DGET = "/usr/bin/dget "
CMD_DGETDSC = "/usr/bin/dget -u "
CMD_DPKG = "/usr/bin/dpkg "
CMD_DPKG_CHECKBUILDDEPS = "/usr/bin/dpkg-checkbuilddeps "
CMD_DPKG_CHECKBUILDDEPS2TO1 = "/usr/bin/dpkg-checkbuilddeps  2>&1 "
CMD_DPKG_CHECKBUILDDEPS_PROCESSED = """/usr/bin/dpkg-checkbuilddeps  2>&1 \
| fgrep ': Unmet build dependencies' \
| sed 's/\(.*\)\(: Unmet build dependencies:\)\s*\(.*\)/\{0}/' \
| sed 's/([^)]*)//g'""".format(3)
CMD_DPKG_BUILDPACKAGEB = "/usr/bin/dpkg-buildpackage -b "
CMD_DPKG_DEB_BUILD = "/usr/bin/dpkg-deb --build "
CMD_DPKG_GENCONTROL = "/usr/bin/dpkg-gencontrol "
CMD_DPKG_SELECTIONS = "/usr/bin/dpkg --get-selections "
CMD_DPKG_STATUS = "/usr/bin/dpkg -s "
CMD_DPKG_SEARCH = "/usr/bin/dpkg --search "
CMD_DPKG_QUERY_STATUS = "/usr/bin/dpkg-query -W --showformat='${Status;-30}'"
CMD_DPKG_QUERY_STATUS3 = "/usr/bin/dpkg-query -W \
--showformat='${Status;-30} ${Package;-30} ${Version;-30}' "
CMD_DPKG_QUERY_SIZE = "dpkg-query -W --showformat='${Installed-Size;-10}\t${Package;-50}${Section;-30}\t\n'"
CMD_DPKG_QUERY_SIZE_ALL = "dpkg-query -W --showformat='${Installed-Size;-10}\t${Package;-50}${Section;-30}\t\n' \
 | sort -g"
CMD_DPKG_QUERY_STATUS4 = "/usr/bin/dpkg-query -W \
--showformat='${Installed-Size;-10}\t${Status;-30}\t${Section;-15}\t${Package;-30}\n' "
CMD_INFO_EITHER = "/bin/vdir /var/lib/dpkg/info/*.{list,md5sums}"
CMD_INFO_LIST = "/bin/vdir /var/lib/dpkg/info/*.list"
CMD_NETSELECT_APT = "/usr/bin/netselect-apt"

RE_STATUS_INSTALLED = re.compile('Status:.*installed')
RE_NO_PACKAGES_MATCHING = re.compile(r'no\spackages\sfound\smatching\b')
RE_OK_INSTALLED = re.compile(r'\bok\sinstalled\b')
RE_DOT_LIST = re.compile(r'.list\Z')
SELECTIONS2LIST = ['install','hold']
""" Above has 2 entries. Below has 4 as also includes 
status that indicate purged or marked for deinstall """
SELECTIONS4LIST = ['install','hold','deinstall','purge']

_list_dot_d_array = []
_sources_list_object = None


def list_dot_d_array_extend(extend_with):
    """ Used to as a helper to maintain a list of sources .list files
    to support cleanup functions which might want to undo any sources .list
    additions on abort / other controlled situation
    Example: files_directories.py abort_with_cleanup()
    """
    global _list_dot_d_array
    extended = True
    try:
        if hasattr(extend_with, "__iter__") and type(extend_with) == type(''):
            extend_with = [extend_with]
            # Could have coded up list_dot_d_array_append(append_string) but
            # just fix up any api misuse by making list from string instead.
        _list_dot_d_array.extend(extend_with)
    except Exception,e:
        extended = False
    return extended


def clear_list_dot_d_array():
    """ Clear list_dot_d_array. Usually called when you have
    manually removed sources.list files created for a temporary purpose
    Example call:
    len_now, len_stored = clear_list_dot_d_array()
    """
    len_stored = len(_list_dot_d_array)
    _list_dot_d_array = []
    return (0,len_stored)


def get_list_dot_d_array():
    return _list_dot_d_array


def tilde_expand(path_short,abort_on_fail=True):
    if path_short.startswith('~/'):
        """ path_expanded = path.expanduser(path_short)
        will not do as you are interested in the remote user
        not the user account on your local machine! """
        expandres = run("dirname ~/.")
        if expandres.failed:
            if abort_on_fail:
                abort("Failed during path expansion of {0}".format(path_short))
            path_expanded = path.short
        else:
            path_expanded = path_short.replace('~/',"{0}/".format(expandres),1)
        """ path_expanded = run("dirname ~/.") """
    else:
        return path_short
    return path_expanded


def first_arg_path_restricted(function_to_decorate):
    """ Decorator to ensure that if first argument is a valid path
    then it is either in home (~/) or /root/ or /tmp/ or it gets set None
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is None:
                pass
            elif type(first_arg) != type(''):
                pass
            else:
                try:
                    if chr(47) not in filepath:
                        #print 'Wrapped function', function_to_decorate.__name__, 'first arg set None'
                        args_list = list(args)
                        args_list[0] = None
                        args = tuple(args_list)
                    elif first_arg.startswith('/root/'):
                        pass
                    elif first_arg.startswith('~/'):
                        #print 'Wrapped function', function_to_decorate.__name__, 'first arg expanded'
                        args_list = list(args)
                        expanded = tilde_expand(first_arg,False)
                        if expanded == first_arg:
                            args_list[0] = expanded
                        else:
                            args_list[0] = None
                        args = tuple(args_list)
                    elif first_arg.startswith('~'):
                        #print 'Wrapped function', function_to_decorate.__name__, 'first arg expanded'
                        args_list = list(args)
                        expanded = tilde_expand(first_arg,False)
                        if expanded == first_arg:
                            args_list[0] = expanded
                        else:
                            args_list[0] = None
                        args = tuple(args_list)
                    else:
                        #print 'Wrapped function', function_to_decorate.__name__, 'first arg is now prefixed'
                        args_list = list(args)
                        args_list[0] = path.join('/root/',first_arg)
                        args = tuple(args_list)               
                except:
                    pass
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def sources_list_extract(print_flag=False,verbose=False):
    extract_lines = []
    with settings(warn_only=True):
        #REGEX = '.*(/sources.list|/sources.list.d/.*\.list)'
        find_line = "{0} /etc/apt/ {1} {2}{3}{2} -exec cat {4} | {5} '://';".format(
            cmdbin.CMD_FIND,cmdbin.REGEX_OPTS_EGREP,chr(39),
            cmdbin.REGEX_SOURCES_LIST,cmdbin.EXEC_TAIL,cmdbin.CMD_FGREP)
        ros_res = cmdbin.run_or_sudo(find_line)
        res_lines = cmdbin.run_res_lines_stripped(ros_res)
        if verbose is True:
            # lines beginning deb
            for line in res_lines:
                if line.startswith('deb'):
                    extract_lines.append(line)
        else:
            # Exclude deb-src but include lines beginning deb
            for line in res_lines:
                if line.startswith('deb '):
                    extract_lines.append(line)
    if print_flag is True and len(extract_lines) > 0:
        for line in extract_lines:
            print line
    return extract_lines


def has_backports():
    has_flag = False
    with settings(warn_only=True):
        find_line = "{0} /etc/apt/ {1} {2}{3}{2} -exec cat {4} | {5} -v '^$';".format(
            cmdbin.CMD_FIND,cmdbin.REGEX_OPTS_EGREP,chr(39),
            cmdbin.REGEX_SOURCES_LIST,cmdbin.EXEC_TAIL,cmdbin.CMD_EGREP)
        ros_res = cmdbin.run_or_sudo(find_line)
        res_lines = cmdbin.run_res_lines_stripped(ros_res)
        for line in res_lines:
            if line.startswith('deb '):
                if '-backports ' in line:
                    #print "MATCH: {0}".format(line)
                    has_flag = True
        """ find /etc/apt/ -regextype posix-egrep -regex \
        '.*(/sources.list|/sources.list.d/.*\.list)' -exec cat {} \;
        Above is a longhand representation of our find_line construction.
        What follows is okay but will bork when nothing in sources.list.d
        grep_line = "{0} '^deb ' {1}/sources.list {1}/sources.list.d/*.list".format(
            cmdbin.CMD_EGREP,'/etc/apt') """
    return has_flag


def dpkg_status_lines_list(packagename=None,status='installed'):
    """ The status 'installed' is assumed and is the only status currently
    supported. This function could be enhanced to accept other values
    as second argument by using addition re similar to RE_STATUS_INSTALLED
    """
    if not packagename:
        return []

    with settings(warn_only=True):
        """ cmdbin.run_or_sudo("{0} {1}".format(CMD_DPKG_STATUS,packagename)) """
        status_res = run("{0} {1}".format(CMD_DPKG_STATUS,packagename))
        #cmdbin.print_success_and_failed(status_res)

    status_list = []
    if status_res.succeeded:
        status_list = [ ln for ln in status_res.split('\n') \
                            if RE_STATUS_INSTALLED.match(ln) ]

    return status_list


def dpkg_size_dicts(packagename=None):
    """ Returns three (ordered) dicts with package information.
    All are OrderedDicts rather than plain dicts and ordered by SIZE
    For presentation the third dict _formatted ordered to begin size,packagename.
    """
    with settings(warn_only=True):
        with hide('output'):
            if not packagename:
                status_res = run(CMD_DPKG_QUERY_SIZE_ALL)
            else:
                """ cmdbin.run_or_sudo("{0} {1}".format(CMD_DPKG_QUERY_SIZE,packagename)) """
                status_res = run("{0} {1}".format(CMD_DPKG_QUERY_SIZE,packagename))
                #cmdbin.print_success_and_failed(status_res)
    installed_lines = cmdbin.run_res_lines_stripped(status_res)
    installed_size = OrderedDict()
    installed_section = OrderedDict()
    installed_formatted = OrderedDict()
    for line in installed_lines:
        line_array = line.split()
        size = 0
        size_formatted = 0
        if len(line_array) > 1:
            size = line_array[0]
            pname = line_array[1]
        if size > 0:
            try:
                size = int(line_array[0])
            except:
                pass
            if size > 1000:
                size = size//1000
                size_formatted = "{0:>10}M".format(size)
            else:
                size_formatted = "{0:>10}k".format(size)
        psection = ''
        if len(line_array) > 2:
            psection = line_array[2]
        installed_size[pname] = size
        installed_section[pname] = psection
        installed_formatted[pname] = "{0:>11} {1:<50} {2:<30}".format(
            size_formatted,pname,psection)
    return installed_size,installed_section,installed_formatted


def dpkg_size(packagename=None,verbose=True,alphabetical=False,section=None):
    """ Report on installed size of package(s)
    """
    installed_size,installed_section,installed_formatted = dpkg_size_dicts(packagename)
    """ All of the above are OrderedDicts rather than plain dicts and ordered by SIZE """
    lines = []
    if section is not None:
        if alphabetical is True:
            verbose = True
            for key in sorted(installed_section.keys()):
                if installed_section[key] == section:
                    lines.append(installed_formatted[key])
        elif verbose:
            for key,section_value in installed_section.items():
                if section_value == section:
                    lines.append(installed_formatted[key])
        else:
            for key,section_value in installed_section.values():
                if section_value == section:
                    line = installed_formatted[key]
                    lines.append(line[:-20])
    elif alphabetical is True:
        """ If user requests alphabetical then assume verbose = True regardless """
        verbose = True
        for pname in sorted(installed_formatted.keys()):
            lines.append(installed_formatted[pname])
    elif verbose:
        lines = installed_formatted.values()
    else:
        #print(installed_formatted)
        lines = [ line[:-30] for line in installed_formatted.values() ]
    return lines


def dpkg_status_installed(packages_list=[],return_type='flag'):
    """ By default this function returns True or False,
    but giving second argument of 'outstanding' you would
    instead have a list returned.
    """
    packages_list_count = len(packages_list)
    if packages_list_count < 1:
        return False
    installed_list_count = 0
    installed_res = True
    outstanding_list = []
    for package in packages_list:
        if set(package).issubset(cmdbin.SET_PRINTABLE):
            res_lines = []
            with settings(warn_only=True):
                with hide('warnings'):
                    status_res = run("{0} {1}".format(CMD_DPKG_QUERY_STATUS4,package))
                    res_lines = cmdbin.run_res_lines_stripped(status_res)
            if status_res.succeeded:
                for line in res_lines:
                    line_search = line[:42]
                    if RE_NO_PACKAGES_MATCHING.search(line_search):
                        outstanding_list.append(package)
                    elif RE_OK_INSTALLED.search(line_search):
                        installed_list_count += 1
                    else:
                        outstanding_list.append(package)
            else:
                outstanding_list.append(package)
        else:
            installed_res = False
            break
    #print len(outstanding_list)
    if installed_res and installed_list_count == packages_list_count:
        outstanding_list = []
    else:
        installed_res = False
    if return_type == 'outstanding':
        return outstanding_list
    return installed_res


def dpkg_status_installed1(packagename):
    """ Return a simple True or False - does the package
    appear to be ok installed?
    """
    if packagename is None or len(packagename) < 2:
        return False
    installed = False
    query_line = "{0} {1}".format(CMD_DPKG_QUERY_STATUS4,packagename)
    with settings(warn_only=True):
        """ Warning only now set. Next hide """
        with hide('stdout','warnings'):
            status_res = run("{0} {1}".format(CMD_DPKG_QUERY_STATUS4,packagename))
            for line in cmdbin.run_res_lines_stripped_searching(status_res,' installed'):
                installed = True
                break
    return installed


def dpkg_installed_version(packages_list=[],return_type=None):
    """ By default this function returns a list of versions,
    but giving second argument of 'outstanding' you would
    instead have a list of outstanding / missing.
    """
    version_list = []
    packages_list_count = len(packages_list)
    if packages_list_count < 1:
        return version_list
    installed_list_count = 0
    installed_res = True
    outstanding_list = []
    for package in packages_list:
        if set(package).issubset(cmdbin.SET_PRINTABLE):
            res_lines = []
            with settings(warn_only=True):
                with hide('warnings'):
                    status_res = run("{0} {1}".format(CMD_DPKG_QUERY_STATUS3,package))
                    res_lines = cmdbin.run_res_lines_stripped(status_res)
            if status_res.succeeded:
                for line in res_lines:
                    line_search = line[:31]
                    if RE_NO_PACKAGES_MATCHING.search(line_search):
                        outstanding_list.append(package)
                    elif RE_OK_INSTALLED.search(line_search):
                        line_array = line.split(package)
                        if len(line_array) > 1:
                            installed_list_count += 1
                            version_list.append(line_array[1].strip())
                    else:
                        outstanding_list.append(package)
            else:
                version_list.append(0)
                outstanding_list.append(package)
        else:
            installed_res = False
            break
    #print len(outstanding_list)
    if installed_res and installed_list_count == packages_list_count:
        outstanding_list = []
        return version_list
    else:
        installed_res = False
    if return_type == 'outstanding':
        return outstanding_list
    return version_list


def dpkg_installed_version1(packagename=None,verbose=False):
	""" Single package version of dpkg_installed_version that
	will always return a number (int or float)

	Only when verbose=True will remote output be shown in full

	-1 indicates package is NOT installed

	chr(45) is dash / hyphen ; chr(46) is dot / period
	"""

	installed_version = -1
	if packagename is None or len(packagename) < 2:
		return installed_version
	
	res_lines = []

	if set(packagename).issubset(cmdbin.SET_PRINTABLE):
		with settings(warn_only=True):
			if verbose:
				with hide('warnings'):
					status_res = run("{0} {1}".format(CMD_DPKG_QUERY_STATUS3,packagename))
			else:
				with hide('stdout','warnings'):
					status_res = run("{0} {1}".format(CMD_DPKG_QUERY_STATUS3,packagename))
			res_lines = cmdbin.run_res_lines_stripped(status_res)

	if len(res_lines) > 0 and status_res.succeeded:
		for line in res_lines:
			line_search = line[:31]
			if RE_NO_PACKAGES_MATCHING.search(line_search):
				#break
				pass
			elif RE_OK_INSTALLED.search(line_search):
				line_array = line.split(packagename)
				if len(line_array) > 1:
					version_string = line_array[1].strip()
					try:
						installed_version = int(version_string)
					except ValueError:
						try:
							installed_version = float(version_string.partition(chr(46))[0])
						except ValueError:
							installed_version = -1


	return installed_version


def dpkg_installed_version_numeric(packagename=None):
    """ Wrapper around def dpkg_installed_version() which for a 
    single package guarantees a numeric return of 0 or version number
    """
    if packagename is None or len(packagename) < 2:
        return 0
    installed_list = dpkg_installed_version([packagename],None)
    if len(installed_list) < 1:
        return 0
    installed_version = 0
    try:
        translated_version = installed_list[0].translate(
            None,''.join(cmdbin.SET_PRINTABLE_NOT_DIGITS_NOT_PERIOD))
        #print translated_version
        try:
            installed_version = int(translated_version)
        except:
            try:
                installed_version = float(translated_version)
            except:
                installed_array = translated_version.split(chr(46))[:2]
                installed_version = chr(46).join(installed_array)
    except:
        installed_version = 0
    return installed_version


def info_installed(packagename=None,verbose=True):
    installed_list = []
    with hide('output'):
        if packagename:
            info_res = run("/bin/vdir /var/lib/dpkg/info/{0}*.list".format(packagename))
        else:
            info_res = run(CMD_INFO_EITHER)
            #print "CMD_INFO_EITHER returned {0} results".format(len(info_res))
    for line in cmdbin.run_res_lines_stripped_searching(info_res,'.list'):
        try:
            #path_in_full = line.split()[-1]
            last_portion = path.split(line)[-1]
            pname = path.splitext(last_portion)[0]
        except Exception:
            pname = None
        if pname:
            installed_list.append(pname)
    return installed_list


def info_installed2(packagename=None,verbose=True):
    """ Alternative implementation of info_installed() """
    installed_list = []
    with hide('output'):
        if packagename:
            info_cmd = "/bin/vdir /var/lib/dpkg/info/{0}*.{list,md5sums}".format(packagename)
            info_res = run(info_cmd)
        else:
            info_res = run(CMD_INFO_EITHER)
    res_lines = cmdbin.run_res_lines_stripped(info_res)
    for line in res_lines:
        if RE_DOT_LIST.search(line):
            try:
                path_in_full = line.split()[-1]
                last_portion = path.split(path_in_full)[-1]
                pname = path.splitext(last_portion)[0]
            except Exception:
                pname = None
            if pname:
                installed_list.append(pname)
    return installed_list


def info_to_file(filepath):
    """ Wrapper around info_installed which handles 'auto' as argument
    and returns filepath """
    info_list = info_installed()
    info_count = len(info_list)
    if filepath == 'auto':
        (pname, pretty_name_id, pretty_name_word, version_major) = cmdbin.pretty_name_tuple()
        """ pretty_name_id = 'unknown' ; pretty_name = 'Unknown!' ; pretty_name_word = 'zero'
        pretty_dict = {'name' : pname, 'id': pretty_name_id, 'release': pretty_name_word, } """
        from datetime import datetime as dt
        isocompact = dt.now().strftime(cmdbin.ISOCOMPACT_STRFTIME)
        filename_auto = "{0}_{1}_{2}packages__{3}.infolist".format(
            pretty_name_id,pretty_name_word,info_count,isocompact)
        filepath = path.join(tilde_expand('~/',True),filename_auto)
    else:
        try:
            filepath_extensionless = path.splitext(filepath)[0]
            if filepath == filepath_extensionless:
                filepath = "{0}.infolist".format(filepath)
        except:
            pass
    if info_count > 0:
        put_flag = cmdbin.put_using_named_temp_cmdbin(filepath,info_list)
        if not put_flag:
            filepath = None
    else:
        filepath = None
    return filepath


def dpkg_installed_count():
    installed_size,installed_section,installed_formatted = dpkg_size_dicts()
    return len(installed_size)


def info_installed_count():
    return len(info_installed())


def dpkg_installed_counts(verbose=False):
    """ returns a two tuple of the count from dpkg
    and the result of info_installed_count """
    if verbose is not True:
        return (dpkg_installed_count(),info_installed_count())
    count_dpkg = dpkg_installed_count()
    count_info = info_installed_count()
    if count_dpkg == count_info:
        print "Installed: {0} == {1}".format(count_dpkg,count_info)
    elif count_dpkg > count_info:
        print "Installed: {0} > {1}".format(count_dpkg,count_info)
    else:
        print "Installed: {0} < {1}".format(count_dpkg,count_info)
    return (count_dpkg,count_info)


def selection_lines_including(lines,including=None,excluding=None,colon_discard=True):
    """ Including has a higher priority than excluding.
    If including is given special keyword 'anything' then
    given value for excluding is ignored
    """
    return_lines = []
    if including == 'anything':
        # Keyword 'anything' means all 4 status will be included 
        including = SELECTIONS4LIST
        excluding = None
    elif including is None and excluding is None:
        # None for 'excluding' means all 4 status unless 'including' is meaningful
        including = SELECTIONS4LIST
        excluding = None
    if including is not None:
        if type(including) == type(''):
            including = [including]
        if len(set(including).difference(SELECTIONS4LIST)) > 0:
            raise Exception("'including' list should be limited to {0}".format(SELECTIONS4LIST))
    if excluding is not None:
        if type(excluding) == type(''):
            excluding = [excluding]
        if len(set(excluding).difference(SELECTIONS4LIST)) > 0:
            raise Exception("'including' list should be limited to {0}".format(SELECTIONS4LIST))
    if colon_discard:
        for line in lines:
            line_array = line.strip().split()
            if len(line_array) > 0:
                status = line_array[-1]
                joined = ''.join(line_array[:-1]).strip()
                if including is not None and status in including:
                    return_lines.append(joined.split(chr(58))[0])
                elif excluding is not None and status not in excluding:
                    return_lines.append(joined.split(chr(58))[0])
            else:
                # Design choice - be forgiving about blank lines
                pass
    else:
        for line in lines:
            line_array = line.strip().split()
            if len(line_array) > 0:
                status = line_array[-1]
                joined = ''.join(line_array[:-1]).strip()
                if including is not None and status in including:
                    return_lines.append(joined)
                elif excluding is not None and status not in excluding:
                    return_lines.append(joined)
            else:
                # Design choice - be forgiving about blank lines
                pass
    return return_lines


def selections_including(including_status=None,colon_discard=True):
    """ chr(58) is colon (:)
    colon_discard=True says not interested in :i386 markers (and similar)
    that sometimes follow package name to indicate arch of i386/amd64/whatever
    Discarding would mean 'e2fslibs:i386' would be made to appear in returned
    list as simply 'e2fslibs'
    """
    if including_status is None:
        # Assume want 'install' and 'hold' unless instructed otherwise
        including_status = SELECTIONS2LIST
    elif including_status == 'anything':
        # Keyword 'anything' means all 4 status will be included 
        including_status = SELECTIONS4LIST
    elif type(including_status) == type(''):
        including_status = [including_status]
    res_lines = []
    with hide('output'):
        selections_res = run(CMD_DPKG_SELECTIONS)
        res_lines = cmdbin.run_res_lines_stripped(selections_res)
    including_lines = []
    if colon_discard:
        for line in res_lines:
            line_array = line.strip().split()
            status = line_array[-1]
            joined = ''.join(line_array[:-1]).strip()
            if status in including_status:
                including_lines.append(joined.split(chr(58))[0])

    else:
        for line in res_lines:
            line_array = line.strip().split()
            status = line_array[-1]
            joined = ''.join(line_array[:-1]).strip()
            if status in including_status:
                including_lines.append(joined)
    return including_lines


def selections_excluding(excluding_status=None,colon_discard=True):
    """ chr(58) is colon (:)
    colon_discard=True says not interested in :i386 markers (and similar)
    that sometimes follow package name to indicate arch of i386/amd64/whatever
    Discarding would mean 'e2fslibs:i386' would be made to appear in returned
    list as simply 'e2fslibs'
    """
    if excluding_status is None:
        # Assume want 'install' and 'hold' unless instructed otherwise
        excluding_status = ['deinstall','purge']
    elif excluding_status == 'anything':
        # Keyword 'anything' means all 4 status will be included 
        return []
        #excluding_status = SELECTIONS4LIST
    elif type(excluding_status) == type(''):
        excluding_status = [excluding_status]
    selections_res = run(CMD_DPKG_SELECTIONS)
    res_lines = []
    with hide('output'):
        selections_res = run(CMD_DPKG_SELECTIONS)
        res_lines = cmdbin.run_res_lines_stripped(selections_res)
    excluding_lines = []
    if colon_discard:
        for line in res_lines:
            line_array = line.strip().split()
            status = line_array[-1]
            joined = ''.join(line_array[:-1]).strip()
            if status in excluding_status:
                pass
            else:
                excluding_lines.append(joined.split(chr(58))[0])

    else:
        for line in res_lines:
            line_array = line.strip().split()
            status = line_array[-1]
            joined = ''.join(line_array[:-1]).strip()
            if status in excluding_status:
                pass
            else:
                excluding_lines.append(joined)
    return excluding_lines


@first_arg_path_restricted
def selections_raw(filepath=None):
    """ Output of --get-selections
    can be automatically written to a file if you supply a valid filepath
    """
    res_lines = []
    with hide('output'):
        selections_res = run(CMD_DPKG_SELECTIONS)
        res_lines = cmdbin.run_res_lines_stripped(selections_res)
    if len(res_lines) > 0 and filepath:
        put_flag = cmdbin.put_using_named_temp_cmdbin(filepath,res_lines)
    return res_lines


def selections_to_file(filepath):
    """ Wrapper around selections_raw which handles 'auto' as argument
    and returns filepath """
    selections_list = selections_raw()
    selections_count = len(selections_list)
    if filepath == 'auto':
        (pname, pretty_name_id, pretty_name_word, version_major) = cmdbin.pretty_name_tuple()
        """ pretty_name_id = 'unknown' ; pretty_name = 'Unknown!' ; pretty_name_word = 'zero'
        pretty_dict = {'name' : pname, 'id': pretty_name_id, 'release': pretty_name_word, } """
        from datetime import datetime as dt
        isocompact = dt.now().strftime(cmdbin.ISOCOMPACT_STRFTIME)
        filename_auto = "{0}_{1}_{2}packages__{3}.selections".format(
            pretty_name_id,pretty_name_word,selections_count,isocompact)
        filepath = path.join(tilde_expand('~/',True),filename_auto)
    else:
        pass
    if selections_count > 0:
        put_flag = cmdbin.put_using_named_temp_cmdbin(filepath,selections_list)
        if not put_flag:
            filepath = None
    else:
        filepath = None
    return filepath


def packages_summary(print_flag=False,verbose=False):
    return


def packages_marked_hold_summary(print_flag=False,verbose=False):
    #selections_including(['hold'],True):
    selections = selections_including('hold',True)
    selections_count = len(selections)
    if print_flag:
        if selections_count > 0:
            if verbose == True:
                print "Packages held:"
                for pkg in selections:
                    print "- {0}".format(pkg)
            else:
                print "Packages held: {0}".format(selections_count)
        else:
            print "Packages held: None. No packages are marked 'hold'"
    return selections


def held_packages_summary(print_flag=False,verbose=False):
    """ Using held rather than hold in the function name can
    lead to asking for packages 'held' rather than an
    include_list of ['hold']. Use the fuller function name
    (shown on next line) directly if you want to be very clear.
    """
    return packages_marked_hold_summary(print_flag,verbose)


def source_list_lines_debian(pretty_dict,repolabel,backports_main_only=True,cdn=True):
    source_list_lines = None
    if repolabel == 'backports' and backports_main_only:
        source_list_lines = """
# added by fabutil
deb http://ftp.debian.org/{id} {release}-backports main
deb-src http://ftp.debian.org/{id} {release}-backports main
""".format(**pretty_dict)
    elif repolabel == 'backports':
        if cdn is False:
            source_list_lines = """
# added by fabutil
deb http://ftp.debian.org/{id} {release}-backports main contrib non-free
deb-src http://ftp.debian.org/{id} {release}-backports main contrib non-free
""".format(**pretty_dict)
        else:
            source_list_lines = """
# added by fabutil
deb http://cdn.debian.net/{id} {release}-backports main contrib non-free
deb-src http://cdn.debian.net/{id} {release}-backports main contrib non-free
""".format(**pretty_dict)
    elif repolabel in ['contrib','non-free']:
        source_list_lines = """
# added by fabutil
deb http://http.debian.net/{id}/ {release} {repolabel}
deb-src http://http.debian.net/{id}/ {release} {repolabel}
""".format(**pretty_dict)
    else:
        pass
    return source_list_lines


def sources_list_lines(list_dot_d):
    sl_lines = None
    (pname, pretty_name_id, pretty_name_word, version_major) = cmdbin.pretty_name_tuple()
    if pretty_name_id == 'debian':
        """ See www.debian.org/doc/debian-policy/ch-archive.html for more about
        distinction between main and other repositories
        """
        if list_dot_d in ['backports','contrib','non-free']:
            pretty_dict = {'id': pretty_name_id,
                           'release': pretty_name_word,
                           'repolabel': list_dot_d}
            sl_lines = source_list_lines_debian(pretty_dict,list_dot_d)
        else:
            pass
    elif pretty_name_id == 'ubuntu':
        """ Unlike Debian which distinguishes repos by 'freedom to modify/distribute'
        Ubuntu distinguishes repos by level of support by Canonical company.
        A firm grasp on this distinction will help you avoid simplistic maps, but next
        is one to get you started: (contrib+non-free)->company filter->(multiverse+restricted)
        """
        if list_dot_d in ['universe','multiverse']:
            pretty_dict = {'id': pretty_name_id,
                           'release': pretty_name_word,
                           'verse': list_dot_d}
            sl_lines = """
# added by fabutil
deb http://archive.ubuntu.com/{id}/ {release} {verse}
deb-src http://archive.ubuntu.com/{id}/ {release} {verse}
""".format(**pretty_dict)
    else:
        pass
    return sl_lines


def missing_conf_via_reinstall(filepath):
    if filepath is None:
        return False
    if chr(47) in filepath:
        """ Regular file including some path element (/)
        Here forward slash is hardcoded. This software is intended for remote
        systems that are GNU/Linux or GNU variants. """
        pass
    else:
        return False
    reinstalled = False
    ros_res = run("{0}{1}".format(CMD_DPKG_SEARCH,filepath))
    packagename = None
    for line in cmdbin.run_res_lines_stripped_searching(ros_res,chr(47)):
        first = line.split()[0]
        if len(first) > 0:
            packagename = first.rstrip(chr(58))
            break
    installed = False
    if packagename is not None:
        installed = dpkg_status_installed1(packagename)
    if installed:
        apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS_CONFMISS,
                                            packagename)
        ros_res = cmdbin.run_or_sudo(apt_line)
        if len(cmdbin.run_res_lines_stripped_searching(ros_res,'etting')) > 0:
            reinstalled = True
    else:
        print "Package {0} does not seem to be (cleanly?) installed!!".format(packagename)
        print "Suggest intervention for package {0} !!".format(packagename)
    return reinstalled


def missing_conf_via_reinstall_middle(filepath,middle='local'):
    destination = cmdbin.filepath_suffixed_isocompact(filepath,middle)
    copy_line = "{0} {1} {2};".format(cmdbin.CMD_CP_UPDATE_NOCROSS_PRESERVETIME,
                                      filepath,destination)
    rm_line = "{0} {1};".format(cmdbin.CMD_RM_BIGI,filepath)
    with settings(warn_only=True):
        """ Warning only now set. Next hide """
        with hide('stdout','warnings'):
            copy_res = cmdbin.run_or_sudo(copy_line)
            if copy_res.failed:
                return False
            rmres = cmdbin.run_or_sudo(rm_line)
            if rmres.failed:
                return False

    reinstalled_flag = missing_conf_via_reinstall(filepath)
    if reinstalled_flag is not True:
        copy_line = "{0} {2} {1};".format(cmdbin.CMD_CP_UPDATE_NOCROSS_PRESERVETIME,
                                          filepath,destination)
        with settings(warn_only=True):
            """ Warning only now set. Next hide """
            #with hide('stdout','warnings'):
            copy_res = cmdbin.run_or_sudo(copy_line)

    return


def aptget_line(packages_list=[],assumeyes=False,
                recommends=True,confold=False,precheck=False):

    """ Returning an empty string indicates nothing to do (no packages need installing)
    For exceptions/unexpected situations then raise Exceptions
    """

    packages_list_count = len(packages_list)
    if packages_list_count < 1:
        raise Exception('Unexpected value as first argument')

    if precheck:
        packages_list = dpkg_status_installed(packages_list,'outstanding')
        if len(packages_list) < 1:
            return ''

    packages = ' '.join(packages_list)
    apt_line = "{0} install {1}".format(CMD_APT,packages)
    if assumeyes:
        if recommends is False:
            if confold:
                apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS_CONFOLD,
                                                    packages)
            else:
                apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS,
                                                    packages)
        else:
            apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES,packages)
    else:
        if recommends is False:
            apt_line = "{0} install {1}".format(CMD_APT_NORECOMMENDS,packages)
    return apt_line


def aptget_noninterative_line(packages_list=[],assumeyes=False,
                              recommends=True,confold=False,precheck=False):
    """ Returning an empty string indicates nothing to do (no packages need installing)
    For exceptions/unexpected situations then raise Exceptions
    """

    packages_list_count = len(packages_list)
    if packages_list_count < 1:
        raise Exception('Unexpected value as first argument')

    if precheck:
        packages_list = dpkg_status_installed(packages_list,'outstanding')
        if len(packages_list) < 1:
            return ''

    packages = ' '.join(packages_list)
    
    assume_string = ''
    if assumeyes is True:
        assume_string = ' --assume-yes '

    apt_line = "{0}{1} install {2}".format(CMD_APT_NONINTERACTIVE,assume_string,packages)
    if recommends is False:
        apt_line = "{0}{1} install {2}".format(
            CMD_APT_NONINTERACTIVE_NORECOMMENDS,assume_string,packages)

    return apt_line


def requires_packages(packages_list=[],assumeyes=False,
                      recommends=True,confold=False):
    """ Installs required packages.
    Unlike the 'one by one' variant requires_dpkg_package() this function
    has no list_dot_d argument. So if you did need to install from
    'contrib', 'non-free', 'universe', 'multiverse' and you want automatic
    repository adding, then instead use requires_dpkg_package()
    """
    requires_res = False
    ros_line = aptget_line(packages_list,assumeyes,recommends,confold,True)
    ros_res = cmdbin.run_or_sudo(ros_line)
    if ros_res.succeeded:
        requires_res = True  
    return requires_res


def requires_packages6(packages_list=[],assumeyes=False,recommends=False,
                                      confold=False,always_update=False,print_flag=False):
    """ Installs required packages ( 6 argument version )
    Unlike the 'one by one' variant requires_dpkg_package() this function
    has no list_dot_d argument. So if you did need to install from
    'contrib', 'non-free', 'universe', 'multiverse' and you want automatic
    repository adding, then instead use requires_dpkg_package()
    """
    requires_res = False
    if always_update is True:
        prepared_flag = False
        if print_flag:
            ros_res = cmdbin.run_or_sudo(CMD_APT_UPDATE)
            if ros_res.succeeded:
                prepared_flag = True
        else:
            with hide('output'):
                ros_res = cmdbin.run_or_sudo(CMD_APT_UPDATE)
                if ros_res.succeeded:
                    prepared_flag = True
            # Output will be less verbose

    else:
        prepared_flag = True
        
    if not prepared_flag:
        return requires_res

    if print_flag:
        ros_line = aptget_line(packages_list,assumeyes,recommends,confold,True)
        ros_res = cmdbin.run_or_sudo(ros_line)
        if ros_res.succeeded:
            requires_res = True
    else:
        ros_line = aptget_line(packages_list,assumeyes,recommends,confold,True)
        with hide('output'):
            ros_res = cmdbin.run_or_sudo(ros_line)
            if ros_res.succeeded:
                requires_res = True
        # Output will be less verbose
        
    return requires_res


def requires_packages_noninteractive(packages_list=[],assumeyes=False,recommends=True,
                                     confold=False):
    """ Installs required packages non-interactively
    Unlike the 'one by one' variant requires_dpkg_package() this function
    has no list_dot_d argument. So if you did need to install from
    'contrib', 'non-free', 'universe', 'multiverse' and you want automatic
    repository adding, then instead use requires_dpkg_package()
    """
    requires_res = False
    ros_line = aptget_noninterative_line(packages_list,assumeyes,recommends,confold,True)
    ros_res = cmdbin.run_or_sudo(ros_line)
    if ros_res.succeeded:
        requires_res = True  
    return requires_res


def requires_packages_noninteractive6(packages_list=[],assumeyes=False,recommends=False,
                                      confold=False,always_update=False,print_flag=False):
    """ Installs required packages non-interactively ( 6 argument version )
    Unlike the 'one by one' variant requires_dpkg_package() this function
    has no list_dot_d argument. So if you did need to install from
    'contrib', 'non-free', 'universe', 'multiverse' and you want automatic
    repository adding, then instead use requires_dpkg_package()
    """
    requires_res = False
    if always_update is True:
        prepared_flag = False
        if print_flag:
            ros_res = cmdbin.run_or_sudo(CMD_APT_UPDATE)
            if ros_res.succeeded:
                prepared_flag = True
        else:
            with hide('output'):
                ros_res = cmdbin.run_or_sudo(CMD_APT_UPDATE)
                if ros_res.succeeded:
                    prepared_flag = True
            # Output will be less verbose

    else:
        prepared_flag = True
        
    if not prepared_flag:
        return requires_res

    if print_flag:
        ros_line = aptget_noninterative_line(packages_list,assumeyes,recommends,confold,True)
        ros_res = cmdbin.run_or_sudo(ros_line)
        if ros_res.succeeded:
            requires_res = True
    else:
        ros_line = aptget_noninterative_line(packages_list,assumeyes,recommends,confold,True)
        with hide('output'):
            ros_res = cmdbin.run_or_sudo(ros_line)
            if ros_res.succeeded:
                requires_res = True
        # Output will be less verbose

    return requires_res


def requires_dpkg_package4(packagename=None,assumeyes=False,
	recommends=True,verbose=False):
	""" Simplified version of requires_dpkg_package that supports
	four parameters only, hence the name ..._package4

	-1 indicates package is (still) NOT installed despite our best efforts
	"""
	if packagename:
		requires_res = True
	else:
		return False

	installed_version = dpkg_installed_version1(packagename,False)
	if installed_version >= 0:
		if verbose:
			print("Package {0} installed version is {1}".format(packagename,installed_version))
		return installed_version

	with settings(warn_only=True):
		apt_line = "{0} install {1}".format(CMD_APT,packagename)
		if assumeyes:
			if recommends is False:
				apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS,
													packagename)
			else:
				apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES,packagename)
		else:
			if recommends is False:
				apt_line = "{0} install {1}".format(CMD_APT_NORECOMMENDS,packagename)
		ros_res = cmdbin.run_or_sudo(apt_line)

		if ros_res.succeeded:
			installed_version = dpkg_installed_version1(packagename,False)

	if verbose:
		if installed_version < 0:
			print("Package {0} is NOT installed (version defaulted to {1})".format(
					packagename,installed_version))
		else:
			print("Package {0} installed version is {1}".format(packagename,installed_version))

	return installed_version


def requires_dpkg_package(packagename=None,assumeyes=False,
                          recommends=True,confold=False,list_dot_d=None):
    if packagename:
        requires_res = True
    else:
        return False

    if len(dpkg_status_lines_list(packagename)) > 0:
        return True

    requires_res = False

    with settings(warn_only=True):
        apt_line = "{0} install {1}".format(CMD_APT,packagename)
        if assumeyes:
            if recommends is False:
                if confold:
                    apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS_CONFOLD,
                                                        packagename)
                else:
                    apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS,
                                                        packagename)
            else:
                apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES,packagename)
        else:
            if recommends is False:
                apt_line = "{0} install {1}".format(CMD_APT_NORECOMMENDS,packagename)
        ros_res = cmdbin.run_or_sudo(apt_line)
        if ros_res.succeeded:
            requires_res = True  
        elif list_dot_d:
            sl_filename = "{0}.list".format(list_dot_d)
            sl_lines = None
            (pname, pretty_name_id, pretty_name_word, version_major) = cmdbin.pretty_name_tuple()
            if pretty_name_id == 'debian':
                """ See www.debian.org/doc/debian-policy/ch-archive.html for more about
                distinction between main and other repositories
                """
                if list_dot_d in ['contrib','non-free']:
                    pretty_dict = {'id': pretty_name_id,
                                   'release': pretty_name_word,
                                   'repolabel': list_dot_d}
                    sl_lines = """
# added by fabutil
deb http://http.debian.net/{id}/ {release} {repolabel}
deb-src http://http.debian.net/{id}/ {release} {repolabel}
""".format(**pretty_dict)
            elif pretty_name_id == 'ubuntu':
                """ Unlike Debian which distinguishes repos by 'freedom to modify/distribute'
                Ubuntu distinguishes repos by level of support by Canonical company.
                A firm grasp on this distinction will help you avoid simplistic maps, but next
                is one to get you started: (contrib+non-free)->company filter->(multiverse+restricted)
                """
                if list_dot_d in ['universe','multiverse']:
                    pretty_dict = {'id': pretty_name_id,
                                   'release': pretty_name_word,
                                   'verse': list_dot_d}
                    sl_lines = """
# added by fabutil
deb http://archive.ubuntu.com/{id}/ {release} {verse}
deb-src http://archive.ubuntu.com/{id}/ {release} {verse}
""".format(**pretty_dict)
            else:
                pass
            if sl_lines:
                put_flag = cmdbin.put_using_named_temp_sudo("/etc/apt/sources.list.d/{0}".format(sl_filename),
                                                       sl_lines)
                if put_flag:
                    list_dot_d_array_extend(sl_filename)

                    cmdbin.run_or_sudo(CMD_APT_UPDATE)

                    apt_line = "{0} install {1}".format(CMD_APT,packagename)
                    if assumeyes:
                        if recommends is False:
                            if confold:
                                apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS_CONFOLD,
                                                                    packagename)
                            else:
                                apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS,
                                                                    packagename)
                        else:
                            apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES,packagename)
                    else:
                        if recommends is False:
                            apt_line = "{0} install {1}".format(CMD_APT_NORECOMMENDS,packagename)
                    ros_res = cmdbin.run_or_sudo(apt_line)
                    if ros_res.succeeded:
                        requires_res = True
                else:
                    requires_res = False
                    """ Above else: for clarity rather than necessity """

    status_list = dpkg_status_lines_list(packagename)
    print "{0} - {1}".format(packagename, status_list)
    # if len(dpkg_status_lines_list(packagename)) > 0:
    if len(status_list) > 0:
        return True

    return requires_res


def requires_dpkg_package2(packagename=None,assumeyes=False,
	recommends=True,confold=False,list_dot_d=None):
	""" Placeholder for less verbose version of requires_dpkg_package """
	return requires_dpkg_package(packagename,assumeyes,recommends,confold,list_dot_d)


def requires_dpkg_package3(packagename=None,assumeyes=False,
	recommends=True,confold=False,list_dot_d=None):
	#installed_version = 0
	installed_version = dpkg_installed_version_numeric(packagename)
	if 0 == installed_version:
		pass
	else:
		installed_version = requires_dpkg_package4(COLLECTD_APIPAGE_PACKAGE_ENTITIES,True,False)
	return installed_version


def requires_backported_package(packagename=None,assumeyes=False,
                                recommends=True,confold=False):
    requires_res = False
    if packagename is not None:
        if len(packagename) > 0 and type(packagename) == type(''):
            # chr(61) is equals
            if chr(61) in packagename:
                """ Unlike requires_backported_packages() this
                function does not support package=version style
                requests
                """
                pass
            else:
                requires_res = True

    if requires_res is False:
        return False
    requires_res = True

    if len(dpkg_status_lines_list(packagename)) > 0:
        return True

    BACKPORTS='backports'

    has_backports_flag = False
    if has_backports():
        has_backports_flag = True
    else:
        (pname, pretty_name_id, pretty_name_word, version_major) = cmdbin.pretty_name_tuple()
        pretty_dict = {'id': pretty_name_id,
                       'release': pretty_name_word,
                       'repolabel': BACKPORTS}
        sl_filename = "{0}.list".format(BACKPORTS)
        sl_lines = source_list_lines_debian(pretty_dict,BACKPORTS)
        if sl_lines:
            sl_filepath = "/etc/apt/sources.list.d/{0}".format(sl_filename)
            put_flag = cmdbin.put_using_named_temp_sudo(sl_filepath,sl_lines)
            if put_flag:
                has_backports_flag = True
                list_dot_d_array_extend([sl_filename])
                #print get_list_dot_d_array()
                with hide('output'):
                    ros_res = cmdbin.run_or_sudo(CMD_APT_UPDATE)
                    for line in cmdbin.run_res_lines_stripped_searching(ros_res,'backports'):
                        print line
                    """ Rather than have the entire CMD_APT_UPDATE output
                    displayed on screen we print just those lines containing 'backports'
                    """

    if has_backports_flag is False:
        """ Best attempt at adding backports into sources.list.d did not work
        so nothing more we can do """
        return False

    requires_res = False

    with settings(warn_only=True):
        apt_line = "{0} install {1}".format(CMD_APT,packagename)
        if assumeyes:
            if recommends is False:
                if confold:
                    apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS_CONFOLD,
                                                        packagename)
                else:
                    apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS,
                                                        packagename)
            else:
                apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES,packagename)
        else:
            if recommends is False:
                apt_line = "{0} install {1}".format(CMD_APT_NORECOMMENDS,packagename)
        ros_res = cmdbin.run_or_sudo(apt_line)
        if ros_res.succeeded:
            requires_res = True  

    status_list = dpkg_status_lines_list(packagename)
    print "{0}:{1} - {2}".format(requires_res,packagename,status_list)
    if len(status_list) > 0:
        return True

    return requires_res


def requires_backported_package_tidy(packagename=None,assumeyes=False,
                                     recommends=True,confold=False):
    """ Wrapper around requires_backported_package which does the cleanup we might
    otherwise expect from an abort using files_directories.abort_with_message()
    If your logic tests failure from this function and uses abort_with_message() then
    your cleanup will have been duplicated.
    """
    requires_res = requires_backported_package(packagename=packagename,assumeyes=assumeyes,
                                               recommends=recommends,confold=confold)
    list_dot_d_array = get_list_dot_d_array()
    if len(list_dot_d_array) > 0:
        all_removed = True
        for sl in list_dot_d_array:
            sl_ext = path.splitext(sl)[1]
            if sl_ext == '.list':
                rm_line = "{0} /etc/apt/sources.list.d/{1}".format("/bin/rm -v ",sl)
                with settings(warn_only=True):
                    ros_res = cmdbin.run_or_sudo(rm_line)
                    if ros_res.failed:
                        all_removed = False
        if all_removed:
            len_now, len_stored = clear_list_dot_d_array()
    return requires_res


def requires_backported_packages(packages_list=None,assumeyes=False,
                                 recommends=True,confold=False,exists_replace=False):
    if packages_list:
        if hasattr(packages_list, "__iter__"):
            if type(packages_list) == type(''):
                packages_list = [packages_list]
        else:
            return False
    else:
        return False

    version_qualifier_count = 0
    version_qualifier_dict = {}
    # chr(61) is equals
    for pkg in packages_list:
        if chr(61) in pkg:
            version_qualifier_count += 1
            pkg_array = pkg.split(chr(61))
            version_qualifier_dict[pkg_array[0]]=pkg_array[1]
    #print len(version_qualifier_dict),version_qualifier_dict

    if version_qualifier_count == 0:
        """ When we are not given any version qualified packages
        we can shortcut to return True if all the package names
        are marked installed already
        """
        #if len(dpkg_status_lines_list(packagename)) > 0:
        if dpkg_status_installed(packages_list):
            return True
    elif exists_replace is True:
        pass
    else:
        """ Some of required packages have version qualifiers
        and we are not given authority to replace existing packages
        """
        for pkg,version in version_qualifier_dict.iteritems():
            #if dpkg_status_installed([pkg]):
            #print pkg,version
            version_list = dpkg_installed_version([pkg])
            if len(version_list) > 0:
                installed_version = version_list[0]
                if installed_version == 0:
                    pass
                elif installed_version == version.strip():
                    # Matched
                    pass
                else:
                    print "Package {0} version {1} already installed. Will not replace.".format(
                        pkg,installed_version)
                    return False

    requires_res = True  

    BACKPORTS='backports'

    has_backports_flag = False
    if has_backports():
        has_backports_flag = True
    else:
        (pname, pretty_name_id, pretty_name_word, version_major) = cmdbin.pretty_name_tuple()
        pretty_dict = {'id': pretty_name_id,
                       'release': pretty_name_word,
                       'repolabel': BACKPORTS}
        sl_filename = "{0}.list".format(BACKPORTS)
        sl_lines = source_list_lines_debian(pretty_dict,BACKPORTS)
        if sl_lines:
            put_flag = cmdbin.put_using_named_temp_sudo("/etc/apt/sources.list.d/{0}".format(sl_filename),
                                                   sl_lines)
            if put_flag:
                has_backports_flag = True
                list_dot_d_array_extend([sl_filename])
                #print get_list_dot_d_array()
                with hide('output'):
                    ros_res = cmdbin.run_or_sudo(CMD_APT_UPDATE)
                    for line in cmdbin.run_res_lines_stripped_searching(ros_res,'backports'):
                        print line
                    """ Rather than have the entire CMD_APT_UPDATE output
                    displayed on screen we print just those lines containing 'backports'
                    """

    if has_backports_flag is False:
        """ Best attempt at adding backports into sources.list.d did not work
        so nothing more we can do """
        return False

    packages_count = len(packages_list)
    packages_string = ' '.join(packages_list)
    """ Above we have joined and will rely on apt-get to handle the requested
    package list (including any version qualified packages) rather than
    doing any further checking against required versions.
    We make no further use of version_qualifier_dict dictionary, which has
    thus far, only been used for handling exists_replace=False correctly.
    """

    requires_res = False

    with settings(warn_only=True):
        apt_line = "{0} install {1}".format(CMD_APT,packages_string)
        if assumeyes:
            if recommends is False:
                if confold:
                    apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS_CONFOLD,
                                                        packages_string)
                else:
                    apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES_NORECOMMENDS,
                                                        packages_string)
            else:
                apt_line = "{0} install {1}".format(CMD_APT_ASSUMEYES,packages_string)
        else:
            if recommends is False:
                apt_line = "{0} install {1}".format(CMD_APT_NORECOMMENDS,packages_string)
        ros_res = cmdbin.run_or_sudo(apt_line)
        if ros_res.succeeded:
            requires_res = True  

    if requires_res and version_qualifier_count == 0:
        """ When we believe successful and are simple case (no version qualifiers)
        provide an extra print and outstanding check
        """
        outstanding_list = dpkg_status_installed(packages_list,'outstanding')
        if len(outstanding_list) > 0:
            print "backports install not completed for packages: {0}".format(outstanding_list)
            return False

    return requires_res


def requires_binary_package(binary_unpathed=None,packagename_suggestion=None,
                            assumeyes=False):
    """ The word 'binary' here refers to the fact that you supply a binary
    name as the first argument. Two examples follow:
    requires_res = requires_binary_package('ab','apache2-utils',assumeyes=True)
    requires_binary_package('django-admin','python-django',assumeyes=True)
    if assumeyes is True then supply the following flag to apt-get: --assume-yes
    """
    which_res = None
    requires_which = cmdbin.CMD_KERN_RELEASE
    with settings(warn_only=True):
        which_res = run("{0} {1}".format(cmdbin.CMD_WHICH,binary_unpathed))

    #cmdbin.print_success_and_failed(which_res)
    if which_res and which_res.succeeded:
        """ .succeeded is supported in Fabric 1.0 and newer """
        requires_which = which_res
        print(which_res)
        return requires_which
    elif which_res is not None and which_res.failed:
        if assumeyes:
            cmdbin.run_or_sudo("{0} install {1}".format(
                    CMD_APT_ASSUMEYES,packagename_suggestion))
        else:
            cmdbin.run_or_sudo("{0} install {1}".format(
                    CMD_APT,packagename_suggestion))

        with settings(warn_only=True):
            which_res = run("{0} {1}".format(cmdbin.CMD_WHICH,binary_unpathed))

        if which_res and not which_res.failed:
            requires_which = which_res
            print(which_res)
            return requires_which
    else:
        return which_res
    return False


def checkbuilddeps(untarred_dir):
    deps_list = []
    with settings(warn_only=True):
        with cd(untarred_dir):
            check_res = cmdbin.run(CMD_DPKG_CHECKBUILDDEPS2TO1)
            if check_res.succeeded:
                pass
            else:
                if check_res.return_code == 0:
                    pass
                elif check_res.return_code == 1:
                    # unmet dependencies reported
                    deps_line = cmdbin.run_res_lines_tolerant_searching1(
                        check_res,': Unmet build dependencies:')
                    if 'Unmet build dependencies:' in deps_line:
                        #print "found deps string"
                        deps = deps_line.split('Unmet build dependencies:')[1]
                        deps_list = deps.split()
                        #print deps_list
                else:
                    abort("Failed during checkbuilddeps in directory {0}".format(untarred_dir))
    return deps_list


def netselect_apt(verbose=False):
    netselect_return = False
    requires_res = requires_packages(['netselect','netselect-apt'],True,False,False)
    if verbose:
        # hide warnings but show stdout
        with hide('warnings'):
            ros_res = cmdbin.run_or_sudo(CMD_NETSELECT_APT)
    else:
        # hide warnings and stdout
        ros_res = cmdbin.run_or_sudo_warningless(CMD_NETSELECT_APT)
    if ros_res.succeeded and ros_res.find('Writing sources.list'):
        netselect_return = True
        print 'A new sample sources.list has been generated (/root/sources.list?)'
        print 'Inspect and if necessary copy into /etc/apt/ the results you are happy with'
    return netselect_return


def enable_backports(cdn=True,always_update=True):
    backports_flag = has_backports()
    put_flag = None
    if backports_flag is False:
        BACKPORTS='backports'
        (pname, pretty_name_id, pretty_name_word, version_major) = cmdbin.pretty_name_tuple()
        pretty_dict = {'id': pretty_name_id,
                       'release': pretty_name_word,
                       'repolabel': BACKPORTS}
        sl_lines = source_list_lines_debian(pretty_dict,BACKPORTS,cdn)
        put_flag = False
        if sl_lines:
            sl_filename = "{0}.list".format(BACKPORTS)
            sl_filepath = "/etc/apt/sources.list.d/{0}".format(sl_filename)
            with settings(warn_only=True):
                put_flag = cmdbin.put_using_named_temp_sudo(sl_filepath,sl_lines)
    if put_flag is None:
        backports_flag = False
    elif put_flag and always_update is not False:
        backports_flag = True
        with hide('output'):
            ros_res = cmdbin.run_or_sudo(CMD_APT_UPDATE)
            for line in cmdbin.run_res_lines_stripped_searching(ros_res,'backports'):
                print line
            """ Rather than have the entire CMD_APT_UPDATE output
            displayed on screen we print just those lines containing 'backports'
            """
    elif put_flag:
        backports_flag = True
    else:
        backports_flag = False
        print "Exists already in /etc/apt/sources.list.d or other file creation issue"
    return backports_flag


def dget_dsc(durl=None):
    """ Get a remote .dsc and return a list of tar files that were fetched as a result. """
    archive_list = []
    devscripts_res = requires_packages(['devscripts'],True,False,False)
    if devscripts_res:
        # dget command should now be available
        pass
    else:
        return archive_list
    dget_line = "{0} {1}".format(CMD_DGETDSC,durl)
    get_res = cmdbin.run(dget_line)
    if get_res.succeeded and get_res.find('dpkg-source: info:'):
        #build_package_list = ['devscripts','fakeroot','build-essential','debhelper','pbuilder']
        build_package_list = ['devscripts','fakeroot','build-essential','debhelper']
        requires_res = requires_packages(build_package_list,True,False,False)
        if requires_res:
            for line in cmdbin.run_res_lines_stripped_searching(get_res,'dpkg-source: info: unpacking'):
                if 'info: unpacking' in line:
                    slist = line.split('unpacking')
                    archive_list.append(slist[1].strip())
    return archive_list


def package_names_from_control(dir_cd=None,names_max=0):
    """ Extract lines beginning Package from debian/control and put
    the package names in a list.
    cmdbin.DIR_UNBUILT_RELATIVE is set in cmdbin.py and will be './unbuilt'
    cmdbin.DIR_UNBUILT_DEBIAN_RELATIVE is set in cmdbin.py and will be './unbuilt/debian'
    Returns a list (of package names)
    """
    # egrep 'Package: ' ./debian/control
    grep_line = "{0} ./debian/control ".format(cmdbin.CMD_EGREP_CONTROL_PACKAGE)
    names_count = 0
    names_list = []
    if dir_cd:
        pass
    else:
        dir_cd = cmdbin.DIR_UNBUILT_RELATIVE
    with cd(dir_cd):
        ros_res = cmdbin.run_or_sudo(grep_line)
        for line in cmdbin.run_res_lines_stripped(ros_res):
            try:
                pname = line.split('Package:')[1].strip()
                names_list.append(pname)
            except:
                continue
            names_count += 1
            if names_count >= names_max:
                break
    return names_list


def control_from_template(packagename,dir_cd=None):
    """ Ask dpkg to generate a full control file from template
    for package packagename.
    Returns a boolean
    """
    control_flag = False
    if dir_cd:
        pass
    else:
        dir_cd = cmdbin.DIR_UNBUILT_DEBIAN_RELATIVE_DEBIAN

    mkdir_unbuilt = "{0} {1}".format(cmdbin.CMD_MKDIRVP755,dir_cd)
    mkdir_res = run(mkdir_unbuilt)
    if mkdir_res.failed:
        pass

    gen_line = "{0} -Pdebian -p{1}".format(CMD_DPKG_GENCONTROL,packagename)
    if '/debian/' not in dir_cd:
        return control_flag

    dir_as_list = dir_cd.split('/debian/')
    dir_cd = dir_as_list = dir_as_list[-2]

    with cd(dir_cd):
        gen_res = cmdbin.run(gen_line)
        if gen_res.succeeded:
            control_flag = True

    return control_flag
    

def untar_and_build(archive_list=[]):
    built_count = 0
    untarred = False
    archive_debian = None
    archive_source = None
    for arc in archive_list:
        """ For easy detection, hardcoding a requirement
        that the thing we will build is supplied as gz
        """
        if 'gz' in arc:
            if '.debian.' in arc:
                archive_debian = arc
            else:
                archive_source = arc
        else:
            return built_count

    dependencies_installed = False

    untar_res = cmdbin.untar_to_unbuilt(archive_source,0,0)
    if not untar_res:
        # Exit function with False
        return built_count

    untar_res = cmdbin.untar_to_unbuilt(archive_debian)
    if untar_res:

        deps_list = checkbuilddeps(cmdbin.DIR_UNBUILT_RELATIVE)

        if len(deps_list) > 0:
            print "{0} missing dependencies ".format(len(deps_list))
            print "needed to be installed..."
            requires_res = requires_packages(deps_list,True,False,False)
            if requires_res:
                print "...installing...now those dependencies should be available."
                dependencies_installed = True
        else:
            print "No missing dependencies needed to be installed."
            dependencies_installed = True

    if dependencies_installed:
        pname = None
        pname_list = package_names_from_control(cmdbin.DIR_UNBUILT_RELATIVE,1)
        if len(pname_list) == 1:
            pname = pname_list[0]
        else:
            return built_count

        #dpkg-gencontrol -ppython-psutil
        #control_from_template(pname,cmdbin.DIR_UNBUILT_RELATIVE)
        control_flag = control_from_template(pname)
        if not control_flag:
            return built_count

        with cd(cmdbin.DIR_UNBUILT_RELATIVE):
            """ CMD_DPKG_BUILDPACKAGEB = '/usr/bin/dpkg-buildpackage -b '
            CMD_DPKG_DEB_BUILD = '/usr/bin/dpkg-deb --build '
            Do use dpkg-buildpackage rather than dpkg-deb unless you have good reason to.
            """

            #dpkg_deb_line = "{0} . ".format(CMD_DPKG_BUILDPACKAGEB)

            build_res = cmdbin.run(CMD_DPKG_BUILDPACKAGEB)
            if build_res.succeeded:
                for line in cmdbin.run_res_lines_stripped_searching(build_res,
                                                                    'dpkg-deb: building package'):
                    line_as_list = line.split('../')
                    dname_unstripped = line_as_list[-1]
                    # Next strip dot and single quotes from right of name
                    dname = dname_unstripped.rstrip("{0}{1}".format(chr(46),chr(39)))
                    print dname
                print "Do we now have built .deb?"
            else:
                if check_res.return_code == 0:
                    pass

    return built_count



