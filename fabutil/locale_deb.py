#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

""" Locale specifically and anything related to language / encoding / localisation """

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
import re
from . import cmdbin
from . import conf_get_set as getset


CMD_LOCALE = "/usr/bin/locale "
CMD_LOCALE_LC_CTYPE = "/usr/bin/locale -c LC_CTYPE "
CMD_LOCALE_UPDATE = "/usr/sbin/update-locale "
CMD_LOCALEDEF = "/usr/bin/localedef "
CMD_LOCALEDEF_EN_GB = "/usr/bin/localedef -f UTF-8 -i en_GB en_GB.UTF-8;"
CMD_LOCALEGEN = "/usr/sbin/locale-gen "
CMD_LOCALEGEN_ARCHIVE = "/usr/sbin/locale-gen --archive "
CMD_LIST_ARCHIVE_CONTENTS = "/usr/bin/localedef --list-archive "

LOCALE_DEFAULT_CONF = '/etc/default/locale'


def lang_lc_short(verbose=False):
    """ Returns an OrderedDict representation of locale
    limited to just two entries maximum - LANG and LC_CTYPE
    """
    # chr(58) is colon (:)            ; chr(61) is equals (=) ; chr(32) is space
    loclines = []
    if verbose == True:
        locale_res = cmdbin.run(CMD_LOCALE)
        loclines = cmdbin.run_res_lines_stripped(locale_res)
    else:
        with hide('output'):
            locale_res = cmdbin.run(CMD_LOCALE)
            loclines = cmdbin.run_res_lines_stripped(locale_res)
    locale_dict = {}
    for line in loclines:
        if line.startswith('LC_CTYPE='):
            locale_dict['LC_CTYPE']=line.split(chr(61))[1]
        elif line.startswith('LANG='):
            locale_dict['LANG']=line.split(chr(61))[1]
        else:
            pass
    return locale_dict


def lang_lc_only(verbose=False):
    """ Returns an OrderedDict representation of locale
    with key entries populated for all locale output
    beginning LC_

    For clarity: There would normally be more than two key entries
    in the returned dict. If you are only interested in LANG and LC_CTYPE
    specifically, then see lang_lc_short()
    LANG key would not appear in the results from this function.
    """
    # chr(58) is colon (:)            ; chr(61) is equals (=) ; chr(32) is space
    loclines = []
    if verbose == True:
        locale_res = cmdbin.run(CMD_LOCALE)
        loclines = cmdbin.run_res_lines_stripped(locale_res)
    else:
        with hide('output'):
            locale_res = cmdbin.run(CMD_LOCALE)
            loclines = cmdbin.run_res_lines_stripped(locale_res)
    locale_dict = {}
    for line in loclines:
        if line.startswith('LC_') and chr(61) in line:
            line_array = line.split(chr(61))
            locale_dict[line_array[0]]=line_array[1]
    return locale_dict

    
def lang_lc_ctype_report(missing_shows_fail=False):
    """ Report the content of LANG and LC_CTYPE.
    If one or other does not exist and missing_shows_fail is 
    True then return False to caller.
    """
    # chr(58) is colon (:)            ; chr(61) is equals (=) ; chr(32) is space
    report_return = True
    locale_dict_short = lang_lc_short()
    key_hits = 0
    if 'LANG' in locale_dict_short:
        key_hits += 1
        print "Locale LANG is {0}".format(locale_dict_short['LANG'])
    elif missing_shows_fail == True:
        report_return = False
    else:
        pass
    if 'LC_CTYPE' in locale_dict_short:
        key_hits += 1
        print "Locale LC_CTYPE is {0}".format(locale_dict_short['LC_CTYPE'])
    elif missing_shows_fail == True:
        report_return = False
    else:
        pass
    return report_return


def list_archive_contents(verbose=True):
    if verbose == True:
        locale_res = cmdbin.run(CMD_LIST_ARCHIVE_CONTENTS)
        loclines = cmdbin.run_res_lines_stripped(locale_res)
    else:
        with hide('output'):
            locale_res = cmdbin.run(CMD_LIST_ARCHIVE_CONTENTS)
            loclines = cmdbin.run_res_lines_stripped(locale_res)
    if verbose:
        for line in loclines:
            print "archive entry: {0}".format(line)
    return loclines


def list_archive_contents_en(verbose=True):
    match_string = 'en_'
    with hide('output'):
        locale_res = cmdbin.run(CMD_LIST_ARCHIVE_CONTENTS)
        loclines = cmdbin.run_res_lines_stripped_matching(locale_res,match_string)
    if verbose:
        for line in loclines:
            print "archive entry: {0}".format(line)
        if len(loclines) < 1:
            print "Output from --list-archive gave no lines matching string {0}".format(match_string)
    return loclines


def etc_locale_gen_enable(iso639='en',iso3166=None,after_dot=None,verbosity=1):
    """ Make requested locale enabled in /etc/locale.gen

    Some samples with 'keyname' 'value' shown to left of iso639
    en_GB ISO-8859-1                 iso639='en',iso3166='GB'
    en_GB.ISO-8859-15 ISO-8859-15    iso639='en',iso3166='GB',after_dot='ISO-8859-15'
    en_GB.UTF-8 UTF-8                iso639='en',iso3166='GB',after_dot='UTF-8'
    See below for how keyname if formed. value would be last item after the space.

    This function is 'non-destructive' in that it will not change the default locale
    """
    if iso639 is None or iso3166 is None:
        return False
    elif len(iso639.strip()) < 2 or len(iso3166.strip()) < 2:
        return False
    iso_both_spaced = None
    if after_dot is None:
        keyname = "{0}_{1}".format(iso639,iso3166.upper())
    else:
        keyname = "{0}_{1}.{2}".format(iso639,iso3166.upper(),after_dot.upper())
    gen_prefix = 'etc_locale_gen_enable:'
    #error_prefix = 'etc_locale_gen_enable: Error'
    conf_filepath = '/etc/locale.gen'
    with getset.Keyedspace2(conf_filepath) as conf:
        #conf.summary_detailed(True)
        #print len(conf.keys_disabled())
        keys_enabled_printable = conf.keys_enabled_printable()
        if len(keys_enabled_printable) < 1:
            conf.keys_disabled5print()
        if keyname in keys_enabled_printable:
            iso_both_spaced = keyname
            if verbosity > 0:
                print "{0} keyname {1} already enabled. Simply return True".format(gen_prefix,keyname)
        elif keyname in conf.keys_disabled_printable():
            iso_both_spaced = keyname
            if verbosity > 0:
                print "{0} keyname {1} printable but disabled.".format(gen_prefix,keyname)
            conf.context_unhash()
        else:
            keyname_hashed = "{0}{1}".format(chr(35),keyname)
            # Signature: keys_disabled_printable(self,safe=True,hashless=False)
            if keyname_hashed in conf.keys_disabled_printable(True,False):
                iso_both_spaced = keyname
                if verbosity > 0:
                    print "{0} keyname {1} printable but disabled.".format(gen_prefix,keyname_hashed)
                unhash_res = conf.context_unhash(keyname_hashed,verbosity)
                if unhash_res is True:
                    """ Use 1.0 rather than 0.75 as often /etc/locale.gen is all commented
                    out and our change would be the first enabled key
                    sedres = getset.sed_remote_apply(None,conf_filepath,conf.sedlines(),
                                                     'medium',True,0.75,True)  <-- Use 1.0 instead
                    """
                    sedres = getset.sed_remote_apply(None,conf_filepath,conf.sedlines(),
                                                     'medium',True,1.0,True)
                    if sedres is not True:
                        iso_both_spaced = None
                else:
                    print unhash_res
        if iso_both_spaced is None and verbosity > 0:
            print "{0} keyname {1} not found or not printable".format(gen_prefix,keyname)
    return iso_both_spaced


def locale_gen(iso639='en',iso3166=None,after_dot=None,
               filesystem=False,archive=False,verbosity=1):
    """ Issue the locale-gen command.
    Suggest you call etc_locale_gen_enable() beforehand to ensure
    your locale is not commented out in /etc/locale.gen
    """
    if iso639 is None or iso3166 is None:
        return False
    elif len(iso639.strip()) < 2 or len(iso3166.strip()) < 2:
        return False
    if after_dot is None:
        keyname = "{0}_{1}".format(iso639,iso3166.upper())
    else:
        keyname = "{0}_{1}.{2}".format(iso639,iso3166.upper(),after_dot.upper())
    actioned_count = 0
    if filesystem is True:
        filesystem_return = cmdbin.run_or_sudo("{0} {1}".format(CMD_LOCALEGEN,keyname))
        if filesystem_return.succeeded:
            actioned_count += 1
    if archive is True:
        archive_return = cmdbin.run_or_sudo("{0} {1}".format(CMD_LOCALEGEN_ARCHIVE,keyname))
        if archive_return.succeeded:
            actioned_count += 1
    return actioned_count


def locale_gen_both(iso_both='en_GB',after_dot='UTF-8',
               filesystem=False,archive=False,verbosity=1):
    """ Issue the locale-gen command.
    Suggest you call etc_locale_gen_enable() beforehand to ensure
    your locale is not commented out in /etc/locale.gen
    """
    if iso_both is None:
        return False
    elif len(iso_both.strip()) < 3:
        return False
    # We perform no uppercasing of iso3166 as it is part of combined iso_both
    if '.' in iso_both:
        keyname = iso_both
    elif after_dot is None:
        keyname = iso_both
    else:
        keyname = "{0}.{1}".format(iso_both,after_dot.upper())
    actioned_count = 0
    if filesystem is True:
        filesystem_return = cmdbin.run_or_sudo("{0} {1}".format(CMD_LOCALEGEN,keyname))
        if filesystem_return.succeeded:
            actioned_count += 1
    if archive is True:
        archive_return = cmdbin.run_or_sudo("{0} {1}".format(CMD_LOCALEGEN_ARCHIVE,keyname))
        if archive_return.succeeded:
            actioned_count += 1
    return actioned_count


def enable_and_archive(iso639='en',iso3166=None,after_dot=None,verbosity=1):
    """ Enable, generate, store in archive.
    This function is 'non-destructive' in that it will not change the default locale
    """
    if iso639 is None or iso3166 is None:
        return False
    elif len(iso639.strip()) < 2 or len(iso3166.strip()) < 2:
        return False
    archived = False
    iso_both_spaced = etc_locale_gen_enable(iso639,iso3166,after_dot,verbosity)
    if iso_both_spaced is None:
        return False
    permanent = False
    if iso_both_spaced:
        actioned_count = locale_gen_both(iso_both_spaced,None,False,True,verbosity)
        if actioned_count > 1:
            archived = True
    return archived


def en_gb_and_archive(default=False,verbosity=1):
    """ Ensure en_GB locale is available and generated on the remote server.
    The generated locale should be stored in the locale archive also.

    This function is 'non-destructive' in that it will not change the default
    locale unless specifically instructed by default=True.

    The following targets are made available (Shown belows as iso639 iso3166)
    en_GB ISO-8859-1
    en_GB.ISO-8859-15 ISO-8859-15
    en_GB.UTF-8 UTF-8
    """
    #enable_res = etc_locale_gen_enable('en','GB',None,verbosity)
    #enable_res = etc_locale_gen_enable('en','GB','ISO-8859-15',verbosity)
    #enable_res = etc_locale_gen_enable('en','GB','UTF-8',verbosity)
    if verbosity > 0:
        print "Verbose so begin by showing content of archive (next)"
        list_archive_contents_en(True)
    locale_res = enable_and_archive('en','GB','UTF-8',verbosity)
    if locale_res:
        locale_res = enable_and_archive('en','GB','ISO-8859-15',verbosity)
        #if locale_res:
        #    locale_res = enable_and_archive('en','GB',None,verbosity)
    if locale_res:
        if default is True:
            if verbosity > 2:
                print "Next we make a destructive change by permanently ..."
                print "...changing locale={0} in /etc/default/locale".format(locale_res)
        pass
    return

