#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2016 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

""" Functions for sudo settings """

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
import re
try:
    from . import cmdbin
except ValueError:
    import cmdbin
from . import conf_get_set as getset

CMD_SUDO_GROUPLINE="/usr/bin/getent group sudo "
#CMD_POSTGRES_PASSLINE
CMD_SUDO_LIST_USER_TEMPLATE="/usr/bin/sudo -l -U {0}"
SUDO_CONF='/etc/sudoers'
SUDO_CONFD='/etc/sudoers.d'
SUDOERS_DEFAULT='files/sudoers8default.txt'
SUDOERS_PERMIES='files/sudoers8permies.txt'


def sudoers8default2remote(verbose=True):
	""" Put the file sudoers8default.txt to remote home directory """
	put_flag = False
	dir_remote = cmdbin.tilde_expand(None,abort_on_fail=True)
	if verbose:
		put_res = put(SUDOERS_DEFAULT,dir_remote,use_sudo=False)
	else:
		with hide('output'):
			put_res = put(SUDOERS_DEFAULT,dir_remote,use_sudo=False)
	if put_res.succeeded:
		put_flag = True
	return put_flag


def sudoers8permies2remote(verbose=True):
	""" Put the file sudoers8permies.txt to remote home directory """
	put_flag = False
	dir_remote = cmdbin.tilde_expand(None,abort_on_fail=True)
	if verbose:
		put_res = put(SUDOERS_PERMIES,dir_remote,use_sudo=False)
	else:
		with hide('output'):
			put_res = put(SUDOERS_PERMIES,dir_remote,use_sudo=False)

	if put_res.succeeded:
		put_flag = True
	return put_flag


def sudo_mismatches(print_flag=False):
	""" Returns a count of detected mismatches between the detected GNU/Linux
	and the sudoers setup
	"""
	mismatches = 0
	deb_or_rpm = cmdbin.deb_or_rpm()
	if deb_or_rpm is None:
		pass
	elif 'deb' == deb_or_rpm:
		with getset.Keyedequals(SUDO_CONF) as conf:
			if '%wheel' in conf:
				mismatches += 1
	elif 'rpm' == deb_or_rpm:
		# rpm -q --configfiles sudo
		pass
	else:
		pass
	"""
	pretty_tuple = cmdbin.pretty_name_tuple()
	if pretty_tuple[1] in cmdbin.DEBIAN_DERIVATIVES2:
		# Ubuntulike
		if '%wheel' in conf:
			mismatches += 1
	elif pretty_tuple[1] in cmdbin.RPM_BASED2:
		pass
	"""

	if print_flag:
		if mismatches == 0:
			print("Zero mismatches between OS and sudoers groups")
		else:
			print("Mismatches between OS and sudoers groups number {0}".format(mismatches))

	return mismatches

