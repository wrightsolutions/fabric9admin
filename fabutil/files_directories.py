#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2019,2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
import re
import time
from . import cmdbin
from . import acc_id
from . import package_deb as pdeb

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

CMD_STAT = "/usr/bin/stat "
CMD_STAT_OCTAL = "/usr/bin/stat -c%a "
CMD_STAT_GID = "/usr/bin/stat -c%g "
CMD_CHGRP = "/bin/chgrp --preserve-root -c "
CMD_CHMOD = "/bin/chmod --preserve-root -c "
CMD_CHMOD_RECURSIVE = "/bin/chmod --preserve-root -c -R "
CMD_CHOWN = "/bin/chown --preserve-root -c "
CMD_CP = "/bin/cp "
CMD_CP_TIMESTAMPS = "/bin/cp --preserve=timestamps "
CMD_CPR = "/bin/cp -R "
CMD_CPR_TIMESTAMPS = "/bin/cp -R --preserve=timestamps "
CMD_DLP = "/bin/vdir -dlp "
CMD_MKDIR = "/bin/mkdir "
CMD_MKDIRM = "/bin/mkdir -m "
CMD_MKDIRP = "/bin/mkdir -p "
CMD_RMV = "/bin/rm -v "
CMD_RMVFR = "/bin/rm -vfR "
CMD_RMFR = "/bin/rm -fR "

RE_VAR_LIB_ALPHA = re.compile('/var/lib/[a-z][a-z]+')
RE_OPT_LOCAL_ALPHA = re.compile('/opt/local/[a-z][a-z]+')
RE_VAR_LOCAL_ALPHA = re.compile('/var/local/[a-z][a-z]+')
RE_STATUS_INSTALLED = re.compile('Status:.*installed')

_dirpath_fab = None
""" dirpath_fab above is a working / temporary directory which you would
expect to automatically cleanup (or remove) if the upload / deploy fails.
Example: set_dirpath_fab_from_short('~/public_html/poor_cycle.fab')

Attempting to overwrite an existing (live) deployment with a directory
you have assigned to dirpath_fab is probably a bad idea as a failure would
remove your existing live site.
See http://docs.fabfile.org/en/1.4.0/api/contrib/project.html for some
recommended ways of updating an existing project rather than misusing dirpath_fab
"""

def tilde_expand(path_short,abort_on_fail=True):
    if path_short.startswith('~/'):
        """ path_expanded = path.expanduser(path_short)
        will not do as you are interested in the remote user
        not the user account on your local machine! """
        expandres = run("dirname ~/.")
        if expandres.failed:
            if abort_on_fail:
                abort("Failed during path expansion of {0}".format(path_short))
            path_expanded = path.short
        else:
            path_expanded = path_short.replace('~/',"{0}/".format(expandres),1)
        """ path_expanded = run("dirname ~/.") """
    else:
        return path_short
    return path_expanded


def slash_count(dirpath,relative_okay=False,slash='/'):
    counted = 0
    if dirpath and relative_okay:
        """ No need to check if startswith slash """
        return dirpath.count(slash)

    if dirpath.startswith(slash):
        counted = dirpath.count(slash)
    return counted


def dirpath_dlp(dirpath):
    dlp_res = None
    dlp_line = "{0} {1}".format(CMD_DLP,dirpath)
    with settings(warn_only=True):
        dlp_res = run(dlp_line)
    return dlp_res


def set_dirpath_fab_from_short(dirpath_short):
    global _dirpath_fab
    _dirpath_fab = tilde_expand(dirpath_short)
    """ Above we have created dirpath_fab by expanding (if necessary) any tilde """
    return True


def get_dirpath_fab():
    return _dirpath_fab


def rmdir_existing(dirpath,ignore_missing=True,rm_verbose=True):
    """ run_or_sudo() is not always called in preference to plain run()
    so as we do not use sudo unless absolutely necessary.
    Deleting from the users own home directory would not need sudo
    """
    rmdir_flag = False
    if dirpath.startswith('~/'):
        """ set function default return to True """
        rmdir_flag = True
    elif '/djapian/' in dirpath:
        """ set function default return to True """
        rmdir_flag = True
    else:
        """ As a safety feature against dangerous rm -fR
        commands, any directory path that does not begin
        with ~/ will be rejected (function returns False) """
        return False
    exists_dirpath = True
    with settings(warn_only=True):
        """ Tilde expansion only applies when '~' is unquoted """
        exists_dirpath = exists(tilde_expand(dirpath))
        if not exists_dirpath:
            return ignore_missing
    if exists_dirpath:
        if rmdir_flag:
            if rm_verbose:
                rm_line = "{0} {1}".format(CMD_RMVFR,dirpath)
                run(rm_line)
            else:
                rm_line = "{0} {1}".format(CMD_RMFR,dirpath)
                run(rm_line)
        else:
            if rm_verbose:
                cmdbin.run_or_sudo(CMD_RMVFR + dirpath)
            else:
                cmdbin.run_or_sudo(CMD_RMFR + dirpath)
    return rmdir_flag


def abort_with_cleanup(dirpath,abort_message,ignore_missing=True,rm_verbose=True):
    """ Similar functionality to calling abort_with_message('',cleanup_level=2)
    but with extra features available during rmdir
    """
    if dirpath is not None:
        rmdir_existing(dirpath,ignore_missing,rm_verbose)
    list_dot_d_array = pdeb.get_list_dot_d_array()
    if len(list_dot_d_array) > 0:
        for sl in list_dot_d_array:
            sl_ext = path.splitext(sl)[1]
            if sl_ext == '.list':
                rm_line = "{0} /etc/apt/sources.list.d/{1}".format(CMD_RMV,sl)
                with settings(warn_only=True):
                    cmdbin.run_or_sudo(rm_line)
    abort(abort_message)
    return


def abort_with_cleanup1(dirpath,abort_message,ignore_missing=True,rm_verbose=True):
    """ Similar functionality to calling abort_with_message('',cleanup_level=1)
    but with extra features available during rmdir
    Performs similar to abort_with_cleanup() but makes no attempt to cleanup
    sources.list.d
    """
    if dirpath is not None:
        rmdir_existing(dirpath,ignore_missing,rm_verbose)
    abort(abort_message)
    return


def abort_with_message(abort_message,cleanup_level=2):
    """ abort_with_message() was written after abort_with_cleanup() and abort_with_cleanup1()
    The first argument is the abort_message and rather than have dirpath supplied we use getter
    get_dirpath_fab(). rmdir_existing second and third arguments are True rather than
    being supplied as given parameters 'ignore_missing' and 'rm_verbose'.
    cleanup_level=0 would see this function act as if a direct call to abort(abort_message)
    """
    if cleanup_level > 0:
        dirpath_fab = get_dirpath_fab()
        if dirpath_fab:
            rmdir_existing(dirpath_fab,True,True)
    if cleanup_level > 1:
        list_dot_d_array = pdeb.get_list_dot_d_array()
        if len(list_dot_d_array) > 0:
            #print "Some cleanup of sources.list.d to be performed"
            #print list_dot_d_array
            for sl in list_dot_d_array:
                sl_ext = path.splitext(sl)[1]
                if sl_ext == '.list':
                    rm_line = "{0} /etc/apt/sources.list.d/{1}".format(CMD_RMV,sl)
                    #print rm_line
                    with settings(warn_only=True):
                        cmdbin.run_or_sudo(rm_line)
        else:
            #print "No cleanup of sources.list.d to be performed"
            pass
    abort(abort_message)
    return


def put_using_named_temp(remote_filepath,lines,abort_during_words=None,put_sudo=True):

    """ remote_filepath should be a full path
    Example: '/etc/apache2/ports.conf.append'
    See the fabric put api for clarification.
    """
    import tempfile
    abort_message = "Aborting during {0}".format(abort_during_words)
    put_res = None
    named_temp = tempfile.NamedTemporaryFile()
    try:
        named_temp.write(lines)
        named_temp.flush()
        put_res = put(named_temp,remote_filepath,use_sudo=put_sudo)
    except Exception as e:
        abort_with_cleanup(get_dirpath_fab(),"{0} - {1}".format(
                abort_message,unicode(e)) )
    finally:
        named_temp.close()

    if put_res:
        if put_res.failed:
            abort_with_cleanup(get_dirpath_fab(),"{0} put.".format(abort_message))
    else:
            abort_with_cleanup(get_dirpath_fab(),abort_message)
    return put_res


def chgrp_or_sudo(dirpath,newgroup=None):
    if newgroup and len(newgroup) > 1:
        grp = newgroup
    else:
        abort("Aborted before group setting for directory {0}".format(dirpath))
        
    chgrp_line = "{0} {1} {2}".format(CMD_CHGRP,grp,dirpath)
    chgrp_res = cmdbin.run_or_sudo(chgrp_line)
    if cmdbin.print_success_and_failed(chgrp_res,dirpath) < 1:
        abort("Aborted during group setting for directory {0}".format(dirpath))
    return chgrp_res


def group_writable(dirpath,path_startswith1=None,
                   path_startswith2=None,group_write_ext_list=[]):
    """ Only operate where the change affects a known
    path or a known file type. """
    chmod_line = None

    startswith_flag = False
    if path_startswith1 is not None and dirpath.startswith(path_startswith1):
        startswith_flag = True
    elif path_startswith2 is not None and dirpath.startswith(path_startswith2):
        startswith_flag = True

    ext = path.splitext(dirpath)[1]
    if ext and len(ext) > 1:
        """ Has extension """
        print "{0} for {1} having ext of {2}".format(
            startswith_flag,dirpath,ext)
        ext_lstripped = None
        if len(group_write_ext_list) > 0:
            ext_lstripped = ext.lstrip('.')
        if startswith_flag:
            if ext_lstripped:
                if ext_lstripped not in group_write_ext_list:
                    chmod_line = "{0} g+w {1}".format(CMD_CHMOD,dirpath)
            else:
                chmod_line = "{0} g+w {1}".format(CMD_CHMOD,dirpath)
    elif startswith_flag:
        """ No extension detected """
        chmod_line = "{0} g+w {1}".format(CMD_CHMOD,dirpath)

    if not chmod_line:
        return False
    return cmdbin.run_or_sudo(chmod_line)


def group_writable_recursive(dirpath,path_startswith1=None,
                             path_startswith2=None,group_write_ext_list=[]):
    """ Only operate where the change affects a known path. No attempt currently
    to use part_startswith2 but retained for consistent args with group_writable() """
    chmod_line = None

    startswith_flag = False
    if path_startswith1 is not None and dirpath.startswith(path_startswith1):
        startswith_flag = True
    elif RE_VAR_LIB_ALPHA.match(dirpath):
        startswith_flag = True
    elif RE_OPT_LOCAL_ALPHA.match(dirpath):
        startswith_flag = True
    elif RE_VAR_LOCAL_ALPHA.match(dirpath):
        startswith_flag = True

    if startswith_flag:
        stat_line = "{0} {1}".format(CMD_STAT_GID,dirpath)
        run_res = run(stat_line)
        gid = -1
        if run_res.succeeded:
            gid = str(run_res)
        if acc_id.idnum_between(gid,30,100):
            chmod_line = "{0} g+w {1}".format(CMD_CHMOD_RECURSIVE,dirpath)

    if not chmod_line:
        return False
    return cmdbin.run_or_sudo(chmod_line)


def mkdir_existing(dirpath,mode=None,remove_existing=True,
                   mk_verbose=False,non_tilde_remove=False):
    """ With many safety features this function is complex. Removing safety
    feature would result in a simple function of around 10 lines.
    I choose to implement these safety features so that I can make actions
    using this function on production servers with a degree of confidence that
    the features may help prevent unwanted / miscalculated recursive removals.
    """
    """  The following example would fail:
    mkdir_existing("{0}/djapian".format('/var/local'),2751,True,True)
    because optional 5th arg non_tilde_remove is specified True (or not specified).
    When the third argument is False (or default) then the fifth argument
    is irrelevant. However if you try to set the third argument True and
    dirpath is outside of tilde then you need fifth argument True or you fail.
    """
    mkdir_mode = 2711
    if mode:
        mkdir_mode = mode
    return_res = False
    short_flag = False
    userdir_flag = False
    if dirpath.startswith('~/'):
        short_flag = True
        if 'public_html' in dirpath:
            userdir_flag = True
        """ set function default return to True """
        return_res = True
    elif non_tilde_remove:
        """ non_tilde_remove acts as an override for safety feature described
        below providing directory path begins /var/l or /var/www/pub """
        if dirpath.startswith('/var/l'):
            """ set function default return to True """
            return_res = True
        elif dirpath.startswith('/var/www/pub'):
            """ set function default return to True """
            return_res = True
        else:
            return False
    elif remove_existing:
        """ As a safety feature against dangerous rm -fR
        commands, any directory path that does not begin
        with ~/ will be rejected (function returns False) """
        return False

    print dirpath
    exists_dirpath = True
    with settings(warn_only=True):
        """ Tilde expansion only applies when '~' is unquoted """
        # exists_dirpath = exists(dirpath,verbose=True)
        if short_flag:
            #exists_dirpath = exists(tilde_expand(dirpath),verbose=True)
            exists_dirpath = exists(tilde_expand(dirpath))
        else:
            exists_dirpath = exists(dirpath)
        # exists_dirpath = exists(tilde_expand(dirpath))
        #print "exists_dirpath=%s, remove_existing=%s\n" \
        #% (exists_dirpath,remove_existing)

    if exists_dirpath and remove_existing:
        if short_flag:
            run(CMD_RMVFR + dirpath)
        elif return_res:
            rm_line = CMD_RMVFR + dirpath
            print rm_line
            cmdbin.run_or_sudo(rm_line,short_flag)
        else:
            rm_line = CMD_RMVFR + dirpath
            print "Refusing to execute command: {0}".format(rm_line)

    if remove_existing and mode:
        if dirpath.startswith('/var/www/pub'):
            mkdir_line = "mkdir -p -m {0} {1}".format(mkdir_mode,dirpath)
        else:
            mkdir_line = "mkdir -m {0} {1}".format(mkdir_mode,dirpath)
        if mk_verbose:
            print mkdir_line
        mkdir_res = cmdbin.run_or_sudo(mkdir_line,short_flag)

    elif remove_existing and userdir_flag:
        mkdir_line = "mkdir -p {0}".format(dirpath)
        print mkdir_line
        mkdir_res = cmdbin.run_or_sudo(mkdir_line,short_flag)

    elif remove_existing:   # Default for function remove_existing=True
        if dirpath.startswith('/var/www/pub'):
            mkdir_line = "mkdir -p {0}".format(dirpath)
        else:
            mkdir_line = "mkdir " + dirpath
        if mk_verbose:
            print mkdir_line
        mkdir_res = cmdbin.run_or_sudo(mkdir_line,short_flag)

    elif exists_dirpath and mode:
        octal_res = run(CMD_STAT_OCTAL + dirpath)
        if octal_res.failed:
            return False
        """ octal_stat = oct(stat(dirpath)[ST_MODE]) """
        if str(octal_res).endswith(str(mode)):
            """ If mode requested matches exactly existing directory
            then use it as is. """
            return True
        else:
            return False
    elif exists_dirpath:
        return_res = False
        """ We are going to fail but still execute for useful feedback """
        mkdir_line = "mkdir {1}".format(dirpath)
        if mk_verbose:
            print "Prefail: {0}".format(mkdir_line)
        mkdir_res = cmdbin.run_or_sudo(mkdir_line,short_flag)
    elif mode and dirpath.startswith('/var/l'):
        mkdir_line = "mkdir -p -m {0} {1}".format(mkdir_mode,dirpath)
        print mkdir_line
        mkdir_res = cmdbin.run_or_sudo(mkdir_line,short_flag)
    elif userdir_flag:
        mkdir_line = "mkdir -p -m {0} {1}".format(mkdir_mode,dirpath)
        print mkdir_line
        mkdir_res = cmdbin.run_or_sudo(mkdir_line,short_flag)
    else:
        mkdir_line = "mkdir -m {0} {1}".format(mkdir_mode,dirpath)
        print mkdir_line
        mkdir_res = cmdbin.run_or_sudo(mkdir_line,short_flag)

    if mkdir_res.failed:
        return_res = False
    return return_res


def append_not_already(append_file,append_line,append_bin=None):
    if append_bin is None:
        append_bin = '/opt/local/local-bin/append_not_already.sh'

    append_run = "{0} {1} '{2}'".format(append_bin,append_file,append_line)
    print append_run
    append_res = run(append_run,pty=False)
    if not append_res.failed:
        return True
    return False


def dir_files(dirpath,period='hour',pattern=None):
    """ Count the number of files in the given directory that are real files
    and fit with the time period requested.
    Below I list the definitions from cmdbin.py which I will utilize.
    """
    #CMD_FIND_FILES_HOUR_TEMPLATE = "/usr/bin/find {0} -type f -mmin -60 "
    #CMD_FIND_FILES_DAY_TEMPLATE = "/usr/bin/find {0} -type f -mtime -1 "
    #CMD_FIND_FILES_WEEK_TEMPLATE = "/usr/bin/find {0} -type f -mtime -7 "
    #CMD_FIND_FILES_MONTH_TEMPLATE = "/usr/bin/find {0} -type f -mtime -31 "

    flist = []
    if period is None or len(period) < 3:
        return flist

    if cmdbin.dotty_level(dirpath) > 1:
        # We will not be dealing with ../ and the likes
        return flist

    exists_dirpath = exists(tilde_expand(dirpath))
    if not exists_dirpath:
        return flist

    if period.startswith('hour'):
        cmdfind = cmdbin.CMD_FIND_FILES_HOUR_TEMPLATE.format(dirpath)
    elif period == 'day':
        cmdfind = cmdbin.CMD_FIND_FILES_DAY_TEMPLATE.format(dirpath)
    elif period == 'week':
        cmdfind = cmdbin.CMD_FIND_FILES_WEEK_TEMPLATE.format(dirpath)
    elif period == 'month':
        cmdfind = cmdbin.CMD_FIND_FILES_MONTH_TEMPLATE.format(dirpath)
    else:
        cmdfind = None

    if cmdfind is None:
        return flist

    with settings(warn_only=True):
        find_res = cmdbin.run_or_sudo(cmdfind)
        if find_res.failed:
            print("Failed command {0}".format(cmdfind))
        elif pattern is None:
            flist = cmdbin.run_res_lines_stripped(find_res)
        else:
            flist = cmdbin.run_res_lines_stripped_searching(find_res,pattern)

    if len(flist) == 1 and flist[0] == '':
        return []
    return flist


def py_compiled_removal(dirpath,period='hour',pattern=None):
    """ Remove the compiled versions of settings*.py and similar 
    and fit with the time period requested.
    Very little in way of feedback other than True or False
    Below I list some useful definitions from cmdbin.py if you want to do
    more specific things in future by custom appending to the find commands.
    """
    #CMD_FIND_FILES_HOUR_TEMPLATE = "/usr/bin/find {0} -type f -mmin -60 "
    #CMD_FIND_FILES_DAY_TEMPLATE = "/usr/bin/find {0} -type f -mtime -1 "
    #CMD_FIND_FILES_WEEK_TEMPLATE = "/usr/bin/find {0} -type f -mtime -7 "
    #CMD_FIND_FILES_MONTH_TEMPLATE = "/usr/bin/find {0} -type f -mtime -31 "

    if period is None or len(period) < 3:
        return False

    if cmdbin.dotty_level(dirpath) > 1:
        # We will not be dealing with ../ and the likes
        return False

    exists_dirpath = exists(tilde_expand(dirpath))
    if not exists_dirpath:
        return False

    if period.startswith('hour'):
        cmdfind = cmdbin.CMD_FIND_PYC_HOUR_DEL_TEMPLATE.format(dirpath)
    elif period == 'day':
        cmdfind = cmdbin.CMD_FIND_PYC_DAY_DEL_TEMPLATE.format(dirpath)
    elif period == 'week':
        cmdfind = cmdbin.CMD_FIND_PYC_WEEK_DEL_TEMPLATE.format(dirpath)
    else:
        cmdfind = None

    if cmdfind is None:
        return False


    findplus_flag = False
    with settings(warn_only=True):
        find_res = cmdbin.run_or_sudo(cmdfind)
        if find_res.failed:
            print("Failed command {0}".format(cmdfind))
        else:
            findplus_flag = True

    return findplus_flag


def settings_py_compiled_removal(dirpath,period='hour',pattern=None):
    """ Remove the compiled versions of settings*.py and similar 
    and fit with the time period requested.
    Very little in way of feedback other than True or False
    Below I list some useful definitions from cmdbin.py if you want to do
    more specific things in future by custom appending to the find commands.
    """
    #CMD_FIND_FILES_HOUR_TEMPLATE = "/usr/bin/find {0} -type f -mmin -60 "
    #CMD_FIND_FILES_DAY_TEMPLATE = "/usr/bin/find {0} -type f -mtime -1 "
    #CMD_FIND_FILES_WEEK_TEMPLATE = "/usr/bin/find {0} -type f -mtime -7 "
    #CMD_FIND_FILES_MONTH_TEMPLATE = "/usr/bin/find {0} -type f -mtime -31 "

    if period is None or len(period) < 3:
        return False

    if cmdbin.dotty_level(dirpath) > 1:
        # We will not be dealing with ../ and the likes
        return False

    exists_dirpath = exists(tilde_expand(dirpath))
    if not exists_dirpath:
        return False

    if period.startswith('hour'):
        cmdfind = cmdbin.CMD_FIND_SETPYC_HOUR_DEL_TEMPLATE.format(dirpath)
    elif period == 'day':
        cmdfind = cmdbin.CMD_FIND_SETPYC_DAY_DEL_TEMPLATE.format(dirpath)
    elif period == 'week':
        cmdfind = cmdbin.CMD_FIND_SETPYC_WEEK_DEL_TEMPLATE.format(dirpath)
    else:
        cmdfind = None

    if cmdfind is None:
        return False


    findplus_flag = False
    with settings(warn_only=True):
        find_res = cmdbin.run_or_sudo(cmdfind)
        if find_res.failed:
            print("Failed command {0}".format(cmdfind))
        else:
            findplus_flag = True

    return findplus_flag
