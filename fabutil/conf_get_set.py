#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, local, put, run, settings, sudo
from fabric.contrib.console import confirm
#from fabric.contrib.files import exists,append
from os import path,unlink
from os import linesep as os_linesep
import re
import tempfile
try:
    from . import cmdbin
except ValueError:
    import cmdbin

from collections import Counter,defaultdict,OrderedDict
from mimetypes import add_type,guess_type



class Keyedfile(object):
    """ Abstraction of a simple text file format of key value pairs.
    Typical example (2 line file): VALUE1: on
    VALUE2: off
    Looser example (1 line file): VALUE1 on
    This is not intended to replace pre templated systems such as 'augeas'
    and for Apache2 configurations and sshd (pre templated) you might be better
    instead depending on augeas.

    Implementation: Intended to be used in a context ('with') and uses a class
    Keyvalfile() to hold keys and values.
    Keyvalfile() extends OrderedDict and will typically not have as many keys
    as there are lines in the file.
    However Keyvalfile() internally holds additional list and tuple helper variables
    which can be used to reconstruct the original file if you so wish (See limitations below)

    Configuration files on GNU/Linux and some unix/derivatives do cope with duplicate key
    values by allowing later values to supersede [redefine] a directive [key].
    With this in mind it would be preferable if any update method [sed] operated on
    the last line of the file with a matching key.

    Full implementation would support updates in addition to queries, but initially
    just the framework and queries are implemented.

    Limitations and recommended usage:
    The internal data structures are intended to:
    (.) support queries
    (.) support a reliable write of the file including any updates
    The data structures to support both are linked but separate.
    Do the queries first. Do use the reliable write features (when implemented)
    However you are discouraged from performing updates and then attempt to query things further.

    No contract is given here that detailed queries will be reliable once updates
    have taken place. Reiterating to clarify: Query, Update (optional), Write the changes

    By writing this function in a way that encourages context ('with') usage, there
    should be no encouragement to keep this object hanging around once your queries are
    complete, or you have updated then written.

    Limitation around toggling and case:
    on can be toggled to off and vice versa, however we currently make no provision
    for On to be toggled to Off where mixed case is used in the original input file.
    If this were to be added then certainly the function false_true_index() would
    require some thought. Would you accept On being toggle to all lowercase 'off'?

    Two update methods are planned 'put' and 'sed'. 
    In terms of number of interactions with the remote system,
    'put' is more lightweight than 'sed'.
    'sed' involves pushing a sed file to the remote and making the changes at the remote end.

    Design decision: The 'sed' method will now not automatically apply the updates on __exit__
    from the context, and will instead expect the user to be involved in a two stage process,
    the second of which is sending those sed lines to function sed_remote_apply()

    You might prefer the 'sed' method if you see the benefit of the additional code / checks
    in sed_remote_apply(), but do choose the 'put' method if you prefer a lighter touch.

    The original design used 'enabled' and 'disabled' as a binary choice based on simple
    startswith(chr(35). Later design in order to support context_update and similar
    placement of key values in remote files, introduces the term 'hashed' to indicate
    startswith(chr(35)). 'disabled' -> 'hashed', but hashed does not imply 'disabled'.
    """

    MAX_LINELENGTH = 500
    MAX_LINES = 1500
    """ Above class variables are defined once for the class, but in practice
    accessed via self.MAX...
    The intention is to have a hard limit across the class, to help prevent
    accidental reading of things that might lead to memory exhaustion.
    But if you override or redefine things at runtime, then that is at your risk.
    """
    KEYSEP_NAMED = None
    """ One of several methods of removing the guesswork about what separates a
    key from a value, is to subclass and override KEYSEP_NAMED giving it the
    key separator you wish to force use of during parsing.
    """

    HASH_SPACE = "{0}{1}".format(chr(35),chr(32))
    RE_HASHED_BEFORE_SPACEBLOCK_AFTER = re.compile(
        r"(?P<before>\s*{0})(?P<spaceblock>\s+)(?P<after>.*)".format(chr(35))
        )

    def __init__(self, filepath, markers=False, metrics=False, change_method='sed'):
        self._keyvalfile = None
        self._filepath = filepath
        self._markers = markers
        self._metrics = metrics
        self._change_method = change_method
        self._bufio = None
        self._given_linecount = 0
        self._warning_list = []

        given_lines = []
        if self.filepath is None:
            """ Nothing to populate given_lines with """
            given_lines = []
        elif hasattr(self.filepath, "__iter__") and type(self.filepath) != type(''):
            """ Multiple. (Test / debug invocation) passing in a list of lines rather than filename """
            for line in self.filepath:
                self._given_linecount = 1 + self._given_linecount
                if line.endswith(chr(10)):
                    given_lines.append(line)
                else:
                    """ Add a line feed manually so truly simulates the
                    reading from file code involving StringIO """
                    given_lines.append("{0}{1}".format(line,chr(10)))
        elif self.filepath is not None and chr(47) in self.filepath:
            """ Regular file including some path element (/)
            Here forward slash is hardcoded. This software is intended for remote
            systems that are GNU/Linux or GNU variants.
            This branch represents the most common way of using this object.
            """
            try:
                from StringIO import StringIO
                buf = StringIO()
                get_res = get(self.filepath,buf)
                buf.seek(0)
                line = True
                while line:
                    line = buf.readline()
                    self._given_linecount = 1 + self._given_linecount
                    given_lines.append(line)
                self._bufio = buf
            except IOError:
                raise Exception('IOError encountered - does file exist?')

        self._given_lines = given_lines
        given_lines = None

        if self._bufio is not None:
            """ We are not Test / debug invocation so do some extra checking on sanity of file """

            for ext in ['custom','ini','local','shared']:
                add_type('text/plain',".{0}".format(ext))

            guessed = guess_type(self._filepath)
            #print guessed

            filepath_dir = path.dirname(self._filepath)
            filepath_base = path.basename(self._filepath)

            if guessed[0] is None:
                """ If in common configuration directories /etc/default or /etc/sysconfig
                then assume okay for now
                """
                grep_line = None
                if filepath_dir == '/etc/default/':
                    pass
                elif filepath_base == 'resolv.conf':
                    pass
                elif self._filepath.startswith('/etc/sysconfig/'):
                    pass
                elif self._filepath.startswith('/etc/resolvconf/'):
                    pass
                elif filepath_base.endswith('_config'):
                    pass
                elif (self.__class__).KEYSEP_NAMED is None:
                    """ Check nonbinary and for equals (=) or colon (:) """
                    grep_line = "{0} '{1}|{2}' {3};".format(cmdbin.CMD_EGREPI,chr(61),
                                                            chr(58),self._filepath)
                else:
                    """ Check nonbinary and for KEYSEP_NAMED ( equals or colon probably ) """
                    grep_line = "{0} '{1}' {2};".format(cmdbin.CMD_FGREPI,(self.__class__).KEYSEP_NAMED,
                                                        self._filepath)
                """
                try:
                    #get_res = get(self.filepath,buf)
                    #buf.seek(0)
                    #buf.readline()
                except Exception:
                    raise Exception("Binary or undetermined: {0}".format(self._filepath))
                """
                if grep_line is not None:
                    if cmdbin.run_or_sudo(grep_line,quiet=True):
                        pass
                    else:
                        raise Exception("Binary or undetermined: {0}".format(self._filepath))
            elif guessed[0].startswith('text/'):
                pass
            else:
                pass

        return


    """ Class method to handle some logic about circumstances where
    (key,value) should not survive to make up the underlying OrderedDict
    For specialist needs subclass and override this class method.
    """
    @classmethod
    def list_of_tuples_appended(cls,key,value,list_before_append):
        if key is None or len(key) < 1:
            """ Refuse to append None or blank keys """
            return list_before_append
        elif key == value:
            """ Default behaviour is not to append where key
            and value are exactly equal """
            return list_before_append
        else:
            pass
        list_returned = list_before_append[:]
        list_returned.append((key,value))
        return list_returned


    """ Class method to instruct on how to parse a comment line.
    Stored within the class so it stays with the class definition.
    Override this in a subclass if you have specialized parsing needs.
    """
    @classmethod
    def process_comment_line(cls,stripped,keysep):
        """ Returns a 4 tuple with first two items being key and value
        Third item (replit) is True where we have performed a .split(keysep)
        Fourth item (comment) is True. Subclasses may use this item differently.
        """
        # chr(58) is colon (:)            ; chr(61) is equals (=)           ; chr(32) is space
        stripped_array = stripped.split()
        key = ''.join(stripped_array[:2]).lstrip()
        resplit = False
        comment = True
        if keysep is None:
            value = ''.join(stripped_array[2:])
        else:
            if keysep in [chr(61),chr(58)] and keysep in key:
                """ resplit but this time use the keysep we were given """
                stripped_resplit = stripped.split(keysep)
                resplit = True
                key = stripped_resplit[0]
                if len(key.split()[0]) == 1:
                    """ Start of line has space after first character. """
                    key_array = key.split()
                    """ For lines like   # orange:on   we should make
                    the key '#orange' rather than '# orange' """
                    if len(key_array) > 1:
                        key = "{0}{1}".format(key_array[0],''.join(key_array[1:]))
                    value = ''.join(stripped_resplit[1:]).strip()
                else:
                    value = ''.join(stripped_array[2:])
            else:
                value = ''.join(stripped_array[2:])
        return (key,value,resplit,comment)


    """ Class method to instruct on how to parse a line (keysep is not known).
    Stored within the class so it stays with the class definition.
    Override this in a subclass if you have specialized parsing needs.
    """
    @classmethod
    def process_promising_line(cls,stripped):
        """ There is no keysep argument here as if keysep is known then you
        should probably instead be calling process_line().
        """
        # chr(58) is colon (:)            ; chr(61) is equals (=)           ; chr(32) is space
        keysep = None
        keyd = None
        stripped_array = stripped.split()
        stripped_array_len = len(stripped_array)
        if stripped_array_len == 3:
            """ Example1: ['pgsql.max_persistent', '=', '-1']
            Example2: ['AcceptEnv', 'LANG', 'LC_*']
            print stripped_array
            """
                        #if 'persistent' in line:
                        #    print stripped_array_len,stripped_array_len,stripped_array_len
                        #    print line
            """ Check for equals (=) first then colon (:) """
            if len(stripped_array[1]) == 1 and stripped_array[1] in ([chr(61),chr(58)]):
                keysep = stripped_array[1]
                """ resplit but this time use the keysep we now have """
                stripped_array = stripped.split(keysep)
                keyfull = stripped_array[0]
                """ keyd (next) is stripped version """
                keyd = keyfull.strip()
                value = ''.join(stripped_array[1:]).strip()
            else:
                keyfull = stripped_array[0]
                value = ''.join(stripped_array[1:]).strip()
                """ For this sort of line no need to call tuple7, just construct manually """
                #list_of_tuples_all.append((keyfull,line,0,None,0,line[-1],linecount))
        elif stripped_array_len > 3:
            """ Example: ['#Privilege', 'Separation', 'is', 'turned', 'on', 'for', 'security']
            print stripped_array
            """
            keyfull = stripped_array[0]
            value = ''.join(stripped_array[1:]).strip()
            """ For this sort of line no need to call tuple7, just construct manually """
            #list_of_tuples_all.append((keyfull,line,0,None,0,line[-1],linecount))
        else:
            """ Probably of form key=value or key:value with no spacing around =/:
            Check for equals (=) first then colon (:) then finally space """
            if chr(61) in stripped_array[0]:
                keysep = chr(61)
                keyplus = stripped_array[0]
                pos = keyplus.index(keysep)
                keyfull = keyplus[:pos]
                """ keyd (next) is stripped version """
                keyd = keyfull.strip()
                stripped_array = stripped.split(keysep)
                value = ''.join(stripped_array[1:]).strip()
            elif chr(58) in stripped_array[0]:
                keysep = chr(58)
                keyplus = stripped_array[0]
                pos = keyplus.index(keysep)
                keyfull = keyplus[:pos]
                """ keyd (next) is stripped version """
                keyd = keyfull.strip()
                #value = keyplus[pos+1:]
                stripped_array = stripped.split(keysep)
                value = ''.join(stripped_array[1:]).strip()
            elif stripped_array_len == 2:
                keysep = chr(32)
                keyfull = stripped_array[0]
                value = ''.join(stripped_array[1:]).strip()
            else:
                keyfull = stripped_array[0]
                value = ''.join(stripped_array[1:]).strip()
                if keysep is not None:
                    """ resplit but this time use the keysep we now have """
                    stripped_array = stripped.split(keysep)
                    keyfull = stripped_array[0]
                    value = ''.join(stripped_array[1:]).strip()
                    """ No need to call tuple7, just construct manually """
                    #list_of_tuples_all.append((keyfull,line,0,keysep,0,line[-1],linecount))
        if keyd is None and keyfull is not None:
            """ If you have not assigned keyd explicitly above
            then it is simply same value as keyfull """
            keyd = keyfull
        return (keyfull,value,keysep,keyd)


    """ Class method to instruct on how to parse a line (keysep IS KNOWN).
    Stored within the class so it stays with the class definition.
    Override this in a subclass if you have specialized parsing needs.
    """
    @classmethod
    def process_line(cls,stripped,keysep):
        """ keysep argument is mandatory here. If keysep is not known
        should probably instead be calling process_promising_line().
        """
        # chr(58) is colon (:)            ; chr(61) is equals (=)           ; chr(32) is space
        if keysep is None:
            raise Exception('Parse: Only process_promising_line() should be called keysep=None')
        stripped_array = stripped.split(keysep)
        keyfull = stripped_array[0]
        """ keyd (next) is stripped version """
        keyd = keyfull.strip()
        value = ''.join(stripped_array[1:]).strip()
        #if keyd == 'orange':
        #    print "{0}F{1}D{2}".format(keyfull,keyd,value)
        """ The spacing preserved in keyfull will be used later within tuple7() 
        to record before_count and after_count so that we can define very
        focussed sed targets for any requested toggle / replacements """
        return (keyfull,value,keysep,keyd)


    """ Following properties have no set() as only set directly during class init. """
    @property
    def keyvalfile(self):
        return self._keyvalfile

    @property
    def filepath(self):
        return self._filepath

    @property
    def markers(self):
        return self._markers

    @property
    def metrics(self):
        return self._metrics

    @property
    def change_method(self):
        return self._change_method

    """ warning_list property requires no setter as helper warning() is used to append """
    @property
    def warning_list(self):
        return self._warning_list


    def warning(self,warning_line):
        """ Append the given warning to the internal list
        and return the resulting list including the new append.
        The only time an empty list is returned is if there
        is an unexpected input or event.
        """
        list_returned = []
        if warning_line is None:
            return list_returned
        elif len(warning_line.strip()) < 1:
            return list_returned
        else:
            pass
        list_returned = self._warning_list
        try:
            list_returned.append(warning_line)
            # Only replace the existing contents of self._warning_list
            # if no Exception
            assert len(list_returned) > len(self._warning_list)
            self._warning_list = list_returned
        except Exception:
            list_returned = []
        return list_returned


    def except_to_false(function_to_decorate):
        """ Catch raised exceptions and return False if exception caught
        Decorator useful for decorating feedback functions called during exit handling AssertionError 
        """
        def inner_accepting_args(*args, **kwargs):
            #print("Try except wrapping function", function_to_decorate.__name__)
            inner_return = False
            try:
                function_return = function_to_decorate(*args, **kwargs)
                inner_return = True
            except Exception:
                inner_return = False
            return inner_return                        

        return inner_accepting_args


    def except_to_false_verbose(self,function_to_decorate):
        """ Catch raised exceptions and return False if exception caught
        Decorator useful for decorating feedback functions called during exit handling AssertionError
        Two argument variant as we are an instance method.
        """
        def inner_accepting_args(*args, **kwargs):
            print("Try except wrapping function", self.function_to_decorate.__name__)
            inner_return = False
            try:
                function_return = self.function_to_decorate(*args, **kwargs)
                print("Try except wrapped function returned", function_return)
                inner_return = True
            except Exception:
                inner_return = False
            return inner_return                        

        return inner_accepting_args


    def tuple7(self,key_unstripped,line,linenum,keysep=None):
        """ Returns a 7 tuple with fields as follows:
        key, line, before_count, sep, after_count, linesep, linenum

        The first argument should include any right padding of spaces,
        as we will be recording the count of that padding in before_count
        although we will strip the key before then placing it in the tuple.

        Note: We make no attempt to analyze the content of the whitespace
        that may separate key from sep and sep from value, meaning,
        a single space between key and sep would see before_count=1
        a single tab between key and sep would see before_count=1

        ('countryName_default', 'countryName_default\t\t= AU\n', 2, '=', 1, '\n', 2)

        Above is an example of tuple7 return for a line from an openssl.cnf

        Keyvalfile and its subclasses provide a tuple8 method
        that returns a tuple with last items of class Counter
        to support more detailed analysis.
        """
        unstripped_len = len(key_unstripped)
        key_rstripped = key_unstripped.rstrip()
        stripped_len = len(key_rstripped)
        before_count = 0
        if unstripped_len > stripped_len:
            before_count = unstripped_len - stripped_len
        after_count = 0
        """ after_count = 0 (above) as would always be zero when keysep is space """
        if keysep is not None and len(keysep.strip()) > 0:
            remainder = line[(unstripped_len+len(keysep)):]
            remainder_len = len(remainder)
            stripped_len = len(remainder.lstrip())
            if remainder_len > stripped_len:
                after_count = remainder_len - stripped_len
        linesep = None
        try:
            linesep = line[-1]
        except IndexError:
            pass
        return (key_rstripped,line,before_count,keysep,after_count,linesep,linenum)


    def tuple6(self,line,keysep,verbose=False,output_prefix='tuple6:'):
        """ Returns a 6 tuple with fields as follows:
        word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
        You are discouraged from doing any preprocessing on first argument (line)
        if the resulting tuple is going to be part of tuple14.
        """
        word1 = None
        word1firstchar = ''
        keyseppos = -1
        after_word_index = -1
        lastword = 'zZzZz!zZzZz'
        target = None
        # If returned now would see (None,'',-1,-1,'zZzZz!zZzZz',None)
        tuple_default = (word1,word1firstchar,keyseppos,after_word_index,lastword,target)
        if line is None:
            return tuple_default
        if not set(line).issubset(cmdbin.SET_PRINTABLE):
            if verbose:
                print "{0} Non printable in line {1}".format(output_prefix,
                                                             set(line).intersection(cmdbin.SET_PRINTABLE))
            return tuple_default
        if keysep is False:
            if verbose:
                print "{0} keysep not supplied for line {1}".format(output_prefix,line)
            if self.keysep is None:
                print "{0} keysep was not supplied for line and self.keysep is None!".format(output_prefix)
            elif len(self.keysep.strip()) < 1:
                print "{0} keysep was not supplied for line and self.keysep={1}".format(output_prefix,self.keysep)
            elif verbose:
                print "{0} keysep of {1} will be searched for line {2}".format(output_prefix,
                                                                               self.keysep,line)
            keysep = self.keysep

        if (self.__class__).KEYSEP_NAMED == chr(32) and keysep == chr(32):
            # Very specific that 'space' is separator so only split on space.
            line_array = line.lstrip().split(' ')
            #print "Very specific {0}".format(line_array)
        else:
            line_array = line.split()

        if len(line_array) < 1:
            if verbose:
                print "{0} line_array unexpectedly short for line {1}".format(output_prefix,line)
        elif len(line_array[0]) > 0:
            word1 = line_array[0]
            if verbose: print "word1 set to {0}".format(word1)
            word1firstchar = word1[0]
            lastword = line_array[-1].rstrip()
        else:
            if verbose: print "{0} was NOT used to set word1".format(line_array[0])
            pass

        if keysep is None:
            if verbose:
                print "{0} will not search for keysep of {1} in line {2}".format(output_prefix,
                                                                                 keysep,line)
            line_stripped = None
        else:
            line_stripped = line.strip()

        if line_stripped is not None:
            try:
                find_res = line.find(keysep)
                if find_res < 0:
                    """ Setting line_stripped None should prevent any further detailed analysis """
                    line_stripped = None
                else:
                    if verbose:
                        print "{0} keysep {1} found in line {2} at pos {3}".format(
                            output_prefix,keysep,line,find_res)
                    keyseppos = find_res
            except AttributeError:
                line_stripped = None

        if line_stripped is not None and len(keysep.strip()) > 0:
            # Strong key separator such as colon or equals
            if line_stripped.startswith(Keyedfile.HASH_SPACE):
                stripped = line_stripped[2:]
            else:
                stripped = line_stripped
            stripped_array = stripped.split()
            for word in stripped_array:
                if keysep == word:
                    break
                after_word_index += 1
                if keysep in word:
                    break

        if line_stripped is not None and len(keysep.strip()) < 1:
            # key separator is space so redefine keyseppos ignoring the result of previous .find()
            if verbose:
                print "{0} keysep {1} found in line {2} at pos {3}. word1={4}".format(
                    output_prefix,keysep,line,find_res,word1)
            keyseppos = -1
            resep = r"(\s*)({0})(?P<separatorgroup>\s*)(\S*)".format(word1)
            matched = re.match(resep,line)
            if matched:
                separatorgroup = matched.group('separatorgroup')
                keyseppos = matched.start('separatorgroup')
            if keyseppos >= 0:
                if verbose:
                    print "{0} keysep {1} found in line {2} at pos {3}".format(
                        output_prefix,keysep,line,find_res)
                if line_stripped.startswith(Keyedfile.HASH_SPACE):
                    stripped = line[2:keyseppos].strip()
                else:
                    stripped = line[:keyseppos].strip()
                #print keyseppos,stripped
                stripped_array = stripped.split()
                after_word_index = len(stripped_array)


        """ An explanation of after_word_index is due here.
        Summary: The word number of alphanumeric item that comes immediately
        after key separator (keysep).

        Where you have a clearly identified key separator then after_word_index should
        be >= 1. Example using an Apache2 line: 'MaxKeepAliveRequests 100' described below.
        So the key is MaxKeepAliveRequests and the value is 100 and the key separator is space(s).
        Q> What is the index number of the word that immediately follows the key separator?
        Ans> 1.
        Example using this line: 'cups server = mycupsserver:1631'
        Q> What is the index number of the word that immediately follows the key separator?
        Ans> 3.

        See test suite entry test_keysep_after_word_index() and helper function keysep_after_word_index()
        for more detail.
        """
        if keyseppos >= 0:
            if after_word_index < 1 and verbose:
                print "{0} after_word_index (unexpected) is {1} for line {2}...{3}".format(output_prefix,
                                                                                           after_word_index,
                                                                                           word1,lastword)
            target = line[:(1+keyseppos)]
        if verbose:
            print "{0} after_word_index is {1} for line {2}...{3}".format(output_prefix,after_word_index,
                                                                          word1,lastword)
        return (word1, word1firstchar, keyseppos, after_word_index, lastword, target)


    def tuple14nonekey(self,line,linenum,keysep=False):
        """ Function will typically be called with final parameter (keysep) omitted
        If you do supply a keysep then it will be respected in the later tuple creation.

        Typical None key tuple7 example1: (None,line,0,None,0,line[-1],linecount)
        example2 (IndexError): (None,line,0,None,0,None,linecount)

        tuple7: key, line, before_count, sep, after_count, linesep, linenum
        """
        linesep = None
        try:
            linesep = line[-1]
        except IndexError:
            pass
        if keysep is False:
            list14 = [None,line,0,None,0,linesep,linenum]
        else:
            list14 = [None,line,0,keysep,0,linesep,linenum]
        # tup7 = self.tuple7(key_unstripped,line,linenum,keysep)
        list14.append(False) # append comment_flag = False
        # Above have appended comment_flag=False and next we extend
        if keysep is False:
            list14.extend(self.tuple6(line,None))
        else:
            list14.extend(self.tuple6(line,keysep))
        assert 14 == len(list14)
        return tuple(list14)


    def tuple14keyonly(self,key,line,linenum,keysep=False):
        """ Function will typically be called with final parameter (keysep) omitted
        If you do supply a keysep then it will be respected in the later tuple creation.
        
        Typical None key tuple7 example1: (key,line,0,None,0,line[-1],linecount)
        example2 (IndexError): (key,line,0,None,0,None,linecount)

        tuple7: key, line, before_count, sep, after_count, linesep, linenum

        Unlike tuple14nonekey() the first item here placed in list14 is key
        """
        linesep = None
        try:
           linesep = line[-1]
        except IndexError:
            pass
        if keysep is False:
            list14 = [key,line,0,None,0,linesep,linenum]
        else:
            list14 = [key,line,0,keysep,0,linesep,linenum]
        # tup7 = self.tuple7(key_unstripped,line,linenum,keysep)
        list14.append(False) # append comment_flag = False
        # Above have appended comment_flag=False and next we extend
        if keysep is False:
            list14.extend(self.tuple6(line,None))
        else:
            list14.extend(self.tuple6(line,keysep))
        assert 14 == len(list14)
        return tuple(list14)


    def tuple14keycomment(self,key,line,linenum,keysep=False):
        """ Function will typically be called with final parameter (keysep) omitted
        If you do supply a keysep then it will be respected in the later tuple creation.

        Typical None key tuple7 example1: (key,line,0,None,0,line[-1],linecount)
        example2 (IndexError): (key,line,0,None,0,None,linecount)

        tuple7: key, line, before_count, sep, after_count, linesep, linenum

        Unlike tuple14nonekey() the first item here placed in list14 is key
        Unlike tuple14keyonly() we set comment_flag True
        """
        linesep = None
        try:
            linesep = line[-1]
        except IndexError:
            pass
        if keysep is False:
            list14 = [key,line,0,None,0,linesep,linenum]
        else:
            list14 = [key,line,0,keysep,0,linesep,linenum]
        #tup7 = self.tuple7(key_unstripped,line,linenum,keysep)
        list14.append(True) # append comment_flag = True
        # Above have appended comment_flag=True and next we extend
        if keysep is False:
            list14.extend(self.tuple6(line,None))
        else:
            list14.extend(self.tuple6(line,keysep))
        assert 14 == len(list14)
        return tuple(list14)


    def tuple14key(self,key,line,linenum,keysep):
        """ Function will typically be called with final parameter (keysep) omitted
        If you do supply a keysep then it will be respected in the later tuple creation.

        Typical None key tuple7 example1: (key,line,0,None,0,line[-1],linecount)
        example2 (IndexError): (key,line,0,None,0,None,linecount)

        tuple7: key, line, before_count, sep, after_count, linesep, linenum

        Semi-related to tuple14nonekey(), tuple14keyonly(), tuple14keycomment() however
        instead of populating first 7 fields directly we call tuple7() as we are
        interested in storing the real values for before_count and after_count.
        keysep is Mandatory. Supply None for keysep if you must.
        """
        linesep = None
        list14 = list(self.tuple7(key,line,linenum,keysep))
        """
        tup7 = self.tuple7(key_unstripped,line,linenum,keysep)
        if 'Max' in key and chr(32) == (self.__class__).KEYSEP_NAMED:
            print self.tuple6(line,keysep)
        """
        #if 'MaxSpare' in line: print 'MaxSpare',keysep,line.rstrip(),self.tuple6(line,keysep,True)
        #if '8859-15' in line: print '8859-15',keysep,line.rstrip(),self.tuple6(line,keysep,True)
        list14.append(False) # append comment_flag = False
        """ Above added comment_flag=False via append() and next we .extend() """
        list14.extend(self.tuple6(line,keysep))
        assert 14 == len(list14)
        return tuple(list14)



    def __enter__(self):


        class Keyvalfile(OrderedDict):
            """ Inner class that extends OrderedDict and provides a convenient
            API for queries / updates of a keyed value file.

            No contract is given here that detailed queries will be reliable once updates
            have taken place. Reiterating to clarify: Query, Update (optional), Write the changes
            If you query after you have called toggle_safe/sedline_safe/context_update_localhost/
            context_update then no guarantee is given of correctness or consistency.

            Do you queries and plan your changes, then make appropriate calls to toggle_safe
            or context_update_localhost or similar, and then use sed_remote_apply or manual put
            to have the remote file update. Keep to this order.
            There is plenty of help with providing feedback during workings by using verbose
            or verbosity settings. See examples_get_set.py for illustration.

            If you absolutely must do post update queries then here is a possible workflow:
            with getset.Keyedequals('/etc/default/sysstat') as conf:
               # query some values
               # change some values
            with getset.Keyedequals('/etc/default/sysstat') as conf:
               # query some values

            Update mechanisms - overview:
            The preferred update method is sed. There are also some helper functions
            to give you output suitable for manual / user controlled put.


            Update mechanisms - how changes are recorded / initiated:
               Recommended1: toggle_safe -> sedline_safe. Changes applied remotely via sed_remote_apply
               Recommended2: context_update_localhost -> context_update. Changes applied remotely via sed_remote_apply

            Update mechanisms (sed):
            Take the output of list_of_sedlines() and give that to sed_remote_apply()
            Can be combined in a single line if you prefer. Use sedlines() or list_of_sedlines() as you prefer.
            with getset.Keyedequals('/etc/default/sysstat') as conf:
               conf.toggle_safe('ENABLED','true')
               sedres = getset.sed_remote_apply('72a09b2f30c4b1cab7e9a366ee50988c',conf.filepath,conf.sedlines())

            Update mechanisms (manual / user controlled):
            If you prefer to use a manual 'put' command yourself, then there are several
            helpers already written. All begin lines_for_put..
            Recommended: lines_for_put() or lines_for_put_auto()
            Less functional / rigorous alternative lines_for_put_simple() is provided for testing.
            Text search helper lines_for_put_containing() provides a convenient testing helper also
            """

            FALSELIST = ['0','false','False','off','no']
            TRUELIST = ['1','true','True','on','yes']
            FALSETRUELIST=FALSELIST[:]
            FALSETRUELIST.extend(TRUELIST)
            CARET_HASH = "{0}{1}".format(chr(94),chr(35))
            HASH_SPACE = "{0}{1}".format(chr(35),chr(32))

            def __init__(self, *args, **kwargs):
                fpath = kwargs.pop('filepath', None)
                outerclass_name = kwargs.pop('outerclass_name', 'OuterClass')
                warning_list = kwargs.pop('warning_list', [])
                super(Keyvalfile, self).__init__(*args, **kwargs)
                self._filepath = fpath
                self._linecount = 0
                self._linecount_adjustment = 0
                self._lines_tuple = ()
                """ Above is a tuple of lines, below is a list of 7 field tuples """
                self._tuples_all = []
                """ Above tuples_all() is normally set later by an external call to linecount_and_lines_set()
                and the tuples that will be supplied are each 14 field tuples.
                """
                self._conf_linesep = None
                self._change_count = 0
                self._md5sum = 'a'*32
                self._keysep = ' '
                self._keysep_named = None
                self._dict_of_changed = OrderedDict()
                """ Regardless of update method, dict_of_changed and dict_of_seds
                are kept in step remember. """
                self._dict_of_seds = OrderedDict()
                self._keylen_average = 0
                """ Duplicates will not play any part in boosting or reducing the figure
                for keylen_average as we are sourcing direct from the dictionary object
                itself.
                Two reasons why keylen_average_full might be higher than keylen_average:
                (.) keylen_average_full takes into account every non blank key and
                will not exclude duplicate keys from the totalling process.
                (.) Some implementations of the parser routines may choose to strip
                (leading strip?) the key before adding it to the dictionary.
                keylen_average_full in that case may be counting spacing which
                will not be counted when totalling directly from the dictionary keys
                """
                self._keylen_average_full = 0
                """ See the traffic light examples in the test suite for an example
                of input that would produce key_duplicates_count > 0 """
                self._key_duplicates_count = 0
                """ keylist_frozen is a one time snapshot of the keys when the object
                was populated. It will never be updated. Use of self.items() is preferable
                unless you have a discrepancy and feel consulting the snapshot would help
                """
                self._keylist_frozen = []
                self._outerclass_name = outerclass_name
                self._warning_list = warning_list


            """ linecount property requires no setter as only when lines supplied """
            @property
            def filepath(self):
                return self._filepath

            """ linecount property requires no setter as only when lines supplied """
            @property
            def linecount(self):
                return self._linecount

            def linecount_adjustment_get(self):
                return self._linecount_adjustment

            #def linecount_adjustment_set(self,linecount_adjustment):
            #    self._linecount_adjustment = linecount_adjustment

            def linecount_adjustment_decrement(self):
                self._linecount_adjustment = self.linecount_adjustment_get()-1

            def linecount_adjustment_increment(self):
                self._linecount_adjustment = 1+self.linecount_adjustment_get()

            linecount_adjustment = property(linecount_adjustment_get)

            def linecount_adjusted(self):
                return (self._linecount + self._linecount_adjustment)

            @property
            def lines_tuple(self):
                """ Tuple of lines (never updated). See tuples_all if you want a list of 14 field tuples
                Call lines_for_put() if you want something might include changes that you initiated. """
                return self._lines_tuple

            @property
            def tuples_all(self):
                """ tuples_all contains tuples of 14 fields:
                tuple14 = key, line, before_count, sep, after_count, linesep, linenum, comment_flag,
                  word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                See lines_tuple if you instead want a tuple of lines """
                return self._tuples_all

            @property
            def change_count(self):
                return self._change_count

            @property
            def conf_linesep(self):
                return self._conf_linesep

            def change_count_increment(self):
                self._change_count += 1

            def switch_commented_on_toggle_get(self):
                return self._switch_commented_on_toggle

            def switch_commented_on_toggle_set(self,switch_commented_on_toggle):
                self._switch_commented_on_toggle = switch_commented_on_toggle

            switch_commented_on_toggle = property(switch_commented_on_toggle_get,switch_commented_on_toggle_set)


            """ md5sum property requires no setter as only set when lines supplied """
            @property
            def md5sum(self):
                return self._md5sum

            """ keysep property requires no setter as only set when lines supplied """
            @property
            def keysep(self):
                return self._keysep

            """ keysep_named property requires no setter as only set when lines supplied """
            @property
            def keysep_named(self):
                return self._keysep_named

            """ dict_of_seds property requires no setter as only set when lines supplied """
            @property
            def dict_of_seds(self):
                """ Dictionary of seds. Key is line number of original file line on which the
                sed is intended to operate. Value is the sedline to feed to the sed utility
                """
                return self._dict_of_seds

            """ keylen_average property requires no setter as only set when lines supplied """
            @property
            def keylen_average(self):
                return self._keylen_average

            """ keylen_average_full property requires no setter as only set when lines supplied """
            @property
            def keylen_average_full(self):
                return self._keylen_average_full

            """ key_duplicates_count property requires no setter as only set when lines supplied """
            @property
            def key_duplicates_count(self):
                return self._key_duplicates_count

            """ keylist_frozen property requires no setter as only set when lines supplied """
            @property
            def keylist_frozen(self):
                return self._keylist_frozen

            """ warning_list property requires no setter as helper warning() is used to append """
            @property
            def warning_list(self):
                return self._warning_list

            """ outerclass_name property requires no setter as set during initialisation only. """
            @property
            def outerclass_name(self):
                return self._outerclass_name


            """ list_of_sedlines property requires no setter as only constructed at call time """
            @property
            def list_of_sedlines(self):
                """ If we had something that looked like this: 2 s/bb/tt/; 1 s/aa/ss/; 3 s/cc/uu/
                Then we used the linenumber (in this case 2,1,3) and order things in descending order
                so that our resulting list is like: s/cc/uu/; s/bb/tt/ ; s/aa/ss/
                """
                sedlines = []
                for linenum in sorted(self._dict_of_seds.iterkeys(),reverse=True):
                    sedlines.append(self._dict_of_seds[linenum])
                return sedlines


            def sedlines(self,print_flag=False):
                sedlines = self.list_of_sedlines
                if print_flag:
                    for sedline in sedlines:
                        print sedline
                return sedlines


            def sedlines_containing(self,expression,nonkey=True):
                """ Returns a list of sedlines which contain the target expression / string
                nonkey=True (default) says that the target portion of the sed should not be searched.
                chr(47) is forward slash (/)
                """
                sedlines_returned = []
                try:
                    re_searching = re.compile(re.escape(expression))
                except Exception:
                    return sedlines_returned
                if nonkey is True:
                    SPACE_S_SLASH = " s{0}".format(chr(47))
                    for line in self.list_of_sedlines:
                        pos = line.find(SPACE_S_SLASH)
                        if pos >= 0:
                            if re_searching.search(line[(pos+3):]):
                                sedlines_returned.append(line)
                        else:
                            if re_searching.search(line):
                                sedlines_returned.append(line)
                else:
                    for line in self.list_of_sedlines:
                        if re_searching.search(line):
                            sedlines_returned.append(line)
                return sedlines_returned


            def first_n_keys(self,n_int):
                keys = []
                try:
                    n_end = n_int-1
                    from itertools import islice
                    keys = list(islice(self.iterkeys(), 0, n_end))
                except Exception:
                    pass
                return keys


            def warning(self,warning_line):
                """ Append the given warning to the internal list
                and return the resulting list including the new append.
                The only time an empty list is returned is if there
                is an unexpected input or event.
                """
                list_returned = []
                if warning_line is None:
                    return list_returned
                elif len(warning_line.strip()) < 1:
                    return list_returned
                else:
                    pass
                list_returned = self._warning_list
                try:
                    list_returned.append(warning_line)
                    # Only replace the existing contents of self._warning_list
                    # if no Exception
                    assert len(list_returned) > len(self._warning_list)
                    self._warning_list = list_returned
                except Exception:
                    list_returned = []
                return list_returned


            def keysep_after_word_index(self,line,keysep=False,verbosity=0):
                """ Return the word number after which keysep is found.
                If keysep is not supplied then self.keysep is used.
                For our purposes leading hash / octothorpe should not
                be counted as a word. Left strip to ensure we disregard
                left spacing before first word and to reliably identify
                hash / octothorpe to ignore also.

                Concise version of this logic appears in tuple6() but
                this version is retained for testing / verification as
                it provides a verbosity level setting for greater
                control over feedback.
                """
                after_word_index = -1
                if keysep is False:
                    if verbosity > 0:
                        print "keysep not supplied for line {0}".format(line)
                    if self.keysep is None:
                        print "keysep was not supplied for line and self.keysep is None!"
                    elif len(self.keysep.strip()) < 1:
                        print "keysep was not supplied for line and self.keysep={0}".format(self.keysep)
                    elif verbosity > 1:
                        print "keysep of {0} will be searched for in line {1}".format(self.keysep,line)
                    keysep = self.keysep
                if keysep is None:
                    if verbosity > 1:
                        print "will not search for keysep of {0} in line {1}".format(keysep,line)
                    return after_word_index
                if line is None:
                    return after_word_index
                if not set(line).issubset(cmdbin.SET_PRINTABLE):
                    if verbosity > 0:
                        print "Non printable in line {0}".format(set(line).intersection(cmdbin.SET_PRINTABLE))
                    return after_word_index
                try:
                    if line.find(keysep):
                        if verbosity > 1:
                            print "keysep {0} found in line {1}".format(keysep,line)
                    else:
                        return after_word_index
                except AttributeError:
                    raise
                stripped = line.lstrip()
                if stripped.startswith(Keyvalfile.HASH_SPACE):
                    stripped = stripped[2:]
                stripped_array = stripped.split()
                for word in stripped_array:
                    after_word_index += 1
                    if keysep in word:
                        break
                if verbosity > 1:
                    print after_word_index,after_word_index,after_word_index
                return after_word_index
                

            def enclosed_by_char(self,val):
                """ Return chr(34) double quote or chr(39) single quote
                or None to indicate how the value is enclosed
                """
                enclosed_by = None
                if val is True or val is False:
                    return None
                given_len = len(val)
                val_stripped = val.strip(chr(34))
                if 2 == given_len - len(val_stripped):
                    """ enclosed by double quotes """
                    enclosed_by = chr(34)
                else:
                    val_stripped = val.strip(chr(39))
                    if 2 == given_len - len(val_stripped):
                        """ enclosed by single quotes """
                        enclosed_by = chr(39)
                return enclosed_by


            def unenclosed_simple(self,val,max_removals=None):
                enclosed_by = self.enclosed_by_char(val)
                unenc = val
                if enclosed_by is None:
                    pass
                elif max_removals is None or max_removals < 1:
                    unenc = val.strip().replace(enclosed_by,'')
                else:
                    unenc = val.strip().replace(enclosed_by,'',max_removals)
                return unenc


            def boolean_from_count(function_to_decorate):
                """ Obtain function return and if numeric and > 0 then return True.
                Otherwise return False.
                Decorator useful for decorating functions which return counts to give boolean.
                If you check the contract of the function you are decorating carefully, then you
                should not need a boolean wrapper such as this, but use it if you find it convenient.
                Performance critical code should instead check the contract of the called function.
                """
                def inner_accepting_args(*args, **kwargs):
                    #print("Try except wrapping function", function_to_decorate.__name__)
                    inner_return = False
                    function_count = function_to_decorate(*args, **kwargs)
                    inner_count = 0
                    try:
                        inner_count = int(recorded_count)
                    except Exception:
                        inner_count = 0
                    inner_return = False
                    if inner_count > 0:
                        inner_return = True
                    return inner_return                        

                return inner_accepting_args


            def counter_tuple(self,tuple14):
                """ 
                tuple14 = key, line, before_count, sep, after_count, linesep, linenum, comment_flag,
                  word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                """
                counter_default = Counter()
                tuple4 = (0,counter_default,counter_default,counter_default)
                if tuple14 is None:
                    return tuple14
                if len(tuple14) > 10:
                    tup = tuple14
                else:
                    return tuple4
                counter_length_sum = 0
                keyseppos = tup[10]
                if keyseppos < 1:
                    return tuple4
                line = tup[1]
                keysep = tup[3]
                keyseppos = tup[10]
                before = ''
                after = ''
                if keyseppos == 0:
                    left_of_keysep = ''
                elif keyseppos > 0:
                    left_of_keysep = line[:keyseppos]
                else:
                    left_of_keysep = None
                if left_of_keysep is not None:
                    if len(keysep.strip()) > 0:
                        re1 = r"(?P<beforegroup>\s+){0}".format(keysep)
                        matched1 = re.search(re1,line[:(keyseppos+1)])
                        if matched1:
                            #left = matched1.group('leftgroup')
                            before = matched1.group('beforegroup')
                            #print matched1,keyseppos,matched1.start(),matched1.end(),len(left),len(before),line
                    else:
                        # Use strip and len() to make a substring
                        diff = keyseppos - len(left_of_keysep.rstrip())
                        if diff > 0:
                            before = left_of_keysep[-diff:]
                    re2 = r"({0})(?P<aftergroup>\s*)(\S*)".format(keysep)
                    matched2 = re.match(re2,line[keyseppos:])
                    if matched2:
                        # key_unstripped was likely supplied rstripped so adjust
                        after = matched2.group('aftergroup')
                before_count = tup[2]
                after_count = tup[4]
                #print len(before),before_count,line
                assert before_count == len(before)
                assert after_count == len(after)
                before_counter = Counter(before)
                after_counter = Counter(after)
                left = ''
                diff_left = len(left_of_keysep) - len(left_of_keysep.lstrip())
                if diff_left > 0:
                    left = left_of_keysep[:diff_left]
                left_counter = Counter(left)
                counter_length_sum = sum(left_counter.values()) + sum(before_counter.values()) + \
                    sum(after_counter.values())
                return (counter_length_sum,left_counter,before_counter,after_counter)


            def tuple8(self,key_unstripped,line,keysep,verbose=False,output_prefix='tuple8:'):
                """ Returns an 8 tuple with fields as follows:
                key, line, before, sep, after, lastchar, before_counter, after_counter

                The first argument should include any right padding of spaces,
                as we will be recording the count of that padding in before_count
                although we will strip the key before then placing it in the tuple.

                Unlike tuple7 which is used during initialisation in the outer
                class, we do not return linenum.
                """
                key_unstripped_len = len(key_unstripped)
                key_rstripped = key_unstripped.rstrip()
                stripped_len = len(key_rstripped)
                before = ''
                before_count = 0
                if key_unstripped_len > stripped_len:
                    before_count = key_unstripped_len - stripped_len
                    before = key_unstripped[stripped_len:]
                    assert before_count == len(before)
                after = ''
                after_count = 0
                """ after_count = 0 (above) as would always be zero when keysep is space """
                if keysep is not None and len(keysep.strip()) > 0:
                    if before_count == 0:
                        """ First argument proper should be unstripped
                        but where we have a real solid keysep, and before_count
                        appears to be zero, do some extra checking to help
                        the caller
                        """
                        re1 = r"(?P<keygroup>\s*{0})(?P<beforegroup>\s+){1}".format(key_unstripped,keysep)
                        matched = re.match(re1,line)
                        #if 'networking' in key_rstripped:
                        #    print re1,matched,matched,matched,len(key_unstripped),key_unstripped,matched,line
                        if matched:
                            # key_unstripped was likely supplied rstripped so adjust
                            before = matched.group('beforegroup')
                            before_count = len(before)
                            if before_count > 0:
                                if verbose:
                                    print "{0} before_count now {1} (was zero)".format(output_prefix,
                                                                                       before_count)
                                #print before
                re2 = r"(\s*{0}\s*{1})(?P<aftergroup>\s*)".format(key_unstripped,keysep)
                matched = re.match(re2,line)
                if matched:
                    # key_unstripped was likely supplied rstripped so adjust
                    after = matched.group('aftergroup')
                    after_count = len(after)
                assert before_count == len(before)
                assert after_count == len(after)
                before_counter = Counter(before)
                after_counter = Counter(after)
                return (key_rstripped,line,before,keysep,after,line[-1],before_counter,after_counter)


            def tuple8_from_tuple7(self,tup7,recovery=True,verbose=False,output_prefix='tuple8:'):
                """ Returns an 8 tuple with fields as follows:
                key, line, before, sep, after, lastchar, before_counter, after_counter

                The first parameter is a tuple having at least 7 fields. In fact that tuple
                generally has 14 fields now that underlying data structure uses 14 tuple.
                Little benefit in renaming so just work with the contract which states that
                input tuple must have at least seven fields and the order of those first 7 fields is:
                  key, line, before_count, sep, after_count, linesep, linenum
                """
                if len(tup7) < 7:
                    raise Exception('tuple8_from_tuple7: Tuple given as 1st argument should have min. 7 fields.')
                # tuple7: key, line, before_count, sep, after_count, linesep, linenum
                key = tup7[0]
                sep = tup7[3]
                """ Unlike tuple14 / tuple7 which is usually formed under strict conditions,
                tuple8_from_tuple7 is called later in processing and during tests.
                Ensure that even if user / caller supplies a key that has been lstripped,
                that we restore original from key_recovered.
                """
                if sep is not None and len(sep.strip()) > 0 and recovery:
                    line_array = tup7[1].split(sep)
                    key_recovered = line_array[0].rstrip()
                    if len(key_recovered) > len(key):
                        #print key_recovered,key,len(key_recovered),len(key)
                        key = key_recovered
                return self.tuple8(key,tup7[1],sep,verbose,output_prefix)

            
            def tuple8_from_tuple14(self,tup14,recovery=True,verbose=False,output_prefix='tuple8:'):
                """ Wrapper around tuple8_from_tuple7 which supports extra output when verbose=True.

                tuple8_from_tuple7 is the traditional function, written before the underlying data
                structure was extended from 7 fields to 14 fields.
                """
                # tuple8: key, line, before, sep, after, lastchar, before_counter, after_counter
                tup = self.tuple8_from_tuple7(tup14,True) # recovery=True
                if tup is None:
                    if verbose:
                        print "{0} tuple8_from_tuple7({1}) not returned expected.".format(output_prefix,
                                                                                          keyname)
                    return spaced
                elif verbose:
                    gen_prefix = 'tuple8_from_tuple14:'
                    #error_prefix = 'tuple8_from_tuple14: Error'
                    print "{0} Have tup8 for keyname={1}".format(gen_prefix,keyname)
                    if len(tup[0]) > len(keyname):
                        print "{0} tup8 for keyname={1} has a longer recovered key!".format(output_prefix,
                                                                                            keyname)
                else:
                    pass
                return tup


            def tuple14(self,key_unstripped,line,linenum,keysep,comment_flag):
                tup7 = self.tuple7(key_unstripped,line,linenum,keysep)
                list14 = list(tup7)
                list14.append(comment_flag)
                list14.extend(self.tuple6(line,keysep))
                return tuple(list14)


            def tuple5(self,key_unstripped,line,keysep,verbose=False):
                """ Returns a 5 tuple with fields as follows:
                key, before_count, sep, after_count, lastchar
                Instead of reimplementing a lot of logic just take what you
                need from what tuple8 returns.
                tuple8: key, line, before, sep, after, lastchar, before_counter, after_counter
                """
                tup = self.tuple8(key_unstripped,line,keysep,verbose,output_prefix='tuple5:')
                tup5 = (tup[0],len(tup[2]),tup[3],len(tup[4]),tup[5])
                return tup5


            def when_hashed(self,original,retain_left_spacing=True,default_return='blank'):
                # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                if default_return is None:
                    string_when_enabled = None
                elif default_return in ['blank','empty']:
                    string_when_enabled = ''
                elif default_return == 'original':
                    string_when_enabled = original
                else:
                    string_when_enabled = None
                if original.lstrip().startswith(chr(35)):
                    pass
                elif retain_left_spacing:
                    string_when_enabled = "{0}{1}".format(chr(35),original)
                else:
                    string_when_enabled = "{0}{1}".format(chr(35),original.lstrip())
                return string_when_enabled


            def when_disabled(self,original,retain_left_spacing=True,default_return='blank'):
                # No distinction between 'disabled' and 'hashed' in this context
                return self.when_hashed(original,retain_left_spacing,default_return)


            def when_enabled(self,original,retain_left_spacing=True,default_return='blank'):
                # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                if default_return is None:
                    string_when_enabled = None
                elif default_return in ['blank','empty']:
                    string_when_enabled = ''
                elif default_return == 'original':
                    string_when_enabled = original
                else:
                    string_when_enabled = None
                if chr(35) in original:
                    if retain_left_spacing:
                        string_when_enabled = original.replace(chr(35),'',1)
                    else:
                        string_when_enabled = original.lstrip().lstrip(" {0}".format(chr(35)))
                return string_when_enabled


            def keysep_strong_target_clashes(self,verbose=False):
                """ For non whitespace key separator files this function
                returns a list of targets which appear more than once.
                Returns a tuple (empty if no clashes)
                """
                clashes_list = []
                tuples_keysep_strong = self.tuples_all_from_keysep_strong_present()
                tuples_keysep_strong_len = len(tuples_keysep_strong)
                tuples_keysep_strong_targets = [ tup[-1] for tup in tuples_keysep_strong ]
                targets_counted = Counter(tuples_keysep_strong_targets)
                # Use .most_common() without numeric argument to get counts in descending order
                for target,count in targets_counted.most_common():
                    if count < 2:
                        break
                    clashes_list.append(target)
                #print tuples_keysep_strong_targets
                #dict_of_targets_false = OrderedDict((target,False) for key in tuples_keysep_strong_targets)
                if verbose and len(clashes_list) < 1:
                    midsection_clashes = 'based on tuples_keysep_strong'
                    print "Counted {0} key clashes {1} of length {2}".format(diff,midsection_clashes,
                                                                             tuples_keysep_strong_len)
                return tuple(clashes_list)


            def keys_disabled5print(self):
                """ Print up to 5 disabled keys. If none disabled, then print a message saying so """
                disabled_tuple = self.keys_disabled_printable(True)
                if len(disabled_tuple) < 1:
                    print "No keys are classed as 'disabled'"
                else:
                    showing_prefix = 'Showing up to 5 elements'
                    print "{0} from keys_disabled_printable(): {1}".format(showing_prefix,
                                                                           disabled_tuple[:5])
                return

            
            def summary_print(self):
                print "Key value file has {0} lines".format(self.linecount)
                print "Key value file has {0} keys".format(len(self))
                if self.key_duplicates_count == 0:
                    print "No duplicate keys to report"
                else:
                    print "Duplicate key count {0}".format(self.key_duplicates_count)
                return


            def summary_detailed(self,print_flag=False,line_prefix=None):
                lines = []
                self_class_name = (self.__class__).__name__ 
                """ Above self_class_name is the inner class so will always be Keyvalfile
                If you are looking for a label of Keyedfile,Keyedcolon,Keyedequals,Keyedspace
                then use self.outerclass_name
                """
                #self_name = self.__name__
                lines.append("Key value file {0}() has {1} lines".format(self.outerclass_name,self.linecount))
                lines.append("Key value file {0}() has {1} keys".format(self.outerclass_name,len(self)))
                if len(self) < 11:
                    for key,val in self.items(): lines.append("key:{0:<25} value:{1:<50}".format(key,val))
                lines.append("Showing up to 5 keys from self (OrderedDict) {0}".format(self.first_n_keys(5)))
                lines.append("Showing up to 5 elements of keylist_frozen {0}".format(self.keylist_frozen[:5]))
                warning_list = self.warning_list
                if len(warning_list) > 0:
                    for warn in warning_list:
                        lines.append("warning_list: {0}".format(warn))
                if self.key_duplicates_count == 0:
                    lines.append("No duplicate keys to report")
                else:
                    lines.append("Duplicate key count {0}".format(self.key_duplicates_count))
                    lines.append("First duplicate key is {0}".format(self.key_duplicates()[0]))
                lines.append("Key length average: {0}".format(self.keylen_average))
                lines.append("Key length average (full): {0}".format(self.keylen_average_full))
                if self.keysep_named == chr(32):
                    keysep_named = 'SPACE'
                else:
                    keysep_named = self.keysep_named
                if self.keysep == chr(32):
                    keysep = 'SPACE'
                else:
                    keysep = self.keysep
                lines.append("Key separator (named) is {0}. Key separator used is {1}".format(keysep_named,
                                                                                              keysep))
                if keysep is None or keysep == 'SPACE':
                    lines.append('Keysep strong clash count is not applicable for keysep above')
                else:
                    target_clashes = self.keysep_strong_target_clashes()
                    target_clashes_len = len(target_clashes)
                    lines.append("Keysep strong clash count is {0}".format(target_clashes_len))
                    if target_clashes_len > 0:
                        lines.append("Showing up to 5 elements of target clashes {0}".format(target_clashes[:5]))
                keys_enabled = self.keys_enabled()
                keys_enabled_length = len(keys_enabled)
                if keys_enabled_length == 0:
                    lines.append('NO keys are ENABLED!')
                elif keys_enabled_length == 1:
                    lines.append("1 key is enabled {0}".format(keys_enabled[0]))
                else:
                    lines.append("{0} keys are ENABLED.".format(keys_enabled_length))
                if keys_enabled_length > 0:
                    lines.append("Showing up to 5 elements of keys_enabled {0}".format(keys_enabled[:5]))
                    if self.keylen_average < 2 and self.keylen_average_full < 2:
                        lines.append("WARNING: Have {0} enabled keys, but keys seem REMARKABLY SHORT!".format(
                                keys_enabled_length))

                tuples_keysep_strong = self.tuples_all_from_keysep_strong()
                keysep_strong_keys = [ tup[0] for tup in tuples_keysep_strong ]
                lines.append("Showing up to 5 elements of keysep_strong_keys {0}".format(keysep_strong_keys[:5]))
                if line_prefix is not None:
                    lines = [ "{0} {1}".format(line_prefix,line) for line in lines ]
                if print_flag:
                    for line in lines:
                        print line
                return lines


            def summary_detailed_extra(self,print_flag=False,line_prefix=None):
                lines = self.summary_detailed(False)

                lines.append("Count of lines with tuples marked comment=False is {0}".format(
                        len(self.tuples_all_noncomment())))

                if line_prefix is not None:
                    lines = [ "{0} {1}".format(line_prefix,line) for line in lines ]
                if print_flag:
                    for line in lines:
                        print line
                return lines


            def tuple_first_field_equals(self,tup,target):
                equality = False
                first = tup[0]
                #print first,target
                if first == target:
                    equality = True
                return equality


            def tuple_nth_field_equals(self,tup,target,field_index,index_error_action=None):
                equality = False
                try:
                    nth = tup[field_index]
                    #print nth,target
                    if nth == target:
                        equality = True
                except IndexError:
                    if index_error_action == 'raise':
                        raise
                return equality


            def tuple_nth_field_equals_list(self,tup,target,field_index):
                tup0 = self.tuples_all[0]
                # Inspect length of first tuple to establish max for field_index
                if field_index >= len(tup0):
                    # Tuples currently have 7 fields.
                    return []
                tuples_list = []
                for tup in self.tuples_all:
                    if tup[field_index] == target:
                        tuples_list.append(tup)
                return tuples_list


            def tuples_all_from_key(self,target_key,reverse=True):
                """ reversed=True is the appropriate default to fit with the
                contract of these classes. Extract from earlier note at top of outer class Keyedfile():
                ...by allowing later values to supersede [redefine] a directive [key] ...
                This function may well be called more often than the similar function tuples_all_line_from_key()
                Employs an alternative approach: Use ifilter rather than the reversed(filter( 
                code used in tuples_all_line_from_key()
                tuples_all contains tuples of 7 fields:
                key, line, before_count, sep, after_count, linesep, linenum
                """
                from itertools import ifilter
                from functools import partial
                tup = None
                try:
                    tuples = ifilter(partial(self.tuple_first_field_equals,target=target_key),
                                     self.tuples_all)
                except TypeError:
                    tuples = None
                    raise
                try:
                    tuples_list = list(tuples)
                    """ Assign above to a variable so no problem consuming twice """
                    tup = tuples_list[0]
                    if reverse:
                        tup = tuples_list[-1]
                except Exception:
                    tup = None
                return tup


            def tuples_all_line_from_key(self,target_key,reverse=True):
                """ reversed=True is the appropriate defaut to fit with the
                contract of these classes. Extract from earlier note at top of outer class Keyedfile():
                ...by allowing later values to supersede [redefine] a directive [key] ...
                """
                line = None
                if reverse:
                    tuples_all_not_none = reversed(filter(None,self.tuples_all))
                else:
                    tuples_all_not_none = filter(None,self.tuples_all)
                for tup in tuples_all_not_none:
                    if tup[0] == target_key:
                        line = tup[1]
                        break
                return line


            def tuples_all_from_keysep(self,keysep,reverse=False):
                """ Use ifilter rather than the reversed(filter( 
                code used in tuples_all_line_from_key()
                tuples_all contains tuples of 14 fields and here we are interested in tuple[3] (fourth field)
                key, line, before_count, sep, after_count, linesep, linenum
                """
                from itertools import ifilter
                from functools import partial
                tuples_list = []
                try:
                    tuples = ifilter(partial(self.tuple_nth_field_equals,target=keysep,field_index=3),
                                     self.tuples_all)
                except IndexError:
                    tuples_list = []
                    raise
                except TypeError:
                    tuples_list = []
                    raise
                try:
                    if reverse is True:
                        tuples_list = reversed(list(tuples))
                    else:
                        tuples_list = list(tuples)
                except Exception:
                    tuples_list = []
                return tuples_list


            def tuples_all_from_keysep_none(self,reverse=False,index_error_action='raise'):
                # tuples_all contains tuples of 14 fields and here we are interested in tuple[3] (fourth field)
                # key, line, before_count, sep, after_count, linesep, linenum
                tuples_list = []
                for tup in self.tuples_all:
                    try:
                        if tup[3] is None:
                            tuples_list.append(tup)
                    except IndexError:
                        if index_error_action == 'raise':
                            raise
                return tuples_list


            def tuples_all_from_keysep_strong(self,reverse=False,index_error_action='raise'):
                # tuples_all contains tuples of 14 fields and here we are interested in tuple[3] (fourth field)
                # tuple14 = key, line, before_count, sep, after_count, linesep, linenum, comment_flag,
                #   word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                # 'strong' in this context just means keysep not None and not space(s)
                tuples_list = []
                for tup in self.tuples_all:
                    try:
                        if tup[3] is not None and len(tup[3].strip()) > 0:
                            # key separator other than None or space
                            tuples_list.append(tup)
                    except IndexError:
                        if index_error_action == 'raise':
                            raise
                return tuples_list


            def tuples_all_from_keysep_strong_present(self,reverse=False,
                                                      index_error_action='raise',min_keysep_pos=1):
                # tuples_all contains tuples of 14 fields and here we are interested in tuple[3] (fourth field)
                # tuple14 = key, line, before_count, sep, after_count, linesep, linenum, comment_flag,
                #   word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                # 'strong' in this context means
                # (.) keysep not None and not space(s)
                # (.) Parsing found the keysep in the current line and stored its position
                # Set min_keysep_pos=0 if you want 'present' to include keysep at first char of line
                tuples_list = []
                for tup in self.tuples_all:
                    try:
                        if tup[3] is not None and len(tup[3].strip()) > 0:
                            # key separator other than None or space
                            if tup[10] is not None and tup[10] >= min_keysep_pos:
                                # parsing found the keysep
                                tuples_list.append(tup)
                    except IndexError:
                        if index_error_action == 'raise':
                            raise
                return tuples_list


            def tuples_all_noncomment(self,reverse=False):
                """ Use ifilter rather than the reversed(filter( 
                code used in tuples_all_line_from_key()
                tuples_all contains tuples of 14 fields and here we are interested in tuple[7] (eighth field)
                tuple14 = key, line, before_count, sep, after_count, linesep, linenum, comment_flag,
                   word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                """
                from itertools import ifilter
                from functools import partial
                tuples_list = []
                try:
                    tuples = ifilter(partial(self.tuple_nth_field_equals,target=False,field_index=7),
                                     self.tuples_all)
                except IndexError:
                    tuples_list = []
                    raise
                except TypeError:
                    tuples_list = []
                    raise
                try:
                    if reverse is True:
                        tuples_list = reversed(list(tuples))
                    else:
                        tuples_list = list(tuples)
                except Exception:
                    tuples_list = []
                return tuples_list


            def sedqueue(self,linenum,sedline,verbosity=0):
                """ Record the sed in internal dict dict_of_seds.
                Requires that the line change already be recorded in dict_of_changed
                before you call sedqueue. This does not guarantee that dict_of_changed
                and dict_of_seds are always completely in step, but does force a return False
                when it is obvious that those dicts will become out of step.
                """
                queue_return = True
                if sedline is None or len(sedline) < 1 or sedline.startswith('q'):
                    queue_return = False
                elif linenum in self._dict_of_seds:
                    """ False if we already have a sed operation stored for this file line """
                    queue_return = False
                elif sedline in self.list_of_sedlines:
                    """ False if we already have this sedline stored """
                    if verbosity > 1:
                        print "Sedline already stored so not inserting into queue (again)"
                    queue_return = False
                else:
                    """ Check for obvious signs that dict_of_changed and dict_of_seds might
                    be heading towards being out of step. Set queue_return False in this case.
                    """
                    if verbosity > 1:
                        print len(self._dict_of_changed),len(self._dict_of_seds),linenum,sedline
                    dict_diff = (len(self._dict_of_changed)-len(self._dict_of_seds))
                    if dict_diff == 1:
                        self._dict_of_seds[linenum]=sedline
                        self._change_count = self._change_count + 1
                    else:
                        queue_return = False
                return queue_return


            def dequeue_all(self):
                dequeued_count = 0
                sedline_count = len(self._dict_of_seds)
                if sedline_count > 0:
                    self._dict_of_changed = OrderedDict()
                    self._dict_of_seds = OrderedDict()
                    dequeued_count = sedline_count
                    self._change_count = 0
                return dequeued_count
                  

            def linecount_and_lines_set(self,count,lines,tuples_all,keysep=None,keysep_named=None):
                if len(lines) == count:
                    pass
                else:
                    return False
                set_flag = False
                self._linecount = count
                self._lines_tuple = tuple(lines)
                self._tuples_all = tuples_all
                """ lines_tuple is a fixed record of the original input file.
                tuples_all is also a fixed record but in the form of tuples
                where the first field represents a recognisable key (if any)
                and the second field is the original input line.
                The tuples in tuples_all have several fields to aid later processing:
                key, line, before_count, sep, after_count, linesep, linenum
                """
                if keysep is not None:
                    self._keysep = keysep
                    key_duplicates_count = 0
                    keylist_frozen = []
                    """ Next we attempt to measure average key length, but
                    only include in our reckoning those lines which were
                    marked as having a key separator (stored in 4th field)
                    """
                    lines_counted = 0
                    lines_total = 0
                    # tuple: key, line, before_count, sep, after_count, linesep, linenum
                    for tup in tuples_all:
                        key = tup[0]
                        if key is not None:
                            if key in keylist_frozen:
                                key_duplicates_count += 1
                            else:
                                keylist_frozen.append(key)
                            if tup[3] is not None:
                                # Maybe wise to avoid: len(key.strip()) for key_average_full
                                lines_total += len(key)
                                lines_counted += 1
                    if lines_counted > 0:
                        self._keylen_average_full = int(round(lines_total/float(lines_counted)))
                    """ keylen_average_full now populated so turn our attention to keylen_average
                    which is the average of the keys as they are stored in the OrderedDict.
                    Where we have chosen to apply additional stripping before storing the key
                    in OrderedDict, we may see a lower average figure next """
                    conf_len = len(self)
                    if conf_len > 0:
                        keys_total = 0
                        for dict_key in self:
                            if dict_key is not None:
                                keys_total += len(dict_key)
                        """ Duplicates will not play any part in boosting or reducing the figure
                        for keylen_average as we are sourcing direct from the dictionary object
                        itself.
                        Two reasons why keylen_average_full might be higher than keylen_average:
                        (.) keylen_average_full takes into account every non blank key and
                        will not exclude duplicate keys from the totalling process.
                        (.) Some implementations of the parser routines may choose to strip
                        (leading strip?) the key before adding it to the dictionary.
                        keylen_average_full in that case may be counting spacing which
                        will not be counted when totalling directly from the dictionary keys
                        """
                        self._keylen_average = int(round(keys_total/float(conf_len)))

                    self._key_duplicates_count = key_duplicates_count
                    self._keylist_frozen = keylist_frozen[:]
                    set_flag = True
                    keylist_frozen_len = len(keylist_frozen)
                    if len(self) < keylist_frozen_len:
                        self.warning("({0} < {1}) ... len(dictionary) < len(keylist_frozen)".format(len(self),
                                                                                                    keylist_frozen_len))
                        """ keylist_frozen content is driven from tuples_all and some tuples in
                        tuples all may not have made it through the logic in outer class
                        method list_of_tuples_appended() and so will not exists in our dictionary (self)
                        This is expected behaviour and using a specialist subclass Keyedcolon() , Keyedequals(),
                        or Keyedspace() will often see less items from tuples_all filtered out
                        before dictionary creation.
                        """
                    self._lines = lines[:]
                    from hashlib import md5
                    mdf = md5()
                    last_dict = {}
                    for line in lines:
                        mdf.update(line)
                        if len(line) > 0:
                            last = line[-1]
                            if last in last_dict:
                                last_dict[last]=1+last_dict[last]
                            else:
                                last_dict[last]=1
                    self._md5sum = mdf.hexdigest()
                    self._conf_linesep = max(last_dict,key=last_dict.get)
                    # We do nothing with keysep_named other than set it to the supplied value 
                    self._keysep_named = keysep_named
                return set_flag
                

            def keyval(self,key):
                """ User should probably understand that we are OrderedDict but 
                provide a convenient helper also """
                return self.get(key)


            def keyline(self,key):
                return self.keylines_typed(2,[key])


            def keyline1rstripped(self,key):
                lines = self.keylines_typed(2,[key])
                line = '' 
                if 1 == len(lines):
                    try:
                        line0 = lines[0]
                        line = line0.rstrip()
                    except:
                        line = ''
                return line


            def key_duplicates(self,frozen_check=True):
                """ The list returned is in same order as the duplicates
                are encountered in tuples_all.
                Reminder: Keys within the returned list of tuples
                are not necessarily unique
                Use set() to obtain a unique set from what is returned.
                Use sorted() on results if you prefer typical sorted() order
                """
                keylist_frozen = []
                duplicates = []
                for tup in self.tuples_all:
                    key = tup[0]
                    if key is not None:
                        if key in keylist_frozen:
                            duplicates.append(key)
                        else:
                            keylist_frozen.append(key)
                if frozen_check is True:
                    if len(keylist_frozen) == len(self.keylist_frozen):
                        pass
                    else:
                        raise "key_duplicates: len(keylist_frozen) != len(self.keylist_frozen)"
                return duplicates


            def key_duplicates_tuples(self,frozen_check=True):
                """ The list returned is in same order as the duplicates
                are encountered in tuples_all.
                Reminder: Keys within the returned list of tuples
                are not necessarily unique
                """
                keylist_frozen = []
                duplicates_tuples = []
                for tup in self.tuples_all:
                    key = tup[0]
                    if key is not None:
                        if key in keylist_frozen:
                            duplicates_tuples.append(tup)
                        else:
                            keylist_frozen.append(key)
                if frozen_check is True:
                    if len(keylist_frozen) == len(self.keylist_frozen):
                        pass
                    else:
                        raise "key_duplicates_tuples: len(keylist_frozen) != len(self.keylist_frozen)"
                return duplicates_tuples


            def false_true_index(self,val_given):
                """ If val is in a predefined FALSE list, then return a three tuple of:
                (False,index,enclosed_by) where enclosed_by is None if no enclosing quotes
                If val is in a predefined TRUE list, then return a three tuple of:
                (True,index,enclosed_by) where enclosed_by is None if no enclosing quotes
                If we are in neither list then return (None,-1,None)
                """
                return_tuple = (None,-1,None)
                if val_given in Keyvalfile.FALSELIST:
                    idx = Keyvalfile.FALSELIST.index(val_given)
                    return_tuple = (False,idx,None)
                elif val_given in Keyvalfile.TRUELIST:
                    idx = Keyvalfile.TRUELIST.index(val_given)
                    return_tuple = (True,idx,None)
                else:
                    enclosed_by = self.enclosed_by_char(val_given)
                    if enclosed_by is not None:
                        val_stripped = val_given.strip(enclosed_by)
                        if val_stripped in Keyvalfile.FALSELIST:
                            idx = Keyvalfile.FALSELIST.index(val_stripped)
                            return_tuple = (False,idx,enclosed_by)
                        elif val_stripped in Keyvalfile.TRUELIST:
                            idx = Keyvalfile.TRUELIST.index(val_stripped)
                            return_tuple = (True,idx,enclosed_by)
                        else:
                            pass

                return return_tuple


            def record_changed_has_tuple(self,target_key,original,replacement,lineend_check,tuple14=None):
                """ record_changed inner processing
                tuple14 = key, line, before_count, sep, after_count, linesep, linenum, comment_flag,
                   word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                See record_changed_has_tuple_key_hash_remove() for variant where key_hash_remove=True

                The last action of this function is to record the change in the internal dictionary
                dict_of_changed. The separate function sedqueue() should be used if you want
                to ensure changes are added to internal dictionary dict_of_seds.
                """
                assert len(tuple14) > 13
                linenum = tuple14[6]
                line = tuple14[1]
                keysep = tuple14[3]
                change_start = None
                re_constructed = None
                gen_prefix = 'record_changed_has_tuple:'
                error_prefix = 'record_changed_has_tuple: Error'
                if tuple14[2] == 0 and tuple14[4] == 0:
                    re_constructed = r"(\s*{0}{1}\s*)".format(target_key,keysep)
                elif keysep is None:
                    raise Exception("{0} keysep is None for target_key={1}".format(error_prefix,target_key))
                elif len(keysep.strip()) > 0:
                    line_array = line.split(keysep)
                    line_array_zero_len = len(line_array[0])
                    if line_array_zero_len > 0:
                        change_start = 1+line_array_zero_len
                else:
                    # No strong keysep so use tuple8 to help
                    # tuple8: key, line, before, sep, after, lastchar, before_counter, after_counter
                    tup8 = self.tuple8_from_tuple7(tuple14,True)
                    re_constructed = r"(\s*{0}{1}{2}{3}\s*)".format(tup8[0],before,keysep,after)
                #print change_start,re_constructed
                recorded_count = 0
                if change_start is None:
                    if re_constructed is None:
                        raise Exception("{0} init1 failed for target_key={1}".format(gen_prefix,target_key))
                    matched = re.match(re_constructed,line)
                    if matched:
                        change_start = matched.end()
                        # No need to increment end() as re module gives end() suitable for change_start
                    else:
                        raise Exception("{0} init2 failed for target_key={1}".format(gen_prefix,target_key))
                    #print re_constructed,matched,matched,matched.end(),change_start,change_start
                line_tail = line[change_start:]
                line_tail2 = line_tail.replace(original,replacement)
                if line_tail2 == line_tail:
                    #print line_tail,line_tail2,self.__class__.__name__,self.outerclass_name
                    print "record_changed_has_tuple() original:{0} and replacement:{1}".format(original,replacement)
                    raise Exception("{0} UNCHANGED failure for target_key={1}".format(gen_prefix,target_key))
                else:
                    if replacement in Keyvalfile.FALSETRUELIST:
                        pass
                    elif self.unenclosed_simple(replacement) in Keyvalfile.FALSETRUELIST:
                        pass
                    else:
                        #print "{0}{1}->{2}".format(gen_prefix,line_tail.rstrip(),line_tail2.rstrip())
                        pass
                    line_tail = line_tail2
                #print linenum,linenum,linenum,original,original,replacement,replacement,line_tail
                line_changed = "{0}{1}".format(line[:change_start],line_tail)
                #print linenum,linenum,linenum,line_changed
                if lineend_check:
                    """ When lineend_check is set True, do your best to ensure any
                    line ending in the original line is in replacement also. """
                    try:
                        if line_changed[-1] == line[-1]:
                            pass
                        else:
                            line_changed = "{0}{1}".format(line_changed,line[-1])
                    except Exception:
                        pass
                self._dict_of_changed[linenum]=line_changed
                recorded_count += 1
                return recorded_count


            def record_changed_has_tuple_key_hash_remove(self,target_key,original,replacement,
                                                         lineend_check,tuple14=None):
                """ record_changed inner processing
                tuple14 = key, line, before_count, sep, after_count, linesep, linenum, comment_flag,
                   word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                See record_changed_has_tuple() for variant where key_hash_remove=False

                The last action of this function is to record the change in the internal dictionary
                dict_of_changed. The separate function sedqueue() should be used if you want
                to ensure changes are added to internal dictionary dict_of_seds.
                """
                assert len(tuple14) > 13
                linenum = tuple14[6]
                line = tuple14[1]
                keysep = tuple14[3]
                change_start = None
                re_constructed = None
                gen_prefix = 'record_changed_has_tuple_key_hash_remove:'
                error_prefix = 'record_changed_has_tuple_key_hash_remove: Error'
                before_plus_after_both_zero_flag = False
                if tuple14[2] == 0 and tuple14[4] == 0:
                    before_plus_after_both_zero_flag = True
                    re_constructed = r"(\s*{0}{1}\s*)".format(target_key,keysep)
                    #print "{0} re_constructed is set early".format(gen_prefix)
                elif keysep is None:
                    raise Exception("{0} keysep is None for target_key={1}".format(error_prefix,target_key))
                elif len(keysep.strip()) > 0:
                    line_array = line.split(keysep)
                    line_array_zero_len = len(line_array[0])
                    if line_array_zero_len > 0:
                        change_start = 1+line_array_zero_len
                else:
                    # No strong keysep so use tuple8 to help
                    # tuple8: key, line, before, sep, after, lastchar, before_counter, after_counter
                    tup8 = self.tuple8_from_tuple7(tuple14,True)
                    re_constructed = r"(\s*{0}{1}{2}{3}\s*)".format(tup8[0],before,keysep,after)
                #print change_start,re_constructed
                recorded_count = 0
                if change_start is None:
                    if re_constructed is None:
                        raise Exception("{0} init1 failed for target_key={1}".format(gen_prefix,target_key))
                    matched = re.match(re_constructed,line)
                    if matched:
                        if before_plus_after_both_zero_flag:
                            change_start = matched.end()-1
                        else:
                            change_start = matched.end()
                        # No need to increment end() as re module gives end() suitable for change_start
                    else:
                        raise Exception("{0} init2 failed for target_key={1}".format(gen_prefix,target_key))
                    #print re_constructed,matched,matched,matched.end(),change_start,change_start
                line_tail = line[change_start:]
                line_tail2 = line_tail.replace(original,replacement)
                if line_tail2 == line_tail:
                    #print line_tail,line_tail2
                    print "change_start={0} original:{1} and replacement:{2}".format(
                        change_start,original,replacement)
                    raise Exception("{0} UNCHANGED failure for target_key={1}".format(gen_prefix,target_key))
                else:
                    if replacement in Keyvalfile.FALSETRUELIST:
                        pass
                    elif self.unenclosed_simple(replacement) in Keyvalfile.FALSETRUELIST:
                        pass
                    else:
                        #print "{0}{1}->{2}".format(gen_prefix,line_tail.rstrip(),line_tail2.rstrip())
                        pass
                    line_tail = line_tail2
                #print linenum,linenum,linenum,original,original,replacement,replacement,line_tail
                line_when_enabled = self.when_enabled(line,True,None)
                if line_when_enabled is None:
                    raise Exception("record_changed key_hash_remove=True unable to record change.")
                line_when_enabled_diff_length = len(line)-len(line_when_enabled)
                if line_when_enabled_diff_length > 0:
                    start = change_start-line_when_enabled_diff_length
                    if start < 1:
                        raise Exception("record_changed key_hash_remove=True not able to record change.")
                    line_changed = "{0}{1}".format(line_when_enabled[:start],line_tail)
                else:
                    line_changed = "{0}{1}".format(line[:change_start],line_tail)

                if line_changed is not None:
                    #print linenum,linenum,linenum,line_changed
                    if lineend_check:
                        """ When lineend_check is set True, do your best to ensure any
                        line ending in the original line is in replacement also. """
                        try:
                            if line_changed[-1] == line[-1]:
                                pass
                            else:
                                line_changed = "{0}{1}".format(line_changed,line[-1])
                        except Exception:
                            pass
                    self._dict_of_changed[linenum]=line_changed
                    recorded_count += 1

                    # 0,/^
                    # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                    """ Good place to have a reminder of the functionality of keys_hashed()
                    keys_hashed() returns a tuple of keys which are 'hashed'
                    Unlike keys_disabled() our default for set_for_unique is False.
                    keys_disabled() is concerned with a description of a key
                    and it seems logical to think that (by default) a key could
                    only be disabled once.
                    For keys_hashed we are more interested in presence of hash / octothorpe
                    and so we do not enforce uniqueness by default.
                    """
                    recorded_count2 = 0
                
                    """
                    for tup in self.tuples_all:
                        # Tuples will be 14 field tuples with 10th field being keyseppos
                        line = tup[1]
                        line_start = line[:tup[10]]
                        #print target_key,line_start
                        if line_start.rstrip() == target_key:
                            recorded_count2 += 1
                        #if re.match("{0}\s*{1}".format(chr(35),target_key),line_start):
                        #    recorded_count2 += 1

                    if recorded_count2 != recorded_count:
                        raise Exception("recorded_count2={0} does not match recorded_count={1}".format(
                                recorded_count2,recorded_count))
                    """

                return recorded_count


            def record_changed(self,target_key,original,replacement,lineend_check=True,key_hash_remove=False):
                """ Enter the change in the internal dictionary dict_of_changed. """
                #print target_key,original,replacement
                if target_key is None or len(target_key.strip()) < 1:
                    raise Exception("record_changed bad target_key")
                if type(original) != type(''):
                    raise Exception("record_changed bad value for 'original'")
                if type(replacement) != type(''):
                    raise Exception("record_changed bad value for 'replacement'")

                #print target_key,original,replacement
                gen_prefix = 'record_changed:'
                error_prefix = 'record_changed: Error'
                tuple14 = self.tuples_all_from_key(target_key)
                if tuple14 is None:
                    print "{0} tuple14 could not be retrieved for target_key={1}".format(error_prefix,
                                                                                         target_key)
                    return 0
                if key_hash_remove is True:
                    recorded_count = self.record_changed_has_tuple_key_hash_remove(target_key,original,replacement,
                                                                                   lineend_check,tuple14)
                else:
                    recorded_count = self.record_changed_has_tuple(target_key,original,replacement,
                                                                   lineend_check,tuple14)
                try:
                    return_count = int(recorded_count)
                except Exception:
                    return_count = 0
                return return_count


            def record_changed_boolean(*args, **kwargs):
                """ Wrapper around record_changed() to force boolean from count.
                Note: Below we do still have * and ** so that we are giving
                the wrapped function the unpacked versions as is the proper way.
                """
                self = None
                if len(args) > 0:
                    self = args[0]
                return self.boolean_from_count(self.record_changed(*args, **kwargs))


            def possibly_continued(self,candidate,continuation_char=None):
                if candidate is None:
                    return None
                if continuation_char is None:
                    continuation_char = chr(92)
                escaped = None
                continuation_char_escape = False
                candidate_rstripped = candidate.rstrip()
                if candidate_rstripped == continuation_char:
                    continuation_char_escape = True
                if len(candidate_rstripped) > 1 and continuation_char in candidate:
                    if candidate_rstripped[-1] == continuation_char:
                        if candidate_rstripped[-2] == continuation_char:
                            # Already \\ rather than \ so no need to escape
                            pass
                        else:
                            continuation_char_escape = True
                if continuation_char_escape is True:
                    continuation_char_rightmost = candidate.rfind(continuation_char)
                    if continuation_char_rightmost < 0:
                        return None
                    escaped = "{0}{1}{2}".format(candidate[:continuation_char_rightmost],
                                                 continuation_char,
                                                 candidate[continuation_char_rightmost:])
                return escaped


            def mixed_whitespace_tuple(self,before,after,verbosity=0):
                """ Return some meta information about the contents of before
                and after to aid further analysis / reporting.
                The final (third) element of the returned tuple only has
                meaning when the first two elements are False.
                So (..,..,True) must be (False,False,True)

                Function is mainly for testing / debugging / feedback.

                This function can return (False,False,True) when whitespace
                other than spaces or tabs features in before / after.
                Could have been named mixed_spaces_and_tabs_tuple() if that
                seems a better naming.
                """
                before_mixed = False
                after_mixed = False
                before_counter = Counter(before)
                after_counter = Counter(after)
                dependent_counter = Counter("{0}{1}".format(before,after))
                if before_counter[chr(9)] > 0 and before_counter[chr(32)] > 0:
                    before_mixed = True
                if after_counter[chr(9)] > 0 and after_counter[chr(32)] > 0:
                    after_mixed = True
                gen_prefix = 'mixed_whitespace_tuple():'
                if before_mixed is True or after_mixed is True:
                    if verbosity > 0:
                        print "{0} mixed tabs and spaces encountered.".format(gen_prefix)
                    return (before_mixed,after_mixed,False)
                # Final (third) element is worth evaluating
                dependent_mixed = False
                if len(dependent_counter) < 2:
                    pass
                elif len(dependent_counter) > max(len(before_counter),len(after_counter)):
                    dependent_mixed = True
                else:
                    pass
                if verbosity > 0:
                    print "{0} dependent_counter has length {1}".format(gen_prefix,
                                                                        len(dependent_counter))
                    if dependent_mixed is True and verbosity > 1:
                        print "{0} dependent_counter: {1}".format(gen_prefix,
                                                                  dependent_counter)
                        print "{0} dependent_mixed is {1}".format(gen_prefix,
                                                                  dependent_mixed)
                return (before_mixed,after_mixed,dependent_mixed)


            def originally_spaced(self,keyname,verbosity=0,return_default=None,keysep=None):
                """ Give a high fidelity representation of the line (left of keysep) from
                the original file line. Certainly this will be useful where sedlines are being
                generated.

                verbosity settings allow reporting of mixed spaces and tabs and other feedback
                self.keysep will be used as keysep by default.
                Set keysep='original' to specifically use keysep as stored originally
                Set keysep=KEYSEP_NAMED if you wish to force adherence to KEYSEP_NAMED regardless
                Default is to return None if some issue during processing.
                Set return_default='keyname' if you want to get back what you put in on error

                Unlike target_spaced we never return something containing caret (^) so no need
                for a hatted=True parameter

                See wrapper originally_spaced_hatted() if you want caret (^) prefix.
                """
                if return_default == 'keyname':
                    spaced = keyname
                else:
                    spaced = None

                if type(keyname) != type(''):
                    return spaced

                tup7 = None
                if keysep is None:
                    keysep = self.keysep
                elif keysep == 'original':
                    tup7 = self.tuples_all_from_key(keyname)
                    keysep = tup7[3]
                else:
                    pass
                gen_prefix = 'originally_spaced:'
                if tup7 is None:
                    tup7 = self.tuples_all_from_key(keyname)
                    if tup7 is None:
                        if verbosity > 0:
                            repr(spaced)
                            print "{0} tuples_all_from_key({1}) not returned expected.".format(gen_prefix,
                                                                                               keyname)
                        return spaced
                if tup7[2] == 0 and tup7[4] == 0:
                    if keysep is None:
                        return keyname
                    else:
                        return "{0}{1}".format(keyname,keysep)
                """ At this point we have already got rid of all cases where we can predict simple
                returns or do not need to consult tuple8 / original line
                Next we get tuple8.
                """
                # tuple7: key, line, before_count, sep, after_count, linesep, linenum
                # tuple8: key, line, before, sep, after, lastchar, before_counter, after_counter
                tup = self.tuple8_from_tuple7(tup7,True) # recovery=True
                if tup is None:
                    if verbosity > 0:
                        print "{0} tuple8_from_tuple7({1}) not returned expected.".format(gen_prefix,
                                                                                          keyname)
                    return spaced
                elif verbosity > 2:
                    print "{0} Have tup8 for keyname={1}".format(gen_prefix,keyname)
                    if len(tup[0]) > len(keyname):
                        print "{0} tup8 for keyname={1} has a longer recovered key!".format(gen_prefix,keyname)
                else:
                    pass
                before = tup[2]
                after = tup[4]
                # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(9) is tab
                if verbosity > 1:
                    """ Everything in the next ten lines or so is all about feedback and
                    will never be necessary if verbosity < 1. mixed_flag is not needed outside
                    of this block 
                    """
                    mixed_flag = False
                    if verbosity > 2:
                        mixed_tuple3 = self.mixed_whitespace_tuple(before,after,2)
                        # 3 tuple returned is (before_mixed,after_mixed,dependent_mixed)
                        print "{0} keyname={1} has mixed_tuple3 of {2}".format(gen_prefix,keyname,
                                                                               mixed_tuple3)
                        mixed_flag = any(mixed_tuple3)
                    else:
                        before_counter = tup[6]
                        after_counter = tup[7]
                        if before_counter[chr(9)] > 0 and before_counter[chr(32)] > 0:
                            mixed_flag = True
                        if after_counter[chr(9)] > 0 and after_counter[chr(32)] > 0:
                            mixed_flag = True
                    if mixed_flag is True:
                        print "{0} Mixed spaces and tabs detected for keyname={1}".format(gen_prefix,
                                                                                              keyname)
                sep = tup[3]
                if sep is None:
                    spaced = "{0}{1}{2}".format(keyname,before,after)
                else:
                    spaced = "{0}{1}{2}{3}".format(keyname,before,sep,after)
                """ Leave this assertion commented as originally_spaced unlike target spaced will
                not reintroduced spaces left of key if keyname was supplied without them.
                line = tup[1]
                assertion_report = "assertion against {0} gave {1}".format(spaced,line.startswith(spaced))
                assert line.startswith(spaced),assertion_report
                """
                return spaced


            def originally_spaced_hatted(*args, **kwargs):
                """ Wrapper to prepend chr(94) caret to output of originally_spaced()
                Note: Below we do still have * and ** so that we are giving
                the wrapped function the unpacked versions as is the proper way.
                """
                unhatted = originally_spaced(*args, **kwargs)
                if len(unhatted) > 0:
                    spaced = "{0}{1}".format(chr(94),unhatted)
                return


            def target_replace_delimiter_suitable(self,occur_string,present=False,delim_firstchoice=None):
                """ Typically this function will be called with just a single parameter 'occur_string' and
                we are asking for a suitable delimiter that IS NOT present in occur_string

                By switching the flag present to be True, you can switch the behaviour to be IS PRESENT instead

                Supplying a firstchoice will see the processing attempt to respect your firstchoice except
                where it is not possible.
                """
                # chr(47) is forward slash (/)  ; chr(64) is at symbol (@)  ;  chr(94) is caret / hat
                # chr(63) is question mark (?)  ; chr(36) is dollar ($)  ; chr(92) is back slash ; chr(44) comma
                delimiter = None
                if present and (delim_firstchoice is None or len(delim_firstchoice) < 1):
                    # Little used case: We choose a delimiter using firstchoice as supplied 
                    char_counter = Counter(occur_string)
                    if delim_firstchoice in char_counter:
                        delimiter = chr(47)
                    elif chr(47) in char_counter:
                        delimiter = chr(47)
                    elif chr(64) in char_counter:
                        delimiter = chr(64)
                    elif chr(63) in char_counter:
                        delimiter = chr(63)
                    elif chr(36) in char_counter:
                        delimiter = chr(36)
                    elif chr(44) in char_counter:
                        delimiter = chr(44)
                    else:
                        pass
                elif present:
                    # Little used case: We choose a delimiter with no firstchoice set
                    char_counter = Counter(occur_string)
                    if chr(47) in char_counter:
                        delimiter = chr(47)
                    elif chr(64) in char_counter:
                        delimiter = chr(64)
                    elif chr(63) in char_counter:
                        delimiter = chr(63)
                    elif chr(36) in char_counter:
                        delimiter = chr(36)
                    elif chr(44) in char_counter:
                        delimiter = chr(44)
                    else:
                        pass
                elif delim_firstchoice is None or len(delim_firstchoice) < 1:
                    """ We choose a delimiter using firstchoice as supplied 
                    avoiding characters that are in occur_string
                    """
                    char_counter = Counter(occur_string)
                    if delim_firstchoice not in char_counter:
                        delimiter = chr(47)
                    elif chr(47) not in char_counter:
                        delimiter = chr(47)
                    elif chr(64) not in char_counter:
                        delimiter = chr(64)
                    elif chr(63) not in char_counter:
                        delimiter = chr(63)
                    elif chr(36) not in char_counter:
                        delimiter = chr(36)
                    elif chr(44) in char_counter:
                        delimiter = chr(44)
                    else:
                        pass
                else:
                    """ We choose a delimiter where no firstchoice supplied 
                    avoiding characters that are in occur_string
                    This option will probably be the most frequent case
                    used out of the 4 main case options in this function
                    """
                    char_counter = Counter(occur_string)
                    if chr(47) not in char_counter:
                        delimiter = chr(47)
                    elif chr(64) not in char_counter:
                        delimiter = chr(64)
                    elif chr(63) not in char_counter:
                        delimiter = chr(63)
                    elif chr(36) not in char_counter:
                        delimiter = chr(36)
                    elif chr(44) in char_counter:
                        delimiter = chr(44)
                    else:
                        pass

                return delimiter


            def target_spaced_space(self,target_key,verbose=False):
                """ Keysep is space(s).
                Begin with a lookup of the original line by consulting self.tuples_all
                tuples_all contains tuples of 14 fields
                tuple14 = key, line, before_count, sep, after_count, linesep, linenum, comment_flag,
                word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target

                use8flag controls whether we make the call to tuple8_from_tuple14() or not.
                """
                spaced = target_key
                gen_prefix = 'target_spaced_space:'
                error_prefix = 'target_spaced_space Error:'
                use8flag = None # None means undetermined, False means spaced already exact, True means fetch
                tup = self.tuples_all_from_key(target_key)
                if tup is not None:
                    line = tup[1]
                    lastword = tup[12]
                    #print tup
                    if tup[0] is None:
                        raise Exception('Supplied target_key lookup did not find expected tuple')
                    elif self.keysep_named is None:
                        # Keyedfile or some other general purpose variant
                        pass
                    elif len(self.keysep_named.strip()) < 1:
                        # Keyedspace or Keyedspace2 or some other similar subclass
                        if lastword is not None and len(lastword) > 0:
                            #print "line lastword={0} likely Keyedspace() or Keyedspace2()".format(lastword)
                            if line.startswith("{0}{1}".format(tup[13],lastword)):
                                spaced = tup[13] # target is already perfectly spaced so use it
                                use8flag = False
                                # Have set use8flag False which indicates spaced is already exact
                                #print use8flag,tup
                            elif line.startswith("{0} {1}".format(tup[13],lastword)):
                                spaced = "{0} ".format(tup[13]) # target is near perfectly spaced - adjust and use
                                use8flag = False
                                # Have set use8flag False which indicates spaced is already exact
                                #print use8flag,tup
                        else:
                            use8flag = True
                        #print use8flag,use8flag,use8flag,use8flag,target_key,len(spaced),spaced
                    elif tup[3] == self.keysep:
                        # keysep is chr(32) space and tuple agrees
                        use8flag = True
                    else:
                        pass
                tup8 = None
                if use8flag is True:
                    tup8 = tuple8_from_tuple14(tup,True) # recovery=True
                    if tup8 is None or tup8[0] is None:
                        raise Exception('Supplied target_key lookup did not find expected tuple')
                    # tuple8: key, line, before, sep, after, lastchar, before_counter, after_counter
                keyseppos = tup[10]
                # Above we label 11th field from original 14 tuple as keyseppos
                if tup8:
                    """ Have tuple8 so can use after=tup8[4] in conjunction with keyseppos=tup[10]
                    to get a faithful representation for spaced
                    tuple8: key, line, before, sep, after, lastchar, before_counter, after_counter
                    """
                    after = tup8[4]
                    if keyseppos > 0:
                        try:
                            upto_not_including = keyseppos+len(after)+1
                            spaced = line[:upto_not_including]
                        except:
                            pass
                    elif verbose is True:
                        print "{0} working use8flag={1} obtained 8 tuple but spaced unchanged (keyseppos={2})".format(
                            gen_prefix,use8flag,keyseppos)
                elif use8flag is False:
                    # We are trusting target is already perfectly spaced and have already spaced = tup[13]
                    if verbose is True:
                        print "{0} as instructed (use8flag={1}) we are leaving spaced unchanged.".format(
                            gen_prefix,use8flag)
                else:
                    # Have not been told explicitly to skip the fetch of 8 tuple.
                    after_count = tup[4]
                    if use8flag is True:
                        """ We tried and failed to get tuple8 so use what we have
                        which is the original tuple data and do the best we can
                        """
                        if verbose:
                            print "{0} bad fetch 8 tuple (use8flag={1}) so use after_count which is {2}".format(
                                gen_prefix,use8flag,after_count)
                    elif verbose is True:
                        """ use8flag is neither True or False and we have no 8 tuple for detailed analysis """
                        print "{0} best analysis when keyseppos={1} and use8flag={2} ".format(
                            gen_prefix,keyseppos,use8flag),
                        print("uses after_count which is {0} here".format(after_count))                        
                    else:
                        pass
                    keysep_after_word_index = tup[11]
                    target = tup[13]
                    #print keyseppos,after_count,keysep_after_word_index,len(target),target
                    #if 'Max' in target_key:
                    #    print tup
                    try:
                        upto_not_including = keyseppos+after_count+1
                        if upto_not_including >= len(line):
                            spaced = line
                        else:
                            spaced = line[:upto_not_including]
                    except:
                        if verbose is True:
                            print("{0} Assignment to spaced did not go as intended!".format(error_prefix))
                    #if 'ISO-8859-15' in target_key: print use8flag,upto_not_including,spaced,tup
                    if len(spaced) < 1 and self.outerclass_name in ['Keyedequals2'] and keyseppos < 1:
                        if after_count < 1:
                            spaced = line.rstrip()
                    if verbose is True:
                        print("{0} Completed assignment to spaced={1}".format(gen_prefix,spaced))
                return spaced


            def target_spaced_strong(self,target_key,verbose=False):
                """ Probably equals (=) or colon (:) used to separate keys from values.
                Begin with a lookup of the original line by consulting self.tuples_all
                tuples_all contains tuples of 14 fields
                tuple14 = key, line, before_count, sep, after_count, linesep, linenum, comment_flag,
                word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                """
                spaced = "{0}{1}".format(target_key,self.keysep)
                tup = self.tuples_all_from_key(target_key)
                if tup is not None:
                    #print tup
                    gen_prefix = 'target_spaced_strong:'
                    #error_prefix = 'target_spaced_strong Error:'
                    if tup[0] is None:
                        raise Exception('Supplied target_key lookup did not find expected tuple')
                    elif tup[3] == self.keysep:
                        # Have already eliminated the possibility that keysep is chr(32) space
                        line = tup[1]
                        pos = line.index(self.keysep)
                        if verbose is True:
                            print "{0} retrospective reconstruction from tuple14 using pos={1}".format(
                                gen_prefix,pos)
                        # Ensure any lstripping of key does not influence spaced
                        after_count = tup[4]
                        if after_count > 0:
                            spaced = line[:(pos+len(self.keysep)+after_count)]
                            #spaced = "{0}{1}".format(line[:(pos+len(self.keysep))],' '*after_count)
                        else:
                            spaced = line[:(pos+len(self.keysep))]
                    else:
                        raise Exception("Keyed file line uses {0} instead of expected sep={1}".format(
                                tup[3],self.keysep))
                return spaced


            def target_spaced(self,target_key,hatted=True,verbose=False):
                """ Examines the spacing of the existing line indicated by target_key
                and produces a sed target string that will faithfully represent the
                current target INCLUDING appropriate keysep and spacing.

                Except where hatted=False assume that we should prefix the sed target with ^
                
                This function demonstrate two alternative ways of reconstructing from tuple14
                parse time dependent: upto_not_including = keyseppos+after_count+1
                retrospective based on final keysep: pos = line.index(self.keysep)

                # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                """
                spaced = target_key
                if self.keysep is None:
                    pass
                elif len(self.keysep.strip()) < 1:
                    # Keysep likely space
                    spaced = self.target_spaced_space(target_key,verbose)
                elif self.outerclass_name in ['Keyedequals2'] and target_key.startswith(chr(35)):
                    # Keysep probably a stronger value than space and/or we are likely just unhashing
                    spaced = self.target_spaced_space(target_key,verbose)
                else:
                    # Keysep is a stronger value than space - probably equals (=) or colon (:)
                    spaced = self.target_spaced_strong(target_key,verbose)
                if hatted is not False:
                    # Except where hatted=False assume that we should prefix the sed target with ^
                    spaced = "{0}{1}".format(chr(94),spaced)
                return spaced


            def target_and_subsitution(self,target,before,after,delimiter=None):
                """ Form a line similar to 0,/^target/ s/before/after/
                Except for key value lines where the key or value are like ascii explosions
                you should call this function without giving delimiter parameter.
                
                If you have a troublesome key value file then your local knowledge might
                produce a better result than this functions delimiter guesswork -
                In that case supply a delimiter and it will be used regardless.

                Practical use of this function paying particular attention to use of chr(94) caret:

                This function prepends the first argument with caret if it is missing.
                That first argument will be the second item in a sed range pair of form: from,target
                before and after are the pieces of a typical s/before/after/ substitution.

                In practice 'before' is probably going to included paranthesis and may have been sourced
                from a variable named target_paramed in the caller.

                For best results your 'before' item should include a chr(94) caret and be a high fidelity
                match for the line you wish to replace. We do not add a chr(94) caret to 'before' if
                it is missing, but some files (/etc/default/varnish) might not [when sedded this way]
                produce the results you are looking for. Do supply 'before' including a caret for best results.
                """
                # chr(47) is forward slash (/)  ; chr(64) is at symbol (@)  ;  chr(94) is caret / hat
                # chr(63) is question mark (?)  ; chr(36) is dollar ($)  ; chr(92) is back slash ; chr(44) comma
                error_prefix = 'target_and_subsitution() Error:'

                target_and_sub = "0,/{0}zZzZz/ s/zZzZz/zZzZz/".format(chr(94))
                if target is None or len(target.strip()) < 2:
                    """ < 2 is arbitrary limit. You will probably not
                    obtain good results attempting to work with key value
                    files having keys that are < 2. Do alter this value
                    if you feel confident about working effectively with
                    very short keys.
                    """
                    raise Exception("{0} unable to form target_and_sub - bad target:{1}".format(error_prefix,
                                                                                                target))
                elif after is None:
                    raise Exception("{0} unable to form target_and_sub - bad replacement:{1}".format(error_prefix,
                                                                                                     after))
                else:
                    target_and_sub = '0,/^_target_/ s/\(^_target_\)\(.*\)/\\1_replacement_/'

                counter_combined = None
                if delimiter is None or len(delimiter) < 1:
                    # We choose a delimiter
                    combined = "{0}{1}{2}".format(target,before,after)
                    counter_combined = Counter(combined)
                    if chr(47) not in counter_combined:
                        delimiter = chr(47)
                        delimiter_first = delimiter
                    elif chr(64) not in counter_combined:
                        delimiter = chr(64)
                        delimiter_first = "{0}{1}".format(chr(92),delimiter)
                    elif chr(63) not in counter_combined:
                        delimiter = chr(63)
                        delimiter_first = "{0}{1}".format(chr(92),delimiter)
                    elif chr(36) not in counter_combined:
                        delimiter = chr(36)
                        delimiter_first = "{0}{1}".format(chr(92),delimiter)
                    else:
                        # for target,count in targets_counted.most_common():
                        middle_msg = 'automatically allocate delimiter'
                        raise Exception("{0} unable to {1} for {2}".format(error_prefix,middle_msg,combined))
                else:
                    # Have been given a delimiter which we will use regardless.
                    if delimiter in [chr(64),chr(63),chr(36)]:
                        delimiter_first = "{0}{1}".format(chr(92),delimiter)
                    else:
                        delimiter_first = delimiter

                if target.startswith(chr(94)):
                    target_hatted = target
                else:
                    target_hatted = "{0}{1}".format(chr(94),target)
                target_and_sub = "0,{1}{2}{0} s{0}{3}{0}{4}{0}".format(delimiter,delimiter_first,
                                                                       target_hatted,before,after)
                #print len(target_and_sub),target_and_sub
                return target_and_sub


            def sedline(self,target_key,required_val):
                """ Lightweight version of sedline_safe_inner which gives an alternative
                (lightweight) preview.

                Certainly use sedline_safe_inner() if you your file has values wrapped in double quotes or single
                quotes rather than being bare. Example ENABLED="false" rather than ENABLED false

                For files with key and value separated by space (so
                self.kepsep is not (=) or (:) and you do not want safety of specifying
                required_val, then this is a lightweight alternative that should do just fine.
                """
                constructed = ''
                if target_key is not None and len(target_key) > 1:
                    """ Use \\1 below when really we mean \1 so as Python treats properly """
                    if len(self.keysep.strip()) < 1:
                        """ constructed = "0,/{0}\s+/ s/\({0}\)\(.*\)/\\1 {1}/".format(target_key,required)
                        Above we use \\1 when really we mean \1 so as Python expresses it properly """
                        #target = "{0}\s+".format(target_key)
                        target = "{0}\s+".format(self.target_spaced(target_key))
                        target_paramed = "\({0}\)\(.*\)".format(target_key)
                        new_paramed = "\\1 {0}".format(required_val)
                        constructed = self.target_and_subsitution(target,target_paramed,new_paramed)
                    else:
                        """ keysep is equals or colon or something other than space(s)
                        constructed = "0,/{0}{1}/ s/\({0}{1}\)\(.*\)/\\1{2}/".format(
                        target_key,self.keysep,required_val)
                        """
                        #target = "{0}{1}".format(target_key,self.keysep)
                        target = self.target_spaced(target_key)
                        target_paramed = "\({0}{1}\)\(.*\)".format(target_key,self.keysep)
                        new_paramed = "\\1{0}".format(required_val)
                        constructed = self.target_and_subsitution(target,target_paramed,new_paramed)
                return constructed


            def sedline1(self,target_key,required_val,enclosed_by=None):
                """ Variant of sedline() which has an extra parameter 'enclosed_by'

                Lightweight version of sedline_safe_inner which gives an alternative
                (lightweight) preview.

                Using sedline_safe_inner() will give you a closer preview to what will
                be recorded when you call sedline_safe()

                chr(94) is caret / hat
                """
                constructed = ''
                if target_key is not None and len(target_key) > 1:
                    target = self.target_spaced(target_key,False)        
                    target_hatted = "{0}{1}".format(chr(94),target)
                    target_paramed = "\({0}\)\(.*\)".format(target_hatted)
                    if enclosed_by is None:
                        # Use \\1 below when really we mean \1 so as Python treats properly
                        if len(self.keysep.strip()) < 1:
                            """ constructed = "0,/{0}\s+/ s/\({0}\)\(.*\)/\\1 {1}/".format(
                            target_key,required_val)
                            """
                            new_paramed = "\\1 {0}".format(required_val)
                        else:
                            """ keysep is equals or colon or something other than space(s)
                            constructed = "0,/{0}/ s/\({0}\)\(.*\)/\\1{1}/".format(
                            self.target_spaced(target_key),required_val)
                            """
                            new_paramed = "\\1{0}".format(required_val)
                            #print "{0}{3}{1}{3}{2}{3}".format(target,target_paramed,new_paramed,os_linesep)
                    else:
                        # Use \\1 below when really we mean \1 so as Python treats properly
                        """ stored_val = self[target_key]
                        if stored_val.startswith(enclosed_by):
                        """
                        if len(self.keysep.strip()) < 1:
                            """ constructed = "0,/{0}\s+/ s/\({0}\)\(.*\)/\\1 {2}{1}{2}/".format(
                            target_key,required_val,enclosed_by)
                            """
                            new_paramed = "\\1 {1}{0}{1}".format(required_val,enclosed_by)
                        else:
                            """ keysep is equals or colon or something other than space(s)
                            constructed = "0,/{0}/ s/\({0}\)\(.*\)/\\1{2}{1}{2}/".format(
                            self.target_spaced(target_key),required_val,enclosed_by)
                            """
                            new_paramed = "\\1{1}{0}{1}".format(required_val,enclosed_by)
                    constructed = self.target_and_subsitution(target,target_paramed,new_paramed)
                #print "constructed={0}".format(constructed)
                return constructed


            def sedline_safe_inner(self,target_key,required_val,enclosed_by=None,starts_hash_disallowed=True):
                """ Construct a sed target range and substitution to support
                a sed driven update to a single line in a key value text file.

                Use this function for previewing / testing as unlike sedline_safe(), this
                inner function returns the construction, but does not record your intention to change.
                """
                constructed = None
                if target_key is None:
                    return (None,target_key,None,None)
                if len(target_key) < 2:
                    return (None,target_key,None,None)
                if starts_hash_disallowed and target_key.lstrip().startswith(chr(35)):
                    """ Quit early as in this mode we are not to allow substitutions where
                    target_key could also be the target of an update via context_update()
                    """
                    return (None,target_key,None,None)
                #print (target_key,required_val,enclosed_by,starts_hash_disallowed)
                replacement = 'errorval'
                if target_key not in self:
                    return (None,target_key,None,None)
                original_val = self[target_key]
                if enclosed_by is None:
                    replacement = "{0}".format(required_val)
                    """ Above seems unnecessary but helps us deal with incoming int without
                    having to try except for an AttributeError
                    """
                else:
                    if original_val.startswith(enclosed_by):
                        replacement = "{0}{1}{0}".format(enclosed_by,required_val)
                    else:
                        replacement = 'errorunsafe'
                if replacement.startswith('error'):
                    return (None,target_key,None,replacement)
                """ Preliminary logic complete, now form decide on how / where to obtain target """
                target = target_key
                #target_hatted = target
                if self.keysep is not None:
                    #print len(target),'target was',target
                    target = self.target_spaced(target_key,False)
                    #print len(target),'target (after adjustment)',target
                    target_hatted = "{0}{1}".format(chr(94),target)
                if len(self.keysep.strip()) < 1:
                    if len(self.keysep_named.strip()) > 0:
                        if (len(target)-len(target.rstrip())) == 1:
                            """ For space separated lines, where we have
                            not been given a strong keysep_named, unless we are certain
                            that target is already right padded then prepend a space
                            """
                            replacement = " {0}".format(replacement)
                """
                original = self.originally_spaced(target_key,1)
                if original is None:
                    return (None,target_key,None,replacement)
                """
                target_paramed = "\({0}\)\(.*\)".format(target_hatted)
                escaped = self.possibly_continued(replacement)
                # Below we use \\1 below when really we mean \1 so as Python expresses it properly
                if escaped is not None:
                    new_paramed = "\\1{0}".format(escaped)
                    #constructed = "0,/{0}/ s/\({1}\)\(.*\)/\\1{2}/".format(target,original,escaped)
                else:               
                    new_paramed = "\\1{0}".format(replacement)
                    #constructed = "0,/{0}/ s/\({1}\)\(.*\)/\\1{2}/".format(target,original,replacement)
                constructed = self.target_and_subsitution(target,target_paramed,new_paramed)
                return (constructed,target_key,original_val,replacement)


            def sedline_safe(self,target_key,required_val,enclosed_by=None,
                             starts_hash_disallowed=True,verbosity=0):
                """ Use this function when you have a firm intention to update the
                file (rather than just previewing) as calling this function will
                record your intention in dict_of_changed() and elsewhere.

                If you wanted to make the new value (including single quotes)
                to be 'false' instead of existing value 'true', then use this function
                with final argument set to single quote and give the unquoted value
                as required_val parameter.

                The 'safe' in the function name refers to the startswith() check
                on the existing value otherwise sedline returned is empty.

                Unlike the lightweight sedline() function we take a heavier approach
                to ensure that tabs / spaces / other whitespacing is fully preserved
                and to support more cases.
                
                starts_hash_disallowed is protection against clashing with changes produced
                by context_update() and defaults to True.
                If in your entire usage of the underlying key value file, you will never
                be using context_update() then starts_hash_disallowed=False should be fine.

                constructed_tuple returned by sedline_safe_inner is as follows:
                (constructed,target_key,original_val,replacement)

                This function calls record_changed() but NOT sedqueue()
                
                At the moment there is a late call to fetch tuple14 driven by the need
                for a line number.

                If in future sedline_safe_inner() is reworked so it is supplied with tuple14
                and can take advantage of that instead of refetching during methods
                employed in called functions target_spaced() and originally_spaced(),
                then an early call to fetch tuple14 would be more appropriate.
                """
                #verbosity = 2
                constructed_tuple = self.sedline_safe_inner(target_key,required_val,enclosed_by,starts_hash_disallowed)
                # returns (constructed,target_key,original_val,replacement)
                constructed = constructed_tuple[0]
                if constructed is None:
                    # Unlike _inner which is not called directly we return empty string unless all is completed.
                    if verbosity > 1:
                        print "constructed None returned from sedline_safe_inner()!"
                    constructed = ''
                elif len(constructed.strip()) < 2:
                    if verbosity > 1:
                        print "constructed (empty) returned from sedline_safe_inner()!"
                else:
                    gen_prefix = 'sedline_safe:'
                    error_prefix = 'sedline_safe Error:'
                    target_key = constructed_tuple[1]
                    original_val = constructed_tuple[2]
                    replacement = constructed_tuple[3]
                    #print target_key,original_val,replacement
                    if original_val == replacement:
                        """ No change to record so do not call record_changed() """
                        recorded_count = 0
                    else:
                        recorded_count = self.record_changed(target_key,original_val,replacement)
                    #print target_key,recorded_count,recorded_count,recorded_count
                    if recorded_count == 1:
                        pass
                    else:
                        """ If you cannot make sense of the planned change or recording it fails then
                        abandon your construction and set constructed to empty """
                        constructed = ''
                        if verbosity > 1:
                            print "{0} record_changed returned {1}".format(error_prefix,recorded_count)
                    # record_changed() above should have entered the change into internal dict_of_changed
                    if verbosity > 2:
                        print "recorded_count:{0} from constructed={1}".format(recorded_count,constructed)
                return constructed


            def sedline_safe_wrapper(self,target_key,required_val,enclosed_by=None):
                """ Public API for changes to remote files that do not fall into the
                following well defined categories:
                (.) Typical toggles - True to False / ENABLED to DISABLED and similar
                    ( use toggle_safe_wrapper() instead)
                (.) Fixed values such as localhost / 127.0.0.1
                    ( use context_update_localhost() instead )
                (.) Context updates where you want to closely match even small clues in
                    the config file as to where to place a key value update.
                    ( use context_update() instead )

                Returns True or False

                Unlike sedline_safe we fix the value of starts_hash_disallowed=True
                and verbosity=0.

                Manually wrap / call sedline_safe directly if you have a pressing
                need for usage that does not respect those fixed values.
                Note: You will probably need to make your own call to sedqueue()
                in any wrapper.

                See sedline_safe_wrapper0 if you want a variant that thinks unchanged is okay.
                """
                #starts_hash_disallowed=True
                verbosity=0
                constructed = self.sedline_safe(target_key,required_val,enclosed_by,True,verbosity)
                if constructed is None:
                    return False
                elif len(constructed) < 2:
                    return False
                tuple14 = self.tuples_all_from_key(target_key)
                linenum = tuple14[6]
                # sedline_safe() has already taken care of the call to record_changed()
                # next we queue the sed via sedqueue() which enters it into internal dict_of_seds
                queue_return = self.sedqueue(linenum,constructed,verbosity)
                if verbosity > 2:
                    print "sedlines count is {0} after sedqueue() returned {1}".format(len(self.sedlines()),
                                                                                       queue_return)
                return queue_return


            def sedline_safe_wrapper0(self,target_key,required_val,enclosed_by=None):
                """ Variant of sedline_safe_wrapper() that considers zero changes (unchanged)
                a non error. Avoid messagess like 'UNCHANGED failure for target_key'

                Returns True or False

                Do use sedline_safe_wrapper() if you are fixing a known variance from
                the required value. However if you are just issuing a 'state request' then
                the value might already be as you requested.

                To clarify: 0==self.record_changed(target_key,original_val,replacement) is okay here

                To achieve this we include code blocks from both sedline_safe and some already
                seen in sedline_safe_wrapper() to make recorded_count of zero okay.
                """
                #starts_hash_disallowed=True
                verbosity=0
                constructed_tuple = self.sedline_safe_inner(target_key,required_val,enclosed_by,True)
                # returns (constructed,target_key,original_val,replacement)
                constructed = constructed_tuple[0]
                recorded_count = -1
                if constructed is None:
                    # Unlike _inner which is not called directly we return empty string unless all is completed.
                    constructed = ''
                elif len(constructed.strip()) < 2:
                    pass
                else:
                    gen_prefix = 'sedline_safe_wrapper0:'
                    error_prefix = 'sedline_safe_wrapper0 Error:'
                    target_key = constructed_tuple[1]
                    original_val = constructed_tuple[2]
                    replacement = constructed_tuple[3]

                    #print target_key,original_val,replacement
                    if original_val == replacement:
                        """ No change to record so do not call record_changed() """
                        recorded_count = 0
                    else:
                        recorded_count = self.record_changed(target_key,original_val,replacement)
                    #print target_key,recorded_count,recorded_count,recorded_count
                    if recorded_count < 2:
                        """ recorded_count of 1 or 0 is acceptable.
                        Unlike related function sedline_safe_wrapper() we consider 'unchanged' okay
                        """
                        pass
                    else:
                        """ If you cannot make sense of the planned change or recording it fails then
                        abandon your construction and set constructed to empty """
                        constructed = ''
                        if verbosity > 1:
                            print "{0} record_changed returned {1}".format(error_prefix,recorded_count)
                    # record_changed() above should have entered the change into internal dict_of_changed
                    if verbosity > 2:
                        print "recorded_count:{0} from constructed={1}".format(recorded_count,constructed)

                if recorded_count < 0:
                    return False
                elif recorded_count == 0:
                    """ return True as nothing else to do if state is 'unchanged' """
                    return True
                else:
                    tuple14 = self.tuples_all_from_key(target_key)
                    linenum = tuple14[6]
                    # Call to record_changed() has already happened and result is in recorded_count.
                    # next we queue the sed via sedqueue() which enters it into internal dict_of_seds
                    queue_return = self.sedqueue(linenum,constructed,verbosity)
                    if verbosity > 2:
                        print "sedlines count is {0} after sedqueue() returned {1}".format(len(self.sedlines()),
                                                                                           queue_return)
                return queue_return


            def tuple_hashed(self,tup,return_type='flag'):
                """ Takes a two tuple (Example: an entry from tuples_all) and 
                based on the value of first field returns False or the second field
                When return_type is not 'value' then only possible returns are True or False
                """
                if tup[0] is None:
                    return False
                # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                if not tup[0].startswith(chr(35)):
                    return False
                # Have got this far so tup[0] must startwith '#'
                if return_type == 'value':
                    return tup[1]
                return True


            def tuples_hashed(self):
                """ Return a list of tuples. Formed from tuples_all where
                we have filtered and only retained tuples where line starts hash / octothorpe
                Unlike tuple_hashed we are consulting the original line rather than first field
                of 7 tuple.
                tuple7: key, line, before_count, sep, after_count, linesep, linenum
                By consulting the stored line directly we are not relying on any key assignment logic.
                """
                tuples = []
                # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                for tup in self.tuples_all:
                    if tup[1] is None:
                        continue
                    if tup[1].lstrip().startswith(chr(35)):
                        tuples.append(tup)
                return tuples


            def tuples_unhashed(self):
                """ Return a list of tuples. Formed from tuples_all where
                we have filtered and only retained tuples where line does not starts hash / octothorpe
                Unlike tuple_hashed we are consulting the original line rather than first field
                of 7 tuple.
                tuple7: key, line, before_count, sep, after_count, linesep, linenum
                By consulting the stored line directly we are not relying on any key assignment logic.
                """
                tuples = []
                # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                for tup in self.tuples_all:
                    if tup[1] is None:
                        continue
                    if tup[1].lstrip().startswith(chr(35)):
                        continue
                    tuples.append(tup)
                return tuples


            def tuple_disabled(self,tup,return_type='flag'):
                """ Takes a two tuple (Example: an entry from tuples_all) and 
                based on the value of first field returns False or the second field
                When return_type is not 'value' then only possible returns are True or False
                Compared to tuple_hashed() we employ a more rigorous definition. Some implication arrows:
                disabled -> hashed
                hashed is necessary for a tuple to be possibly disabled but does not imply disabled
                In particular: If the original tuple was stored keysep=None then we return False
                Tuple: key, line, before_count, sep, after_count, linesep, linenum
                """
                if tup[0] is None:
                    return False
                # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                if not tup[0].lstrip().startswith(chr(35)):
                    return False
                """ Have got this far so tup[0] must startwith '#' """
                # if 'E@euro' in tup[0]: print tup
                # tuple14 = key, line, before_count, sep, after_count, linesep, linenum, comment_flag,
                # word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                if len(tup) < 3:
                    # Have no extra fields with which to analyse things so proceed beyond this block
                    pass
                elif tup[7] is False and self.keysep_named == chr(32):
                    """ keysep_named is set to SPACE so have subclassed and
                    we should trust the subclass when it sets comment_flag to False.
                    """
                    pass
                elif tup[3] is None:
                    """ When have got this far and find stored keysep (tup[3]) of None
                    then during parsing we have not marked this line as possibly disabled
                    return False to indicate that it is unlikely this tuple is a disabled key value.
                    """
                    return False
                else:
                    # Treat the tuple as a disabled key value so proceed beyond this block
                    pass
                #print 'Disabled:',tup
                if return_type == 'value':
                    return tup[1]
                return True


            def tuple_enabled(self,tup,return_type='flag'):
                """ Takes a tuple (Example: an entry from tuples_all) and 
                based on the value of first field returns False or the second field
                When return_type is not 'value' then only possible returns are True or False """
                if tup[0] is None:
                    return False
                # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                if tup[0].startswith(chr(35)):
                    return False
                if return_type == 'value':
                    return tup[1]
                return True


            def tuples_enabled(self):
                """ Return a list of tuples. Formed from tuples_all where
                we have filtered and only retained tuples where key is enabled """
                tuples = []
                # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                for tup in self.tuples_all:
                    if tup[0] is None or tup[0].startswith(chr(35)):
                        pass
                    else:
                        tuples.append(tup)
                return tuples


            def tuples_without_keysep(self,enabled=True):
                """ Tuple: key, line, before_count, sep, after_count, linesep, linenum """
                if enabled:
                    tuples = []
                    for tup in self.tuples_all:
                        if tup[0] is None or tup[0].startswith(chr(35)):
                            continue
                        if tup[3] is None:
                            tuples.append(tup)
                        else:
                            pass
                else:
                    tuples = []
                    for tup in self.tuples_all:
                        if tup[3] is None:
                            tuples.append(tup)
                        else:
                            pass
                return tuples


            def tuples_with_keysep(self,enabled=True,keysep='default'):
                """ Tuple: key, line, before_count, sep, after_count, linesep, linenum """
                if keysep is None:
                    return tuples_without_keysep()
                if keysep is 'default' and enabled:
                    tuples = []
                    for tup in self.tuples_all:
                        if tup[0] is None or tup[0].startswith(chr(35)):
                            pass
                        elif tup[3] is not None:
                            tuples.append(tup)
                        else:
                            pass
                elif enabled:
                    tuples = []
                    for tup in self.tuples_all:
                        if tup[0] is None or tup[0].startswith(chr(35)):
                            continue
                        tuple_sep = tup[3]
                        if tuple_sep == keysep:
                            tuples.append(tup)
                        else:
                            pass
                else:
                    tuples = []
                    for tup in self.tuples_all:
                        if tup[3] == keysep:
                            tuples.append(tup)
                        else:
                            pass
                return tuples


            def values_enabled(self,list_of_tups):
                """ Returns a list of values where the value is enabled.
                Takes a list of tuples as input. Output is a filtered list of values
                """
                list_returned = []
                for tup in list_of_tups:
                    if tup[0] is None:
                        continue
                    if tup[0].startswith(chr(35)):
                        continue
                    list_returned.append(tup[1])
                return list_returned


            def flags_enabled(self,list_of_tups):
                """ Returns a list of True / False indicating when the value is enabled.
                Takes a list of tuples as input. Output is an unfiltered list of True / False
                """
                list_returned = []
                for tup in list_of_tups:
                    if tup[0] is None:
                        list_returned.append(False)
                        continue
                    if tup[0].startswith(chr(35)):
                        list_returned.append(False)
                        continue
                    list_returned.append(True)
                return list_returned


            def keys_enabled_printable(self):
                # Returns a tuple of keys which are 'enabled' and printable
                enabled = []
                for dict_key in self.keys():
                    if dict_key is None or dict_key.startswith(chr(35)):
                        pass
                    elif set(dict_key).issubset(cmdbin.SET_PRINTABLE):
                        enabled.append(dict_key)
                    else:
                        pass
                return tuple(enabled)


            def keys_enabled(self,printable=False):
                # Returns a tuple of keys which are 'enabled'
                enabled = []
                if printable is True:
                    return self.keys_enabled_printable()
                for dict_key in self.keys():
                    if dict_key is None or dict_key.startswith(chr(35)):
                        pass
                    else:
                        enabled.append(dict_key)
                return tuple(enabled)


            def keyvals_typed(self,linetype,keys_list):
                """ For the purposes of this function we make no distinction
                between 'hashed' and 'disabled'
                When requesting linetype=0 you might instead consider using keys_disabled() 
                if you want a stronger definition that just 'hashed'
                """
                values_returned = []
                if hasattr(keys_list, "__iter__") and type(keys_list) != type(''):
                    pass
                else:
                    print 'keyvals_typed() sees API misuse. An Iterable expected - not given'
                    return values_returned
                if linetype is None:
                    linetype = 1
                else:
                    linetype = int(linetype)
                """ linetype=0 says return only keys in keys_list that are 'hashed'/'disabled'.
                linetype=1 says return only keys in keys_list that are enabled.
                linetype > 1 says return keys in keys_list that are either 'hashed' or 'enabled'.
                """
                if linetype < 1:
                    """ Only key lines where the key matches and the key 'hashed' (starts #) """
                    for target in keys_list:
                        key_target = "#{0}".format(target)
                        if key_target in self:
                            values_returned.append(self.get(key_target))
                elif linetype == 1:
                    """ Only key lines where the key matches and enabled """
                    for key_target in keys_list:
                        if key_target in self:
                            values_returned.append(self.get(key_target))
                else:
                    """ 'in self' as we are an OrderedList of course """
                    for target in keys_list:
                        if target in self:
                            values_returned.append(self.get(target))
                        key_target = "#{0}".format(target)
                        if key_target in self:
                            values_returned.append(self.get(key_target))
                return values_returned


            def keys_disabled(self,safe=True,hashless=False,set_for_unique=True):
                """ This function uses a fixed representation of the file
                and would NOT include any updates you have initiated.
                Returns a tuple of keys.
                Note: when safe=True (default) an additional check is done
                to ensure that any enabled key could not also be considered
                disabled (by being returned in our tuple). 
                Example three line key value file to illustrate:
                KEY=VALUE
                #SHELL=/bin/sh
                SHELL=/bin/bash
                Above we have a current value for SHELL so it seem logical
                to exclude it from any keys_disabled() returned tuple.
                If you directly asked for self['#SHELL'] you would get a hit
                If you directly asked for self['SHELL'] you would get a hit
                Set safe=False if [for our three line example], you would still
                like SHELL to appear in the results from keys_disabled.

                Setting hashless=True affects the literal values of the keys returned.

                hashless=False (default) is recommended if the returned keys
                are intended for lookups after the function has completed.
                """
                filtered = filter(self.tuple_disabled,self.tuples_all)
                """ filtered are two tuples so we will use a for loop below to 
                take just first field (subject to safe check discussed above)
                """
                # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                #print len(filtered),len(filtered)
                disabled = []
                if safe is True:
                    if hashless is True:
                        for tup in filtered:
                            key = tup[0]
                            key_when_enabled = key.lstrip().lstrip(" {0}".format(chr(35)))
                            if key_when_enabled in self:
                                pass
                            else:
                                disabled.append(key_when_enabled)
                    else:
                        for tup in filtered:
                            key = tup[0]
                            key_when_enabled = key.lstrip().lstrip(" {0}".format(chr(35)))
                            if key_when_enabled in self:
                                pass
                            else:
                                disabled.append(key)
                elif hashless is True:
                    for tup in filtered:
                        key = tup[0]
                        key_when_enabled = key.lstrip().lstrip(" {0}".format(chr(35)))
                        disabled.append(key_when_enabled)
                else:
                    for tup in filtered:
                        disabled.append(tup[0])
                if set_for_unique:
                    disabled = set(disabled)
                #print len(disabled),filtered
                """ For a three line key value file like below:
                KEY=VALUE
                #SHELL=/bin/sh
                INACTIVE=-1
                keys_disabled(True,True) would return ['SHELL'] as we dropped the hash / octothorpe
                By comparison for the three line key value file like below:
                KEY=VALUE
                #SHELL=/bin/sh
                SHELL=/bin/bash
                keys_disabled(True,True) would return an empty tuple where safe=True
                """
                return tuple(disabled)


            def keys_disabled_printable(self,safe=True,hashless=False):
                printable = []
                super_tuple = self.keys_disabled(safe,hashless)
                if len(super_tuple) < 1:
                    return super_tuple
                for dict_key in super_tuple:
                    if set(dict_key).issubset(cmdbin.SET_PRINTABLE):
                        printable.append(dict_key)
                return tuple(printable)


            def keys_disabled_value_contains(self,target,safe=True,hashless=False):
                """ Begins by obtaining a list of disabled keys by calling keys_disabled().
                That result is then filtered so that keys are filtered out unless their
                dictionary value contains the target

                Useful when dealing with files that have plenty of # key:value lines rather 
                than #key:value as allows you to use a reverse process to obtain
                a space preserving representation of the key by using a value match.
                """
                if target is None or len(target) < 1:
                    return []
                contains = []
                super_tuple = self.keys_disabled(safe,hashless)
                if len(super_tuple) < 1:
                    return super_tuple
                for dict_key in super_tuple:
                    if target in self[dict_key]:
                        contains.append(dict_key)
                return tuple(contains)


            def keys_hashed(self,hashless=False,set_for_unique=False):
                """ Returns a tuple of keys which are 'hashed'
                Unlike keys_disabled() our default for set_for_unique is False.
                keys_disabled() is concerned with a description of a key
                and it seems logical to think that (by default) a key could
                only be disabled once.
                For keys_hashed we are more interested in presence of hash / octothorpe
                and so we do not enforce uniqueness by default.
                """
                return self.keys_disabled(False,hashless,set_for_unique)


            def keylines_disabled(self):
                """ This function uses a fixed representation of the file
                and would NOT include any updates you have initiated.
                Returns a list of lines. Although sourced from tuples_all, we
                return just the original line and do nothing with the key """
                filtered = filter(self.tuple_disabled,self.tuples_all)
                """ filtered are two tuples so we will use a for loop below to 
                take just second field.
                Example of two tuple in filtered ('AcceptEnv', 'AcceptEnv LANG LC_*\n')
                """
                disabled = []
                for item in filtered:
                    disabled.append(item[1])
                """ Above append (for our example tuple) would append 'AcceptEnv LANG LC_*\n' """
                return disabled


            def keylines_enabled(self):
                """ This function uses a fixed representation of the file
                and would NOT include any updates you have initiated.
                Returns a list of lines. Although sourced from tuples_all, we
                return just the original line and do nothing with the key """
                filtered = filter(self.tuple_enabled,self.tuples_all)
                """ filtered are two tuples so we will use a for loop below to 
                take just second field.
                Example of two tuple in filtered ('AcceptEnv', 'AcceptEnv LANG LC_*\n')
                """
                enabled = []
                for item in filtered:
                    enabled.append(item[1])
                """ Above append (for our example tuple) would append 'AcceptEnv LANG LC_*\n'
                See lines_enabled() for a variant of this function that does rstrip()
                and will therefore remove the line ending character """
                return enabled


            def keylines_enabled_resplit(self):
                enabled = self.keylines_enabled()
                unsplit = ''.join(enabled)
                # print "conf_linesep:{0} {0} {0}".format(self.conf_linesep)
                return unsplit.split(self.conf_linesep)


            def keylines_typed(self,linetype,keys_list):
                """ For the purposes of this function we make no distinction
                between 'hashed' and 'disabled'.
                When requesting linetype=0 you might instead consider using keys_disabled() 
                if you want a stronger definition that just 'hashed'
                """
                lines_returned = []
                if hasattr(keys_list, "__iter__") and type(keys_list) != type(''):
                    pass
                else:
                    print 'keylines_typed() sees API misuse. An Iterable expected - not given'
                    return lines_returned
                if linetype is None:
                    linetype = 1
                else:
                    linetype = int(linetype)
                """ linetype=0 says return only keys in keys_list that are 'hashed'/'disabled'.
                linetype=1 says return only keys in keys_list that are enabled.
                linetype > 1 says return keys in keys_list that are either 'hashed' or 'enabled'.
                """
                if linetype < 1:
                    """ Only key lines where the key matches and the key 'hashed' (starts #) """
                    for target in keys_list:
                        key_target = "#{0}".format(target)
                        if key_target in self:
                            for tup in self.tuples_all:
                                if tup[0] == key_target:
                                    lines_returned.append(tup[1])
                elif linetype == 1:
                    """ Only key lines where the key matches and enabled """
                    for key_target in keys_list:
                        if key_target in self:
                            for tup in self.tuples_all:
                                if tup[0] == key_target:
                                    lines_returned.append(tup[1])
                else:
                    for tup in self.tuples_all:
                        if tup[0] is not None:
                            if tup[0] in keys_list:
                                lines_returned.append(tup[1])
                            elif tup[0].lstrip(chr(35)) in keys_list:
                                lines_returned.append(tup[1])
                return lines_returned


            def keyvals(self,keys_list):
                return self.keyvals_typed(1,keys_list)


            def keylines(self,keys_list):
                return self.keylines_typed(1,keys_list)


            def lines_enabled(self):
                """ This function uses a fixed representation of the file
                and would NOT include any updates you have initiated.
                Returns a list of lines. Although sourced from tuples_all, we
                return just the original line and do nothing with the key """
                filtered = filter(self.tuple_enabled,self.tuples_all)
                """ filtered are two tuples so we will use a for loop below to 
                take just second field.
                Example of two tuple in filtered ('AcceptEnv', 'AcceptEnv LANG LC_*\n')
                """
                enabled = []
                for item in filtered:
                    enabled.append(item[1].rstrip())
                """ Above append (for our example tuple) would append 'AcceptEnv LANG LC_*'
                See keylines_enabled() for a variant of this function that does not rstrip()
                and will therefore retain the line ending character """
                return enabled


            def lines_enabled_contains(self,target):
                """ This function uses a fixed representation of the file
                and would NOT include any updates you have initiated.
                Returns a list of lines. Although sourced from tuples_all, we
                return just the original line and do nothing with the key.
                An example of where you might use this function is by setting
                target=' files' and examining /etc/nsswitch.conf
                if no target is given then the results would be as for lines_enabled()
                """
                if target == '':
                    """ Keeps the logic in the later loop straightforward """
                    target = None
                tuples = self.tuples_enabled()
                enabled = []
                for tup in tuples:
                    if target is None or target in tup[1]:
                        enabled.append(tup[1].rstrip())
                """ Above we have rstrip() the line to lose any line ending delimiter
                Use tuples_enabled() directly and loop over the results if you do
                not want the lines to have been right stripped. """
                return enabled


            def lines_current_simple(self,changed_only=False):
                """ Unlike lines_for_put_simple, you will not find blank lines or
                comment lines in what is returned from this function. """
                if self.change_count < 1 and changed_only is True:
                    return []
                """ Having got this far we need to overlay the original lines with any
                changes recorded in dict_of_changed """
                # chr(35) is hash / octothorpe  ; chr(94) is caret (^)
                # tuple7: key, line, before_count, sep, after_count, linesep, linenum
                lines = []
                for tup in self._tuples_all:
                    linenum = tup[6]
                    line = tup[1]
                    if linenum in self._dict_of_changed:
                        line = self._dict_of_changed[linenum]
                    elif changed_only is True:
                        continue
                    if line is None:
                        lines = []
                        break
                    line_stripped = line.strip()
                    if len(line_stripped) < 3 or line_stripped.startswith(chr(35)):
                        continue
                    lines.append(line)
                return lines


            def keyseppos_from_keyname_desperate(self,keyname,line=None,verbosity=0):
                """ Avoid using this method except in very particular circumstances in
                which you are in complete control of the processing. The original tuples
                stored in tuples_all() should almost always be preferred, but there
                are specific situations where a hackier approach of using len() + len() + len()
                may be almost acceptable. Know that there are usually better approaches and
                that this method should be a method of last resort!
                You may in some cases obtain different results based on whether you supply the
                keyname stripped or unstripped. The responsibility is in this case with the caller
                to supply an appropriate input. Avoid wrapping this method and proliferating its use!

                By accessing the 14 field tuple you can use the 10th field by doing keyseppos = tup[10]
                but here we are being given line as a parameter and there may be circumstances where that
                does not exactly match original line. Thinking about hash removal from commented in particular.

                """
                if line is None:
                    line = self.keyline(keyname)
                tup = self.tuple8(keyname,line,keysep,(verbosity>1))
                keyseppos = len(tup[0]) + len(tup[2]) + len(tup[4])
                #print keyseppos,keyseppos,keyseppos
                return keyseppos


            def expressions3tuple(self,expression_list):
                """ Returns a 3 tuple with each tuple being a list
                First list is targets for a regex match()
                Second list is targets for a regex search()
                Third list is targets removed from the input expression list
                """
                match_targets = []
                search_targets = []
                not_targets = []
                if hasattr(expression_list, "__iter__") and type(expression_list) != type(''):
                    pass
                else:
                    print 'expressions3tuple() sees API misuse. An Iterable expression_list expected.'
                    return (match_targets,search_targets,not_targets)
                # chr(35) is hash / octothorpe  ; chr(94) is caret (^)
                for expression in expression_list:
                    if expression is None:
                        not_targets.append(expression)
                    expression_stripped = expression.strip()
                    if len(expression_stripped) < 1:
                        not_targets.append(expression)
                    if expression_stripped == chr(94):
                        not_targets.append(expression)
                    elif expression.startswith(chr(94)):
                        # Lose the leading caret
                        match_targets.append(expression[1:])
                    else:
                        search_targets.append(expression)
                assert len(expression_list) == len(match_targets) + len(search_targets) + len(not_targets)
                #print len(expression_list),len(match_targets),len(search_targets),len(not_targets)
                return (match_targets,search_targets,not_targets)


            def lines_noncomment_containing_any(self,expression_list):
                """ Each expression in the list should be a simple string or
                a simple string beginning caret (^). Regex expressions which require
                a raw definition are not explicitly supported.
                tuple14 = key, line, before_count, sep, after_count, linesep, linenum, comment_flag,
                  word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                """
                # chr(35) is hash / octothorpe  ; chr(94) is caret (^)
                if hasattr(expression_list, "__iter__") and type(expression_list) != type(''):
                    pass
                else:
                    """ API misuse is corrected rather than blocked here.
                    print "API misuse corrected for expression_list supplied as type {0}".format(
                        type(expression_list))
                    """
                    expression_list = [expression_list]
                (match_list,search_list,not_list) = self.expressions3tuple(expression_list)
                if len(match_list) == 0 and len(search_list) == 0:
                    return []
                lines = []
                for tup in self._tuples_all:
                    if tup[7] is not False:
                        # comment_flag needs to be False for line to be processed further
                        continue
                    line = tup[1]
                    for exp in match_list:
                        if re.match(exp,line):
                            lines.append(line)
                            # Use break to terminate the inner loop
                            break
                    for exp in search_list:
                        if re.search(exp,line):
                            lines.append(line)
                            # Use break to terminate the inner loop
                            break
                """ If re.match and re.search are ever reworked to use a different approach, then
                also take the opportunity to re.escape() before re.search() or during re.compile()
                """
                return lines


            def lines_current_containing(self,expression):
                """ Expression should be a simple string or a simple string
                beginning caret (^). Regex expressions which require
                a raw definition are not explicitly supported.

                Use similar function lines_for_put_containing() if you
                do not wish lines beginning hash / octothorpe excluded.
                """
                # chr(35) is hash / octothorpe  ; chr(94) is caret (^)
                expression_stripped = expression.strip()
                lines = []
                if expression is None or len(expression_stripped) < 1:
                    return lines
                if expression_stripped == chr(94):
                    return lines
                if expression.startswith(chr(94)):
                    # re match() for matching
                    re_containing = re.compile(expression[1:])
                    for line in self.lines_current_simple():
                        if re_containing.match(line):
                            lines.append(line)
                else:
                    # re search() for matching
                    """ Reminder about lines_current_simple() which does NOT include
                    comment lines in its results. So for config files such as salt
                    which are mostly comments / commented out placeholders you
                    would expect very few lines returned by lines_current_simple
                    anyway, even before you filter based on a regex.
                    """
                    #print len(self._dict_of_changed),self._dict_of_changed,len(self.lines_current_simple())
                    re_containing = re.compile(expression)
                    for line in self.lines_current_simple():
                        if re_containing.search(line):
                            lines.append(line)
                return lines


            def lines_for_put_simple(self):
                """ Output of all current lines with no diff checking at all
                Use instead lines_current_simple() if you want to exclude comment lines
                and blanks.
                """
                """ Use self.lines_tuple if you want the original instead of current """
                if self.change_count < 1:
                    return self._lines_tuple
                """ Having got this far we need to overlay the original lines with any
                changes recorded in dict_of_changed """
                lines = []
                linenum = -1
                for line in self.lines_tuple:
                    linenum += 1
                    if linenum not in self._dict_of_changed:
                        lines.append(line)
                        continue
                    line_changed = self._dict_of_changed[linenum]
                    if line_changed is not None and len(line_changed.strip()) > 2:
                        lines.append(line_changed)
                        #print "Line {0} (changed) has been appended.".format(linenum)
                    else:
                        lines = []
                        break
                return lines


            def lines_for_put_delimited(self):
                """ Simple output of all current key values with each
                line in the list of the following form:
                key keysep value.

                For clarity: This does not output the original lines nor the original lines
                plus changes, it instead generates lines on the fly from key values stored
                in the internal dictionary.
                """
                lines = []

                for k,v in self.items():
                    lines.append("{0} {1} {2}".format(k,self.keysep,v))

                # lines.append("{0} = {1}".format(k,v))
                return lines


            def lines_for_put_containing(self,expression):
                """ Expression should be a simple string or a simple string
                beginning caret (^). Regex expressions which require
                a raw definition are not explicitly supported.
                """
                # chr(35) is hash / octothorpe  ; chr(94) is caret (^)
                expression_stripped = expression.strip()
                lines = []
                if expression is None or len(expression_stripped) < 1:
                    return lines
                if expression_stripped == chr(94):
                    return lines
                if expression.startswith(chr(94)):
                    re_containing = re.compile(expression[1:])
                    for line in self.lines_for_put_simple():
                        if re_containing.match(line):
                            lines.append(line)
                else:
                    re_containing = re.compile(expression)
                    for line in self.lines_for_put_simple():
                        if re_containing.search(line):
                            lines.append(line)
                return lines


            def lines_for_put(self,simratio_quick=0,simratio=0):
                """ Output a list of the new lines in full suitable for manual put rather
                than any partial or inplace updates.
                """
                # Use self.lines_tuple if you want the original instead of current
                if self.change_count < 1:
                    return self._lines_tuple
                """ Having got this far we need to overlay the original lines with any
                changes recorded in dict_of_changed """
                lines = []
                if simratio_quick == 0 and simratio == 0:
                    return self.lines_for_put_simple()
                elif simratio_quick == 0:
                    from difflib import SequenceMatcher
                    for tup7 in self._tuples_all:
                        linenum = tup7[6]
                        if linenum not in self._dict_of_changed:
                            continue
                        line_changed = self._dict_of_changed[linenum]
                        if line_changed is not None and len(line_changed.strip()) > 2:
                            line_original = tup7[1]
                            sm = SequenceMatcher(None,line_original,line_changed)
                            if sm.ratio() >= simratio:
                                lines.append(line_changed)
                            else:
                                lines = []
                                break
                        else:
                            lines = []
                            break
                elif simratio == 0:
                    from difflib import SequenceMatcher
                    for tup7 in self._tuples_all:
                        linenum = tup7[6]
                        if linenum not in self._dict_of_changed:
                            continue
                        line_changed = self._dict_of_changed[linenum]
                        if line_changed is not None and len(line_changed.strip()) > 2:
                            line_original = tup7[1]
                            sm = SequenceMatcher(None,line_original,line_changed)
                            if sm.quick_ratio() >= simratio_quick:
                                lines.append(line_changed)
                            else:
                                lines = []
                                break
                        else:
                            lines = []
                            break
                else:
                    from difflib import SequenceMatcher
                    for tup7 in self._tuples_all:
                        linenum = tup7[6]
                        if linenum not in self._dict_of_changed:
                            continue
                        line_changed = self._dict_of_changed[linenum]
                        if line_changed is not None and len(line_changed.strip()) > 2:
                            line_original = tup7[1]
                            sm = SequenceMatcher(None,line_original,line_changed)
                            if sm.quick_ratio() >= simratio_quick and sm.ratio() >= simratio:
                                lines.append(line_changed)
                            else:
                                lines = []
                                break
                        else:
                            lines = []
                            break
                return lines


            def lines_for_put_auto(self):
                """ Output a list of the new lines in full suitable for manual put rather
                than any partial or inplace updates.
                RATIO_AUTO=0.5 setting will be used to decide if the list should be cleared
                because the changes seem too much.
                """
                # Use self.lines_tuple if you want the original instead of current
                if self.change_count < 1:
                    return self._lines_tuple
                """ Having got this far we need to overlay the original lines with any
                changes recorded in dict_of_changed """
                RATIO_AUTO=0.5
                lines = []
                from difflib import SequenceMatcher
                for tup7 in self._tuples_all:
                    linenum = tup7[6]
                    if linenum not in self._dict_of_changed:
                        continue
                    line_changed = self._dict_of_changed[linenum]
                    if line_changed is not None and len(line_changed.strip()) > 2:
                        line_original = tup7[1]
                        sm = SequenceMatcher(None,line_original,line_changed)
                        if sm.ratio() >= RATIO_AUTO:
                            lines.append(line_changed)
                        else:
                            lines = []
                            break
                    else:
                        lines = []
                        break
                return lines


            def numbered_line_from_key(self,keyname):
                """ Given a key return a 2 tuple of line number and line

                Return (-1,None) if there is an issue.

                The use case for this function is small, typically
                you have already called target_spaced() and just
                want a thin wrapper around tuples_all_from_key fetch.

                You are encouraged to fetch and check elements of the 14 tuple
                if you are working with other of those 14 data items anyway.
                """
                linenum = -1
                line = None
                tup = self.tuples_all_from_key(keyname)
                if tup is not None:
                    line = tup[1]
                    linenum = tup[6]
                return (linenum,line)


            def sedline_toggle(self,target_key):
                """ There is nothing to stop you using this function directly,
                however you are encouraged to use toggle_safe() wrapper as
                it can provide extra useful checks """
                sedline = None
                enclosed_by = None
                stored_val = self[target_key]
                tuple3 = self.false_true_index(stored_val)
                # Example contents of tuple3: (False,index,enclosed_by)
                if tuple3[0] is not None:
                    if tuple3[0] is False:
                        target_val = Keyvalfile.TRUELIST[tuple3[1]]
                        sedline = self.sedline_safe(target_key,target_val,tuple3[2])
                    elif tuple3[0] is True:
                        target_val = Keyvalfile.FALSELIST[tuple3[1]]
                        sedline = self.sedline_safe(target_key,target_val,tuple3[2])
                return sedline

            
            def sedline_toggle_wrapper(self,target_key,verbosity=0):
                """ There is nothing to stop you using this function directly,
                however you are encouraged to use toggle_safe() wrapper as
                it can provide extra useful checks """
                make_return = False
                if target_key in self:
                    tuple14 = self.tuples_all_from_key(target_key)
                    if tuple14 is None:
                        return make_return
                    sedline = self.sedline_toggle(target_key)
                    if sedline is not None and len(sedline) > 5:
                        if tuple14 is not None and len(tuple14) > 5:
                            linenum = tuple14[6]
                            make_return = self.sedqueue(linenum,sedline,verbosity)
                return make_return


            def toggle_safe_inner(self,target_key,target_value,
                                  safe=True,autoquote_level=0):
                """ Returns a 4 tuple: return_flag,new_value,old_value,change_count

                Note: target_value should be a string and you are expected to
                have already preprocessed (where appropriate) a target_value that
                was a boolean.

                return_flag values: False if target_value is supplied and it is not
                in ['0','1','false','true','False','True','off','on','no','yes']
                False if we are safe=True and the toggle action
                would have no effect.

                  Example1 Loose: toggle_safe('X11Forwarding')
                  Example2 Better: toggle_safe('PermitRootLogin','yes')
                  Example2 in words might be expressed as flick PermitRootLogin
                to 'yes' only if it is currently present and detected as 'no'
                Example3 Alternative: toggle_safe('X11Forwarding','yes',False)
                In example 3 we are specifically telling the function that X11Forwarding
                should be set to 'yes' but if it is already 'yes' then function still
                returns True (do not treat 'leave value unchanged' as an error)

                Way of invoking this function that supplies a target value but asks for 
                'leave value unchanged' to be treated as success.
                  toggle_safe(READ_ENV,'yes',False,2) (says safe=False and autoquote_level=2)
                ( Above is also most correcting in terms of quotes )
                """
                toggle_return = True
                after = None
                before = None
                change_count = 0
                if target_key not in self:
                    """ Nothing we can toggle so do no more """
                    toggle_return = False
                elif target_value is not None:
                    """ Do some prechecking before possible calling sedline_toggle_wrapper """
                    stored_value = self[target_key]
                    tuple3before = self.false_true_index(stored_value)
                    tuple3after = self.false_true_index(target_value)
                    #print tuple3before,tuple3after
                    before = stored_value
                    if tuple3after[0] is None:
                        toggle_return = False
                    else:
                        after = target_value
                        if tuple3after[0] == tuple3before[0]:
                            if safe:
                                """ When 'safe' treat 'leave value unchanged' as an error """
                                toggle_return = False
                            else:
                                toggle_return = True
                            change_count = 0
                        else:
                            enclosed_by_before = self.enclosed_by_char(stored_value)
                            enclosed_by_after = self.enclosed_by_char(target_value)
                            if enclosed_by_after is None:
                                if enclosed_by_before is None:
                                    toggle_return = self.sedline_toggle_wrapper(target_key)
                                elif autoquote_level > 0:
                                    """ autoquote_level > 0 includes automatic quoting if
                                    quoting ommitted so proceed with toggle call """
                                    toggle_return = self.sedline_toggle_wrapper(target_key)
                                else:
                                    toggle_return = False
                            elif enclosed_by_after == enclosed_by_before:
                                toggle_return = self.sedline_toggle_wrapper(target_key)
                            elif autoquote_level > 1:
                                """ autoquote_level=2 includes automatic correction of
                                wrong quotes supplied by user so proceed with toggle call """
                                toggle_return = self.sedline_toggle_wrapper(target_key)
                            else:
                                toggle_return = False
                            if toggle_return is True:
                                change_count = 1
                else:
                    """ No target_value supplied so just call sedline_toggle_wrapper() """
                    toggle_return = self.sedline_toggle_wrapper(target_key)
                tuple4 = tuple([toggle_return,after,before,change_count])
                return tuple4


            def toggle_safe(self,target_key,target_value,
                            safe=True,autoquote_level=0):
                """ Variant of toggle() that requires a minimum of 2 arguments """
                # if target_value is False or target_value is True: target_value = str(target_value)
                tuple4 = self.toggle_safe_inner(target_key=target_key,target_value=target_value,
                                                safe=safe,autoquote_level=autoquote_level)
                return tuple4[0]


            def toggle(self,target_key,target_value=None,
                       safe=False,autoquote_level=0):
                """ If you have manually checked the existing setting then
                you might call this function with just a 'target_key' argument.
                For safer usage (better catching of logic / user errors) use
                two or more arguments.
                """
                if target_value is False or target_value is True:
                    target_value = str(target_value)
                tuple4 = self.toggle_safe_inner(target_key=target_key,target_value=target_value,
                                                safe=safe,autoquote_level=autoquote_level)
                return tuple4[0]


            def toggle_new(self,target_key,target_value=None,
                           safe=False,autoquote_level=0):
                """ If you have manually checked the existing setting then
                you might call this function with just a 'target_key' argument.
                Returns new_value (after) when a change was made, otherwise returns None
                """
                if target_value is False or target_value is True:
                    target_value = str(target_value)
                tuple4 = self.toggle_safe_inner(target_key=target_key,target_value=target_value,
                                                safe=safe,autoquote_level=autoquote_level)
                print tuple4
                changed_count = tuple4[3]
                if changed_count > 0:
                    toggle_return = tuple4[1]
                else:
                    toggle_return = None
                return toggle_return


            def toggle4(self,target_key,target_value,
                        safe=False,autoquote_level=0):
                """ Variant of toggle() that requires a minimum of 2 arguments
                and returns a tuple rather than a single boolean.
                """
                if target_value is False or target_value is True:
                    target_value = str(target_value)
                tuple4 = self.toggle_safe_inner(target_key=target_key,target_value=target_value,
                                                safe=safe,autoquote_level=autoquote_level)
                return tuple4


            def context_unhash_inner(self,keyname,safe=True,hashless=False,verbosity=0):
                """ For a 'disabled' key, enable it by removing the hash (#) prefix.
                Refuse (and return False) if:
                (.) Refuse if the key is not considered 'disabled'
                (.) Refuse if 'enabling' the key would clash with an existing key value

                when_enabled() signature: original,retain_left_spacing=True,default_return='blank'
                when_enabled() we will call with 2nd arg retain_left_spacing=True
                when_enabled() can call with 3rd arg default_return='original'
                
                Set safe=False for more forgiving input and less checking.

                # chr(35) is hash / octothorpe  ; chr(32) is space  ; chr(94) is hat / caret
                """
                target = None
                constructed = ''
                linenum = -1
                keyname_record = keyname
                line = None
                default6tuple = (target,constructed,keyname,linenum,keyname_record,line)
                gen_prefix = 'context_unhash_inner:'
                #error_prefix = 'context_unhash_inner Error:'
                keyname_when_hashed = None
                keyname_when_enabled = None
                if keyname is None:
                    return default6tuple
                elif keyname.startswith(chr(35)):
                    # Expected branch if all input is perfect
                    keyname_when_hashed = keyname
                    keyname_when_enabled = self.when_enabled(keyname,True,None)
                    if verbosity > 1:
                        print "{0} keyname={1} ; keyname_when_hashed={2} ; keyname_when_enabled={3}".format(
                            gen_prefix,keyname,keyname_when_hashed,keyname_when_enabled)
                    if keyname_when_enabled is None:
                        return default6tuple
                elif hashless is True:
                    # The user told us to expect this
                    keyname_when_enabled = keyname
                    keyname_when_hashed = self.when_hashed(keyname_when_enabled,True,None)
                elif safe is False:
                    # Make a small effort to correct user input by removing any leading hash
                    keyname_when_hashed = keyname
                    keyname = keyname.lstrip(" {0}".format(chr(35)))
                    keyname_when_enabled = self.when_enabled(keyname,True,None)
                    if keyname_when_enabled is None:
                        # We expect this as we lstrip()'d to get keyname hashless already
                        if verbosity > 1:
                            print "{0} keyname was lstripped() giving keyname={1}".format(gen_prefix,keyname)
                    else:
                        # Unexpected
                        if verbosity > 1:
                            print "{0} keyname lstrip() gave keyname={1} unexpected".format(gen_prefix,keyname)
                        return default6tuple
                else:
                    if verbosity > 1:
                        print "{0} executing safe={1} for keyname={2} will not proceed".format(
                            gen_prefix,safe,keyname)
                    return default6tuple

                #print keyname_when_hashed,keyname_when_enabled
                if keyname_when_hashed is None:
                    # Unless already have it, get keyname_when_hashed
                    keyname_when_hashed = self.when_hashed(keyname,True,None)

                if verbosity > 1:
                    print "{0} keyname={1} ; keyname_when_hashed={2} ; keyname_when_enabled={3}".format(
                        gen_prefix,keyname,keyname_when_hashed,keyname_when_enabled)

                if keyname_when_hashed is None:
                    return default6tuple
                elif safe is True and keyname_when_hashed not in self.keys_disabled(safe):
                    if verbosity > 1:
                        print "{0} keyname={1} when hashed not found in disabled list!".format(gen_prefix,keyname)
                    return default6tuple
                elif safe is True and keyname_when_enabled in self.keys_enabled():
                    if verbosity > 1:
                        print "{0} keyname={1} if unhashed / enabled would clash!".format(gen_prefix,keyname)
                    return default6tuple
                elif keyname_when_hashed not in self:
                    return default6tuple
                else:
                    pass

                tup = None
                target = None
                if keyname_when_enabled is None and self.keysep_named is None:
                    # Keyedfile or some other general purpose variant
                    if verbosity > 1:
                        print("Keyedfile or some other general purpose variant and keyname_when_enabled is None.")
                    #target_hatted = self.target_spaced(keyname_when_hashed)
                    if 1==1:
                        pass
                    target = self.target_spaced(keyname_when_hashed,False,(verbosity>0))
                    linenum,line = self.numbered_line_from_key(keyname_when_hashed)
                    """
                    target_paramed = "\({0}\s*\)\({1}\)\(.*\)".format(chr(94),chr(35))
                    # In next line we keep 1 and 3 but drop 2
                    new_paramed = '\\1\\3'
                    """
                elif self.keysep_named is None:
                    # Keyedfile or some other general purpose variant
                    if verbosity > 1:
                        print("Keyedfile or some other general purpose variant.")
                    #target_hatted = self.target_spaced(keyname_when_hashed)
                    target = self.target_spaced(keyname_when_hashed,False,(verbosity>0))
                    linenum,line = self.numbered_line_from_key(keyname_when_hashed)
                    if verbosity > 1:
                        print("target={0} based on keyname_when_hashed={1}".format(target,keyname_when_hashed))
                        print("...gives linenum={0} and line={1}".format(linenum,line))
                    """
                    target_paramed = "\({0}\s*\)\({1}\)\(.*\)".format(chr(94),chr(35))
                    # In next line we keep 1 and 3 but drop 2
                    new_paramed = '\\1\\3'
                    """

                elif len(self.keysep_named.strip()) < 1:
                    # Keyedspace() or Keyedspace2() or similar
                    """ tuple14 = key, line, before_count, sep, after_count, linesep, linenum,
                    comment_flag, word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                    """
                    tup = self.tuples_all_from_key(keyname)
                    if tup is not None:
                        line = tup[1]
                        linenum = tup[6]
                        target = tup[13]

                if linenum < 0 or line is None:
                    if verbosity > 1:
                        print "{0} keyname={1} has unexpected entry for linenum or line!".format(
                            gen_prefix,keyname)
                    return default6tuple
                elif target is None:
                    """ If all is well, should have a good target assignment already """
                    if verbosity > 1:
                        print("context_unhash_inner() has target set None!")
                    return default6tuple

                # chr(35) is hash / octothorpe  ; chr(32) is space  ; chr(94) is hat / caret
                target_paramed = "{0}{1}".format(chr(94),target)
                new_paramed = None
                if chr(35) in target:
                    target_unhashed = target.replace(chr(35),'',1)
                    assert 1 == (len(target) - len(target_unhashed))
                    if target_unhashed.startswith(chr(32)) and tup is not None:
                        if tup[10] == 1:
                            new_paramed = target_unhashed[1:]
                        else:
                            new_paramed = target_unhashed
                    else:
                        new_paramed = target_unhashed
                else:
                    return default6tuple

                assert chr(35) in target
                assert chr(35) in target_paramed
                constructed = self.target_and_subsitution(target,target_paramed,new_paramed)
                if verbosity > 1:
                    print("constructed (2nd item in returned tuple) will be: {0}".format(constructed))
                #print keyname_when_hashed,target,target_paramed
                #print target,constructed,keyname,linenum,keyname_record,line
                return (target,constructed,keyname,linenum,keyname_when_enabled,line)


            def context_unhash_constructed(self,keyname,safe=True,hashless=False,verbosity=0):
                """ Return a 'constructed' sed
                to make a 'disabled' key into 'enabled' by unhashing.

                You are encouraged to call context_unhash() unless either of the following apply:
                (.) you are testing or have another valid reason for not recording the change
                (.) you wish to take responibility in your own code for calling record_changed & sedqueue

                This function DOES NOT call record_changed() or sedqueue().
                """
                constructed = ''
                # constructed = self.context_unhash_inner(keyname,True,False,verbosity)
                constructed_tuple = self.context_unhash_inner(keyname,safe,hashless,verbosity)
                # constructed_tuple is (target,constructed,keyname,linenum,keyname_record)
                if constructed_tuple[1] is not None:
                    constructed = constructed_tuple[1]
                return constructed


            def context_unhash(self,keyname,verbosity=0,safe=True,hashless=False):
                """ Make a 'disabled' key into 'enabled' by unhashing.

                This function calls record_changed() AND sedqueue() directly.
                """
                unhashed = False
                lineend_check = True # change to False to disable

                # constructed_tuple = self.context_unhash_inner(keyname,safe,hashless,verbosity)
                constructed_tuple = self.context_unhash_inner(keyname,safe,hashless,verbosity)
                # constructed_tuple is (target,constructed,keyname,linenum,keyname_record,line)
                constructed = constructed_tuple[1]
                linenum = constructed_tuple[3]
                keyname_record = [4]
                line = constructed_tuple[5]

                if verbosity > 1:
                    print("context_unhash() preview of what might be passed to sedqueue is ({0},{1})".format(
                        linenum,constructed))
                
                if len(constructed) < 3:
                    return unhashed

                gen_prefix = 'context_unhash:'
                error_prefix = 'context_unhash Error:'
                recorded_count = 0
                if keyname_record is None or (len(keyname_record) > len(keyname)):
                    raise Exception("{0} keyname_record={1} unable to record change.".format(
                            error_prefix,keyname_record))
                elif keyname_record == keyname:
                    raise Exception("{0} keyname_record={1} unable to record unchanged.".format(
                            error_prefix,keyname_record))
                else:
                    pass

                line_when_enabled = self.when_enabled(line,True,None)
                if line_when_enabled is None:
                    raise Exception("{0} keyname={1} unable to record change.".format(
                            error_prefix,keyname))

                line_when_enabled_diff_length = len(line)-len(line_when_enabled)
                if line_when_enabled_diff_length == 1:
                    pass
                else:
                    line_when_enabled = None

                if verbosity > 1:
                    print(linenum,linenum,linenum,line_when_enabled)

                if line_when_enabled is not None:
                    #print linenum,linenum,linenum,line_when_enabled
                    if lineend_check:
                        """ When lineend_check is set True, do your best to ensure any
                        line ending in the original line is in replacement also. """
                        try:
                            if line_when_enabled[-1] == line[-1]:
                                pass
                            else:
                                line_when_enabled = "{0}{1}".format(line_when_enabled,line[-1])
                        except Exception:
                            pass

                    try:
                        self._dict_of_changed[linenum]=line_when_enabled
                        recorded_count += 1
                    except Exception:
                        pass
                # record_changed_has_tuple_key_hash_remove(keyname,original,replacement,
                #                                          lineend_check,tuple14=None)

                if verbosity > 2:
                    print "recorded_count={0} from constructed={1}".format(recorded_count,constructed)

                if recorded_count > 0:
                    # Call to record_changed() has already happened and result is in recorded_count.
                    # next we queue the sed via sedqueue() which enters it into internal dict_of_seds
                    queue_return = self.sedqueue(linenum,constructed,verbosity)
                    if verbosity > 2:
                        print "{0} sedlines count is {1} after sedqueue() returned {2}".format(
                            gen_prefix,len(self.sedlines()),queue_return)
                    if queue_return is True:
                        unhashed = True
                return unhashed


            def context_update_inner(self,keyname,new_value,verbosity=0,unchanged_okay=False):
                """ verbosity of 1 will give you some feedback,
                verbosity of 2 or higher will give you more feedback.
                when_enabled() 2nd arg is retain_left_spacing=True
                when_enabled() 3rd arg is default_return
                Index 1 item in the returned tuple should be a valid sedline if all is well.
                """
                target = None
                constructed = ''
                original_value = None
                linenum = -1
                keyname_record = keyname
                keyname_when_hashed = self.when_hashed(keyname,True,'original')
                gen_prefix = 'context_update_inner:'
                error_prefix = 'context_update_inner: Error'
                # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
                """ tuple14 = key, line, before_count, sep, after_count, linesep, linenum,
                comment_flag, word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                """
                if keyname in self.keys_enabled_printable():
                    if verbosity > 1:
                        print "{0} keyname:{1} is in keys_enabled_printable()".format(gen_prefix,keyname)
                    tup = self.tuples_all_from_key(keyname)
                    if tup is None:
                        error_line = "{0} using tuple access key: {1}".format(error_prefix,
                                                                              keyname)
                        if verbosity > 1:
                            print error_line
                        raise Exception(error_line)

                    keysep = tup[3]
                    linenum = tup[6]

                    original_value = self[keyname]
                    if 'Keyedspace' == self.outerclass_name or 'Keyedspace2' == self.outerclass_name:
                        # Here for specialised class Keyedspace2 we attempt to cope with situation
                        # where value contains spaces and reconstruct original_value from the source line
                        # so that compressed (unspaced) value does not result in an UNCHANGE rejection later
                        # Run this block also for Keyedspace class as similar enough to warrant the extra treatment
                        tup_value = tup[1][(tup[10]+1):]
                        if chr(32) in tup_value:
                            original_value = tup_value
                            #print(original_value,original_value)

                    #keyname_when_enabled = self.when_enabled(keyname,True,'original')
                    if verbosity > 1:
                        print "{0} working with context:{1} from keyname:{1}".format(gen_prefix,keyname)
                    #for tup in self.tuples_all_from_keysep_strong():
                    #    print tup
                    line = tup[1]
                    #line = self.tuples_all_line_from_key(keyname_when_enabled)
                    if verbosity > 1:
                        print "{0} verbosity={1} line_when_enabled is line".format(gen_prefix,verbosity)
                        msg_main = 'last 3 items shown self.keysep_named,self.keysep,keysep'
                        print "{0} {1}  {2} {3} {4} {5}".format(gen_prefix,msg_main,
                                                                keyname,
                                                                self.keysep_named,
                                                                self.keysep,keysep)
                        assert keysep == self.keysep_named
                        """ tuple14 = key, line, before_count, sep, after_count, linesep, linenum,
                        comment_flag, word1, word1firstchar, keyseppos, keysep_after_word_index, lastword, target
                        """
                    if verbosity > 1:
                        print "{0} tuple14: {1}".format(gen_prefix,tup)
                    if new_value == original_value:
                        error_line = "{0} for keyname={1} have new_value={2} (unchanged)".format(error_prefix,
                                                                                          keyname,new_value)
                        if unchanged_okay is True:
                            # Set target to None which should prevent further processing.
                            target = None
                            if verbosity > 1:
                                print error_line.replace('Error','Warning')
                        else:
                            if verbosity > 1:
                                print error_line
                            raise Exception(error_line)
                    target = tup[13]
                    keyseppos = tup[10]
                    if verbosity > 1:
                        print target,original_value,new_value,keyseppos
                    if target is not None and keyseppos > 0:
                        """ constructed = "0,/{0}/ s/\({0}\)\(.*\)/\\1{1}/".format(target,new_value)
                        Above we use \\1 when really we mean \1 so as Python expresses it properly """
                        target_paramed = "\({0}\)\(.*\)".format(target)
                        new_paramed = "\\1{0}".format(new_value)
                        constructed = self.target_and_subsitution(target,target_paramed,new_paramed)
                    #print keyname_when_hashed,keysep,keyname_when_hashed,keysep
                elif keyname in self:
                    """ Key not printable """
                    if verbosity > 0:
                        print "{0} Unprintable key:{1}".format(gen_prefix,keyname)
                elif keyname_when_hashed in self.keys_disabled_printable(True):
                    """ key exists but in original file is prefixed with hash / octothorpe.
                    keys_disabled arg reminder: 1st arg is safe=True ; 2nd arg is hashless=False
                    """
                    if verbosity > 1:
                        print "{0} keyname_when_hashed:{1} is in keys_disabled_printable()".format(gen_prefix,
                                                                                                   keyname_when_hashed)
                    keyname_record = keyname_when_hashed
                    tup = self.tuples_all_from_key(keyname_when_hashed)
                    original_value = self[keyname_when_hashed]
                    if tup is None:
                        error_line = "{0} using tuple access key: {1}".format(error_prefix,
                                                                              keyname)
                        if verbosity > 1:
                            print error_line
                        raise Exception(error_line)

                    keysep = tup[3]
                    linenum = tup[6]

                    keyname_when_enabled = self.when_enabled(keyname,True,'original')
                    if verbosity > 1:
                        print "{0} working with context:{1} from keyname:{2}".format(gen_prefix,
                                                                                     keyname_when_hashed,
                                                                                     keyname)
                    #for tup in self.tuples_all_from_keysep_strong():
                    #    print tup
                    # tuple7: key, line, before_count, sep, after_count, linesep, linenum
                    line = tup[1]
                    #line = self.tuples_all_line_from_key(keyname_when_enabled)
                    line_when_enabled = self.when_enabled(line,True,None)
                    if line_when_enabled is None:
                        line_when_enabled = line
                        if verbosity > 1:
                            print "{0} line_when_enabled=line".format(gen_prefix)
                    elif verbosity > 1:
                            print ("{0} line_when_enabled is {1}".format(gen_prefix,line_when_enabled)).rstrip()
                    if verbosity > 1:
                        print "{0} verbosity={1} tuple8 populated next.".format(gen_prefix,verbosity)
                    tup8 = ()
                    # tuple8 returns: key, line, before, sep, after, lastchar, before_counter, after_counter
                    if self.keysep_named is None:
                        tup8 = self.tuple8(keyname_when_enabled,line_when_enabled,self.keysep,(verbosity>0))
                    else:
                        # Use keysep_named when forming tuple8
                        if verbosity > 1:
                            msg_main = 'last 3 items shown self.keysep_named,self.keysep,keysep'
                            print "{0} {1}  {2} {3} {4} {5}".format(gen_prefix,msg_main,
                                                                    keyname_when_enabled,
                                                                    self.keysep_named,
                                                                    self.keysep,keysep)
                        assert keysep == self.keysep_named
                        tup8 = self.tuple8(keyname_when_enabled,line_when_enabled,self.keysep_named,(verbosity>0))
                    if verbosity > 1:
                        print "{0} tuple8: {1}".format(gen_prefix,tup8)
                    if len(self.keysep.strip()) > 0:
                        target = self.target_spaced(keyname_when_hashed)
                    else:
                        target = keyname_when_hashed
                    original = self.originally_spaced(keyname_when_hashed,2)
                    original1 = keyname_when_enabled
                    if target.startswith(Keyvalfile.CARET_HASH):
                        original1 = target.replace(Keyvalfile.CARET_HASH,'',1)
                    else:
                        original1 = target.replace(chr(35),'',1)
                    if new_value == original_value:
                        error_line = "{0} for keyname={1} have new_value={2} (!unchanged!)".format(error_prefix,
                                                                                          keyname,new_value)
                        if unchanged_okay is True:
                            # Set target to None which should prevent further processing.
                            target = None
                            if verbosity > 1:
                                print error_line.replace('Error','Warning')
                        else:
                            if verbosity > 1:
                                print error_line
                            raise Exception(error_line)
                    constructed = None
                    if target is not None:
                        # re.sub - If the pattern is not found, string is returned unchanged
                        line_insert = re.sub(original_value, new_value, line_when_enabled, count=1)
                        if line_insert == line_when_enabled:
                            if len(target) > len(keyname):
                                # Strong possibility we are enabling and updating key at same time
                                if verbosity > 1:
                                    print "target={0} as keyname={1} was originally hashed".format(target,keyname)
                                #tup = self.tuples_all_from_key(keyname)
                                """
                                keyseppos = self.keyseppos_from_keyname_desperate(keyname,
                                                                                  line_when_enabled,verbosity)
                                print keyseppos,keyseppos,keyseppos
                                line_when_enabled = "{0}{1}".format(
                                    line_when_enabled[:keyseppos],line[(keyseppos+len(keysep)):])
                                """
                                key_unstripped = self.originally_spaced(keyname_when_hashed,verbosity,None,keysep)
                                keyseppos = self.keyseppos_from_keyname_desperate(
                                    key_unstripped,line_when_enabled,verbosity)
                                if verbosity > 2:
                                    print keyseppos,keyseppos,keyseppos
                                    print keyname,line_when_enabled,tup
                                original_value2 = line[keyseppos:]
                                #tup = self.tuple14(key_unstripped,line_when_enabled,linenum,keysep,False)
                                #tup = Keyedfile.tuple14key(key_unstripped,line_when_enabled,linenum,'=')
                                line_insert = re.sub(original_value2, new_value, line.replace(chr(35),'',1), count=1)
                                if verbosity > 1:
                                    print "Manually check: Adjusted line_when_enabled is now {0}".format(
                                        line_when_enabled)
                                if line_insert == line_when_enabled:
                                    error_line1 = "{0} re.sub return unchanged for keyname={1}".format(
                                        error_prefix,keyname)
                                    error_line2 = "original_value:{0}".format(original_value)
                                    error_line3 = "new_value:{0}".format(new_value)
                                    if verbosity > 1:
                                        print error_line1
                                        print error_line2
                                        print error_line3
                                        print "Adjusted line_when_enabled is now {0}".format(line_when_enabled)
                                        print original_value2
                                        print line_insert
                                    raise Exception(error_line1)
                                else:
                                    # Update original_value variable so late stage change is recorded in the tuple
                                    original_value = original_value2
                                #line_insert = re.sub(original_value2, new_value, line.replace(chr(35),'',1), count=1)
                                # chr(47) is forward slash (/)  ; chr(64) is at symbol (@)  ;  chr(94) is caret / hat
                                # Strong possibility we are enabling and updating key at same time
                                #'0,/.*Belt.*/ s/.*Belt.*/&\nBraces/'
                                combined_string = "{0}{1}".format(target,line_insert)
                                #constructed = "0,/{0}/ s/{0}/&\n {1}".format(target,line_insert)
                                delim = self.target_replace_delimiter_suitable(combined_string,False,chr(47))
                                constructed = "0,/{1}/ s{0}{1}.*${0}{2}{0}".format(delim,target,line_insert)
                                if constructed is not None and verbosity > 2:
                                    print(constructed)
                        # Although next is called constructed for consistency it might be better thought
                        # of as sed_one_to_two or sed_append or something more detailed.
                        if constructed is None:
                            constructed = "/{0}/i {1}".format(target,line_insert)
                            # Above is /i rather than /a deliberately as we are usually working output
                            # from /usr/bin/tac
                            # print keyname_when_hashed,keysep,keysep,keysep,keyname_when_hashed,keysep,constructed
                            # tuple5 returns (key, before_count, sep, after_count, lastchar)

                else:
                    """ Missing key or key not printable """
                    if verbosity > 0:
                        print "{0} missing or unprintable - key:{1}".format(gen_prefix,keyname)
                return (target,constructed,keyname,original_value,new_value,linenum,keyname_record)


            def context_update(self,keyname,new_value,verbosity=0,unchanged_okay=False):
                """ Perform an 'in context' update of a value in a key value config file

                This function will attempt to find an 'enabled' key and update the value.
                If there is no 'enabled' key but a disabled key, then we use the disabled key
                as a marker, and sed insert at that marker the new required line.
                
                This function calls record_changed() AND sedqueue() directly.
                """
                update_return = False
                constructed_tuple = self.context_update_inner(keyname,new_value,verbosity,unchanged_okay)
                # constructed_tuple is (target,constructed,keyname,original_value,new_value,linenum,keyname_record)
                target = constructed_tuple[0]
                constructed = constructed_tuple[1]
                if constructed is None:
                    constructed = ''
                elif len(constructed.strip()) < 2:
                    if target is None and constructed_tuple[3] == new_value:
                        """ constructed = "/^{0}/ s${1}$[unchanged]$".format(target,new_value)
                        new_value would normally be third arg to target_and_substitution but
                        special case
                        """
                        constructed = self.target_and_subsitution(target,new_value,'[unchanged]')
                    else:
                        constructed = ''
                else:
                    gen_prefix = 'context_update:'
                    error_prefix = 'context_update Error:'
                    # (target,constructed,keyname,original_value,new_value,linenum,keyname_record)
                    keyname = constructed_tuple[2]
                    original_value = constructed_tuple[3]
                    new_value = constructed_tuple[4]
                    linenum = constructed_tuple[5]
                    keyname_record = constructed_tuple[6]
                    recorded_count = 0
                    if keyname_record is None or (len(keyname_record) < len(keyname)):
                        recorded_count = 0
                    elif keyname_record == keyname:
                        """ Final parameter to record_changed() key_hash_remove=False (default) is fine for this """
                        if verbosity > 1:
                            print "Next call record_changed({0},{1},{2},True,False)".format(keyname_record,
                                                                                            original_value,
                                                                                            new_value)
                        recorded_count = self.record_changed(keyname_record,original_value,new_value,True,False)
                    else:
                        """ Final parameter to record_changed() key_hash_remove will be set TRUE as the
                        context update we are doing, in sed terms, is a locate/copy/insert type.
                        We have no way of reflecting that sort of complex operation completely in
                        a simple line overlay and so use the final parameter key_hash_remove True.
                        Using key_hash_remove True in this way we are making our recorded change
                        a replacment of existing line with the insert part of the complex sed operation
                        """
                        if verbosity > 1:
                            complex_message = 'to reflect complex sed'
                            print "Next call record_changed({0},{1},{2},True,True) {3}".format(keyname_record,
                                                                                               original_value,
                                                                                               new_value,
                                                                                               complex_message)
                        recorded_count = self.record_changed(keyname_record,original_value,new_value,True,True)

                    if verbosity > 2:
                        print "record_changed returned {0}".format(recorded_count)

                    #print keyname,recorded_count,recorded_count,recorded_count
                    if recorded_count == 1:
                        # record_changed() above should have entered the change into internal dict_of_changed
                        # next we queue the sed via sedqueue() which enters it into internal dict_of_seds
                        if verbosity > 2:
                            print "{0} <- count ... constructed -> {1}".format(recorded_count,constructed)
                        update_return = self.sedqueue(linenum,constructed,verbosity)
                    else:
                        """ If you cannot make sense of the planned change or recording it fails then
                        abandon your construction and return empty string """
                        if verbosity > 2:
                            print "{0} <- count ... constructed -> {1}".format(recorded_count,constructed)
                        constructed = ''
                        if verbosity > 2:
                            print "{0} record_changed returned {1}".format(error_prefix,recorded_count)
                        return ''
                return constructed


            def context_update_localhost(self,keyname,verbosity=0,unchanged_okay=False,new_value=None):
                """ Thin wrapper around context_update() to allow keywords such as
                ['ip4','ipv4','ip6','ipv6'] """
                if new_value == None:
                    new_value = 'localhost'
                elif new_value in ['ip4','ipv4']:
                    new_value = '127.0.0.1'
                elif new_value in ['ip6','ipv6']:
                    new_value = '::1'
                else:
                    pass
                return self.context_update(keyname,new_value,verbosity,unchanged_okay)


            def sed_checksum(self,sedlines,reverse=True,debug_filename=None,shell=None):
                """ Put the sedlines and original lines into a script and execute remotely
                to produce a readable result and checksum (md5)
                By default the original lines will be reversed so that the sed lines are
                simulating what happens if seds applied by working up from bottom of original
                file [lines]. Call function with reverse=False to not have reversed(),
                but this should not be the default.
                Returns a 3 tuple:
                (.) The lines of the script
                (.) The md5sum of the result
                (.) The lines of the result
                """
                scriptlines = []
                lines = []
                if sedlines is None or len(sedlines) < 1:
                    return (scriptlines,None,lines)
                tempfile_res = False

                scriptlines = ['#!/bin/dash\n']
                md5after_estimate = cmdbin.md5_from_iterable(self.lines_for_put())
                estimate_line = "# Forward estimate: {0} -> {1}\n".format(self.md5sum,md5after_estimate)
                scriptlines.append(estimate_line)
                if len(sedlines) > 1:
                    sed_cmdline = "{0} '{1}'".format(cmdbin.CMD_SEDE,' \\'.join(sedlines))
                else:
                    sed_cmdline = "{0} '{1}'".format(cmdbin.CMD_SEDE,sedlines[0])
                scriptlines.append(sed_cmdline)
                if reverse:
                    scriptlines.append(" << 'ORIGINALENDMARK' | {0}\n".format(cmdbin.CMD_TAC))
                    #scriptlines.append(" << 'ORIGINALENDMARK' | {0} 1>&2 \n")
                else:
                    scriptlines.append(" << 'ORIGINALENDMARK'\n")
                original_lines = self.lines_tuple
                if len(original_lines) < 1:
                    return (scriptlines,None,lines)
                if reverse:
                    scriptlines.extend(reversed(original_lines))
                else:
                    scriptlines.extend(original_lines)
                scriptlines.append('ORIGINALENDMARK')

                if debug_filename is not None:
                    if set(debug_filename).issubset(cmdbin.SET_PRINTABLE):
                        debug_filepath = "/tmp/{0}.dash".format(debug_filename)
                        with open(debug_filepath,'w') as f:
                            for line in scriptlines:
                                f.write(line)

                temp_res = False
                script_temp = tempfile.NamedTemporaryFile(delete=False)
                try:
                    for sline in scriptlines:
                        script_temp.write(sline)
                    script_temp.flush()
                    # script_temp.seek(0) # seek(0) so later read() from start
                    script_temp.close()
                except Exception as e:
                    raise
                finally:
                    temp_res = True
                if not temp_res:
                    return (scriptlines,None,lines)

                if shell is not None and shell.endswith('sh'):
                    sed_cmdline = "/bin/{0} {1}".format(shell,script_temp.name)
                else:
                    sed_cmdline = "/bin/dash {0}".format(script_temp.name)
                sed_rc,lines = cmdbin.local_popen_split_retaining(sed_cmdline)
                unlink(script_temp.name)
                #print sed_rc,len(lines)
                if sed_rc == 0:
                    pass
                else:
                    return (scriptlines,None,lines)
                mdf = cmdbin.md5_from_iterable(lines)
                if mdf is None:
                    return (scriptlines,None,lines)
                #print cmdbin.md5dict_from_iterable(lines)
                return (scriptlines,mdf,lines)


        list_of_tuples = []
        """ list_of_tuples will often be shorter in length than list_of_tuples_all
        as list_of_tuples_all will have an entry for EVERY LINE IN THE FILE
        Blank lines and other lines which are not able to be allocated a
        recognisable key will appear in list_of_tuples_all with first field of None
        """
        list_of_tuples_all = []
        linecount = -1
        lines = []
        """ When populating list_of_tuples_all we store the line unchanged
        and including any line separator. That way we have a high fidelity
        record of the original file content in a convenient iterable and
        this should allow full reconstruction of the file if desired.
        ( Why retain the line separator? We could attempt to strip based on
        os.linesep however this Python code is running locally but the file
        has been pulled from a remote system. )
        By contrast in list_of_tuples the second field will be stripped of spaces
        for convenient lookup of an individual value, but would not
        allow full reconstruction of the file.
        Use what is best depending on your purpose at the time.
        """

        keysep = None
        """ Most of the detailed parsing is handed off to 3 class methods which can
        be overridden in a subclass to suit your particular need. 3 class methods are:
        (.) process_comment_line
        (.) process_promising_line
        (.) process_line
        comment_line method returns a 2 tuple, other methods return a 4 tuple
        Third field of any 4 tuple is keysep
        Fourth field (keyd) allows for differences in stripping between
        the values that will be subsequently stored in list_of_tuples_all and list_of_tuples
        The fourth field will be the dictionary key used to populate the underlying OrderedDict
        """
        keysep_named_strong = False
        keysep_named_strong_agrees = False
        if (self.__class__).KEYSEP_NAMED is not None and len((self.__class__).KEYSEP_NAMED.strip()) > 0:
            keysep_named_strong = True
        for line in self._given_lines:
            linecount += 1
            stripped = line.strip()
            stripped_len = len(stripped)
            keysep_named_strong_agrees = False
            if stripped_len < 1:
                list_of_tuples_all.append(self.tuple14nonekey(line,linecount))
            elif stripped_len == 1 and stripped == chr(35):
                """ Comment marker and nothing else """
                list_of_tuples_all.append(self.tuple14nonekey(line,linecount))
            elif stripped_len > self.MAX_LINELENGTH:
                """ MAX_LINELENGTH class variable accessed via self.MAX_LINELENGTH """
                raise Exception('MAX_LINELENGTH limit exceeded. Limited to prevent memory exhaustion')
            else:
                """ Examine the line further. Line will almost certainly make it into list_of_tuples """
                if keysep_named_strong and keysep == (self.__class__).KEYSEP_NAMED:
                    keysep_named_strong_agrees = True
                if stripped.split()[0] == chr(35):
                    """ Comment line of form # something. comment4tuple is (key,value,resplit,comment) """
                    comment4tuple = (self.__class__).process_comment_line(stripped,keysep)
                    key = comment4tuple[0]
                    value = comment4tuple[1]
                    comment_flag = comment4tuple[3]
                    #if 'orange' in stripped:
                    #    print "orangey: {0}|{1}|{2}|{3}".format(stripped,key,value,comment_flag)
                    if keysep_named_strong_agrees:
                        """ Where we are a subclass with a KEYSEP_NAMED not None and not space,
                        we should take extra precautions to ensure that using first word
                        of comment as the key could not inadvertently replace a true
                        hashed key value line.
                        """
                        resplit_flag = comment4tuple[2]
                        if keysep in line:
                            #list_of_tuples_all.append(self.tuple14key(key,line,linecount,keysep,comment_flag))
                            list_of_tuples_all.append(self.tuple14key(key,line,linecount,keysep))
                            """ keyfull is used above, keyd is used below and is the key to go to OrderedDict """
                            list_of_tuples = (self.__class__).list_of_tuples_appended(key,value,
                                                                                      list_of_tuples)
                        else:
                            line_array = line.strip().split()
                            value = ''.join(line_array)
                            if len(value) > len(key):
                                # If key is not already just line with spaces removed then make it so.
                                key = value
                            else:
                                # Take no chances - just set key and value to be the entire line
                                key = line
                                value = line
                                """ For comment lines no need for calling tuple7, just construct manually """
                            list_of_tuples_all.append(self.tuple14keycomment(key,line,linecount))
                            list_of_tuples = (self.__class__).list_of_tuples_appended(key,value,list_of_tuples)
                    elif comment_flag is False:
                        # Probably we have subclassed and overridden process_comment_line()
                        if key == chr(35):
                            if len(value.strip()) > 1:
                                line_array = line.strip().split()
                                value = ''.join(line_array)
                                key = value
                            else:
                                # Take no chances - just set key and value to be the entire line
                                key = line
                                value = line
                            """ For comment lines no need for calling tuple7, just construct manually """
                            list_of_tuples_all.append(self.tuple14keycomment(key,line,linecount))
                        elif (self.__class__).KEYSEP_NAMED == chr(32):
                            # Form using tuple14key() rather than comment normal tuple14keycomment()
                            list_of_tuples_all.append(self.tuple14key(key,line,linecount,chr(32)))
                        list_of_tuples = (self.__class__).list_of_tuples_appended(key,value,list_of_tuples)
                    else:
                        # Probably keysep is space and usual comment handling.
                        """ For comment lines no need for calling tuple7, just construct manually """
                        list_of_tuples_all.append(self.tuple14keycomment(key,line,linecount))
                        list_of_tuples = (self.__class__).list_of_tuples_appended(key,value,list_of_tuples)
                    #list_of_tuples.append((key,value))
                elif keysep is None:
                    """ No keysep set yet, but it will be soon. """
                    if (self.__class__).KEYSEP_NAMED is None:
                        """ KEYSEP_NAMED is usually None unless we have subclassed """
                        if stripped[0].startswith(chr(35)):
                            """ This block is going to set key=stripped and value=stripped 
                            Note: Some comments where, there is no gap between hash / octothorpe
                            and the first word of the comment, might also find their way into
                            this processing block.
                            Do use a subclass of Keyedfile() if you want commented key value pairs
                            at the top of the file, to be properly treated as commented key value pairs
                            rather than receiving same treatment as any old comment 
                            """
                            list_of_tuples_all.append(self.tuple14keycomment(stripped,line,linecount))
                            list_of_tuples = (self.__class__).list_of_tuples_appended(stripped,stripped,
                                                                                      list_of_tuples)
                            #list_of_tuples.append((stripped,stripped))
                        else:
                            keyfull,value,keysep,keyd = (self.__class__).process_promising_line(stripped)
                            """ We WILL OVERRIDE the existing keysep using third field returned """
                            list_of_tuples_all.append(self.tuple14key(keyfull,line,linecount,keysep))
                            """ keyfull is used above, keyd is used below and is the key to go to OrderedDict """
                            list_of_tuples = (self.__class__).list_of_tuples_appended(keyd,value,list_of_tuples)
                            #list_of_tuples.append((keyd,value))
                    else:
                        """ Make keysep equal to KEYSEP_NAMED.
                        No need to call process_promising_line() for guesswork,
                        instead act appropriately now we know what keysep is to be.
                        """
                        keysep = (self.__class__).KEYSEP_NAMED
                        if keysep in stripped:
                            keyfull,value,keysep2,keyd = (self.__class__).process_line(stripped,keysep)
                            list_of_tuples_all.append(self.tuple14key(keyfull,line,linecount,keysep))
                            """ keyfull is used above, keyd is used below and is the key to go to OrderedDict """
                            list_of_tuples = (self.__class__).list_of_tuples_appended(keyd,value,
                                                                                      list_of_tuples)
                            #list_of_tuples.append((keyd,value))
                        else:
                            """ This block is going to set key=stripped and value=stripped 
                            Note: Some comments where, there is no gap between hash / octothorpe
                            and the first word of the comment, might also find their way into
                            this processing block. """
                            list_of_tuples_all.append(self.tuple14keyonly(stripped,line,linecount))
                            list_of_tuples = (self.__class__).list_of_tuples_appended(stripped,stripped,
                                                                                      list_of_tuples)
                            #list_of_tuples.append((stripped,stripped))
                elif keysep in stripped:
                    """ keysep is not None and it does appear in the line being processed.
                    Likely a file with keys and vals separated by =/: or space
                    """
                    keyfull,value,keysep2,keyd = (self.__class__).process_line(stripped,keysep)
                    #if 'orange' in stripped or '_safe' in stripped:
                    #    print "keysepped print: {0}|{1}|{2}|{3}".format(stripped,keyd,value,keyfull,keysep)
                    """ We will not override the existing keysep so third field goes to keysep2
                    Fourth field above (keyd) is a mechanism for having a different (perhaps stripped?)
                    key added to list_of_tuples(). First field will be added to list_of_tuples_all either
                    directly or via tuple7 (see below)
                    """
                    list_of_tuples_all.append(self.tuple14key(keyfull,line,linecount,keysep))
                    """ keyfull is used above, keyd is used below and is the key to go to OrderedDict """
                    list_of_tuples = (self.__class__).list_of_tuples_appended(keyd,value,
                                                                              list_of_tuples)
                    #list_of_tuples.append((keyd,value))
                elif (self.__class__).KEYSEP_NAMED is None:
                    """ keysep is not None AND it DOES NOT appear in the line being processed. """
                    keyfull,value,keysep2,keyd = (self.__class__).process_line(stripped,keysep)
                    #if 'orange' in stripped or '_safe' in stripped:
                    #    print "keysepped print: {0}|{1}|{2}|{3}|{4}".format(stripped,keyd,value,keyfull,keysep)
                    """ We will not override the existing keysep so third field goes to keysep2
                    Fourth field above (keyd) is a mechanism for having a different (perhaps stripped?)
                    key added to list_of_tuples(). First field will be added to list_of_tuples_all either
                    directly or via tuple7 (see below)
                    """
                    list_of_tuples_all.append(self.tuple14key(keyfull,line,linecount,None))
                    """ keyfull is used above, keyd is used below and is the key to go to OrderedDict """
                    list_of_tuples = (self.__class__).list_of_tuples_appended(keyd,value,
                                                                              list_of_tuples)
                    #list_of_tuples.append((keyd,value))
                else:
                    """ This block is going to set key=stripped and value=stripped 
                    Note: Some comments where, there is no gap between hash / octothorpe
                    and the first word of the comment, might also find their way into
                    this processing block. """
                    key = stripped
                    value = stripped
                    if keysep_named_strong:
                        """ Where we are a subclass with a KEYSEP_NAMED not None of space
                        we should take extra precautions to ensure that using first word
                        of comment as the key could not inadvertently replace a true
                        hashed key value line.
                        """
                        if keysep == (self.__class__).KEYSEP_NAMED and keysep in line:
                            pass
                        else:
                            line_array = line.strip().split()
                            value = ''.join(line_array)
                            if len(value) > len(key):
                                key = value
                            else:
                                key = line
                                value = line
                    #if stripped[0].startswith(chr(35)): print stripped,linecount
                    list_of_tuples_all.append(self.tuple14keyonly(key,line,linecount))
                    list_of_tuples = (self.__class__).list_of_tuples_appended(key,value,
                                                                              list_of_tuples)
                    #list_of_tuples.append((key,value))

            if linecount > self.MAX_LINES:
                """ MAX_LINES class variable accessed via self.MAX_LINES """
                raise Exception('MAX_LINES limit exceeded. Limited to prevent memory exhaustion')

        if self._given_linecount < 1:
            self.warning("given_linecount < 1 ... have {0} for len(list_of_tuples)".format(len(list_of_tuples)))
        if linecount < 1:
            self.warning("linecount < 1 ... have {0} for len(list_of_tuples)".format(len(list_of_tuples)))
        elif len(list_of_tuples) > self._given_linecount:
            raise Exception("given_linecount={0} but have {1} tuples in list_of_tuples".format(linecount,
                                                                                               len(list_of_tuples)))
        else:
            pass
        #assert linecount == len(list_of_tuples)
        assert linecount <= self._given_linecount
        # Above <= should be read in conjunction with note for call to linecount_and_lines_set() in next block
        if len(self.warning_list) > 0:
            self._keyvalfile = Keyvalfile(list_of_tuples,filepath=self.filepath,
                                          outerclass_name=(self.__class__).__name__,
                                          warning_list=self.warning_list)
        else:
            self._keyvalfile = Keyvalfile(list_of_tuples,filepath=self.filepath,
                                          outerclass_name=(self.__class__).__name__)
        """
        if linecount in [13,14,23]:
            for tup in list_of_tuples:
                print linecount,tup
        """
        """ for conf_k,conf_v in self._keyvalfile.items():
            if conf_k in ['SHELL','#SHELL']:
                print conf_v """
        # Use self._given_linecount as linecount calculated above may not include linefeed only lines
        self.keyvalfile.linecount_and_lines_set(self._given_linecount,self._given_lines,
                                                list_of_tuples_all,keysep,(self.__class__).KEYSEP_NAMED)
        return self.keyvalfile


    def __exit__(self, extype, exvalue, extraceback):
        """ Int this context __exit__ function we use update_flag being False
        as a 'no further processing' marker.
        """
        exit_return = False
        """ Context Manager __exit__ returning False mean propagate the error """
        update_flag = True
        if self._bufio is not None:
            try:
                self._bufio.close()
            except Exception:
                """ If we fail here then assume any update attempt
                would be inadvisable so set update_flag False
                """
                update_flag = False

        if extype is None:
            if update_flag and self.keyvalfile.change_count > 0:
                """ When using 'put' update method we need to perform update here """
                pass
            elif self.keyvalfile.change_count > 0:
                warntail = "but no changes actioned!"
                warnline = "File {0} had {1} changes pending {2}".format(self.filepath,
                                                                         self.keyvalfile.change_count,
                                                                         warntail)
                print warnline
        else:
            from exceptions import AssertionError
            if extype == AssertionError:
                """ Test suite failures typically """
                exline = "Exception at context exit: {0}".format(exvalue)
                #print exline
                if self.keyvalfile is not None:
                    print exline
                    self.except_to_false_verbose(self.keyvalfile.summary_detailed_extra(True,'Exception aid:'))
                    print exline
                else:
                    print exline
                    print 'AssertionError encountered before self.keyvalfile populated.'
                    print exline
            else:
                exline = "File {0} encountered a {1} exception at context exit!".format(self.filepath,
                                                                                        extype)
                print exline
                print exvalue
                tbline = None
                try:
                    from traceback import format_exc
                    tbline = format_exc()
                except:
                    pass
                if tbline is not None:
                    exit_return = True
                    """ We have provided all possible information about the exception,
                    so instead of propagating it, we set above True """
                    print tbline
        return exit_return


        if update_flag and self.keyvalfile.change_count > 0:
            """ For both 'put' and 'sed' update methods we consult
            dict_of_changed to check length tallies with change_count.
            Regardless of update method, dict_of_changed and dict_of_seds
            are kept in step remember. """
            if self._change_count == len(self.dict_of_seds):
                update_flag = True
            else:
                update_flag = False
        else:
            update_flag = False

        if update_flag and self.change_method == 'put':
            """ Apply changes internaly and then use 'put' """
            print self.keyvalfile.lines()
        elif update_flag and self.change_method == 'sed':
            """ Sed method of updating using a /usr/bin/tac output of original file
            involves cmdbin.TAC -> cmdbin.SEDNF -> cmdbin.TAC """
            pass
        elif update_flag == False:
            exit_return = True
        return exit_return


class Keyedlarge(Keyedfile):
    """ Extension of parent Keyedfile for specialisation.
    If you have a configuration file with very long lines then the
    extra allowance in MAX_LINELENGTH might suit your needs.

    If you know the character that separates key from value
    for sure then you might instead use subclasses Keyedcolon
    and/or Keyedequals which in addition to working with
    given knowledge of what separates key from value
    also have MAX_LINES set 4000.

    Is a file having more than 4000 lines really a configuration file?
    """

    MAX_LINELENGTH = 1000
    MAX_LINES = 4000
    """ Above class variables are defined once for the class, but in practice
    accessed via self.MAX...
    The intention is to have a hard limit across the class, to help prevent
    accidental reading of things that might lead to memory exhaustion.
    But if you override or redefine things at runtime, then that is at your risk.
    """

    def __init__(self, *args, **kwargs):
        super(Keyedlarge, self).__init__(*args, **kwargs)


class Keyedcolon(Keyedfile):
    """ Extension of parent Keyedfile for specialisation.
    chr(58) is colon (:)            ; chr(61) is equals (=)
    Use this class to force key value separation by colon (:)
    """

    MAX_LINES = 4000
    KEYSEP_NAMED = chr(58)

    def __init__(self, *args, **kwargs):
        super(Keyedcolon, self).__init__(*args, **kwargs)


class Keyedequals(Keyedfile):
    """ Extension of parent Keyedfile for specialisation.
    chr(58) is colon (:)            ; chr(61) is equals (=)
    Use this class to force key value separation by colon (:)
    """

    MAX_LINES = 4000
    KEYSEP_NAMED = chr(61)

    def __init__(self, *args, **kwargs):
        super(Keyedequals, self).__init__(*args, **kwargs)


class Keyedequals2(Keyedfile):
    """ Extension of parent Keyedfile for specialisation.
    chr(58) is colon (:)            ; chr(61) is equals (=)
    Use this class to force key value separation by equals (=)
    Two techniques are available for forcing keysep to be
    assumed. The first is for a class to set KEYSEP_NAMED
    (recommended). The second (used here) is to override
    process_promising_line() which we do below.
    """

    MAX_LINES = 4000
    #KEYSEP_NAMED = chr(61)
    
    def __init__(self, *args, **kwargs):
        super(Keyedequals2, self).__init__(*args, **kwargs)


    @classmethod
    def process_promising_line(cls,stripped):
        """ There is no keysep argument here as if keysep is known then you
        should probably instead be calling process_line().
        """
        # chr(58) is colon (:)            ; chr(61) is equals (=)           ; chr(32) is space
        keysep = chr(61)
        keyfull,value,keysep2,keyd = cls.process_line(stripped,keysep)
        return (keyfull,value,keysep,keyd)


class Keyedspace(Keyedfile):
    """ Extension of parent Keyedfile for specialisation.
    chr(58) is colon (:)            ; chr(61) is equals (=)           ; chr(32) is space
    Use this class to force key value separation by space
    """

    MAX_LINES = 4000
    KEYSEP_NAMED = chr(32)

    def __init__(self, *args, **kwargs):
        super(Keyedspace, self).__init__(*args, **kwargs)


class Keyedspace2(Keyedspace):
    """ Extension of parent Keyedspace for specialisation.
    chr(58) is colon (:)            ; chr(61) is equals (=)           ; chr(32) is space
    Use this class to force key value separation by space

    Example of file that you might consider using this subclass instead
    of Keyedspace is /etc/locale.gen

    Use this subclass where you wish to:
    (1) expend much effort ensuring the last item in tuple6 and tuple14 (target) is space preserving
    [ target always include spacing around key (left of value) ]
    (2) Make extra effort to analyse what on face of things appear to be comment lines, to treat
    the contents as disabled key value where appropriate.

    We override tuple6() to adjust target in order to achieve extra preservation (1)
    
    For (2) we override process_comment_line() and based on analysis, redirect to process_line() as required.
    """

    #MAX_LINES = 4000
    """ Inherit MAX_LINES but set KEYSEP_NAMED explicitly """
    KEYSEP_NAMED = chr(32)

    def __init__(self, *args, **kwargs):
        super(Keyedspace2, self).__init__(*args, **kwargs)


    """ Class method to instruct on how to parse a comment line.
    In this subclass it is just a thin wrapper around process_line.
    """
    @classmethod
    def process_comment_line(cls,stripped,keysep):
        """ Returns a 4 tuple with first two items being key and value
        Third item (replit) is True where we have performed a .split(keysep)
        Fourth item (comment) is True. Subclasses may use this item differently.
        """
        # chr(35) is hash / octothorpe  ; chr(32) is space
        stripped_array = stripped.split(cls.KEYSEP_NAMED)
        #stripped_array = stripped.split(KEYSEP_NAMED)
        if len(stripped_array) < 2:
            return super(Keyedspace2, cls).process_comment_line(stripped,keysep)
        resplit = False
        comment = True # This is True with good cause.
        key = stripped_array[0]
        value = None
        if stripped.startswith(cls.HASH_SPACE):
            if key == chr(35):
                # compress out the space to give a proper key that begins hash
                key = ''.join(stripped_array[:2])
                if len(stripped_array) > 3:
                    # comment=True is still appropriate. Form value.
                    value = ''.join(stripped_array[2:])
                elif len(stripped_array) > 2:
                    # set comment=False. Form appropriate value.
                    value = ''.join(stripped_array[2:])
                    comment = False
                else:
                    # comment=True is still appropriate. value is all of stripped
                    value = stripped
                if False and 'de_DE@euro' in stripped:
                    """ de_DE@euro - German locale for Germany with Euro
                    # de_DE@euro ISO-8859-15
                    """
                    print len(stripped_array),stripped,key,value
            else:
                matched = cls.RE_HASHED_BEFORE_SPACEBLOCK_AFTER.match(key)
                if matched:
                    #comment = False
                    # matched r"(?P<before>\s*#)(?P<spaceblock>\s+)(?P<after>.*)"
                    spaceblock = matched.group('spaceblock')
                    key_compressed = "{0}{1}".format(matched.group('before'),matched.group('after'))
                    if len(key_compressed) < 2:
                        #Usually the 2nd arg to process_line would be keysep, but here we force it keysep_named
                        #keyfull,value,keysep2,keyd = cls.process_line(stripped,keysep)
                        keyfull,value,keysep2,key = cls.process_line(stripped,cls.KEYSEP_NAMED)
                    elif len(key_compressed) == (len(key)-len(before)):
                        # comment_flag False has resulted in a key formed by removing a spaceblock
                        key = key_compressed
                        #Usually the 2nd arg to process_line would be keysep, but here we force it keysep_named
                        #keyfull,value,keysep2,keyd = cls.process_line(stripped,keysep)
                        keyfull,value,keysep2,keyd = cls.process_line(stripped,cls.KEYSEP_NAMED)
        if value is None:
            # value None so we still have processing to do. Use process_line() to obtain key and value
            # Usually the second argument to process_line would be keysep, but here we force it keysep_named
            #keyfull,value,keysep2,keyd = cls.process_line(stripped,keysep)
            keyfull,value,keysep2,key = cls.process_line(stripped,cls.KEYSEP_NAMED)
        #if keyfull == chr(35) or keyd == chr(35): print keyfull,keyd
        #print key,value,comment
        return (key,value,resplit,comment)


    def tuple6(self, *args, **kwargs):
        """ Subclass override of tuple6() which utilises the superclass function, but applies some
        'after super' analysis, and provides an enhanced value for target where appropriate.

        tuple6 of superclass is simple logic similar to following:
        if keyseppos > 0: target = line[:(1+keyseppos)]
        """
        # chr(35) is hash / octothorpe  ; chr(32) is space

        #tuple6(self,line,keysep,verbose=False,output_prefix='tuple6:')
        tup = super(Keyedspace2, self).tuple6(*args, **kwargs)
        #tuple6 returns (word1, word1firstchar, keyseppos, after_word_index, lastword, target)

        lastword = None
        line = None
        candidate_target_type = -1
        #if tup[2] < 1: pass
        if tup[2] > 0 and tup[3] < 1:
            """ When keyseppos > 0 and after_word_index >= 0 may well be a line
            of form similar to '# disabled_key value' and so check target to
            see if we need to adjust
            """
            candidate_target_type = 0
        elif tup[2] > 0 and tup[3] >= 1:
            """ When keyseppos > 0 and after_word_index >= 1

            Most likely lines entering here will be candidate_target_type=1
            but we also assign in very specific circumstances candidate_target_type=2

            target_type 1 we will make an attempt to extend target to include
            spacing to right of key separator.

            target_type 2 we will make target None as target has no worth
            """
            candidate_target_type = 1
            if 'line' in kwargs:
                line = kwargs['line']
            else:
                line = args[0]
            #if line.startswith("{0}{1}".format(target,lastword)):
            if line.startswith("{0}{1}".format(tup[5],tup[4])):
                #print "Luckily target already perfect set candidate_target_type to 7 target={0}".format(tup[5])
                #print len(line),line,tup
                candidate_target_type = 7
            left_of_keysep = ''
            if tup[2] > 0:
                left_of_keysep = line[:tup[2]]
                if len(left_of_keysep.strip(" {0}".format(chr(35)))) < 2 and len(tup[5]) < 3:
                    candidate_target_type = 2
        else:
            candidate_target_type = -1

        lastword = tup[4]
        cls_keysep_named = None
        if candidate_target_type in [0,1,2,3,4,5]:
            # Initialise / set useful variables for later use.
            if line is not None:
                # Avoid reassignment if we already obtained line in previous block
                pass
            elif 'line' in kwargs:
                line = kwargs['line']
            else:
                line = args[0]
            target = tup[5]

            if line is None or target is None:
                # Reset candidate_target_type to -1 as we are no longer a candidate.
                #print "line is None or target is None!!!"
                candidate_target_type = -999
            elif target in line:
                pass
            else:
                # Reset candidate_target_type to -1 as we are no longer a candidate.
                candidate_target_type = -1

            if tup[2] < 1:
                # Reset candidate_target_type to -1 as we are no longer a candidate.
                candidate_target_type = -1
            else:
                cls_keysep_named = (self.__class__).KEYSEP_NAMED
                if line[tup[2]] == cls_keysep_named:
                    # Consider adjusting the target as keyseppos (tup[2]) meets our requirement.
                    pass
                else:
                    # Reset candidate_target_type to -1 as we are no longer a candidate.
                    candidate_target_type = -1
            """
            tuple6(self,line,keysep,verbose=False,output_prefix='tuple6:')
            tuple6 returns (word1, word1firstchar, keyseppos, after_word_index, lastword, target)

            tuple6 of superclass is simple logic similar to following:
            if keyseppos > 0: target = line[:(1+keyseppos)]
            """
            keyseppos = tup[2]

        """
        if tup[2] > 0 and 'Max' in tup[5]:
            print "target={0} ; keyseppos={1} ; after_word_index={2} ; lastword={3} ; target_type={4}".format(
                tup[5],tup[2],tup[3],tup[4],candidate_target_type)
        if candidate_target_type == 1 and 'Max' in tup[5]:
            print lastword,line
        if lastword is None or line is None:
            print "target ({0}) unaltered for line type {0} ending {1}".format(tup[5],candidate_target_type,lastword)
        elif line.startswith("{0}{1}".format(tup[5],lastword)):
            print "target already complete for line type {0} ending {1}".format(
                candidate_target_type,lastword)
        """

        if candidate_target_type < 0:
            """ Any candidate_target_type < 0 will just drop through (target unchanged) """
            #print tup
            pass
        elif candidate_target_type > 5:
            """ Any candidate_target_type > 5 will just drop through (target unchanged) """
            #print tup
            pass
        elif cls_keysep_named is None:
            pass
        elif lastword is None:
            pass
        elif candidate_target_type == 0:
            """ form probably similar to '# disabled_key value' so consider adjusting target """
            # if len(target.strip(" {0}".format(chr(35)))) < 1:
            if target.strip() == chr(35):
                """ form similar to '# disabled_key value' so adjust target='#' to be
                more useful.
                """
                line_stripped = line.strip()
                stripped_array = line_stripped.split()
                #if len(stripped_array) > 2:
                if len(stripped_array) == 3 and len(stripped_array[1]) > 1:
                    re_target = r"(?P<before>\s*){0}(?P<word1>\s*{1}\s*)(.*)".format(chr(35),stripped_array[1])
                    matched = re.search(re_target,line)
                    if matched:
                        before_match = matched.group('before')
                        word1match = matched.group('word1')
                        target_including_hash = "{0}{1}{2}".format(before_match,chr(35),word1match)
                        #target_adjusted = "{0}{1}".format(before_match,word1match)
                        if line.startswith(target_including_hash) and len(target_including_hash) > len(target):
                            #print "line having lastword={0} has target adjusted {1}->{2}<-".format(
                            #    lastword,target,target_including_hash)
                            target = target_including_hash
                            # ADJUST THE TARGET to make it more useful
                            list6 = list(tup)
                            list6[5] = target
                            """ The following line is one of few places that tup will be adjusted,
                            otherwise tup just falls through from the call to super.tuple6()
                            """
                            tup = tuple(list6)
                #print stripped_array
        elif candidate_target_type == 1:
            """ Attempt to extend target to include spacing to right of key separator """
            # Consider adjusting the target as keyseppos (tup[2]) meets our requirement
            left_of_keysep = ''
            if keyseppos > 0:
                left_of_keysep = line[:keyseppos]
            remaining = None
            try:
                remaining = line[(keyseppos+1):]
            except:
                remaining = None
            if remaining is not None and remaining.startswith(cls_keysep_named):
                extra_count = len(remaining)-len(remaining.lstrip())
                if extra_count > 0:
                    # ADJUST THE TARGET
                    list6 = list(tup)
                    target_extended = line[:(1+keyseppos+extra_count)]
                    #print "line having lastword={0} has target extended {1}->{2}<-".format(
                    #    lastword,target,target_extended)
                    assert len(target_extended) > len(target)
                    list6[5] = target_extended
                    """ The following line is one of few places that tup will be adjusted,
                    otherwise tup just falls through from the call to super.tuple6()
                    """
                    tup = tuple(list6)
        elif candidate_target_type == 2:
            """ target likely a standalone hash or other short target of little worth.
            Mark target as None """
            # ADJUST THE TARGET to make it stand out as unusable
            list6 = list(tup)
            target = None   # Make target be None
            list6[5] = target
            """ The following line is one of few places that tup will be adjusted,
            otherwise tup just falls through from the call to super.tuple6()
            """
            tup = tuple(list6)
            #if '8859-1' in line: print lastword,tup[2],tup[2],candidate_target_type,tup
        else:
            pass
        return tup


def sed_diff_lines(filepath,sedlines=[],reverse=True,verbose=False):

    filepath_dir = path.dirname(filepath)
    filepath_base = path.basename(filepath)
    sed_diff_lines = []
    with cd(filepath_dir):
        sedfile = "{0}.sed".format(filepath_base)
        put_res = cmdbin.put_using_named_temp_sudo(sedfile,sedlines)
        if put_res:
            if verbose:
                print "put of .sed was successful"
        else:
            return False
        sed_diff_line = "{0} {1} < {2} | {3} {4} - ;".format(cmdbin.CMD_SEDF,sedfile,filepath_base,
                                                      cmdbin.CMD_DIFF_UNIFIED_ZERO,filepath)
        """ The preview here is not a high fidelity preview as we are not using cmdbin.CMD_TAC
        which the true applying of change action would do. However it is a reasonable preview
        and its counts can help you identify undesirable behaviour during debugging by
        highlighting difference in counts produced by applying sed to unreversed (here)
        and reversed (the actual apply action)

        Example that can lead to difference in counts due to loose sed substitution might
        be the following sed applied to /etc/default/varnish file:
        '0,/^DAEMON_OPTS=/ s/\(DAEMON_OPTS=\)\(.*\)/\1"-a :6089 \\/'
        """
        with settings(warn_only=True):
            """ Warning only now set, next hide """
            with hide('stdout','warnings'):
                sed_diff_res = cmdbin.run_or_sudo(sed_diff_line)
                if sed_diff_res.return_code == 1:
                    from os import linesep
                    sed_diff_lines = sed_diff_res.split(linesep)
    return sed_diff_lines


def sed_remote(md5current,filepath,sedlines=[],reverse=True,verbose=False):
    """ Put the .sed up to the remote machine and use /bin/sed to
    apply the requested changes to the remote file.

    The first argument is named md5current to indicate that the md5sum
    you supply (if not None) is the md5sum of the file with its CURRENT CONTENT.

    Give first argument None if you wish to disable checking of md5sum in cases
    (.) Where you do not know the md5sum
    (.) The calling script is old and the remote system configs may be for newer package versions
    md5current is an optional feature but worth the effort for important configs.

    See sed_checksum() if you want a facility to check the md5sum of the content AFTER CHANGES
    """
    remote_return = False
    filepath_dir = path.dirname(filepath)
    filepath_base = path.basename(filepath)
    sed_diff_lines = []
    with cd(filepath_dir):
        sedfile = "{0}.sed".format(filepath_base)
        sedfile_micro = sedfile
        if verbose:
            rather_than_reusing = 'rather than reusing any earlier copy from diffing'
            print "Fresh {0} will be sent to remote {1}".format(sedfile,rather_than_reusing)
        #sedfile_micro = cmdbin.filepath_micro(sedfile,False)
        sedfile_micro = cmdbin.filepath_micro(sedfile,True)
        if sedfile_micro.startswith('/etc/ssh'):
            # Temporary fix until new cmdbin function put_sed_using_named_temp_sudo() created
            rm_res = cmdbin.rm_bigi_or_sudo(sedfile_micro,filedir='/etc/ssh',verbose=True)
        put_res = cmdbin.put_using_named_temp_sudo(sedfile_micro,sedlines)
        if put_res is False:
            return False
        if verbose:
            print "{0}->remote was successful".format(sedfile_micro)
            """ To see the put request complete on the remote server you might
            Debian and derivatives: fgrep '.sed' /var/log/auth.log
            Rpm based: fgrep '.sed' /var/log/secure
            """
        from datetime import datetime as dt
        isocompact = dt.now().strftime(cmdbin.ISOCOMPACT_STRFTIME)
        assert len(isocompact) == 15
        intermediate_base = "{0}.tmp.{1}{2}".format(filepath_base,isocompact,chr(126))
        intermediate_base_reversed = "{0}.reversed.{1}{2}".format(filepath_base,isocompact,chr(126))
        sed_line = None
        if reverse is False:
            # Will not use any intermediate file but instead attempts --in-place
            md5now = cmdbin.md5text_or_sudo(filepath)
            if md5now is None:
                return False
            elif md5now == md5current:
                # supply --in-place==isocompact for sed inplace using suffix isocompact
                # CMD_SED rather than CMD_SEDF as we have an extra option prior to filename here
                sed_line = "{0} --in-place={1} -f {2} {3};".format(cmdbin.CMD_SED,isocompact,
                                                                sedfile_micro,filepath_base)
            else:
                pass
        else:
            """ Will need to do things without using --in-place option.
            Files left behind by the processes: file.tmp.isocompact~
            Set reverse=False if you prefer sed to try an 'in place' replacement
            without any intermediate files.
            """
            sed_line = "{0} {1} | {2} {3} > {4};".format(cmdbin.CMD_TAC,filepath_base,
                                                                  cmdbin.CMD_SEDF,sedfile_micro,
                                                                  intermediate_base_reversed)
        if sed_line is None:
            return False

        with settings(warn_only=True):
            with cd(filepath_dir):
                """ Warning only now set, next hide """
                with hide('output'):
                    sed_res = cmdbin.run_or_sudo(sed_line)
                    if sed_res.succeeded:
                        remote_return = True

        if remote_return is False:
            return False

        if len(set(md5current)) < 2:
            # No md5sum comparison requested via 'a'*32 or similar
            md5current = None


        if reverse is False:
            # sed --in-place was used so new content is already in place.
            pass
        else:
            """ Do the md5current check late so there is only a single
            copy command still to be done once md5sum verified """
            remote_return = False
            if md5current is not None:
                md5now = cmdbin.md5text_or_sudo(filepath)
                if md5now is None:
                    if verbose:
                        print "Late checking of md5 of current remote {0} not completed!".format(filepath)
                    return False
                elif md5now == md5current:
                    if verbose:
                        print "Late checking of md5 of current remote {0} was completed.".format(filepath)
                else:
                    return False
            tac_line = "{0} {1} > {2};".format(cmdbin.CMD_TAC,intermediate_base_reversed,
                                               intermediate_base)
            tac_res = cmdbin.run_or_sudo(tac_line)
            if tac_res.succeeded:
                cmdbin.rm_bigi_or_sudo(intermediate_base_reversed,filepath_dir)
                cmdbin.rm_bigi_or_sudo(sedfile_micro,filepath_dir)

                copy_line = "{0} {1} {2};".format(cmdbin.CMD_CP_UPDATE_NOCROSS_PRESERVETIME,
                                                      intermediate_base,filepath_base)
                copy_res = cmdbin.run_or_sudo(copy_line)
                if copy_res.succeeded:
                    remote_return = True
            elif verbose:
                print "Remote file was not overwritten. Returning False"
    return remote_return


def sed_remote_apply(md5current,filepath,sedlines=[],diffcheck='low',
                     reverse=True,maxchanged=0.75,verbose=False):
    """ diffcheck low,medium,high is primarily about the use of diff tool
    but low,medium,high is used in other decisions so might be thought of
    as an operating mode also.

    'low' Simple approach to counting difference where diffutils is available.
    Our maxchanged implementation relies on presence of diffutils. When diffutils is not available
    diffcheck='low' (unlike 'medium' and 'high') will continue and apply the sed.
    There is an md5sum check just before the final move.

    'medium' requires diffutils exist on the remote system. Medium approach to counting difference
    Will use maxchanged parameter and prevent replacement of file on remote system if the
    changes exceed maxchanged. As with 'low' there is an md5sum check.

    'high' High approach to counting difference.
    Will use maxchanged parameter and prevent replacement of file on remote system if the
    changes exceed maxchanged. As with 'low' and 'medium' there is an md5sum check.

    Calling this function with maxchanged=0 will cause a premature exit after
    diff results have been inspected. Think of it as a 'live preview' that
    can be useful for testing to supplement sed_checksum()

    The first argument is named md5current to indicate that the md5sum
    you supply (if not None) is the md5sum of the file with its CURRENT CONTENT.

    Give first argument None if you wish to disable checking of md5sum in cases
    (.) Where you do not know the md5sum
    (.) The calling script is old and the remote system configs may be for newer package versions
    md5current is an optional feature but worth the effort for important configs.

    See sed_checksum() if you want a facility to check the md5sum of the content AFTER CHANGES
    """
    apply_return = False
    if diffcheck not in ['low','medium','high']:
        """ Treat bad value given for diffcheck as 'medium' diffcheck level """
        diffcheck = 'medium'
    if sedlines is None:
        return False
    try:
        if len(sedlines) < 1:
            return False
    except TypeError:
        return False
    res_lines = []
    diffutils_flag = False
    with settings(warn_only=True):
        version_res = cmdbin.run_or_sudo(cmdbin.CMD_DIFF_VERSION,True,True)
        res_lines = cmdbin.run_res_lines_stripped(version_res)
        if len(cmdbin.run_res_lines_stripped_searching1(version_res,'diffutils')) > 0:
            diffutils_flag = True
    if diffutils_flag is False and diffcheck in ['medium','high']:
        return False
    md5sum = cmdbin.md5text_or_sudo(filepath)
    if md5sum is None:
        # md5text_or_sudo not as expected.
        return False
    elif md5current is None:
        # No md5sum comparison requested
        pass
    elif len(set(md5current)) < 2:
        # No md5sum comparison requested via 'a'*32 or similar
        pass
    elif md5current == md5sum:
        # md5current requested equals what we obtained from md5text_or_sudo()
        pass
    else:
        if verbose:
            print "Quitting before diff checking / remote apply as mismatch {0} md5".format(
                md5current)
        return False

    if diffutils_flag is False and diffcheck == 'low':
        """ When diffutils is not available diffcheck='low' (unlike 'medium' and 'high')
        will continue and apply the sed. """
        return sed_remote(md5sum,filepath,sedlines,reverse,verbose)

    diff_lines = sed_diff_lines(filepath,sedlines,reverse,verbose)
                
    single_plus_lines = []
    for line in diff_lines:
        if line.startswith(chr(43)) and not line.startswith('+++'):
            single_plus_lines.append(line)
    unified_count = len(single_plus_lines)
    if diffcheck in ['medium','high'] and unified_count == 0:
        """ Diff could not detect any changes made by the sed """
        print 'Diff could not detect any changes made by the sed!'
        return False
    plural_suffix = ''
    if unified_count > 1:
        plural_suffix = 's'
    if verbose:
        print "Sed change preview counted {0} change{1}".format(unified_count,plural_suffix)
        for line in single_plus_lines:
            print line
    if diffcheck in ['low','medium']:
        pass
    elif unified_count > len(sedlines):
        """ 'high' should never action a sed which results in count of changes > (number of sedlines)
        'low' and 'medium' leave it up to the user to test and check output produced
        verbose=True for change preview counts.

        The principle here is that there should be 1 change per sed instruction.

        In most cases we are making a single change so an acceptable check then would
        be to error on unified_count > 1, but multiple sedlines are used and each expects 1 change each
        """
        blocking_msg = 'Blocking remote change'
        raise Exception("{0} count of {1} as diffcheck set 'high'".format(blocking_msg,
                                                                          unified_count))
    # For medium we might do some extra checking using cmdbin.CMD_DIFF_SIDEBYSIDE_NOCOMMON
    # sed_remote_apply() is a prototype and more work is needed especially for 'medium' and 'high'
    if maxchanged == 0:
        """ Previewing/Testing/Debugging so go no further """
        return False
    """ At this point we only have sedlines and do not have the configuration
    object itself so cannot do noncomment_count = len(tuples_all_noncomment()) """
    if maxchanged < 1:
        unhashed_count = cmdbin.linecount_unhashed_alnum(filepath,'zero')
        if unhashed_count < 1:
            unhashed_count = cmdbin.linecount_unhashed(filepath,'zero')
    if maxchanged < 1:
        if unhashed_count < 1:
            if verbose:
                print "Bypassing: maxchanged={0} was requested but unable to get unhashed count.".format(
                    maxchanged)
            return False
        else:
            """ When forming the ratio figure to compare against
            maxchanged use a count of unhashed lines.
            """
            unhashed_count_float = float(unhashed_count)
            ratio = (unified_count / unhashed_count_float)
            if ratio > maxchanged:
                if verbose:
                    print "Aborting: maxchanged={0} was requested.".format(maxchanged)
                    print "Aborting: Detected ratio={0}".format(ratio)
                return False
            elif verbose:
                acceptable_suffix = 'which is acceptable. Sed will be applied'
                print "{0} in {1} gives ratio of {2:.4f} {3}.".format(unified_count,unhashed_count,
                                                                      ratio,acceptable_suffix)
    elif verbose:
        print "Bypassing: maxchanged={0} was requested.".format(maxchanged)
    return sed_remote(md5sum,filepath,sedlines,reverse,verbose)


def useradd_shell(preferred='/bin/sh'):
    """ Returns a three tuple of 'now',changed,'before'
    Example Return ('/bin/bash',True,'/bin/zsh')
    Above indicates that '/bin/bash' is the current value,
    it was previously '/bin/zsh' and the True indicates that we changed.
    /bin/sh is often the default on Debian,
    whereas on CentOS specifically set as /bin/bash
    """
    """ This is a dummy implementation as augeas relies on predefined templates
    so probably overkill for general (non-specific) key value style files """
    shell_now = preferred
    shell_original = preferred
    """
    from augeas import augeas
    aug = augeas()
    shell_original = aug.get('/etc/default/useradd/SHELL')
    shell_now = shell_original
    """
    return (shell_now,False,shell_original)

