#!/usr/bin/env python
# -*- coding: utf-8 -*-
from unittest import TestCase
from os import linesep
import re
from collections import Counter,defaultdict,OrderedDict
from string import ascii_lowercase,digits,printable,punctuation
from unicodedata import name as unicode_name

import cmdbin
import conf_get_set as getset

class KeyedLocal(TestCase):
    """ Key value files are not in themselves hard, but the problem of consistency
    and small differences in spacing / formatting or in the key separator used,
    make writing generic parsers a task involving consideration of many cases.

    If you want to understand why YAML was created then reading these tests and
    adding a handful of your own, should help understand the motivation for
    creating YAML and JSON and other conformant formats. YAML balances a
    lightweight parser with a few recommended guidelines on spacing / indentation
    """

    def setUp(self):
        """ Even if the application under testing does inline replaces,
        that will not affect some of these constants as they employ:
        (.) reversed() in definition
        (.) shallow copy and reverse just prior to test use. See [::-1]
        Some constants, particularly the multiline definitions do not
        employ this technique.
        """

        # chr(33) is exclamation mark (!)      ; chr(34) is double quote (")     ; chr(39) is single quote (')
        # chr(35) is hash / octothorpe             ; chr(58) is colon (:)            ; chr(61) is equals (=)

        self.SET_COLON_SPACE_EQUALS = set([chr(58),chr(32),chr(61)])
        self.SET_PRINTABLE = set(printable)
        self.SET_PUNCTUATION = set(punctuation)
        self.SET_LOWER_PLUS_DIGITS = set(ascii_lowercase).union(digits)

        self.APACHE2NAMEVIRTUALHOST = "".join(reversed('NameVirtualHost *:80{0}Listen 80{0}'.format(linesep)))
        self.APACHE2MAX2 = "".join(reversed(
                'MaxKeepAliveRequests 100{0}    MaxSpareServers      10{0}'.format(linesep)))
        """ Having defined two lines involving some sort of Max above, we now define an
        alternative form which is easier on the eye and to manually line up in your thinking
        use both or whichever suits you best in your own tests. Below will have an extra newline
        because of the triple quotes definition, but other than that the above and below forms
        should be the same
        """
        self.APACHE2MAX3 = """
MaxKeepAliveRequests 100
    MaxSpareServers      10
"""
        self.APACHE2MAX3_SPLIT = self.APACHE2MAX3.split(linesep)

        self.APACHE2MAX6 = """
 MaxSpareServers      1
  MaxSpareServers      2
   MaxSpareServers      3
    MaxSpareServers      4
 MaxSpareServers      5
  MaxSpareServers      6
"""
        self.APACHE2MAX6_SPLIT = self.APACHE2MAX6.split(linesep)

        """ Next is also lifts selected items/ideas from an Apache file
        and is based on a 2 section selective extract from a typical ports.conf """
        self.PORTS2SECTION = """
<IfModule mod_ssl.c>
    # If you add NameVirtualHost *:443 here, you will also have to change
    # the VirtualHost statement in /etc/apache2/sites-available/default-ssl
    # to <VirtualHost *:443>
    Listen 443
</IfModule>

<IfModule mod_gnutls.c>
    #Listen 443
    Listen 44443
</IfModule>
"""
        self.PORTS2SECTION_SPLIT = self.PORTS2SECTION.split(linesep)
        self.PORTS1LISTEN = "".join(reversed('Listen 80{0}'.format(linesep)))
        self.PORTS1LISTENAST = "".join(reversed('Listen *:80{0}'.format(linesep)))
        self.PORTS1LISTENLOCAL = "".join(reversed('Listen 127.0.0.1:80{0}'.format(linesep)))
        self.CRON_READ_ENV = "".join(reversed('#{0}READ_ENV="yes"{0}#{0}'.format(linesep)))
        self.ENABLED_FALSE = "".join(reversed('#{0}ENABLED="false"{0}#{0}'.format(linesep)))
        self.TCPKEEPALIVEPLUSPLUS = r"""TCPKeepAlive yes
AcceptEnv LANG LC_*
Subsystem sftp /usr/lib/openssh/sftp-server
"""
        self.TCPKEEPALIVEPLUSPLUSSPLIT = self.TCPKEEPALIVEPLUSPLUS.split(linesep)
        """ Next we simulate some lines from /etc/default/useradd with a final hacky example
        - perhaps it might have been manually edited in a hurry
        """
        self.USERADD_RPMBASED5 = "".join(reversed("GROUP=100{0}HOME=/home{0}INACTIVE=-1{0}EXPIRE={0}SHELL=/bin/bash".format(linesep)))
        self.USERADD_RPMBASED5HACKY = "".join(reversed("GROUP=100{0}HOME=/home{0}INACTIVE=-1{0}EXPIRE={0}SHELL = /bin/sh{0}#SHELL=/bin/tcsh{0}# SHELL=/bin/zsh4{0}SHELL  =/bin/bash".format(linesep)))

        self.EN_GB = """
en_GB ISO-8859-1
# en_GB.ISO-8859-15 ISO-8859-15
en_GB.UTF-8 UTF-8
"""
        self.EN_GB_SPLIT = self.EN_GB.split(linesep)

        self.EN_GB2 = """
# en_GB.ISO-8859-15 ISO-8859-15
en_GB.UTF-8 UTF-8
"""
        self.EN_GB2_SPLIT = self.EN_GB2.split(linesep)

        self.TUPLE14EURO1COMMENT = ('#de_BE@euro', '# de_BE@euro ISO-8859-15\n', 0, None, 0, '\n', 98, True,
                                    '#', '#', -1, -1, 'ISO-8859-15', None)
        self.TUPLE14EURO2COMMENT = ('#de_DE@euro', '# de_DE@euro ISO-8859-15\n', 0, None, 0, '\n', 103, True,
                                    '#', '#', -1, -1, 'ISO-8859-15', None)
        self.TUPLE14EURO1DISABLED = ('#de_BE@euro', '# de_BE@euro ISO-8859-15\n', 0, None, 0, '\n', 98, False,
                                     '#', '#', -1, -1, 'ISO-8859-15', None)
        self.TUPLE14EURO2DISABLED = ('#de_DE@euro', '# de_DE@euro ISO-8859-15\n', 0, None, 0, '\n', 103, False,
                                     '#', '#', -1, -1, 'ISO-8859-15', None)

        self.TRAFFIC3OFF = """
red:off
orange:off
green:off
"""
        self.TRAFFIC3OFF_SPLIT = self.TRAFFIC3OFF.split(linesep)
        self.TRAFFIC3GREEN = """
green:off
green:off
green:on
"""
        self.TRAFFIC3GREEN_SPLIT = self.TRAFFIC3GREEN.split(linesep)
        """ Simulating real life would see a requirement for one colour to be 'on' and the
        other two to be 'off' however we are simply treating each as an individual key value
        and do not implement any interdependency - traffic lights colours just provide
        a convenient example of a file that has 3 key values separated by (in this case) colon (:)
        """
        self.TRAFFIC3HACKY = """
#red:on
red:on
red:off
#orange:on
#orange : on
# orange:on
#orange : on
#orange   :   on
orange:on
orange :on
orange :  off
orange:on
orange :on
orange :  off
green:off
"""
        self.TRAFFIC3HACKY_SPLIT = self.TRAFFIC3HACKY.split(linesep)

        """ Next we mix in a directive style set of indents as if traffic lights
        configuration suddenly inherited the IfModule features of Apache.
        We do this to document current behaviour and to illustrate how the default
        Keyedfile class would treat such a chimera.
        """
        self.TRAFFIC3MODULES = """
red:off
orange:off
<IfModule traffic.c>
    orange:on
</IfModule>
green:on
    green:off
"""
        self.TRAFFIC3MODULES_SPLIT = self.TRAFFIC3MODULES.split(linesep)

        """ Next we try and deliberately confuse any parser guesswork by inserting
        a bad directive at the start (borrowed from smb). Early lines are important
        to any guesswork mechanism so inconsistent use of key value separators
        is discouraged. Here we are testing so create some artificial inconsistency.
        """
        self.TRAFFIC3OFF_BAD = """
cups server = mycupsserver:1631
red:off
orange:off
green:off
"""
        self.TRAFFIC3OFF_BAD_SPLIT = self.TRAFFIC3OFF_BAD.split(linesep)
        """ Next we alter the above example to give first line that
        might be marked 'space separated' by guesswork
        """
        self.TRAFFIC3OFF_BAD2 = """
mycupsserver 1631
red:off
orange:off
green:off
"""
        self.TRAFFIC3OFF_BAD2_SPLIT = self.TRAFFIC3OFF_BAD2.split(linesep)
        """ Next we alter the above example to give first line that
        might be marked 'equals separated' by guesswork
        """
        self.TRAFFIC3OFF_BAD3 = """
    port        = 3306
red:off
orange:off
green:off
"""
        self.TRAFFIC3OFF_BAD3_SPLIT = self.TRAFFIC3OFF_BAD3.split(linesep)

        self.PHP5INI_SIMILAR = """[PHP]
;;;;;;;;;;;;;;;;;;;;;;;
; Not a real php5.ini ;
;;;;;;;;;;;;;;;;;;;;;;;
; conf_get_set.py does not aim to support this sort of file
; as there are already good solutions (configparser) for working with ini files
; But we include it in our tests as it provides good edge cases / counter examples

{0}PostgresSQL{1}
; Allow or prevent persistent links.
pgsql.allow_persistent = On

{2} Detect broken persistent links always with pg_pconnect().
{2} Auto reset feature requires a little overheads.
pgsql.auto_reset_persistent = Off

; Maximum number of persistent links.  -1 means no limit.
pgsql.max_persistent = {3}1

; Comment line containing an = (equals symbol)
""".format(chr(91),chr(93),chr(59),chr(45))

        """ Above mixes in chr() definitions so that any encoding issues will
        be highlighted in any manual inspection of the output
        """

        # chr(91) is open bracket ([)      ; chr(93) is close bracket (])
        # chr(59) is semicolon (;)             ; chr(45) is minus sign  (-)

        self.PHP5INI_SIMILAR_SPLIT = self.PHP5INI_SIMILAR.split(linesep)
        self.PHP5INI_FILLER = '; filler'
        self.PHP5INI_FILLER_NEWLINED = "; filler{0}".format(linesep)
        self.PHP5INI_SIMILAR_LARGE = self.PHP5INI_SIMILAR + self.PHP5INI_FILLER_NEWLINED*2000
        self.PHP5INI_SIMILAR_LARGE_SPLIT = self.PHP5INI_SIMILAR_LARGE.split(linesep)

        """ Next example borrow some aspects of the look and feel of a my.cnf MySQL configuration
        We are particularly interested in repetition so that we can test toggling and that the
        correct target lines are changed when we update using 'put' method or 'sed' method
        Note: We have ressurected an old mysql directive skip-networking and given it a
        toggle friendly value. The representation below borrows some aspects of a mysql cnf
        file but should not be considered a model / true representation.
        """
        self.MYSQL5CNF_SIMILAR = """
# The MySQL database server configuration file.
 
[client]
#    port        = 3
    #port        = 6
    port        = 3306
   port         = 336
     port       = 33366
#socket          = /var/run/mysqld/mysqld.sock
socket          = /var/run/mysqld/mysqld2.sock
#socket          = /var/run/mysqld/mysqld.sock

[mysqld_safe]
  socket          = /var/run/mysqld/mysqld2.sock
  skip-networking   = on
   skip-networking   = off

[mysqld]
socket          = /var/run/mysqld/mysqld.sock
   port         = 336
  skip-networking   = on
"""
        self.MYSQL5CNF_SIMILAR_SPLIT = self.MYSQL5CNF_SIMILAR.split(linesep)

        self.MERGETOOLSRC = """
meld.gui=True
meld.args=--label='local' $local --label='base' $base --label='other' $other
meld.diffargs=-a --label='$plabel1' $parent --label='$clabel' $child
tkdiff.args=$local $other -a $base -o $output
tkdiff.gui=True
tkdiff.priority=-8
tkdiff.diffargs=-L '$plabel1' $parent -L '$clabel' $child
xxdiff.args=--show-merged-pane --exit-with-merge-status --title1 local --title2 base --title3 other --merged-filename $output --merge $local $base $other
xxdiff.gui=True
xxdiff.priority=-8
xxdiff.diffargs=--title1 '$plabel1' $parent --title2 '$clabel' $child
"""
        self.MERGETOOLSRC_SPLIT = self.MERGETOOLSRC.split(linesep)

        self.MERGETOOLSDIACRITIC = """
# Code reference at wikipedia.org/wiki/Acute_accent has lowercase e acute tabulated
# unicode: U+00E9
# html: &#233;
# \xc3\xa9 LATIN SMALL LETTER E WITH ACUTE
# Verify by websearching for 'diacritic' and looking up in a result table
ecmerge.args=$base $local $other --mode=merge3 --title0=base --title1=local --title2=other --to=$output
ecmerge.regkey=Software\Elli\xc3\xa9 Computing\Merge
ecmerge.regkeyalt=Software\Wow6432Node\Elli\xc3\xa9 Computing\Merge
ecmerge.gui=True
ecmerge.diffargs=$parent $child --mode=diff2 --title1='$plabel1' --title2='$clabel'
"""
        self.MERGETOOLSDIACRITIC_SPLIT = self.MERGETOOLSDIACRITIC.split(linesep)

        self.MINION4 = """
default_include: minion.d/*.conf
#master: saltnshake
# Normally the minion is not isolated to any single environment on the master
# master or minion as root, but have a non-root group be given access to
"""
        self.MINION4_SPLIT = self.MINION4.split(linesep)

        self.MINION4LEFT = """
default_include: minion.d/*.conf
#master: saltnshake
#Normally the minion is not isolated to any single environment on the master
#master or minion as root, but have a non-root group be given access to
"""
        self.MINION4LEFT_SPLIT = self.MINION4LEFT.split(linesep)

        self.OPENSSL14 = """
# This sets a mask for permitted string types. There are several options. 
# default: PrintableString, T61String, BMPString.
# pkix	 : PrintableString, BMPString (PKIX recommendation before 2004)
# utf8only: only UTF8Strings (PKIX recommendation after 2004).
# nombstr : PrintableString, T61String (no BMPStrings or UTF8Strings).
# MASK:XXXX a literal mask value.
# WARNING: ancient versions of Netscape crash on BMPStrings or UTF8Strings.
string_mask = utf8only

# req_extensions = v3_req # The extensions to add to a certificate request

[ req_distinguished_name ]
countryName			= Country Name (2 letter code)
countryName_default		= AU
countryName_min			= 2
countryName_max			= 2
"""
        # Above is an extract from openssl.cnf and has 14 non blank lines
        self.OPENSSL14_SPLIT = self.OPENSSL14.split(linesep)

        self.OPENSSL4 = """
countryName			= Country Name (2 letter code)
countryName_default		= AU
countryName_min			= 2
countryName_max			= 2
"""
        # Above is an extract from openssl.cnf and has 4 non blank lines
        self.OPENSSL4_SPLIT = self.OPENSSL4.split(linesep)
        """ Reminder: Key value files with sections have a built in module in Python
        for parsing, but here we use openssl.cnf as it has some lines which
        can be useful for testing. Below is a link to the Python 3.4 version of configparser
          http://docs.python.org/3.4/library/configparser.html (and Python 2 versions also exist)
        """

        self.VARNISH3 = """
# Configuration file for varnish
# This file contains 4 alternatives, please use only one.
## Alternative 1, Minimal configuration, no VCL
# DAEMON_OPTS="-a :6081 \\
#              -T localhost:6082 \\
# 	     -b localhost:8080 \\
# 	     -u varnish -g varnish \\
#            -S /etc/varnish/secret \\
# 	     -s file,/var/lib/varnish/$INSTANCE/varnish_storage.bin,1G"

## Alternative 2, Configuration with VCL
DAEMON_OPTS="-a :6081 \\
             -T localhost:6082 \\
             -f /etc/varnish/default.vcl \\
             -S /etc/varnish/secret \\
             -s malloc,256m"

## Alternative 3, Advanced configuration
# VARNISH_LISTEN_PORT=6081
# # Telnet admin interface listen address and port
# VARNISH_ADMIN_LISTEN_ADDRESS=127.0.0.1
# VARNISH_ADMIN_LISTEN_PORT=6082
# # DAEMON_OPTS is used by the init script.  If you add or remove options, make
# # sure you update this section, too.
# DAEMON_OPTS="-a ${VARNISH_LISTEN_ADDRESS}:${VARNISH_LISTEN_PORT} \\
#              -f ${VARNISH_VCL_CONF} \\
#              -T ${VARNISH_ADMIN_LISTEN_ADDRESS}:${VARNISH_ADMIN_LISTEN_PORT} \\
#              -t ${VARNISH_TTL} \\
#              -w ${VARNISH_MIN_THREADS},${VARNISH_MAX_THREADS},${VARNISH_THREAD_TIMEOUT} \\
# 	       -S ${VARNISH_SECRET_FILE} \\
#              -s ${VARNISH_STORAGE}"
#

## Alternative 4, Do It Yourself
# DAEMON_OPTS=""
"""
        # Above we use \\ when really we mean \ so as Python treats it properly
        """ The extract from a varnish config file which we are using includes shell continuation
        characters (\) which appear as single slash in the original file. However we are defining
        a multline Python string here so to get a faithful reproduction we escape making (\) into (\\)

        Use of .format note: As we have already, use of shell style placeholders ${...} we might best
        avoid any attempt to use .format() so {0}{0} and .format(chr(92)) will not be attempted.

        Extract from /etc/default/varnish and has DAEMON_OPTS key commented and uncommented.
        """
        self.VARNISH3_SPLIT = self.VARNISH3.split(linesep)

        self.NAGIOS341BROKER = """
cfg_file=/etc/nagios3/commands.cfg

# EVENT BROKER OPTIONS
# Controls what (if any) data gets sent to the event broker.
# Values:  0      = Broker nothing
#         -1      = Broker everything
#         <other> = See documentation

event_broker_options=-1



# EVENT BROKER MODULE(S)
# This directive is used to specify an event broker module that should
# by loaded by Nagios at startup.  Use multiple directives if you want
# to load more than one module.  Arguments that should be passed to
# the module at startup are seperated from the module path by a space.
#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# WARNING !!! WARNING !!! WARNING !!! WARNING !!! WARNING !!! WARNING
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# Do NOT overwrite modules while they are being used by Nagios or Nagios
# will crash in a fiery display of SEGFAULT glory.  This is a bug/limitation
# either in dlopen(), the kernel, and/or the filesystem.  And maybe Nagios...
#
# The correct/safe way of updating a module is by using one of these methods:
#    1. Shutdown Nagios, replace the module file, restart Nagios
#    2. Delete the original module file, move the new module file into place, restart Nagios
#
# Example:
#
#   broker_module=<modulepath> [moduleargs]

#broker_module=/somewhere/module1.o
#broker_module=/somewhere/module2.o arg1 arg2=3 debug=0

"""
        self.NAGIOS341BROKER_SPLIT = self.NAGIOS341BROKER.split(linesep)
        return


    def test_keysep(self):
        lines = self.APACHE2NAMEVIRTUALHOST[::-1].split(linesep)
        """ This is a tricky example if it were to appear as the first line of a file
        as we are simulating here. Should the program identify space as a separator of
        key value or should it identify colon (:) as separator of key value? 
        """
        with getset.Keyedfile(lines) as conf:
            self.assertEqual(conf.keysep,' ')
            tup7 = conf.tuples_all_from_key('NameVirtualHost')
            self.assertEqual('*:80',conf['NameVirtualHost'],"Not equal '*:80'. Tuple:{0}".format(tup7))
        return


    def test_word_in_value(self):
        with getset.Keyedfile(self.TCPKEEPALIVEPLUSPLUSSPLIT) as conf:
            self.assertEqual(conf.linecount,4)
            #print conf.tuples_all
            #for conf_k,conf_v in conf.items():
            #    print "{0}...{1}".format(conf_k,conf_v)
            self.assertEqual(len(conf),3)
            """ An explanation of the previous two assertEqual tests follows:
            len(self.TCPKEEPALIVEPLUSPLUSSPLIT) is 4 so conf.linecount should be 4 also
            There are in fact only 3 key value pairs and as conf is an underlying OrderedDict
            we can simply len(conf) and should expect the answer 3
            """
            self.assertTrue('AcceptEnv' in conf)
            """ Next we expect the character count for LANGLC_* (no spacing) to be 8 """
            self.assertEqual(8,len(conf['AcceptEnv']))
            keylines = conf.keylines(['AcceptEnv'])
            self.assertEqual(1,len(keylines))
            accept_env_line = keylines[0]
            accept_env_line_array = accept_env_line.split()
            self.assertEqual(3,len(accept_env_line_array))
        return


    def test_target_spaced5(self):
        lines_useradd = self.USERADD_RPMBASED5[::-1].split(linesep)
        with getset.Keyedfile(lines_useradd) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(conf.linecount,5)
            # Five lines total. Fifth line is the SHELL definition
            val = '[undefined]'
            if 'SHELL' in conf:
                val = conf.keyval('SHELL')
                tup = conf.tuples_all_from_key('SHELL')
            self.assertTrue('/bin/' in val,"value with key 'SHELL' not found or unexpected value")
            if tup is not None:
                """ before_count stored in original tuple (third field) should be 0 """
                self.assertEqual(0,tup[2])
            target_spaced_shell = conf.target_spaced('SHELL')
            self.assertEqual(target_spaced_shell,'^SHELL=')


    def test_target_spaced8(self):
        lines_useradd = self.USERADD_RPMBASED5HACKY[::-1].split(linesep)
        with getset.Keyedfile(lines_useradd) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(conf.linecount,8)
            # Eight lines total. Fifth, Sixth, Seventh, Eighth lines have some bearing on SHELL definition 
            val = '[undefined]'
            tup = None
            if 'SHELL' in conf:
                val = conf.keyval('SHELL')
                tup = conf.tuples_all_from_key('SHELL')
            self.assertTrue('/bin/' in val,"value with key 'SHELL' not found or unexpected value")
            self.assertEqual('/bin/bash',val)
            if tup is not None:
                """ before_count stored in original tuple (third field) should be 2 """
                self.assertEqual(2,tup[2])
            target_spaced_shell = conf.target_spaced('SHELL')
            #print "TARGET:{0}".format(target_spaced_shell)
            self.assertEqual(target_spaced_shell,'^SHELL  =',"Spacing issue? Line had val={0}".format(val))
        return


    def test_sed_of_target_spaced(self):
        return


    def test_traffic3(self):
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        with getset.Keyedfile(self.TRAFFIC3OFF_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(58))
            self.assertEqual(conf.linecount,5)
            conf_items = list(conf.items())
            """ conf is an OrderedDict so the order should be entirely predictable """
            self.assertEqual(conf_items,[('red', 'off'),('orange', 'off'),('green', 'off')])
            self.assertEqual(5,conf.keylen_average)
            self.assertEqual(chr(10),conf.conf_linesep)
        return


    def test_traffic3green(self):
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        with getset.Keyedfile(self.TRAFFIC3GREEN_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(58))
            self.assertEqual(conf.linecount,5)
            conf_items = list(conf.items())
            """ conf is an OrderedDict so the order should be entirely predictable """
            """ Writing the following commented line would be wrong as duplicates
            do not make it into the OrderedDict:
            self.assertEqual(conf_items,[('green', 'off'),('green', 'off'),('green', 'on')])
            """
            self.assertEqual(1,len(conf))
            self.assertEqual(2,conf.key_duplicates_count)
            self.assertEqual(5,conf.keylen_average)
            self.assertEqual(chr(10),conf.conf_linesep)
        return


    def test_traffic3hacky_nonsubclass(self):
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        #rint self.TRAFFIC3HACKY_SPLIT
        print "<<< test_traffic3hacky_nonsubclass next >>>"
        with getset.Keyedfile(self.TRAFFIC3HACKY_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(58))
            self.assertEqual(conf.linecount,17)
            self.assertEqual(4,len(conf))
            #print conf.tuples_all_from_keysep_none()
            self.assertEqual(0,len(conf.keys_disabled()))
            """ By calling with safe=False we should achieve 1 below
            which is the disabled orange.
            Because we have not used Keyedcolon() class the first line
            #red:on
            will not be treated as a key value (enabled or otherwise)
            """
            self.assertEqual(1,len(conf.keys_disabled(False)))
            self.assertEqual(set(['#orange']),set(conf.keys_disabled(False)))
            """ Calling again but this time with set_for_unique=False will
            increase the results from 1 to the 4 shown below
            """
            self.assertEqual(4,len(conf.keys_disabled(False,set_for_unique=False)))
            self.assertEqual(4,len(conf.keys_hashed()))
            self.assertEqual(set(['#orange']),set(conf.keys_hashed()))
            self.assertEqual(5,conf.keylen_average)
            #conf.summary_detailed(True)
            #print conf.lines_tuple
            #rint conf.tuples_all
            #rint(conf.keylines_disabled())
            enabled = conf.keylines_enabled()
            #print enabled
            enabled = conf.keylines_enabled_resplit()
            #print enabled
            """
            for conf_k,conf_v in conf.items():
                print "{0}DDD{1}".format(conf_k,conf_v)
            for tup in conf.tuples_all:
                # Tuple: key, line, before_count, sep, after_count, linesep, linenum
                print "{0}TTT".format(tup[0])
            """
            """ For the next line we would have 6 if we counted the enabled keys only, but
            when counting duplicates we included keys which begin '#' and count those
            duplicates also. Hence the 10 total
            """
            self.assertEqual(10,conf.key_duplicates_count)
            self.assertEqual(chr(10),conf.conf_linesep)
            orange_commented_count = len(conf.keylines_typed(0,['orange']))
            self.assertEqual(5,orange_commented_count)
            orange_or_commented_orange_count = len(conf.keylines_typed(2,['orange']))
            self.assertEqual(11,orange_or_commented_orange_count)
            """ By loading up on duplicate 'orange' we have managed to boost
            the keylen_average figure from 5 to 6 with calculation shown below:
            To NEAREST INTEGER 6=(4+3+3+3+7+7+7+7+7+6+6+6+6+6+6+5)/16
            We test 6 equality keylen_average_full next """
            self.assertEqual(6,conf.keylen_average_full,"Disagreement: keylen_average_full")
            """ When calculating keylen_average we will have a much shorter list
            of items to sum as we are just interested in keys in the dictionary
            and so duplicates will have no part in the calculation.
            To NEAREST INTEGER 5=(4+3+7+6)/4
            We test 5 equality keylen_average next """
            self.assertEqual(5,conf.keylen_average,"Disagreement: keylen_average")
        return


    def test_traffic3hacky(self):
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        #rint self.TRAFFIC3HACKY_SPLIT
        print "<<< test_traffic3hacky next >>>"
        with getset.Keyedcolon(self.TRAFFIC3HACKY_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(58))
            self.assertEqual(conf.linecount,17)
            self.assertEqual(5,len(conf))
            self.assertEqual(5,conf.keylen_average)
            self.assertEqual(0,len(conf.keys_disabled()))
            """ By calling with safe=False we should achieve 1 + 1 below.
            The 1 disabled key for orange plus the early red gives 2 total.
            By using Keyedcolon() rather than generic parent class we have ensured early red counts
            """
            self.assertEqual(2,len(conf.keys_disabled(False)))
            self.assertEqual(set(['#red','#orange']),set(conf.keys_disabled(False)))
            """ Calling again but this time with set_for_unique=False will
            increase the results from 2 to the 6 shown below
            """
            self.assertEqual(6,len(conf.keys_disabled(False,set_for_unique=False)))
            self.assertEqual(6,len(conf.keys_hashed()))
            self.assertEqual(set(['#red','#orange']),set(conf.keys_hashed()))
            #conf.summary_detailed(True)
            #print conf.lines_tuple
            #print conf.tuples_all
            #print(conf.keylines_disabled())
            enabled = conf.keylines_enabled()
            #print enabled
            enabled = conf.keylines_enabled_resplit()
            #print enabled
            """
            for conf_k,conf_v in conf.items():
                print "{0}DDD{1}".format(conf_k,conf_v)
            for tup in conf.tuples_all:
                # Tuple: key, line, before_count, sep, after_count, linesep, linenum
                print "{0}TTT".format(tup[0])
            """
            """ For the next line we would have 6 if we counted the enabled keys only, but
            when counting duplicates we included keys which begin '#' and count those
            duplicates also. Hence the 10 total
            """
            self.assertEqual(10,conf.key_duplicates_count)
            self.assertEqual(chr(10),conf.conf_linesep)
            orange_commented_count = len(conf.keylines_typed(0,['orange']))
            self.assertEqual(5,orange_commented_count)
            orange_or_commented_orange_count = len(conf.keylines_typed(2,['orange']))
            self.assertEqual(11,orange_or_commented_orange_count)
            """ By loading up on duplicate 'orange' we have managed to boost
            the keylen_average figure from 5 to 6 with calculation shown below:
            To NEAREST INTEGER 6=(4+3+3+3+7+7+7+7+7+6+6+6+6+6+6+5)/16
            We test 6 equality keylen_average_full next """
            self.assertEqual(6,conf.keylen_average_full,"Disagreement: keylen_average_full")
            """ When calculating keylen_average we will have a much shorter list
            of items to sum as we are just interested in keys in the dictionary
            and so duplicates will have no part in the calculation.
            To NEAREST INTEGER 5=(4+3+7+6)/4
            We test 5 equality keylen_average next """
            self.assertEqual(5,conf.keylen_average,"Disagreement: keylen_average")
        return


    def test_traffic3modules(self):
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        #print self.TRAFFIC3HACKY_SPLIT
        print "<<< traffic3modules detailed next >>>"
        with getset.Keyedfile(self.TRAFFIC3MODULES_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(58))
            self.assertEqual(conf.linecount,9)
            """
            for conf_k,conf_v in conf.items():
                print "{0}DDD{1}".format(conf_k,conf_v)
            """
            orange_or_commented_orange_count = len(conf.keylines_typed(2,['orange']))
            self.assertEqual(2,orange_or_commented_orange_count)
            self.assertEqual(5,len(conf))
        return


    def test_traffic3inconsistent(self):
        """ Inconsistent as first line looks like following:
        cups server = mycupsserver:1631
        ...but we are a really a colon separated (:) file
        Here keysep is guessed correctly as colon (:)
        """
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        print "<<< traffic3inconsistent detailed next >>>"
        with getset.Keyedfile(self.TRAFFIC3OFF_BAD_SPLIT) as conf:
            self.assertNotEqual(chr(32),conf.keysep)
            # Test above that keysep is not space, below that keysep is correct (:)
            self.assertEqual(chr(58),conf.keysep)
            self.assertEqual(conf.linecount,6)
            values_list = [ v for v in conf.itervalues() ]
            values_set = set(values_list)
            self.assertEqual(2,len(values_set))
            self.assertEqual(3,len(conf.keylines_typed(1,['red','orange','green'])))
            self.assertTrue(conf.toggle_safe('green','on'))
        return


    def test_traffic3inconsistent2(self):
        """ Inconsistent as first line looks like following:
        mycupsserver 1631
        ...but we are a really a colon separated (:) file
        Here keysep is guessed to be space ( )
        """
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        print "<<< traffic3inconsistent2 detailed next >>>"
        with getset.Keyedfile(self.TRAFFIC3OFF_BAD2_SPLIT) as conf:
            self.assertNotEqual(chr(58),conf.keysep)
            # Test above that keysep is not space, below that keysep is correct (:)
            self.assertEqual(chr(32),conf.keysep)
            self.assertEqual(conf.linecount,6)
            self.assertEqual(4,len(conf))
            """
            for conf_k,conf_v in conf.items():
                print "{0}DDD{1}".format(conf_k,conf_v)
            """
            values_list = [ v for v in conf.itervalues() ]
            values_set_nonblanks = set([ v for v in values_list if len(v) > 0])
            self.assertEqual(1,len(values_set_nonblanks))
            """ Should just be single non blank value of 1631 """
            self.assertEqual(0,len(conf.keylines_typed(1,['red','orange','green'])))
            self.assertFalse(conf.toggle_safe('green','on'))
        return


    def test_traffic3inconsistent3(self):
        """ Inconsistent as first line looks like following:
        port        = 3306
        ...but we are a really a colon separated (:) file
        Here keysep is guessed to be equals (=)
        """
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        print "<<< traffic3inconsistent3 detailed next >>>"
        with getset.Keyedfile(self.TRAFFIC3OFF_BAD3_SPLIT) as conf:
            self.assertNotEqual(chr(58),conf.keysep)
            # Test above that keysep is not space, below that keysep is correct (:)
            self.assertEqual(chr(61),conf.keysep)
            self.assertEqual(conf.linecount,6)
            """
            for conf_k,conf_v in conf.items():
                print "{0}DDD{1}".format(conf_k,conf_v)
            """
            values_list = [ v for v in conf.itervalues() ]
            values_nonblanks = [ v for v in values_list if len(v) > 0]
            self.assertEqual(1,len(values_nonblanks))
            """ Should just be single non blank value of 3306 """
            self.assertEqual('3306',values_nonblanks[0])
            # Final two parameters of next command says safe=True and autoquote_level=1
            conf.toggle_safe('green','on',True,1)
        return


    def test_toggle_twice_disallowed(self):
        lines_useradd = self.CRON_READ_ENV[::-1].split(linesep)
        with getset.Keyedfile(lines_useradd) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(conf.linecount,4)
            self.assertFalse(conf.toggle_safe('ENABLED','true',True))
            """ Above should fail as we do not have an ENABLED key """
            self.assertFalse(conf.toggle_safe('READ_ENV',"'no'",True))
            """ Above should fail because when quotes are provided in
            the new value, and third arg is True, there is a safety
            check to ensure we are not adding quotes that were not there
            around the existing value. If this was successful then
            our program just allowed us to switch the quotes from double (")
            around the original value, to single (') around our new value.
            """
            self.assertFalse(conf.toggle_safe('READ_ENV','no',True))
            self.assertTrue(conf.toggle_safe('READ_ENV','no',True,1))
            """ Trying the same thing twice should fail """
            self.assertFalse(conf.toggle_safe('READ_ENV','no',True,1))
        return


    def test_lines_for_put_simple(self):
        lines_useradd = self.CRON_READ_ENV[::-1].split(linesep)
        with getset.Keyedfile(lines_useradd) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(conf.linecount,4)
            self.assertTrue(conf.toggle_safe('READ_ENV','no',True,1))
            put_lines = conf.lines_for_put()
            read_env_count = 0
            for line in put_lines:
                if 'READ_ENV' in line:
                    read_env_count += 1
            self.assertEqual(1,read_env_count)
            self.assertEqual(4,len(put_lines))
        return


    def test_linecount_max(self):
        """ conf_get_set does not really support ini style files with sections,
        there are already better solutions (configparser) for handling them.
        However for testing and examples the large file length and other features
        of php.ini can provide useful input
        """
        # chr(33) is exclamation mark (!)      ; chr(34) is double quote (")     ; chr(39) is single quote (')
        # chr(35) is hash / octothorpe             ; chr(58) is colon (:)            ; chr(61) is equals (=)
        # chr(91) is open bracket ([)      ; chr(93) is close bracket (])
        # chr(59) is semicolon (;)             ; chr(45) is minus sign  (-)

        #print self.PHP5INI_SIMILAR

        similar_len = len(self.PHP5INI_SIMILAR_SPLIT)
        print "Attempt to open a conf (Keyedfile) having {0} lines next".format(similar_len)
        space_equals_space_count = 0
        with getset.Keyedfile(self.PHP5INI_SIMILAR_SPLIT) as conf:
            self.assertTrue(set(conf.keysep).issubset(self.SET_COLON_SPACE_EQUALS))
            for tup in conf.tuples_all:
                # Tuple: key, line, before_count, sep, after_count, linesep, linenum
                if tup[2] == 1 and tup[3] == chr(61) and tup[4] == 1:
                    space_equals_space_count += 1
                #print tup
        """ There are 3 lines of the form key = value in PHP5INI_SIMILAR
        and they are consistently spaced. But we also deliberately added
        an equals into a comment line. We do not recognise semicolon as
        meaning a comment line and so split that line as if it were a key value
        and so that makes 4 """
        self.assertEqual(4,space_equals_space_count)
                    
        large_len = len(self.PHP5INI_SIMILAR_LARGE_SPLIT)
        print "Attempt to open a conf (Keyedlarge) having {0} lines next".format(large_len)
        with getset.Keyedlarge(self.PHP5INI_SIMILAR_LARGE_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(conf.linecount,2021)

        print "Attempt to open a conf (Keyedfile) having {0} lines next".format(large_len)
        """ All working as expected we should see an Exception thrown by Keyedfile, which
        is caught and examined to ensure it contains 'MAX_LINES limit exceeded'.
        If instead we encounter an AssertionError or an Exception that does not contain
        our expected string, then we should raise or report something
        """
        from exceptions import AssertionError
        try:
            with getset.Keyedfile(self.PHP5INI_SIMILAR_LARGE_SPLIT) as conf:
                """ We should not reach this far as Exception should have been thrown """
                self.assertEqual(conf.keysep,chr(61))
        except AssertionError:
            raise
        except Exception,ex:
            """ Expect Exception to have been thrown
            and assert that it is correct Exception """
            self.assertTrue('MAX_LINES limit exceeded' in str(ex))
        return
    

    def test_repeated_and_single_keys(self):
        """ Here we test input which has repeated keys. Normally this would happen if
        conf_get_set was being used against a file which was the subject of many
        manual alterations, or a file which has a nested structure.
        conf_get_set is not designed to deal with sections or nesting, but there is no harm
        in defining expected behaviour tests and it may provide a useful illustration / example
        """
        # chr(58) is colon (:)            ; chr(61) is equals (=)           ; chr(32) is space
        """ Now what happens for our sample ports.conf is that the first 6 lines are treated
        well, and most things after those 6 lines produce a duplicate key. This 7 item list may help
          ['<IfModule', '#If', '#the', '#to', 'Listen', '</IfModule>', '#Listen']
        So whilst it is perfectly normal to have repeated directives in apache, we are treating
        the ports.conf file as a flat key value structure. In our case keys which are encountered
        again are treated as a redefinition. Do read the traffic3hacky tests also.
        """
        with getset.Keyedfile(self.PORTS2SECTION_SPLIT) as conf:
            #print conf.tuples_all
            self.assertEqual(conf.keysep,chr(32))
            """ Above checks that by the end of file the separator between key and value
            is thought to be space ' ' """
            self.assertEqual(conf.linecount,13)
            """ So we have 13 lines of which 7 are unique keys """
            self.assertEqual(7,len(conf))
            self.assertEqual(3,conf.key_duplicates_count)
            """ If you consider enabled as 'not beginning with #' then you will expect 6 next """
            self.assertEqual(6,len(conf.tuples_enabled()))
            """ During initial analysis using generic Keyedfile() class, even after keysep
            has been identified, lines which do not include that keysep will have None in
            the tuple field for keysep. There are two such lines in this test case - 
            the lines beginning </IfModule> do not contain a space and our keysep has been
            guessed at as space.
            """
            self.assertEqual(2,len(conf.tuples_without_keysep()))
            self.assertEqual(4,len(conf.tuples_with_keysep()))
            self.assertEqual(4,len(conf.tuples_with_keysep(chr(32))))
            """ Listen was orinally set to 443 but in the second <IfModule block
            it is redefined as 44443. """
            self.assertEqual('44443',conf['Listen'])
        return


    def test_key_duplicates_plusone_part1(self):
        """ Part 1 of plusone_part1 ; plusone_part2 ; plusone_part3 ; plusone_part4
        In part1 we are concerned mainly with counts of duplicates and analysis
        around strong keysep lines when using generic Keyedfile()
        """
        # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
        print "<<< test_key_duplicates_plusone_part1 >>>"
        with getset.Keyedfile(self.MYSQL5CNF_SIMILAR_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(23,conf.linecount)
            self.assertEqual(8,conf.key_duplicates_count)
            #conf.summary_detailed(True)
            tuples_keysep_strong = conf.tuples_all_from_keysep_strong()
            tuples_keysep_strong_target = 12
            tuples_keysep_strong_len = len(tuples_keysep_strong)
            if tuples_keysep_strong_len != tuples_keysep_strong_target:
                for tup in tuples_keysep_strong:
                    print tup
            self.assertEqual(tuples_keysep_strong_target,tuples_keysep_strong_len,
                             "Unexpected len(tuples_keysep_strong) of {0}".format(tuples_keysep_strong_len))
        return


    def test_key_duplicates_plusone_part2(self):
        """ Part 4 of plusone_part1 ; plusone_part2 ; plusone_part3 ; plusone_part4
        To understand key_dups_diff_set below need to focus on these two input lines:
        [client]
        #    port        = 3
        What happens in the generic Keyedfile() is that because (at that point) we do not
        know what keysep is, there is no dictionary key entry added for '#port'
        but instead the line is treated as unimportant / comment
        Whereas when using subclass Keyedequals() we have been told (in advance) that equals
        is the key value separator, and so we treat this line as a commented out / hashed 
        key value line
        """
        # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
        print "<<< test_key_duplicates_plusone_part2 >>>"
        with getset.Keyedequals(self.MYSQL5CNF_SIMILAR_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(23,conf.linecount)
            self.assertEqual(9,conf.key_duplicates_count)
            tuples_keysep_strong = conf.tuples_all_from_keysep_strong()
            target_clashes = conf.keysep_strong_target_clashes()
            keysep_strong_clash_count = len(target_clashes)
            keysep_strong_clash_count_expected = 4
            if keysep_strong_clash_count != keysep_strong_clash_count_expected:
                for tup in tuples_keysep_strong:
                    print ("keysep strong {0} entry is: {1}".format(tup[0],tup[1])).rstrip()
            keysep_strong_keys = [ tup[0] for tup in tuples_keysep_strong ]
            self.assertEqual(keysep_strong_clash_count_expected,keysep_strong_clash_count)
        return


    def test_key_duplicates_plusone_part3(self):
        """ Part 3 of plusone_part1 ; plusone_part2 ; plusone_part3 ; plusone_part4
        To understand key_dups_diff_set below need to focus on these two input lines:
        [client]
        #    port        = 3
        What happens in the generic Keyedfile() is that because (at that point) we do not
        know what keysep is, there is no dictionary key entry added for '#port'
        but instead the line is treated as unimportant / comment
        Whereas when using subclass Keyedequals() we have been told (in advance) that equals
        is the key value separator, and so we treat this line as a commented out / hashed 
        key value line
        """
        # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
        print "<<< test_key_duplicates_plusone_part3 >>>"
        with getset.Keyedfile(self.MYSQL5CNF_SIMILAR_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(23,conf.linecount)
            #conf.summary_detailed(True)
            tuples_keysep_strong = conf.tuples_all_from_keysep_strong()
            keysep_strong_keys12 = [ tup[0] for tup in tuples_keysep_strong ]
            self.assertNotIn('#port',keysep_strong_keys12)
            self.assertEqual(0,len(conf.keys_disabled()))
            """ By calling with safe=False and set_for_unique=True we should achieve 1 below.
            Function signature: keys_disabled(self,safe=True,hashless=False,set_for_unique=True)
            """
            self.assertEqual(1,len(conf.keys_disabled(False)))
            keys_hashed = conf.keys_hashed()
            hashed_set = set(keys_hashed)
            self.assertEqual(2,len(keys_hashed))
            self.assertEqual(set(['#socket']),hashed_set)
        return


    def test_key_duplicates_plusone_part4(self):
        """ Part 4 of plusone_part1 ; plusone_part2 ; plusone_part3 ; plusone_part4
        To understand key_dups_diff_set below need to focus on these two input lines:
        [client]
        #    port        = 3
        What happens in the generic Keyedfile() is that because (at that point) we do not
        know what keysep is, there is no dictionary key entry added for '#port'
        but instead the line is treated as unimportant / comment
        Whereas when using subclass Keyedequals() we have been told (in advance) that equals
        is the key value separator, and so we treat this line as a commented out / hashed 
        key value line
        """
        # chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ; chr(61) is equals (=)
        print "<<< test_key_duplicates_plusone_part4 (preliminary) >>>"
        with getset.Keyedfile(self.MYSQL5CNF_SIMILAR_SPLIT) as conf:
            key_dups8 = conf.key_duplicates()
        """ Above we have used Keyedfile() just to obtain the proper value for key_dups8
        Next we use the proper specialist subclass
        """
        print "<<< test_key_duplicates_plusone_part4 >>>"
        with getset.Keyedequals(self.MYSQL5CNF_SIMILAR_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(23,conf.linecount)
            key_dups_diff_set = set(conf.key_duplicates()).difference(key_dups8)
            self.assertEqual(set(['#port']),key_dups_diff_set)
            self.assertEqual(9,conf.key_duplicates_count)
            #conf.summary_detailed(True)
            tuples_keysep_strong = conf.tuples_all_from_keysep_strong()
            tuples_keysep_strong_target = 14
            tuples_keysep_strong_len = len(tuples_keysep_strong)
            if tuples_keysep_strong_len != tuples_keysep_strong_target:
                for tup in tuples_keysep_strong:
                    print tup
            self.assertEqual(tuples_keysep_strong_target,tuples_keysep_strong_len,
                             "Unexpected len(tuples_keysep_strong) of {0}".format(tuples_keysep_strong_len))
            keysep_strong_keys14 = [ tup[0] for tup in tuples_keysep_strong ]
            self.assertIn('#port',keysep_strong_keys14)
            #for tup in tuples_keysep_strong:
            #    print tup[0]
            self.assertEqual(0,len(conf.keys_disabled()))
            keys_disabled2target = 2
            """ By calling with safe=False we should achieve 2 for next assertEqual(2,...). """
            keys_disabled2 = conf.keys_disabled(False)
            keys_disabled2length = len(keys_disabled2)
            if keys_disabled2length != keys_disabled2target:
                for key in keys_disabled2:
                    print keys_disabled2length,key,"value: {0}".format(conf[key])
            self.assertEqual(keys_disabled2length,keys_disabled2target)
            """ Now it is possible to obtain 4 keys from conf.keys_disabled by
            setting third parameter set_for_unique=False
            Function signature: keys_disabled(self,safe=True,hashless=False,set_for_unique=True)
            """
            self.assertEqual(4,len(conf.keys_disabled(False,set_for_unique=False)))
            keys_hashed = conf.keys_hashed()
            hashed_set = set(keys_hashed)
            """ len(keys_hashed) >= len(conf.keys_disabled(...,set_for_unique=False) and
            in fact they are both 4 in this test case
            """
            self.assertEqual(4,len(keys_hashed))
            self.assertIn('#port',hashed_set)
        return


    def test_early_comments(self):
        print "<<< test_early_comments next >>>"
        from hashlib import md5
        mdf = None
        with getset.Keyedfile(self.OPENSSL14_SPLIT) as conf:
            self.assertEqual(18,conf.linecount)
            self.assertEqual(0,conf.key_duplicates_count)
            mdf = md5()
            for line in conf.lines_tuple:
                mdf.update(line)
            self.assertIn('countryName_default',conf)
        self.assertNotEqual(None,mdf)
        self.assertEqual('18e5e644b2f51d4dabb160dbafa1d527',mdf.hexdigest())
        return


    def test_toggle_success(self):
        from hashlib import md5
        mdf = None
        print "<<< test_toggle_success next >>>"
        with getset.Keyedfile(self.MYSQL5CNF_SIMILAR_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(23,conf.linecount)
            self.assertEqual(8,conf.key_duplicates_count)
            mdf = md5()
            for line in conf.lines_tuple:
                #print line
                mdf.update(line)
            self.assertIn('skip-networking',conf)
            self.assertTrue(conf.toggle_safe('skip-networking','off'))
            self.assertEqual(1,len(conf.dict_of_seds))
            #for sed in conf.list_of_sedlines: print sed
            self.assertIn(21,conf.dict_of_seds)
            # Above checks that line 21 was identified by our toggle_safe call as the target 
        self.assertNotEqual(None,mdf)
        self.assertEqual('5d2f0aae1c84592bd1289e0fec4daa6b',mdf.hexdigest())
        return


    def test_toggle_success2(self):
        print "<<< test_toggle_success2 next >>>"
        with getset.Keyedfile(self.MERGETOOLSRC_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(13,conf.linecount)
            self.assertEqual(0,conf.key_duplicates_count)
            self.assertEqual(11,len(conf))
            """ If you consider enabled as 'not beginning with #' then you will expect 6 next """
            self.assertEqual(11,len(conf.tuples_enabled()))
            self.assertIn('meld.gui',conf)
            self.assertTrue(conf.toggle_safe('meld.gui','False'))
            self.assertTrue(conf.toggle_safe('tkdiff.gui','false'))
        return


    def test_toggle_fail1(self):
        print "<<< test_toggle_fail1 next >>>"
        with getset.Keyedfile(self.MYSQL5CNF_SIMILAR_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(23,conf.linecount)
            self.assertEqual(8,conf.key_duplicates_count)
            self.assertGreaterEqual(2,len(conf['skip-networking']),"Expected on or off for skip-networking")
            """ Next lines_enabled_contains() consults the original line
            as stored in the 7 tuple at creation time """
            self.assertEqual(3,len(conf.lines_enabled_contains('skip-networking')))
            self.assertEqual(['  skip-networking   = on',
                              '   skip-networking   = off',
                              '  skip-networking   = on'],
                             conf.lines_enabled_contains('skip-networking'))
            # Above test assertEqual will automatically use type specific assertListEqual
            """ Next we expect failure as toggle_safe() is supposed to 
            fail when our toggle would result in
            no change in on/off state """
            self.assertFalse(conf.toggle_safe('skip-networking','on'))
        return


    def test_toggle_and_put1(self):
        """ In this test we go as far as checking that lines_for_put() returns
        the expected representation of the file lines after successful toggle
        """
        print "<<< test_toggle_and_put1 next >>>"
        with getset.Keyedfile(self.MYSQL5CNF_SIMILAR_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(23,conf.linecount)
            self.assertTrue(conf.toggle_safe('skip-networking','off'))
            md5before = '5d2f0aae1c84592bd1289e0fec4daa6b'
            self.assertEqual(md5before,cmdbin.md5_from_iterable(conf.lines_tuple))
            md5after = '9c7d92f15bdceb21640fe98007e7bae9'
            self.assertEqual(md5after,cmdbin.md5_from_iterable(conf.lines_for_put()))
        return


    def test_toggle_and_put3(self):
        """ In this test we go as far as checking that lines_for_put() returns
        the expected representation of the file lines after successful toggle
        """
        print "<<< test_toggle_and_put3 next >>>"
        with getset.Keyedfile(self.MERGETOOLSRC_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(13,conf.linecount)
            self.assertEqual(0,conf.key_duplicates_count)
            self.assertEqual(11,len(conf))
            self.assertIn('meld.gui',conf)
            self.assertTrue(conf.toggle_safe('meld.gui','False'))
            self.assertTrue(conf.toggle_safe('tkdiff.gui','false'))
            self.assertTrue(conf.toggle_safe('xxdiff.gui','false'))
            md5before = 'f1f7a423dd0a80caf39daaa19f69072d'
            self.assertEqual(md5before,cmdbin.md5_from_iterable(conf.lines_tuple))
            md5after = '1835f2ddfcbbcf47b2e526dd789c526a'
            self.assertEqual(md5after,cmdbin.md5_from_iterable(conf.lines_for_put()))
        return

   
    def test_sedline1(self):
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        # chr(34) is double quote (")  ;
        print "<<< test_sedline1 next >>>"
        lines = self.ENABLED_FALSE[::-1].split(linesep)
        with getset.Keyedfile(lines) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(conf.linecount,4)
            sedline_make_true = '0,/^ENABLED=/ s/\(^ENABLED=\)\(.*\)/\\1"true"/'
            # Above we use \\1 below when really we mean \1 so as Python expresses it properly
            self.assertEqual(sedline_make_true,conf.sedline1('ENABLED','true',chr(34)))
        with getset.Keyedfile(self.MYSQL5CNF_SIMILAR_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(23,conf.linecount)
            self.assertIn('skip-networking',conf)
            SKIPNET = 'skip-networking'
            sedline_skipnet = "0,/^  {0}   = / s/\(^  {0}   = \)\(.*\)/\\1off/".format(SKIPNET)
            # Above we use \\1 below when really we mean \1 so as Python expresses it properly
            self.assertEqual(sedline_skipnet,conf.sedline1('skip-networking','off',None))
        return


    def test_toggle_and_seds1(self):
        """ The sedline is defined with a range pattern ( 0,/^  skip-networking   = / ) to ensure
        that the first matching line ends the search.

        The MYSQL5CNF_SIMILAR_SPLIT may have a physical representation stored in
        files/mysql5.cnf if you want to grep to confirm that 2 lines match that pattern:
        egrep '^  skip-networking   = ' files/mysql5.cnf
        """
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        print "<<< test_toggle_and_seds1 next >>>"
        with getset.Keyedfile(self.MYSQL5CNF_SIMILAR_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(23,conf.linecount)
            self.assertIn('skip-networking',conf)
            self.assertTrue(conf.toggle_safe('skip-networking','off'))
            self.assertEqual(1,len(conf.dict_of_seds))
            SKIPNET = 'skip-networking'
            sedline = "0,/^  {0}   = / s/\(^  {0}   = \)\(.*\)/\\1off/".format(SKIPNET)
            # Above we use \\1 below when really we mean \1 so as Python expresses it properly
            self.assertEqual(conf.dict_of_seds[21],sedline)
            #for linenum,sed in conf.dict_of_seds.iteritems():
            #    print "{0}    {1}".format(linenum,sed)
            dfilename = None
            """ dfilename = 'toggle_and_sedlines1'
            Give dfilename a value as in above example line to have inspectable
            output written as /tmp/toggle_and_sedlines1.dash or similar on local machine """
            scriptlines,md5after,lines = conf.sed_checksum(conf.list_of_sedlines,
                                                           debug_filename=dfilename)
            self.assertIsNotNone(md5after)
            textreport,compared = cmdbin.iterable1iterable2compare(conf.lines_tuple,lines,False)
            self.assertEqual(md5after,'9c7d92f15bdceb21640fe98007e7bae9')
            self.assertFalse(any(compared))
            self.assertGreater(scriptlines,1)
            self.assertGreater(lines,1)
        return


    def test_sed_append_broker_module(self):
        print "<<< test_sed_append_broker_module >>>"
        # chr(9) is tab  ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        with getset.Keyedequals(self.NAGIOS341BROKER_SPLIT) as conf:
            key_brokmod = 'broker_module'
            key_brokmod_hashed = '#broker_module'
            self.assertEqual(conf.keysep,chr(61))
            self.assertIn(key_brokmod_hashed,conf.keys_hashed())
            self.assertIn(key_brokmod_hashed,conf.keys_disabled_printable())
            brokmod_compressed = conf[key_brokmod_hashed]
            self.assertEqual(brokmod_compressed,'/somewhere/module2.o arg1 arg23 debug0')
            re_brokmod_equals = re.compile('#broker_module=')
            search_count = 0
            for line in reversed(conf.lines_tuple):
                if re_brokmod_equals.search(line):
                    search_count += 1
                # Having completed a s// subsitution style search() next break if .match()
                if re_brokmod_equals.match(line):
                    # Use break to Simulate range 0,/^#broker_module=/
                    break
            self.assertEqual(1,search_count)
            noncomment_directive = conf.lines_noncomment_containing_any(['broker_module'])
            #for commented_line in noncomment_directive:
            #    print line
            adjusted_count = conf.linecount_adjusted()
            self.assertEqual(39,adjusted_count)
            self.assertEqual(3,len(noncomment_directive))
            md5before = 'a19938e2da8ca53d61e9c1f1cb41fbc6'
            value2 = '/usr/lib/ndoutils/ndomod-mysql-3x.o config_file=/etc/nagios/ndomod.cfg'
            verbosity=0
            update_res = conf.context_update('broker_module',value2,verbosity)
            if len(update_res) < 1:
                print "sed_remote_apply returned {0}".format(update_res)
            else:
                """ Context update completed so change recorded. Next apply it to remote """
                sedlines = conf.sedlines((verbosity>0))
            #dfilename = None
            dfilename = 'sed_append_broker_module'
            """ Give dfilename a value as in above example line to have inspectable
            output written as /tmp/sed_append_broker_module.dash or similar on local machine """
            scriptlines,md5after,lines = conf.sed_checksum(conf.list_of_sedlines,
                                                           debug_filename=dfilename)
            self.assertIsNotNone(md5after)
            md5after116f = 'fd6b0a268096791b160d1b33bfae116f'
            md5after773 = '8c81e143d25be99d4e7fe88197259773'
            self.assertNotEqual(md5after,md5after116f)
            self.assertEqual(md5after,md5after773)
            textreport,compared = cmdbin.iterable1iterable2compare(conf.lines_tuple,lines,False)

        return


    def test_value_diacritic(self):
        print "<<< test_value_diacritic next >>>"
        with getset.Keyedfile(self.MERGETOOLSDIACRITIC_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual(12,conf.linecount)
            self.assertIn('ecmerge.regkey',conf)
            regkey = conf['ecmerge.regkey']
            regkey8 = regkey.decode('utf-8')
            unichr233 = unichr(233)
            #print regkey.decode('utf-8')
            self.assertIn(unichr(233),regkey8)
            target_name = 'LATIN SMALL LETTER E WITH ACUTE'
            self.assertGreaterEqual(cmdbin.unicode_name_index(target_name,regkey8),0)
            self.assertGreater(cmdbin.unicode_name_index(target_name,regkey8[:5]),3)
        return


    def test_one_liner_no_linefeed(self):
        """
        tuple5 returns: key, before_count, sep, after_count, lastchar
        tuple7: key, line, before_count, sep, after_count, linesep, linenum
        """
        print "<<< test_one_liner_no_linefeed next >>>"
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        lines = ['missing: linefeed']
        with getset.Keyedcolon(lines) as conf:
            self.assertEqual(conf.keysep,chr(58))
            enabled = conf.tuples_enabled()
            self.assertEqual(1,len(enabled))
            self.assertEqual('linefeed',conf['missing'])
            warning_list = conf.warning_list
            self.assertGreater(len(warning_list),0)
        return


    def test_one_liners_ports(self):
        """
        Test the following one liners to see if guesswork alone gives the
        correct delimiter (space) in all cases.
        self.PORTS1LISTEN = "".join(reversed('Listen 80{0}'.format(linesep)))
        self.PORTS1LISTENAST = "".join(reversed('Listen *:80{0}'.format(linesep)))
        self.PORTS1LISTENLOCAL = "".join(reversed('Listen 127.0.0.1:80{0}'.format(linesep)))
        """
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        print "<<< test_one_liners_ports next >>>"
        lines = self.PORTS1LISTENAST[::-1].split(linesep)
        with getset.Keyedspace(lines) as conf:
            self.assertEqual(conf.keysep,chr(32))
            self.assertEqual('*:80',conf['Listen'])
        lines = self.PORTS1LISTENLOCAL[::-1].split(linesep)
        with getset.Keyedspace(lines) as conf:
            self.assertEqual(conf.keysep,chr(32))
            self.assertEqual('127.0.0.1:80',conf['Listen'])
        lines = self.PORTS1LISTEN[::-1].split(linesep)
        with getset.Keyedspace(lines) as conf:
            self.assertEqual(conf.keysep,chr(32))
            self.assertEqual('80',conf['Listen'])
        """ Having tested things with the specialist class Keyedspace,
        now use the generic (guess the separator) class Keyedfile
        """
        lines = self.PORTS1LISTENAST[::-1].split(linesep)
        with getset.Keyedfile(lines) as conf:
            self.assertEqual(conf.keysep,chr(32))
            self.assertEqual('*:80',conf['Listen'])
        lines = self.PORTS1LISTENLOCAL[::-1].split(linesep)
        with getset.Keyedfile(lines) as conf:
            self.assertEqual(conf.keysep,chr(32))
            self.assertEqual('127.0.0.1:80',conf['Listen'])
        lines = self.PORTS1LISTEN[::-1].split(linesep)
        with getset.Keyedfile(lines) as conf:
            self.assertEqual(conf.keysep,chr(32))
            self.assertEqual('80',conf['Listen'])
        return


    def test_tuple5one_liners_master(self):
        """
        tuple5 returns: key, before_count, sep, after_count, lastchar
        tuple7: key, line, before_count, sep, after_count, linesep, linenum
        """
        print "<<< test_tuple5one_liners_master next >>>"
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        lines = ["master: localhost{0}".format(linesep)]
        with getset.Keyedcolon(lines) as conf:
            self.assertEqual(conf.keysep,chr(58))
            enabled = conf.tuples_enabled()
            self.assertEqual(1,len(enabled))
            self.assertEqual('localhost',conf['master'])
            tup7 = enabled[0]
            #print tup7
            self.assertEqual(tup7[3],chr(58))
            tup5 = conf.tuple5(tup7[0],tup7[1],tup7[3],False)
            self.assertEqual(0,tup5[1])
            self.assertEqual(1,tup5[3])
        return


    def test_keysep_after_word_index(self):
        """ Test where keysep occurs in the line (word index)
        We have previously used TRAFFIC3OFF_BAD in our tests
        labelled inconsistent, but here we are going to use
        specialist subclass Keyedcolon and aim test situation
        where keysep_after_word_index returns > 1
        """
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        print "<<< test_keysep_after_word_index next >>>"
        with getset.Keyedcolon(self.TRAFFIC3OFF_BAD_SPLIT) as conf:
            self.assertEqual(chr(58),conf.keysep)
            server_lines = conf.lines_current_containing('server')
            #after_word_index = conf.keysep_after_word_index(server_lines[0],verbosity=2)
            after_word_index = conf.keysep_after_word_index(server_lines[0],verbosity=0)
            self.assertEqual(3,after_word_index)
        return


    def test_tuple5one_liners_interface(self):
        """
        tuple5 returns: key, before_count, sep, after_count, lastchar
        tuple7: key, line, before_count, sep, after_count, linesep, linenum
        """
        print "<<< test_tuple5one_liners_interface next >>>"
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        lines = ["interface: 127.0.0.1{0}".format(linesep)]
        with getset.Keyedcolon(lines) as conf:
            self.assertEqual(conf.keysep,chr(58))
            enabled = conf.tuples_enabled()
            self.assertEqual(1,len(enabled))
            self.assertEqual('127.0.0.1',conf['interface'])
            tup7 = enabled[0]
            self.assertEqual(tup7[1],lines[0])
            #print tup7
            self.assertEqual(tup7[3],chr(58))
            tup5 = conf.tuple5(tup7[0],tup7[1],tup7[3],False)
            self.assertEqual(0,tup5[1])
            """ Above is testing before_count, and below is testing after_count """
            self.assertEqual(1,tup5[3])
        return


    def test_counter_tuple_manual(self):
        with getset.Keyedequals(self.OPENSSL4_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual('2',conf['countryName_max'])
            tup14 = conf.tuples_all_from_key('countryName_max')
            (counter_length_sum,left_counter,before_counter,after_counter) = conf.counter_tuple(tup14)
            self.assertEqual(3,before_counter[chr(9)])
            self.assertEqual(1,after_counter[chr(32)])
            self.assertEqual(0,len(left_counter))
            self.assertEqual(4,counter_length_sum)
        return


    def test_tuple8tabs(self):
        """
        tuple8 args: key_unstripped, line, keysep, verbose=False
        tuple8 returns: key, line, before, sep, after, lastchar, before_counter, after_counter
        tuple7: key, line, before_count, sep, after_count, linesep, linenum
        Original tuple7 entries for countryName_default and countryName_max:
        ('countryName_default', 'countryName_default\t\t= AU\n', 2, '=', 1, '\n', 2)
        ('countryName_max', 'countryName_max\t\t\t= 2\n', 3, '=', 1, '\n', 4)
        """
        print "<<< test_tuple8tabs next >>>"
        # chr(9) is tab  ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        with getset.Keyedequals(self.OPENSSL4_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual('AU',conf['countryName_default'])
            tup7 = conf.tuples_all_from_key('countryName_default')
            tup8 = conf.tuple8(tup7[0],tup7[1],tup7[3],False)
            #tup8 = conf.tuple8_from_tuple7(tup7)
            before_counter = tup8[6]
            self.assertEqual(2,before_counter[chr(9)])
            after_counter = tup8[7]
            self.assertEqual(1,after_counter[chr(32)])
            # Above is countryName_default, next is countryName_max
            self.assertEqual('2',conf['countryName_max'])
            #enabled = conf.tuples_enabled()
            tup7 = conf.tuples_all_from_key('countryName_max')
            #tup8 = conf.tuple8(tup7[0],tup7[1],tup7[3],True)
            tup8 = conf.tuple8_from_tuple7(tup7)
            #print tup8
            before_counter = tup8[6]
            self.assertEqual(3,before_counter[chr(9)])
            after_counter = tup8[7]
            self.assertEqual(1,after_counter[chr(32)])
        return


    def test_mixed_whitespace_tuple_and_sedline1(self):
        """
        tuple8 args: key_unstripped, line, keysep, verbose=False
        tuple8 returns: key, line, before, sep, after, lastchar, before_counter, after_counter
        tuple7: key, line, before_count, sep, after_count, linesep, linenum
        Original tuple7 entries for countryName_default and countryName_max:
        ('countryName_default', 'countryName_default\t\t= AU\n', 2, '=', 1, '\n', 2)
        ('countryName_max', 'countryName_max\t\t\t= 2\n', 3, '=', 1, '\n', 4)
        """
        print "<<< test_mixed_whitespace_tuple_and_sedline1 next >>>"
        # chr(9) is tab  ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        with getset.Keyedequals(self.OPENSSL4_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            self.assertEqual('AU',conf['countryName_default'])
            tup7 = conf.tuples_all_from_key('countryName_default')
            tup8 = conf.tuple8_from_tuple7(tup7)
            before_counter = tup8[6]
            self.assertEqual(2,before_counter[chr(9)])
            after_counter = tup8[7]
            self.assertEqual(1,after_counter[chr(32)])
            before = tup8[2]
            after = tup8[4]
            #mixed_tuple3 = conf.mixed_whitespace_tuple(before,after,2)
            mixed_tuple3 = conf.mixed_whitespace_tuple(before,after)
            self.assertEqual(mixed_tuple3,(False,False,True))
            sedline_default = '0,/^countryName_default\t\t= / s/\(^countryName_default\t\t= \)\(.*\)/\\1GB/'
            # Above we use \\1 below when really we mean \1 so as Python expresses it properly
            #print sedline_default,sedline_default
            #print conf.sedline1('countryName_default','GB',None)
            self.assertEqual(sedline_default,conf.sedline1('countryName_default','GB',None))
            originally_spaced2tabs = 'countryName_default\t\t= '
            #self.assertEqual(originally_spaced2tabs,conf.originally_spaced('countryName_default',3))
            self.assertEqual(originally_spaced2tabs,conf.originally_spaced('countryName_default'),1)
        return


    def test_keysep_space_targets_max1spaced2(self):
        lines = self.APACHE2MAX2[::-1].split(linesep)
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        print "<<< test_keysep_space_targets_max1spaced2 MaxKeepAliveRequests next >>>"
        with getset.Keyedspace2(lines) as conf:
            self.assertEqual(conf.keysep,chr(32))
            tup14 = conf.tuples_all_from_key('MaxKeepAliveRequests')
            """
            print "{0}{0}".format(tup14[13])
            tup6 = tup14[8:]
            print tup6[0],tup6[0],tup6[0],tup6
            """
            self.assertEqual(21,len(tup14[13]))
            """
            tup8 = conf.tuple8_from_tuple14(tup14)
            before_counter = tup8[6]
            self.assertEqual(0,before_counter[chr(32)])
            after_counter = tup8[7]
            self.assertEqual(5,after_counter[chr(32)])
            before = tup8[2]
            after = tup8[4]
            # 'MaxKeepAliveRequests 100{0}    MaxSpareServers      10{0}'.format(linesep)
            """
        return


    def test_keysep_space_targets_max2spaced2(self):
        lines = self.APACHE2MAX2[::-1].split(linesep)
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        print "<<< test_keysep_space_targets_max2spaced2 MaxSpareServers next >>>"
        with getset.Keyedspace2(lines) as conf:
            self.assertEqual(conf.keysep,chr(32))
            tup14 = conf.tuples_all_from_key('MaxSpareServers')
            #print "{0}{0}".format(tup14[13])
            tup6 = tup14[8:]
            #print tup6[0],tup6[0],tup6[0],tup6
            #print tup14,tup6
            self.assertEqual(25,len(tup14[13]))
            """
            tup8 = conf.tuple8_from_tuple14(tup14)
            before_counter = tup8[6]
            self.assertEqual(0,before_counter[chr(32)])
            after_counter = tup8[7]
            self.assertEqual(5,after_counter[chr(32)])
            before = tup8[2]
            after = tup8[4]
            # 'MaxKeepAliveRequests 100{0}    MaxSpareServers      10{0}'.format(linesep)
            """
        return


    def test_keysep_space_targets_max3spaced2(self):
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        print "<<< test_keysep_space_targets_max3spaced2 two Maxs next >>>"
        with getset.Keyedspace2(self.APACHE2MAX3_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(32))
            tup14 = conf.tuples_all_from_key('MaxKeepAliveRequests')
            target_total_length = len(tup14[13])
            #print "{0}{0}".format(tup14[13])
            tup14 = conf.tuples_all_from_key('MaxSpareServers')
            target_total_length += len(tup14[13])
            #print "{0}{0}".format(tup14[13])
            self.assertEqual(46,target_total_length)
            # 'MaxKeepAliveRequests 100{0}    MaxSpareServers      10{0}'.format(linesep)
        return


    def test_key_left_padding_max6(self):
        """ Test sedline_safe uses appropriate left padding when making change
        to key 'MaxSpareServers' so that:
        (.) Single line out of the six possibles would be updated by sed
        (.) Last line out of the six possibles would be updated by sed
        """
        print "<<< test_key_left_padding_max6 >>>"
        with getset.Keyedspace2(self.APACHE2MAX6_SPLIT) as conf:
            #conf.summary_detailed(True)
            key1 = 'MaxSpareServers'
            self.assertIn(key1,conf.keys_enabled())
            maxspare2left = '  MaxSpareServers'
            self.assertIsNone(conf.tuples_all_from_key(maxspare2left))
            tup14 = conf.tuples_all_from_key('MaxSpareServers')
            self.assertIsNotNone(tup14)
            target = tup14[13]
            target_unexpected = chr(35)
            target_expected = maxspare2left
            self.assertIsNotNone(target)
            #target_spaced = conf.target_spaced_space('MaxSpareServers',True)
            target_spaced = conf.target_spaced_space('MaxSpareServers')
            self.assertEqual(target_spaced,target)
            self.assertTrue(target_spaced.startswith(maxspare2left))
            sedline_expected = '0,/^  MaxSpareServers      / s/\(^  MaxSpareServers      \)\(.*\)/\\19/'
            sedline_res = conf.sedline_safe_wrapper('MaxSpareServers',9)
            #sedline = conf.sedline_safe('MaxSpareServers',9)
            self.assertEqual(True,sedline_res)
            # With extra debugging printing might have seen output 'record_changed_has_tuple:6->9'
            sedline = ''
            sedlines = conf.sedlines()
            if len(sedlines) == 1:
                sedline = sedlines[0]
            self.assertEqual(sedline,sedline_expected)
        return


    def test_process_comment_line_replacements4(self):
        print "<<< test_process_comment_line_replacements4 next >>>"
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        with getset.Keyedcolon(self.MINION4_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(58))
            enabled = conf.tuples_enabled()
            self.assertEqual(1,len(enabled))
            self.assertEqual('saltnshake',conf['#master'])
            # Assert Typical Failure: 'saltnshake' != 'orminionasroot,buthaveanon-rootgroupbegivenaccessto'
            tup7 = enabled[0]
            #print tup7
            self.assertEqual(tup7[3],chr(58))
            tup5 = conf.tuple5(tup7[0],tup7[1],tup7[3],False)
            self.assertEqual(0,tup5[1])
            self.assertEqual(1,tup5[3])
        return


    def test_process_comment_line_replacements4left(self):
        print "<<< test_process_comment_line_replacements4left next >>>"
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        from hashlib import md5
        mdf = None
        with getset.Keyedcolon(self.MINION4LEFT_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(58))
            enabled = conf.tuples_enabled()
            self.assertEqual(1,len(enabled))
            self.assertEqual('saltnshake',conf['#master'])
            # Assert Typical Failure: 'saltnshake' != 'orminionasroot,buthaveanon-rootgroupbegivenaccessto'
            mdf = md5()
            for line in conf.lines_tuple:
                mdf.update(line)
            tup7 = enabled[0]
            #print tup7
            self.assertEqual(tup7[3],chr(58))
            tup5 = conf.tuple5(tup7[0],tup7[1],tup7[3],False)
            self.assertEqual(0,tup5[1])
            self.assertEqual(1,tup5[3])
        self.assertNotEqual(None,mdf)
        self.assertEqual('c70d889310c904287de158ccea062087',mdf.hexdigest())
        return


    def test_process_comment_line_replacements4general(self):
        """ This test uses Keyedfile instead of specialist subclass Keyedcolon.
        When using specialist subclasses that have KEYSEP_NAMED set, there
        is some extra protection when dealing with hashed / commented lines
        to prevent replacement of previous entries which may be hashed
        key value lines. Using Keyedfile() we do not benefit from that protection
        and the hashed key 'master' is overritten with a line from later in the file
        This is what our equality to 'orminionasroot,buthaveanon-rootgroupbegivenaccessto'
        is testing.
        See earlier test test_process_comment_line_replacements4() to compare outcomes.
        """
        print "<<< test_process_comment_line_replacements4general next >>>"
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        with getset.Keyedfile(self.MINION4_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(58))
            enabled = conf.tuples_enabled()
            self.assertEqual(1,len(enabled))
            self.assertEqual('orminionasroot,buthaveanon-rootgroupbegivenaccessto',conf['#master'])
            # Assert Typical Failure: 'orminionasroot,buthaveanon-rootgroupbegivenaccessto' != 'saltnshake'
            tup7 = enabled[0]
            #print tup7
            self.assertEqual(tup7[3],chr(58))
            tup5 = conf.tuple5(tup7[0],tup7[1],tup7[3],False)
            self.assertEqual(0,tup5[1])
            self.assertEqual(1,tup5[3])
        return


    def test_rangestop_loose(self):
        """ This function and related function test_rangestop_loose_reverse() are
        intended as logic tests to aid with design / testing of behaviour
        of sed ranges used in sed_diff_lines() and sed_remote()
        without any dependency on a remote server setup being available

        The plan is to move away from using sed ranges and use direct
        single match targets, and these functions will help facilitate that change.
        """
        print "<<< test_rangestop_loose next >>>"
        # chr(9) is tab  ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        with getset.Keyedequals(self.VARNISH3_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            daemon_opts = conf['DAEMON_OPTS']
            self.assertEqual(daemon_opts,'"-a :6081 \\')
            """ Above we use \\ when really we mean \ so Python expresses it properly """
            re_daemon_opts_equals = re.compile('DAEMON_OPTS=')
            search_count = 0
            for line in conf.lines_tuple:
                if re_daemon_opts_equals.search(line):
                    search_count += 1
                # Having completed a s// subsitution style search() next break if .match()
                if re_daemon_opts_equals.match(line):
                    # Use break to Simulate range 0,/^DAEMON_OPTS=/
                    break
            self.assertEqual(2,search_count)
            """ Unlike test_rangestop_loose_reverse() we equal 2 above """
        return


    def test_rangestop_loose_reverse(self):
        """ This function and related function test_rangestop_loose_reverse() are
        intended as logic tests to aid with design / testing of behaviour
        of sed ranges used in sed_diff_lines() and sed_remote()
        without any dependency on a remote server setup being available

        The plan is to move away from using sed ranges and use direct
        single match targets, and these functions will help facilitate that change.
        """
        print "<<< test_rangestop_loose_reverse next >>>"
        # chr(9) is tab  ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        with getset.Keyedequals(self.VARNISH3_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            daemon_opts = conf['DAEMON_OPTS']
            self.assertEqual(daemon_opts,'"-a :6081 \\')
            """ Above we use \\ when really we mean \ so Python expresses it properly """
            re_daemon_opts_equals = re.compile('DAEMON_OPTS=')
            search_count = 0
            for line in reversed(conf.lines_tuple):
                if re_daemon_opts_equals.search(line):
                    search_count += 1
                # Having completed a s// subsitution style search() next break if .match()
                if re_daemon_opts_equals.match(line):
                    # Use break to Simulate range 0,/^DAEMON_OPTS=/
                    break
            self.assertEqual(3,search_count)
            """ Unlike test_rangestop_loose() we equal 3 above """
        return


    def test_sedline1_delimiter_avoid_slash(self):
        # chr(47) is forward slash (/)  ; chr(64) is at symbol (@)  ;  chr(94) is caret / hat
        print "<<< test_sed_delimiter_avoid_slash >>>"
        with getset.Keyedequals(self.MYSQL5CNF_SIMILAR_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(61))
            sedline_avoid_slash = conf.sedline1('socket','/var/lib/mysql/mysqld.sock')
            #print "For rpm based might want this sed {0}".format(sedline_avoid_slash)
            self.assertEqual(4,Counter(sedline_avoid_slash)[chr(47)])
        return


    def test_space2subclass_tuple_enabled(self):
        # chr(32) is space  ; # chr(35) is hash / octothorpe 
        print "<<< test_space2subclass_tuple_enabled >>>"
        with getset.Keyedspace2(self.EN_GB_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(32))
            key1 = 'en_GB'
            self.assertIn(key1,conf)
            tup14 = conf.tuples_all_from_key(key1)
            self.assertEqual(chr(32),tup14[3])
            self.assertFalse(tup14[7])
            self.assertIn(key1,conf.keys_enabled())
            target_unexpected = chr(35)
            target_expected = 'en_GB '
            self.assertNotEqual(target_unexpected,tup14[13])
            self.assertEqual(target_expected,tup14[13])
            self.assertFalse(conf.tuple_disabled(self.TUPLE14EURO1COMMENT))
            self.assertFalse(conf.tuple_disabled(self.TUPLE14EURO2COMMENT))
        return


    def test_space2subclass_tuple_disabled(self):
        """ Check that en_GB.ISO-8859-15 is treated as a disabled key
        rather than just a comment line. Form a sedline and check it looks as expected.
        """
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        print "<<< test_space2subclass_tuple_disabled >>>"
        with getset.Keyedspace2(self.EN_GB_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(32))
            self.assertIn('en_GB',conf)
            key15 = '#en_GB.ISO-8859-15'
            tup14 = conf.tuples_all_from_key(key15)
            self.assertEqual(chr(32),tup14[3])
            self.assertFalse(tup14[7])
            self.assertIn(key15,conf.keys_hashed())
            self.assertIn(key15,conf.keys_disabled_printable())
            #print tup14
            utf8disabled = conf.keys_disabled_value_contains('UTF-8')
            self.assertEqual(0,len(utf8disabled))
            latin15disabled = conf.keys_disabled_value_contains('8859-15')
            self.assertEqual(1,len(latin15disabled))
            latin1disabled = conf.keys_disabled_value_contains('8859-1')
            self.assertEqual(1,len(latin1disabled))
            """ latin1disabled is actually matching the only disabled line shown next:
            # en_GB.ISO-8859-15 ISO-8859-15
            But check your assumption below.
            """
            #print latin1disabled
            """ Our handling of keys and values means we remove the space between
            hash and en_GB so # en_GB becomes #en_GB ....
            """
            #self.assertIn('#en_GB.ISO-8859-15',latin1disabled)
            self.assertIn(key15,latin1disabled)
            #sedline = conf.context_unhash_inner(key15,safe=True,hashless=False)
            #sedline = conf.context_unhash_inner(key15,True,False)
            print "Next we request an unhash change for line having key={0}".format(key15)
            sedline_expected = '0,/^# en_GB.ISO-8859-15 / s/^# en_GB.ISO-8859-15 /en_GB.ISO-8859-15 /'
            sedline_unhash = conf.context_unhash_constructed(key15,False)
            self.assertEqual(sedline_unhash,sedline_expected)
        return


    def test_space2subclass_tuple_disabled2(self):
        """ Check that en_GB.ISO-8859-15 is treated as a disabled key
        rather than just a comment line, even when it is first non blank line in file.

        Subclass Keyedspace2() has some handling to ensure that
        a useless target like '#' is replaced with a more useful
        value. Use target_unexpected and target_expected to test this is working.
        """
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        print "<<< test_space2subclass_tuple_disabled2 >>>"
        # We use EN_GB2 which has 2 rather than 3 entries
        with getset.Keyedspace2(self.EN_GB2_SPLIT) as conf:
            self.assertEqual(conf.keysep,chr(32))
            self.assertNotIn('en_GB',conf)
            key15 = '#en_GB.ISO-8859-15'
            tup14 = conf.tuples_all_from_key(key15)
            self.assertEqual(chr(32),tup14[3])
            self.assertFalse(tup14[7])
            self.assertIn(key15,conf.keys_hashed())
            self.assertIn(key15,conf.keys_disabled_printable())
            #print tup14
            self.assertEqual(True,conf.tuple_disabled(self.TUPLE14EURO1DISABLED))
            self.assertEqual(True,conf.tuple_disabled(self.TUPLE14EURO2DISABLED))
            utf8disabled = conf.keys_disabled_value_contains('UTF-8')
            self.assertEqual(0,len(utf8disabled))
            latin15disabled = conf.keys_disabled_value_contains('8859-15')
            self.assertEqual(1,len(latin15disabled))
            target_unexpected = chr(35)
            target_expected = '# en_GB.ISO-8859-15 ' # Trailing space is expected and important
            self.assertNotEqual(target_unexpected,tup14[13])
            self.assertEqual(target_expected,tup14[13])
        return



