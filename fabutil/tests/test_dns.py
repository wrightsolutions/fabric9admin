#!/usr/bin/env python
# -*- coding: utf-8 -*-
from unittest import TestCase
from os import linesep
from string import ascii_lowercase,digits,printable,punctuation

import cmdbin
import conf_get_set as getset

class KeyedLocal(TestCase):
    """ Key value files are not in themselves hard, but the problem of consistency
    and small differences in spacing / formatting or in the key separator used,
    make writing generic parsers a task involving consideration of many cases.

    If you want to understand why YAML was created then reading these tests and
    adding a handful of your own, should help understand the motivation for
    creating YAML and JSON and other conformant formats. YAML balances a
    lightweight parser with a few recommended guidelines on spacing / indentation
    """

    def setUp(self):
        """ Even if the application under testing does inline replaces,
        that will not affect some of these constants as they employ:
        (.) reversed() in definition
        (.) shallow copy and reverse just prior to test use. See [::-1]
        Some constants, particularly the multiline definitions do not
        employ this technique.
        """

        # chr(33) is exclamation mark (!)      ; chr(34) is double quote (")     ; chr(39) is single quote (')
        # chr(35) is hash / octothorpe             ; chr(58) is colon (:)            ; chr(61) is equals (=)

        self.SET_COLON_SPACE_EQUALS = set([chr(58),chr(32),chr(61)])
        self.SET_PRINTABLE = set(printable)
        self.SET_PUNCTUATION = set(punctuation)
        self.SET_LOWER_PLUS_DIGITS = set(ascii_lowercase).union(digits)

        self.RESOLV1NAMESERVER = "".join(reversed('nameserver 8.8.8.8{0}options attempts:3{0}'.format(linesep)))
        self.RESOLV1DISORDERED = "".join(reversed('options attempts:3{0}nameserver 8.8.8.8{0}'.format(linesep)))
        # Next also lifts selected items/ideas from a resolv.conf file
        self.RESOLV2NAMESERVERSHORT = """
nameserver 8.8.8.8
nameserver 8.8.4.4
options attempts:3
options timeout:2
options rotate
"""
        self.RESOLV2NAMESERVERSHORT_SPLIT = self.RESOLV2NAMESERVERSHORT.split(linesep)
        self.RESOLV2NAMESERVERFULL = """
domain cable.buried.net
search cable.buried.net buried.net
nameserver 8.8.8.8
nameserver 8.8.4.4
options attempts:3
options timeout:2
options rotate
options inet6
#options single-request
options single-request-reopen
# From manpage of resolv.conf:
# The keyword and value must appear on a single line,
# and the keyword (e.g., nameserver) must start the line.
# The value follows the keyword, separated by white space.
# Any problems with 'options rotate' see following test
# python -c 'from socket import gethostbyname;print [ "Y" if gethostbyname("debian.org") else "." for x in range(10) ]'
"""
        self.RESOLV2NAMESERVERFULL_SPLIT = self.RESOLV2NAMESERVERFULL.split(linesep)
        self.RESOLV2DISORDERED = """
options attempts:3
options timeout:2
options rotate
nameserver 8.8.8.8
nameserver 8.8.4.4
"""
        self.RESOLV2DISORDERED_SPLIT = self.RESOLV2DISORDERED.split(linesep)
        return


    def test_keysep1good(self):
        lines = self.RESOLV1NAMESERVER[::-1].split(linesep)
        with getset.Keyedfile(lines) as conf:
            self.assertEqual(conf.keysep,' ')
        return


    def test_keysep1good(self):
        # chr(58) is colon (:) ; chr(61) is equals (=) ; chr(32) is space ; chr(10) is line feed
        lines = self.RESOLV1DISORDERED[::-1].split(linesep)
        with getset.Keyedfile(lines) as conf:
            self.assertFalse(conf.keysep==chr(58))
            self.assertEqual(conf.keysep,chr(32))
        return


    def test_value_numeric_keyedfile(self):
        with getset.Keyedfile(self.RESOLV2NAMESERVERSHORT_SPLIT) as conf:
            self.assertEqual(conf.linecount,7)
            self.assertEqual(2,len(conf))
            self.assertNotIn('options timeout',conf)
        return


    def test_value_numeric_keyedcolon(self):
        """ When using Keyedcolon lines which do not contain colon (:)
        will never make it into the underlying OrderedDict.
        So in fact only two lines are processed into the OrderedDict:
        options attempts:3
        options timeout:2
        """
        with getset.Keyedcolon(self.RESOLV2NAMESERVERSHORT_SPLIT) as conf:
            self.assertEqual(conf.linecount,7)
            """
            for conf_k,conf_v in conf.items():
                print "{0}DDD{1}".format(conf_k,conf_v)
            """
            self.assertEqual(2,len(conf))
            self.assertIn('options timeout',conf)
            #for tup in conf.tuples_all:
        return

