#!/usr/bin/env python
# -*- coding: utf-8 -*-
from unittest import TestCase
from os import linesep
from string import ascii_lowercase,digits,printable,punctuation

import cmdbin
import package_deb as pdeb

class PackageDeb(TestCase):

    def setUp(self):
        """ Even if the application under testing does inline replaces,
        that will not affect some of these constants as they employ:
        (.) reversed() in definition
        (.) shallow copy and reverse just prior to test use. See [::-1]
        Some constants, particularly the multiline definitions do not
        employ this technique.
        """

        # chr(33) is exclamation mark (!)      ; chr(34) is double quote (")     ; chr(39) is single quote (')
        # chr(35) is hash / octothorpe             ; chr(58) is colon (:)            ; chr(61) is equals (=)

        self.SET_COLON_SPACE_EQUALS = set([chr(58),chr(32),chr(61)])
        self.SET_PRINTABLE = set(printable)
        self.SET_PUNCTUATION = set(punctuation)
        self.SET_LOWER_PLUS_DIGITS = set(ascii_lowercase).union(digits)
        self.FIVETABS = "{0}{0}{0}{0}{0}".format(chr(9))

        self.RESOLV1NAMESERVER = "".join(reversed('nameserver 8.8.8.8{0}options attempts:3{0}'.format(linesep)))
        # Next also lifts selected items/ideas from a resolv.conf file
        self.PYTHON27SEVEN = """
python-xapian{0}install
python-yaml{0}install
python-zmq{0}hold
python2.6{0}purge
python2.6-minimal{0}deinstall
python2.7{0}install
python2.7-minimal{0}install
""".format(self.FIVETABS)
        self.PYTHON27SEVEN_SPLIT = self.PYTHON27SEVEN.split(linesep)
        return


    def test_selection_anything(self):
        selections1 = pdeb.selection_lines_including(self.PYTHON27SEVEN_SPLIT,None,None,True)
        selections2 = pdeb.selection_lines_including(self.PYTHON27SEVEN_SPLIT,'anything',None,True)
        self.assertEqual(7,len(selections1))
        self.assertEqual(selections1,selections2)
        #selections_including('hold',True)
        return


    def test_selection4(self):
        selections1 = pdeb.selection_lines_including(self.PYTHON27SEVEN_SPLIT,['install'],None,True)
        selections2 = pdeb.selection_lines_including(self.PYTHON27SEVEN_SPLIT,'install',None,True)
        self.assertEqual(4,len(selections1))
        self.assertEqual(selections1,selections2)
        #selections_including('hold',True)
        return


    def test_selection_hold1(self):
        selections1 = pdeb.selection_lines_including(self.PYTHON27SEVEN_SPLIT,['hold'],None,True)
        EX3=['deinstall','install','purge']
        selections2 = pdeb.selection_lines_including(self.PYTHON27SEVEN_SPLIT,None,EX3,True)
        self.assertEqual(1,len(selections1))
        self.assertEqual(selections1,selections2)



    def test_keysep1good(self):
        """
        lines = self.RESOLV1NAMESERVER[::-1].split(linesep)
        with getset.Keyedfile(lines) as conf:
            self.assertEqual(conf.keysep,' ')
        """
        return




    def test_value_numeric_keyedfile(self):
        """
        with getset.Keyedfile(self.RESOLV2NAMESERVERSHORT_SPLIT) as conf:
            self.assertEqual(conf.linecount,7)
            self.assertEqual(2,len(conf))
            self.assertNotIn('options timeout',conf)
            self.assertIn('options timeout',conf)
        """
        return

