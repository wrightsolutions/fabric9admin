#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from collections import Counter,defaultdict,OrderedDict
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
#from fabric.contrib.files import exists,append
from os import path
import re
from string import ascii_lowercase,digits,printable,punctuation,whitespace
try:
    from . import cmdbin
except ValueError:
    import cmdbin
from . import conf_get_set as getset
#from . import package_deb as pdeb

CMD_SALT_FINGERPRINT = "/usr/bin/salt-key --no-color -F "
CMD_SALT_FINGERPRINT_MASTER = "/usr/bin/salt-key --no-color -F master;"
CMD_SALT_FINGERPRINT_HIGHLIGHTED = "/usr/bin/salt-key -F "
CMD_SALT_FINGERPRINT_MASTER_HIGHLIGHTED = "/usr/bin/salt-key -F master;"
CMD_SALT_MASTER_REGISTERED_PUBKEY = "/usr/bin/salt-key -p "
CMD_SALT_CALL_STATE_HIGHSTATE = "/usr/bin/salt-call state.highstate;"
CMD_SALT_CALL_STATE_HIGHSTATE_LOCAL = "/usr/bin/salt-call state.highstate --local;"
CMD_SALT_CALL_LIST_FUNCTIONS = "/usr/bin/salt-call sys.list_functions "
CMD_SALT_CALL_LIST_FUNCTIONS_LOCAL = "/usr/bin/salt-call --local sys.list_functions "
CMD_SALT_CALL_VERSION_LOCAL = "/usr/bin/salt-call --local test.version | /bin/grep -v ':' "
CMD_SALT_CALL_VERSION_LOCAL_REPORT = "/usr/bin/salt-call --local test.versions_report "
ZERO_ONE_TWO_THREE=['ZERO','ONE','TWO','THREE']
ETC_MASTER='/etc/salt/master'
ETC_MINION='/etc/salt/minion'


def fingerprint_valid(fingerprint,validity_level=2,delimiter=chr(58)):
    # chr(58) is colon (:)   ; chr(32) is space
    return cmdbin.md5valid(fingerprint,validity_level,delimiter)


def state_highstate_info(local=False):
    """ Test master connection by using state.highstate
    Returns a list of lines from the output that contains '[INFO'
    """
    info_lines = []
    with hide('output'):
        if local is True:
            ros_res = cmdbin.run_or_sudo(CMD_SALT_CALL_STATE_HIGHSTATE_LOCAL)
        else:
            ros_res = cmdbin.run_or_sudo(CMD_SALT_CALL_STATE_HIGHSTATE)
        """ Could check for string 'ERROR' first in output but probably okay
        to just go get matches of '[INFO'
        """
        info_lines = cmdbin.run_res_lines_stripped_searching(ros_res,'[INFO')
    return info_lines


def state_highstate_print(local=False):
    lines = state_highstate_info(local)
    for line in lines:
        print line
    return


def fetch_fingerprint(target='master',prefix=True,highlighted=False):
    """ Fetch the public key fingerprint from the target (master or minion)
    Try and avoid prefix is False and highlighted is True
    """
    if target is None:
        return ''
    ros_res = ''
    if target == 'master' and prefix is True and highlighted is True:
        with hide('output'):
            ros_res = cmdbin.run_or_sudo(CMD_SALT_FINGERPRINT_MASTER_HIGHLIGHTED)
    elif target == 'master':
        with hide('output'):
            ros_res = cmdbin.run_or_sudo(CMD_SALT_FINGERPRINT_MASTER)
    elif prefix is True and highlighted is True:
        with hide('output'):
            ros_res = cmdbin.run_or_sudo("{0}{1}".format(CMD_SALT_FINGERPRINT_HIGHLIGHTED,target))
    else:
        with hide('output'):
            ros_res = cmdbin.run_or_sudo("{0}{1}".format(CMD_SALT_FINGERPRINT,target))
    fingerprint = cmdbin.run_res_lines_stripped_searching1(ros_res,'.pub')
    if prefix is not True and len(fingerprint) > 1:
        """ Remove the prefix 'master.pub' or similar
        Previous logic should have prevented situation where we highlight but then remove prefix
        """
        fingerprint = fingerprint.split()[1]
    return fingerprint


def print_fingerprint(target='master',prefix=True,highlighted=False):
    if highlighted is True and prefix is False:
        highlighted = False
    fingerprint = fetch_fingerprint(target,prefix,highlighted)
    if len(fingerprint) > 5:
        print fingerprint
    else:
        print 'master fingerprint not fetched.'
        return False
    return True


def verify_fingerprint_used(fingerprint,target='minion'):
    """ Verify the supplied fingerprint is the fingerprint in use for the target (minion or master) """
    fingerprint_used = fetch_fingerprint(target,False,False)
    used_flag = False
    if len(fingerprint_used) > 5:
        if fingerprint_used == fingerprint:
            used_flag = True
    return used_flag


def verify_fingerprint_used_minion(fingerprint,verbose=False):
    """ Verify that the supplied fingerprint matches the minion fingerprint in use """
    if fingerprint is None or len(fingerprint) < 5:
        return False
    used_flag = verify_fingerprint_used(fingerprint)
    if verbose and used_flag is False:
        print "NOT verified. You supplied fingerprint ending {0}".format(fingerprint[-5:])
        print_fingerprint('minion',True,True)
    return used_flag


def listen_dictionary(target='master'):
    listen_dict = {}
    if target == 'master':
        filepath = ETC_MASTER
    else:
        filepath = ETC_MINION
    with getset.Keyedcolon(filepath) as conf:
        if 'interface' in conf.keys_enabled():
            listen_dict['interface'] = conf['interface']
        elif 'interface' in conf.keys_disabled(True):
            keyname_when_hashed = conf.when_hashed('interface',True,'original')
            listen_dict['interface_disabled'] = conf[keyname_when_hashed]
        else:
            pass
        # Next: tcp port used by the publisher for which default is 4505
        if 'publish_port' in conf.keys_enabled():
            listen_dict['publish_port'] = conf['publish_port']
        elif 'publish_port' in conf.keys_disabled(True):
            keyname_when_hashed = conf.when_hashed('publish_port',True,'original')
            listen_dict['publish_port_disabled'] = conf[keyname_when_hashed]
        else:
            pass
        # Next: return port (communication port) for which default is 4506
        if 'ret_port' in conf.keys_enabled():
            listen_dict['ret_port'] = conf['ret_port']
        elif 'ret_port' in conf.keys_disabled(True):
            keyname_when_hashed = conf.when_hashed('ret_port',True,'original')
            listen_dict['ret_port_disabled'] = conf[keyname_when_hashed]
        else:
            pass
    return listen_dict


def minion_hardcoded_master_simple(verbose=False,filepath=ETC_MINION):
    fingerprint_hardcoded = ''
    master_fingerprint_value = None
    with getset.Keyedcolon(filepath) as conf:
        if 'master_finger' in conf.keys_enabled():
            master_fingerprint_value = conf['master_finger']
        elif 'master_finger' in conf.keys_disabled():
            disabled_value = conf['#master_finger']
            if verbose:
                print "Hardcoded value (disabled) is {0}".format(disabled_value)
        else:
            pass
    if master_fingerprint_value is not None:
        fingerprint_hardcoded = master_fingerprint_value.strip()
    return fingerprint_hardcoded


def minion_hardcoded_master_dict(filepath=ETC_MINION):
    """ Create a dictionary with keys 'enabled' and / or 'disabled'
    having value(s) equal to master fingerprint """
    fingerprint_dict = {}
    master_fingerprint_value = None
    disabled_value = None
    with getset.Keyedcolon(filepath) as conf:
        if 'master_finger' in conf.keys_enabled():
            master_fingerprint_value = conf['master_finger']
        #if 'master_finger' in conf.keys_disabled(True,False):
        if 'master_finger' in conf.keys_disabled_printable(True):
            keyname_when_hashed = conf.when_hashed('master_finger',True,'original')
            disabled_value = conf[keyname_when_hashed]
        else:
            pass
    if master_fingerprint_value is not None:
        fingerprint_dict['enabled'] = master_fingerprint_value.strip()
    if disabled_value is not None:
        fingerprint_dict['disabled'] = disabled_value.strip()
    return fingerprint_dict


def minion_hardcoded_master(minion_filepath=ETC_MINION):
    fingerprint_dict = minion_hardcoded_master_dict(minion_filepath)
    if 'enabled' in fingerprint_dict:
        return fingerprint_dict['enabled']
    return ''


def minion_localhost_master(verbosity=1,minion_filepath=ETC_MINION):
    """ Set salt minion to look to LOCALHOST ONLY for configuration / instructions
    Use context_update style seds via context_update_localhost()
    There are around 150 enabled or placeholder (noncomment) lines in minion conf
    The other 350 or so are comments that are not placeholders.
    """
    updated_flag = False
    with getset.Keyedcolon(minion_filepath) as conf:
        if verbosity > 1:
            noncomment_master = conf.lines_noncomment_containing_any(['master'])
            # Above supplies a list to lines_noncomment_containing_any()
            print "Before change here are {0} lines / placeholders ...".format(len(noncomment_master))
            print "... from minion conf which contain master and may be relevant"
            for line in noncomment_master:
                print line.rstrip()
        #update_res = conf.context_update('master','localhost',2)
        update_res = conf.context_update_localhost('master',2,True)
        if len(update_res) > 0:
            """ Here I document four calls and what they are likely to return in order
            to show expected results and to also provide a convenient comparison
            of different ways of getting to the current line for 'master' in the minion conf

            print conf.lines_current_simple()
            lines_current_simple will not return any comment lines so because of the nature
            of a salt minion conf (mostly commented placeholders) you will get very few results
            even though when measured a salt minion conf is near 500 lines.

            print conf.lines_current_simple(True)
            As above although here we are asking for changed_only=True just to illustrate the
            use of the changed_only parameter. It will not make any difference in this case
            as we have changed the line containing 'master' so it will be returned changed_only=True

            print conf.lines_current_containing('master')
            lines_current_containing is a wrapper around lines_current_simple that filters on expression / regex

            print conf.lines_for_put_containing('^master')
            A bare lines_for_put would give near 500 lines of results but here we are using a wrapper
            which filters (those 500 lines) on expression / regex.
            """
            sedlines = conf.sedlines((verbosity>1))
            #apply_res = getset.sed_remote_apply('a'*32,minion_filepath,sedlines,'medium')
            #apply_res = getset.sed_remote_apply('a'*32,minion_filepath,sedlines,'medium',True,0,True)
            apply_res = getset.sed_remote_apply('a'*32,minion_filepath,sedlines,'medium',True,0.75,True)
            if apply_res is True:
                updated_flag = True
            elif verbosity > 1:
                print "sed_remote_apply returned {0}".format(apply_res)
    if updated_flag is True:
        pass
    elif verbosity > 0:
        print "context_update_localhost on minion conf did not complete as expected!"
        print "Was minion already set localhost only before your request?"
    return updated_flag


def verify_fingerprint_hardcoded_minion_simple(fingerprint,verbose=False):
    """ Verify that the supplied fingerprint matches the minion hardcoded fingerprint """
    if fingerprint is None or len(fingerprint) < 5:
        return False
    fingerprint_hardcoded = minion_hardcoded_master(verbose)
    verified_flag = False
    if len(fingerprint_hardcoded) > 5:
        if fingerprint_hardcoded == fingerprint:
            verified_flag = True
    if verbose:
        hardcoded_words = 'fingerprint hardcoded'
        if verified_flag is True:
            print "VERIFIED. {0} matches supplied fingerprint ending {1}".format(hardcoded_words,
                                                                                 fingerprint[-5:])
            used_fingerprint = fetch_fingerprint('minion',False,False)
            if used_fingerprint == fingerprint:
                pass
            elif len(used_fingerprint) > 5:
                print "Fingerprint in active use (as reported) ends {0}".format(used_fingerprint[-5:])
            else:
                print "Fingerprint in active use (as reported) differs"
        elif len(fingerprint_hardcoded) > 5:
            print "NOT Verified. Supplied fingerprint ends {0}. 'In use' fingerprint ends {1}".format(
                fingerprint[-5:],
                used_fingerprint[-5:])
        else:
            print "NOT Verified. {0} differs from supplied fingerprint ending {1}".format(hardcoded_words,
                                                                                          fingerprint[-5:])
    return verified_flag


def minion_hardcode_given_fingerprint(fingerprint,verbose=False):
    """ Hardcode the supplied fingerprint into the config file of the minion """
    if verify_fingerprint_used_minion(fingerprint,verbose):
        if verbose:
            print "minion fingerprint in use matches {0}".format(fingerprint)
        return True
    return


def minion_hardcode_master_fingerprint(fingerprint='localhost',verbose=False):
    """ Fetch master fingerprint and hardcode it into the config file of the minion """
    if fingerprint is None:
        return False
    hardcoded_flag = False
    if fingerprint.startswith('local'):
        master_fingerprint = fetch_fingerprint('master',False,False)
        if len(master_fingerprint) > 5:
            if minion_hardcode_given_fingerprint(master_fingerprint,verbose):
                hardcoded_flag = True
        elif verbose:
            print "Localhost fingerprint invalid / missing {0}".format(master_fingerprint)
    return hardcoded_flag


def master_localhost_only(verbosity=1,master_filepath=ETC_MASTER):
    """ Set salt master to listen LOCALHOST ONLY for minion communication / control
    Use context_update style seds via context_update_localhost()
    """
    updated_flag = False
    with getset.Keyedcolon(master_filepath) as conf:
        if verbosity > 1:
            noncomment_listen = conf.lines_noncomment_containing_any(['interface','port'])
            # Above supplies a list to lines_noncomment_containing_any()
            print "Before change here are {0} lines / placeholders ...".format(len(noncomment_listen))
            print "... from master conf which contain master and may be relevant"
            for line in noncomment_listen:
                print line.rstrip()
        #update_res = conf.context_update('interface','localhost',2)
        update_res = conf.context_update_localhost('interface',2,True,cmdbin.LOOPBACK4)
        if len(update_res) < 1:
            if verbosity > 1:
                print "sed_remote_apply returned {0}".format(update_res)
        else:
            """ Context update completed so change recorded. Next apply it to remote """
            sedlines = conf.sedlines((verbosity>1))
            #apply_res = getset.sed_remote_apply('a'*32,master_filepath,sedlines,'medium')
            #apply_res = getset.sed_remote_apply('a'*32,master_filepath,sedlines,'medium',True,0,True)
            apply_res = getset.sed_remote_apply('a'*32,master_filepath,sedlines,'medium',True,0.75,True)
            if apply_res is True:
                updated_flag = True
            elif verbosity > 1:
                print "sed_remote_apply returned {0}".format(apply_res)

    if updated_flag is True:
        pass
    elif verbosity > 0:
        print "context_update_localhost on master conf did not complete as expected!"
        print "Was master already set localhost only before your request?"
    return updated_flag


def local_version(verbose=False,print_flag=False):
    """ Report output of --local test.version or test.versions_report """
    with hide('output'):
        if verbose is True:
            vres = cmdbin.run_or_sudo(CMD_SALT_CALL_VERSION_LOCAL_REPORT)
        else:
            vres = cmdbin.run_or_sudo(CMD_SALT_CALL_VERSION_LOCAL)
    local_output = cmdbin.run_res_lines_stripped(vres)
    if print_flag:
        for line in local_output:
            print line
    return local_output


def local_version_string():
    """ Wrapper around local_version to produce a string in every case """
    version = ''
    version_list = local_version(False)
    if len(version_list) > 0:
        version = version_list[0]
    return version


def controller_level(verbosity=0,master_filepath=ETC_MASTER,
                     minion_filepath=ETC_MINION):
    """ For salt setup where the master is acting as a controller to several
    minions. Current quality of salt setup (levels 0,1,2,3) 
    Set verbosity to 1 or 2 if you want printed feedback on quality level returned
    See local_level() for an alternative / variant.
    """
    return 0


def local_level(verbosity=0,master_filepath=ETC_MASTER,minion_filepath=ETC_MINION):
    """ For salt setup where there is a single localhost master and a single localhost minion
    Current quality of salt setup (levels 0,1,2,3) 
    Set verbosity to 1 or 2 if you want printed feedback on quality level returned
    See controller_level() for an alternative / variant.
    """
    quality_level = 0
    masterprint = fetch_fingerprint('master',False,False)
    listen_dict_master = {}
    if fingerprint_valid(masterprint):
        listen_dict_master = listen_dictionary('master')
        if 'interface' in listen_dict_master:
            if listen_dict_master['interface'] == cmdbin.LOOPBACK4:
                quality_level = 1
        if quality_level < 1 and verbosity > 1:
            print "Salt master does not have listen restricted to 127.0.0.1"
    elif verbosity > 1:
        print "Salt master fingerprint invalid? {0}".format(masterprint)
    """ The first quality point is earned for the master having
    a valid md5sum fingerprint and listening 'local only'.
    """
    if quality_level > 0:
        minion_dict = {}
        master_finger_value = None
        with getset.Keyedcolon(minion_filepath) as conf:
            if 'master_finger' in conf.keys_enabled():
                master_finger_value = conf['master_finger']
            """
            elif 'master_finger' in conf.keys_disabled(True):
                keyname_when_hashed = conf.when_hashed('interface',True,'original')
                master_finger_value = conf[keyname_when_hashed]
            """
            minion_dict = dict(conf.items())
            # Above we created a plain dictionary from conf.items
            # So that we do not have to keep fetching if just doing simple lookups
    """ The second quality point is earned for ... TODO
    """
    if quality_level > 1:
        pass
    if verbosity > 0:
        print "Salt current level is {0}".format(ZERO_ONE_TWO_THREE[quality_level])
    return quality_level


def quality_level(quality_level=None,verbosity=0):
    """ Set quality to supplied level provided it is not below existing level
    ( Care is taken not to reduce the quality below its existing level )

    Quality level is an arbitrary combination of Listen, keys, and security limiting.

    Call quality_level(3) to have your existing setup set to maximum quality.

    Rather than use this arbitrary combination, you can treat this function as
    a demonstrator and documenter of useful functions to call in establishing a salt setup.
    """
    if quality_level is None:
        return local_level(verbosity)
    elif quality_level < 1 or quality_level > 3:
        raise Exception("Quality level must be 1,2,3. Level supplied {0}".format(quality_level))
    return


def detailed_extra(target='master',print_flag=True):
    if target == 'master':
        filepath = ETC_MASTER
    else:
        filepath = ETC_MINION
    lines = None
    with getset.Keyedcolon(filepath) as conf:
        lines = conf.summary_detailed_extra(print_flag,filepath)
    return lines


def salt_conf_reset(target='minion',verbose=False):
    """ Reset configuration for master / minion using the original
    configuration file from the package.
    """
    if target == 'master':
        filepath = ETC_MASTER
    else:
        filepath = ETC_MINION
    from . import package_deb
    return package_deb.missing_conf_via_reinstall_middle(filepath)


def sysdoc_functions_search(module='file',pattern=None,local=True):
    if local:
        cmd_salt_call = CMD_SALT_CALL_LIST_FUNCTIONS_LOCAL
    else:
        cmd_salt_call = CMD_SALT_CALL_LIST_FUNCTIONS
    if pattern is not None and len(pattern.strip()) < 1:
        pattern = None
    matched = []
    with hide('output'):
        ros_res = cmdbin.run_or_sudo("{0} {1}".format(cmd_salt_call,module))
    if pattern is None:
        dashed = cmdbin.run_res_lines_stripped_searching(ros_res,'-')
        matched = [ match.strip().strip('-').strip() for match in dashed ]
    elif set(pattern).issubset(cmdbin.SET_PRINTABLE):
        patterned = cmdbin.run_res_lines_stripped_searching(ros_res,pattern)
        matched = [ match.strip().strip('-').strip()
                    for match in patterned if match.strip().startswith('-') ]
    else:
        pass
    return matched


def module_having_functions(module='file',print_flag=True):
    functions = sysdoc_functions_search(module,pattern=None,local=True)
    if len(functions) > 0:
        print "Salt --local reports {0} module having {1} functions".format(
            module,len(functions))
    else:
        print "Salt --local reports {0} module having {1} functions - missing?".format(
            module,len(functions))
    return


def sysdoc_functions_egrep(module='file',pattern=None,local=True):
    if local:
        cmd_salt_call = CMD_SALT_CALL_LIST_FUNCTIONS_LOCAL
    else:
        cmd_salt_call = CMD_SALT_CALL_LIST_FUNCTIONS
    if pattern is not None and len(pattern.strip()) < 1:
        pattern = None
    matched = []
    if pattern is None:
        with hide('output'):
            ros_res = cmdbin.run_or_sudo("{0} {1}".format(cmd_salt_call,module))
        matched = cmdbin.run_res_lines_stripped(ros_res)
    elif set(pattern).issubset(cmdbin.SET_PRINTABLE):
        with hide('output'):
            ros_res = cmdbin.run_or_sudo("{0} {1} | /bin/egrep '{2}'".format(
                    cmd_salt_call,module,pattern))
        patterned = cmdbin.run_res_lines_stripped(ros_res)
        matched = [ match.strip().strip('-').strip()
                    for match in patterned if match.strip().startswith('-') ]
    else:
        pass
    return matched
