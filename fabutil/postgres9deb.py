#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from collections import Counter,defaultdict,OrderedDict
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
#from fabric.contrib.files import exists,append
from os import path
import re
from string import ascii_lowercase,digits,printable,punctuation,whitespace
try:
    from . import cmdbin
except ValueError:
    import cmdbin
from . import conf_get_set as getset
from . import package_deb as pdeb
from . import active_file_net as afn
from ordered2dict import KeyedOD,KeyedEqualsOD


CMD_PSQL_AS_POSTGRES_SETTINGS = """su -l -c 'psql -A --pset={0}footer=off{0} --pset={0}pager=off{0} -c \
{0}select name,setting,context,vartype,boot_val,reset_val,category,unit from pg_settings;{0}' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ)
CMD_PSQL_AS_POSTGRES_SETTINGS_CONTEXT_ORDERED = """su -l -c 'psql -A --pset={0}footer=off{0} \
--pset={0}pager=off{0} -c \
{0}select name,setting,context,vartype,boot_val,reset_val,category,unit from pg_settings order by context,name,setting;{0}' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ)
# --pset={0}pager=off{0} -c \
CMD_PSQL_AS_POSTGRES_SETTINGS_CONTEXT_TEMPLATE = """su -l -c 'psql -A --pset={0}footer=off{0} \
--pset={0}pager=off{0} -c \
{0}select name,setting,context,vartype,boot_val,reset_val,category,unit from pg_settings where context={1}{2}0{3}{1} order by name,setting;{0}' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ,cmdbin.BRACES_OPEN,cmdbin.BRACES_CLOSE)
# internal (initialdeployment)
# postmaster (outofhours)
# sighup (onreload)
# backend (onreloadprogressive)
# superuser (anytimeprivileged)
# user (anytimeanyone)
CMD_PSQL_AS_POSTGRES_SETTINGS_DIFFER = """su -l -c 'psql -A --pset={0}footer=off{0} --pset={0}pager=off{0} -c \
{0}select name,setting,context,vartype,boot_val,reset_val,category,unit from pg_settings where setting <> boot_val;{0}' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ)
CMD_PSQL_AS_POSTGRES_SETTINGS_DIFFER_PLUS = """su -l -c 'psql -A --pset={0}footer=off{0} --pset={0}pager=off{0} -c \
{0}select name,setting,context,vartype,boot_val,reset_val,category,unit from pg_settings where setting <> boot_val or name = {1}data_directory{1};{0}' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ)
# chr(34) is double quote and chr(39) is single quote
""" One, Fifteen, then Nine but more likely you either One
or else you would be doing Fifteen then Nine
/bin/kill -1
/bin/kill -15, /bin/kill -TERM, /bin/kill -9, /bin/kill -KILL
"""
CMD_PSQL_AS_POSTGRES_LOGGING = """su -l -c 'psql -A --pset={0}footer=off{0} \
--pset={0}pager=off{0} -c \
{0}select name,setting,context,vartype,boot_val,reset_val,category,unit from pg_settings where name in ({1}log_destination{1},{1}log_directory{1},{1}log_duration{1},{1}logging_collector{1},{1}log_min_duration_statement{1},{1}log_min_error_statement{1},{1}log_statement{1});{0}' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ)
CMD_PG_DATA_MAIN_CLEAROUT = """su -l -c 'cd *.*/;rm -fR main/*' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ)
CMD_PG_BASEBACKUP_TEMPLATE = """su -l -c 'pg_basebackup -h {2}0{3} -D /var/lib/postgresql/{4}/main -U {5} -v -P -x' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ,cmdbin.BRACES_OPEN,cmdbin.BRACES_CLOSE,'9.1','remote100')
CMD_PG_DATA_MAIN_CLEAROUT2 = """su -l -c 'cd {2}0{3}/*.*/;rm -fR main/*' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ,cmdbin.BRACES_OPEN,cmdbin.BRACES_CLOSE)
CMD_PG_BASEBACKUP_TEMPLATE2 = """su -l -c 'pg_basebackup -h {2}0{3} -D {2}1{3}/{4}/main -U {5} -v -P -x' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ,cmdbin.BRACES_OPEN,cmdbin.BRACES_CLOSE,'9.1','remote100')

CMD_PG_CLIENT_ACTIVITY = """su -l -c 'psql -A --pset={0}footer=off{0} --pset={0}pager=off{0} -c \
{0}SELECT usename,application_name,client_addr,backend_start,query_start,current_query FROM pg_stat_activity;{0}' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ)
CMD_PG_CLIENT4_ACTIVITY = """su -l -c 'psql -A --pset={0}footer=off{0} --pset={0}pager=off{0} -c \
{0}SELECT usename,application_name,client_addr,backend_start, \
query_start,current_query FROM pg_stat_activity \
WHERE client_addr = {1}{2}0{3}{1};{0}' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ,cmdbin.BRACES_OPEN,cmdbin.BRACES_CLOSE)

CMD_TABLESPACE_SIZE = """su -l -c 'psql -A --pset={0}footer=off{0} --pset={0}pager=off{0} -c \
{0}SELECT spcname, pg_size_pretty(pg_tablespace_size(spcname)) FROM pg_tablespace \
WHERE spcname={1}{2}0{3}{1};{0}' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ,cmdbin.BRACES_OPEN,cmdbin.BRACES_CLOSE)
CMD_TABLESPACES_PRETTY = """su -l -c 'psql -A --pset={0}footer=off{0} --pset={0}pager=off{0} -c \
{0}SELECT spcname, pg_size_pretty(pg_tablespace_size(spcname)) FROM pg_tablespace;{0}' postgres
""".format(chr(34),cmdbin.SQBACKSLASHSQSQ)

PG_PORT = 5432
PG_HEADER_LINES_TO_SKIP = 1
PG9DEBIANDATADIR = '/var/lib/postgresql'
PG9DEBIANLOGDIR = '/var/log/postgresql'
PG9CONTEXT_DICT = {'initialdeployment': 'internal',
'outofhours': 'postmaster',
'onreload': 'sighup',
'onreloadprogressive': 'backend',
'anytimeprivileged': 'superuser',
'anytimeanyone': 'user'
}
PG9DEBIAN_DIFFER_PLUS_PREFERRED = """ differ should produce a conf < 50 lines generally
You might prefer differ_plus if you are running Debian
and/or want a minimal conf that can support Postgresql
as differ_plus forces automatic inclusion of 
the directive data_directory
"""

# su -l -c 'psql -A --pset="footer=off" -c "select name,setting,context,vartype,boot_val,reset_val,category
# from pg_settings where name in ('\''log_destination'\'','\''log_directory'\'','\''log_duration'\'',
# '\''logging_collector'\'','\''log_min_duration_statement'\'','\''log_min_error_statement'\'',
# '\''log_statement'\'');"' postgres


def postgres_logfile_real(dir_to_list,return_requested='lines',verbosity=0,print_flag=False):
    """
    pg.active_file_reg_from_dir signature is:
    dir_to_list='/var/log/postgresql',return_requested='lines',
    verbosity=0,print_flag=False)
    """
    if dir_to_list is None:
        dir_to_list = PG9DEBIANLOGDIR
        print_flag = True
    return afn.active_file_reg_from_dir(dir_to_list,return_requested=return_requested,
                                        verbosity=verbosity,print_flag=print_flag)


def postgres_logging_summary_as_list(dir_to_list=None,print_flag=False):
    """ Return a list of logging related directives
    """

    logging_list = []
    with hide('output'):
        ros_res = cmdbin.run_or_sudo(CMD_PSQL_AS_POSTGRES_LOGGING)
        logging_lines = cmdbin.run_res_lines_stripped(ros_res)
        logging_list = cmdbin.sql_pipe_delimited_to_list_of_lists(8,1,logging_lines,1)

    if print_flag is True:
        for log_directive_list in logging_list:
            print(log_directive_list)
    return logging_list
    

def postgres_logging_summary_as_dict(dir_to_list=None,print_flag=False):
    """ setting - current setting
    boot_val - default setting from compiled in or configuration file or similar
    reset_val - value that will be set if you choose to RESET this variable
    """

    logging_dict = {}
    with hide('output'):
        ros_res = cmdbin.run_or_sudo(CMD_PSQL_AS_POSTGRES_LOGGING)
        logging_lines = cmdbin.run_res_lines_stripped(ros_res)
        logging_dict = cmdbin.sql_pipe_delimited_to_dict_of_lists(8,1,logging_lines,1)

    if print_flag is True:
        for log_directive_list in logging_dict:
            print(log_directive_list)
    return logging_dict


def postgres_logging_summary_to_conf(dir_to_list=None,print_flag=False):

    logging_dict = {}
    with hide('output'):
        ros_res = cmdbin.run_or_sudo(CMD_PSQL_AS_POSTGRES_LOGGING)
        logging_lines = cmdbin.run_res_lines_stripped(ros_res)
        #logging_list = cmdbin.sql_pipe_delimited_to_dict_of_lists(8,1,logging_lines,1)
        logging_dict = cmdbin.sql_pipe_delimited_to_remote_conf(8,1,2,logging_lines,1)

    if print_flag is True:
        for log_directive_list in logging_dict:
            print(log_directive_list)
    return logging_dict


def postgres_settings_context_as_list(pcontext='sighup',print_flag=False):

    settings_list = []
    if pcontext in ['internal','postmaster','sighup','backend','superuser','user']:
        pass
    elif pcontext in PG9CONTEXT_DICT:
        pcontext = PG9CONTEXT_DICT[pcontext]
    else:
        return settings_list

    cmd_sql = CMD_PSQL_AS_POSTGRES_SETTINGS_CONTEXT_TEMPLATE.format(pcontext)
    with hide('output'):
        ros_res = cmdbin.run_or_sudo(cmd_sql)
        settings_lines = cmdbin.run_res_lines_stripped(ros_res)
        settings_list = cmdbin.sql_pipe_delimited_to_list_of_lists(8,1,settings_lines,1)

    if print_flag is True:
        for log_directive_list in settings_list:
            print(log_directive_list)

    return settings_list


def postgres_settings_order_list(print_flag=False):

    settings_list = []
    cmd_sql = CMD_PSQL_AS_POSTGRES_SETTINGS_CONTEXT_ORDERED
    with hide('output'):
        ros_res = cmdbin.run_or_sudo(cmd_sql)
        settings_lines = cmdbin.run_res_lines_stripped(ros_res)
        settings_list = cmdbin.sql_pipe_delimited_to_list_of_lists(8,1,settings_lines,1)

    if print_flag is True:
        for log_directive_list in settings_list:
            print(log_directive_list)

    return settings_list


def postgres_settings_list(print_flag=False):

    settings_list = []
    cmd_sql = CMD_PSQL_AS_POSTGRES_SETTINGS
    with hide('output'):
        ros_res = cmdbin.run_or_sudo(cmd_sql)
        settings_lines = cmdbin.run_res_lines_stripped(ros_res)
        settings_list = cmdbin.sql_pipe_delimited_to_list_of_lists(8,1,settings_lines,1)

    if print_flag is True:
        for log_directive_list in settings_list:
            print(log_directive_list)

    return settings_list


def postgres_settings_order_dict(print_flag=False):

    settings_dict = {}
    cmd_sql = CMD_PSQL_AS_POSTGRES_SETTINGS_CONTEXT_ORDERED
    with hide('output'):
        ros_res = cmdbin.run_or_sudo(cmd_sql)
        settings_lines = cmdbin.run_res_lines_stripped(ros_res)
		#sql_pipe_delimited_to_dict_of_lists(column_count,key1indexed,output_lines,lines_to_skip)
        settings_dict = cmdbin.sql_pipe_delimited_to_dict_of_lists(8,1,settings_lines,1)

    if print_flag is True:
        for directive_dict in settings_dict:
            print(directive_dict)

    return settings_dict


def postgres_settings_dict(print_flag=False):
	""" Form a settings dictionary representing the settings for Postgresql.
	When print_flag is set True then keys of the dictionary are printed
	"""

	settings_dict = {}
	cmd_sql = CMD_PSQL_AS_POSTGRES_SETTINGS
	with hide('output'):
		ros_res = cmdbin.run_or_sudo(cmd_sql)
		settings_lines = cmdbin.run_res_lines_stripped(ros_res)
		settings_dict = cmdbin.sql_pipe_delimited_to_dict_of_lists(8,1,settings_lines,1)

	if print_flag is True:
		for directive_dict in settings_dict:
			print(directive_dict)

	return settings_dict


def postgres_settings_local_tmp(verbose=False,sortkeys=False):
	""" Output settings to /tmp/ on local machine.
	"""
	written_count = 0

	with hide('output'):
		ros_res = cmdbin.run_or_sudo(CMD_PSQL_AS_POSTGRES_SETTINGS)
		settings_lines = cmdbin.run_res_lines_stripped(ros_res)
		key1indexed = 1
		keyed = KeyedEqualsOD(8,key1indexed,settings_lines)
		keyed.populate_from_lines(PG_HEADER_LINES_TO_SKIP)

	if len(keyed) > 0:
		written_count = keyed.to_local_tmp(sortkeys)

	return written_count


def postgres_settings_local_tmp1(verbose=False,sortkeys=False):
	""" Output settings to /tmp/ on local machine.
    Will print one line of confirmation on success hence the name tmp1
	"""
	written_count = 0

	with hide('output'):
		ros_res = cmdbin.run_or_sudo(CMD_PSQL_AS_POSTGRES_SETTINGS)
		settings_lines = cmdbin.run_res_lines_stripped(ros_res)
		key1indexed = 1
		keyed = KeyedEqualsOD(8,key1indexed,settings_lines)
		keyed.populate_from_lines(PG_HEADER_LINES_TO_SKIP)

	if len(keyed) > 0:
		written_count = keyed.to_local_tmp(sortkeys)
		print("Postgresql conf ( {0} lines ) written to your local machine".format(
				written_count))

	return written_count


def postgres_settings_local_tmp_pcontext(verbose=False,pcontext=None,sortkeys=False):
	""" Output settings to /tmp/ on local machine.
	Restrict query to the given context (pcontext) such as superuser
	"""
	settings_list = []
	if pcontext in ['internal','postmaster','sighup','backend','superuser','user']:
		cmd_sql = CMD_PSQL_AS_POSTGRES_SETTINGS_CONTEXT_TEMPLATE.format(pcontext)
	elif pcontext in PG9CONTEXT_DICT:
		pcontext = PG9CONTEXT_DICT[pcontext]
		cmd_sql = CMD_PSQL_AS_POSTGRES_SETTINGS_CONTEXT_TEMPLATE.format(pcontext)
	elif sortkeys is True:
		cmd_sql = CMD_PSQL_AS_POSTGRES_SETTINGS_CONTEXT_ORDERED
	else:
		cmd_sql = CMD_PSQL_AS_POSTGRES_SETTINGS

	with hide('output'):
		ros_res = cmdbin.run_or_sudo(cmd_sql)
		settings_lines = cmdbin.run_res_lines_stripped(ros_res)
		key1indexed = 1
		keyed = KeyedEqualsOD(8,key1indexed,settings_lines)
		keyed.populate_from_lines(PG_HEADER_LINES_TO_SKIP)

	if len(keyed) < 1:
		return settings_dict

	written_count = keyed.to_local_tmp(sortkeys)

	return written_count


def postgres_settings_local_tmp_log_only(verbose=False,sortkeys=False):
	""" Output settings to /tmp/ on local machine.
	Restrict the query results to show only those with a key including 'log'
	"""
	written_count = 0

	with hide('output'):
		ros_res = cmdbin.run_or_sudo(CMD_PSQL_AS_POSTGRES_SETTINGS)
		settings_lines = cmdbin.run_res_lines_stripped(ros_res)
		key1indexed = 1
		keyed = KeyedEqualsOD(8,key1indexed,settings_lines)
		keyed.populate_from_lines(PG_HEADER_LINES_TO_SKIP)

	if len(keyed) > 0:
		written_count = keyed.to_local_tmp_log_only(sortkeys)

	return written_count


def postgres_settings_local_tmp_wal_only(verbose=False,sortkeys=False):
	""" Output settings to /tmp/ on local machine.
	Restrict the query results to show only those with a keys related to
	wal.
	pg_xlog is subdirectory containing WAL (Write Ahead Log) files
	pg_xlog/ can be a symbolic link pointing to someplace outside the cluster directory,
	having pg_xlog as symbolic link as described is a common setup anyway for performance reasons.

	If wal_level > minimal then you should probably have an archive command set and the following:
	  archive_mode = on
	Streaming but forgetting to set up wal archiving while also
	leaving wal_keep_segments low, might not support streaming the way you require.
	"""
	written_count = 0

	with hide('output'):
		ros_res = cmdbin.run_or_sudo(CMD_PSQL_AS_POSTGRES_SETTINGS)
		settings_lines = cmdbin.run_res_lines_stripped(ros_res)
		key1indexed = 1
		keyed = KeyedEqualsOD(8,key1indexed,settings_lines)
		keyed.populate_from_lines(PG_HEADER_LINES_TO_SKIP)

	if len(keyed) > 0:
		written_count = keyed.to_local_tmp_wal_only(sortkeys)

	return written_count


def postgres_settings_local_tmp_differ(verbose=True,sortkeys=False):
	""" Output settings to /tmp/ on local machine.
	Restrict the query results to show only those with a setting value
	that differs from the field 'boot_val'
	"""
	written_count = 0

	with hide('output'):
		ros_res = cmdbin.run_or_sudo(CMD_PSQL_AS_POSTGRES_SETTINGS_DIFFER)
		settings_lines = cmdbin.run_res_lines_stripped(ros_res)
		key1indexed = 1
		keyed = KeyedEqualsOD(8,key1indexed,settings_lines)
		keyed.populate_from_lines(PG_HEADER_LINES_TO_SKIP)

	if len(keyed) > 0:
		written_count = keyed.to_local_tmp(sortkeys)
		if verbose:
			print(PG9DEBIAN_DIFFER_PLUS_PREFERRED)

	return written_count


def postgres_settings_local_tmp_differ_plus(verbose=False,sortkeys=False,selective=False):
	""" Output settings to /tmp/ on local machine.

	Restrict the query results to show only those with a setting value
	that differs from the field 'boot_val'

	Additionally we force inclusion of the setting name 'data_directory'
	as this is mandatory for Debian systems to support /usr/bin/pg_ctlcluster

	selective=True will see a filtering to prevent the following Debian messages:
	FATAL:  parameter "lc_collate" cannot be changed
	FATAL:  parameter "lc_ctype" cannot be changed
	FATAL:  parameter "server_encoding" cannot be changed
	"""
	written_count = 0

	with hide('output'):
		ros_res = cmdbin.run_or_sudo(CMD_PSQL_AS_POSTGRES_SETTINGS_DIFFER_PLUS)
		settings_lines = cmdbin.run_res_lines_stripped(ros_res)
		key1indexed = 1
		keyed = KeyedEqualsOD(8,key1indexed,settings_lines)
		keyed.populate_from_lines(PG_HEADER_LINES_TO_SKIP)
		if selective:
			keyed.pop('lc_collate',None)
			keyed.pop('lc_ctype',None)
			keyed.pop('server_encoding',None)
		#if selective:
		#	print(settings_lines[0])

	if len(keyed) > 0:
		written_count = keyed.to_local_tmp(sortkeys)

	return written_count


def postgres_base_backup_from_given_master1(master4=None):
	""" Single argument version of basebackup that is used when data_dir is
	supplied and differs from PG9DEBIANDATADIR
	"""
	db_result = False
	cmd = None
	if len(set(master4)) == len(set(master4).intersection(cmdbin.SET_DIGITS_AND_PERIOD)):
		cmd = CMD_PG_BASEBACKUP_TEMPLATE.format(master4)
	if cmd is not None:
		clearout_res = cmdbin.run_or_sudo(CMD_PG_DATA_MAIN_CLEAROUT)
		if clearout_res.succeeded:
			basebackup_res = cmdbin.run_or_sudo(cmd)
			if basebackup_res.succeeded:
				db_result = True
	return db_result


def postgres_base_backup_from_given_master2(master4=None,data_dir=None):
	""" Two argument version of basebackup that is used when data_dir is
	supplied and differs from PG9DEBIANDATADIR
	"""
	db_result = False
	cmd = None
	if len(set(master4)) == len(set(master4).intersection(cmdbin.SET_DIGITS_AND_PERIOD)):
		cmd = CMD_PG_BASEBACKUP_TEMPLATE2.format(master4,data_dir)
	if cmd is None:
		return db_result
	if data_dir is not None and len(data_dir) > 2:
		cmd_clearout = CMD_PG_DATA_MAIN_CLEAROUT2.format(data_dir)
		clearout_res = cmdbin.run_or_sudo(cmd_clearout)
		if clearout_res.succeeded:
			basebackup_res = cmdbin.run_or_sudo(cmd)
			if basebackup_res.succeeded:
				db_result = True
	return db_result


def postgres_base_backup_from_given_master(master4=None,data_dir=None):
	""" Selector/Wrapper around a one argument version or two argument version
	of basebackup caller.
	"""
	if data_dir is None:
		return postgres_base_backup_from_given_master1(master4)
	elif data_dir == PG9DEBIANDATADIR:
		return postgres_base_backup_from_given_master1(master4)
	return postgres_base_backup_from_given_master2(master4,data_dir)


def connections_active():
	db_result = False
	activity_res = cmdbin.run_or_sudo(CMD_PG_CLIENT_ACTIVITY)
	if activity_res.succeeded:
		db_result = True
	return db_result


def connections_active4(client4=None):
	""" 
	client4 is expected to be an ipv4 address in string aa.bb.cc.dd format
	"""
	db_result = False
	cmd_activity = None
	if len(set(client4)) == len(set(client4).intersection(cmdbin.SET_DIGITS_AND_PERIOD)):
		cmd_activity = CMD_PG_CLIENT4_ACTIVITY.format(client4)
	if cmd is None:
		return db_result

	activity_res = cmdbin.run_or_sudo(cmd_activity)
	if activity_res.succeeded:
		db_result = True
	return db_result


def tablespace_size(tspacename=None):
	db_result = False
	if tspacename is None or len(tspacename) < 2:
		return db_result

	tspace_list = []

	cmd_sql = CMD_TABLESPACE_SIZE.format(tspacename)
	tspace_res = cmdbin.run_or_sudo(cmd_sql)
	tspace_lines = cmdbin.run_res_lines_stripped(tspace_res)
	tspace_list = cmdbin.sql_pipe_delimited_to_list_of_lists(2,1,tspace_lines,1)

	return tspace_list


def tablespace_sizes(tspacename=None):

	if tspacename is not None and len(tspacename) > 1:
		return tablespace_size(tspacename)

	tspace_list = []

	with hide('output'):
		tspace_res = cmdbin.run_or_sudo(CMD_TABLESPACES_PRETTY)
        tspace_lines = cmdbin.run_res_lines_stripped(tspace_res)
        tspace_list = cmdbin.sql_pipe_delimited_to_list_of_lists(2,1,tspace_lines,1)

	return tspace_list


def tablespace_sizes_print(tspacename=None):

	db_result = False

	if tspacename is not None and len(tspacename) > 1:
		tspace_list = tablespace_size(tspacename)
		for named_list in tspace_list:
			print(named_list)
		return tspace_list

	tspace_list = []

	with hide('output'):
		tspace_res = cmdbin.run_or_sudo(CMD_TABLESPACES_PRETTY)
        tspace_lines = cmdbin.run_res_lines_stripped(tspace_res)
        tspace_list = cmdbin.sql_pipe_delimited_to_list_of_lists(2,1,tspace_lines,1)

	for named_list in tspace_list:
		print(named_list)

	return tspace_list


def tablespace(mode='size',tspacename=None):
	return tablespace_sizes(tspacename)


def postgres_passline(fieldnum=None,verbose=False):
	""" Postgres entry in password file.
	Seven fields with 6th (index 5) being directory and 7th (index 6) being shell
	chr(58) is colon (:)
	"""
	line_array = []
	if fieldnum is None:
		get_res = run(cmdbin.CMD_POSTGRES_PASSLINE)
		if get_res.succeeded:
			try:
				line = cmdbin.run_res_lines_stripped(get_res)[0]
				line_array = line.split(chr(58))
			except:
				line_array = []
		if verbose is True:
			print(line)
	elif fieldnum < 0:
		return line_array

	get_res = run(cmdbin.CMD_POSTGRES_PASSLINE)
	if get_res.succeeded:
		try:
			line_array = cmdbin.run_res_lines_stripped(get_res)[0].split(chr(58))
		except:
			line_array = []

	fieldval = ''
	if len(line_array) > 0:
		try:
			fieldval = line_array[fieldnum]
		except:
			fieldval = ''
		if verbose is True:
			print(fieldval)

	return line_array
	
