#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015,2014,2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f debian.py -u root -H aa.bb.cc.dd sshd_summary
# fab -a -f debian.py -u root -H aa.bb.cc.dd netselect_apt
# fab -a -f debian.py -u root -H aa.bb.cc.dd netselect_apt:false
# fab -a -f debian.py -u root -H aa.bb.cc.dd nameserver_check
# fab -a -f debian.py -u root -H aa.bb.cc.dd nameserver_check:domain=wrightsolutions.co.uk
# fab -a -f debian.py -u root -H aa.bb.cc.dd en_gb_and_archive
# fab -a -f debian.py -u root -H aa.bb.cc.dd en_gb_and_archive_permanent
# fab -a -f debian.py -u root -H aa.bb.cc.dd en_gb_and_archive_permanent:verbose=true
# fab -a -f debian.py -u root -H aa.bb.cc.dd list_archive_contents_en:verbose=true
# fab -a -f debian.py -u root -H aa.bb.cc.dd varnish_port:port=6081
# fab -a -f debian.py -u root -H aa.bb.cc.dd apache2ports
# fab -a -f debian.py -u root -H aa.bb.cc.dd apache2potential
# fab -a -f debian.py -u root -H aa.bb.cc.dd apache2max2examples
# fab -a -f debian.py -u root -H aa.bb.cc.dd apache2max2examples:maxspare=15
# fab -a -f debian.py -u root -H aa.bb.cc.dd installed_size
# fab -a -f debian.py -u root -H aa.bb.cc.dd installed_size:packagename=locales
# fab -a -f debian.py -u root -H aa.bb.cc.dd installed_size:verbose=false
# fab -a -f debian.py -u root -H aa.bb.cc.dd installed_size:alphabetical=true
# fab -a -f debian.py -u root -H aa.bb.cc.dd installed_size:section=editors
# fab -a -f debian.py -u root -H aa.bb.cc.dd dget_build:dscurl=ftparchive.gplhost.com/debian/pool/icehouse-backports/main/p/python-psutil/python-psutil_1.2.1-1~bpo70+1.dsc
# fab -a -f debian.py -u root -H aa.bb.cc.dd sudoers2remote
# fab -a -f debian.py -u root -H aa.bb.cc.dd sudoers_mismatches


import fabutil as futil
from fabutil import conf_get_set as getset
# import pprint; pp2 = pprint.PrettyPrinter(indent=2)


def first_arg_false_to_false(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is True or first_arg in ['true','True','TRUE']:
                # Ensure we never interfere with any first arg intended as positive boolean
                pass
            elif first_arg is False:
                # This branch will likely never be encountered
                pass
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def first_arg_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        print 'Wrapped function', function_to_decorate.__name__
        if len(args) > 0:
            print 'Wrapped function', function_to_decorate.__name__, 'has args'
            first_arg = args[0]
            if first_arg is True:
                # Ensure we never interfere with any first arg intended as positive boolean
                print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as True'
                pass
            elif first_arg is False:
                # Ensure we never interfere with any first arg intended as negative boolean
                print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as False'
                pass
            elif first_arg in ['true','True','TRUE']:
                print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to True'
                args_list = list(args)
                args_list[0] = True
                args = tuple(args_list)
            elif first_arg in ['false','False','FALSE']:
                print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to False'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if 'verbose' in kwargs:
            #print "Wrapped function", function_to_decorate.__name__, "has kwarg 'verbose'"

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as True'
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as False'
                pass
            elif verbose_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to True'
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to False'
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


@first_arg_false_to_false
def print_preamble(backports_epel_report=True):
    """ Print preamble showing some short fingerprint style markers for the server.
    Even if we know we are Debian or RPM based, we might
    still distinguish servers based on availability of extra repositories.
    backports_epel_true=True (default) gives you some feedback about extra
    repositories.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print pretty_tuple
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    if not backports_epel_report:
        # Do not bother reporting about backports / epel
        pass
    elif pretty_tuple[1] == 'debian':
        print "Backports: {0}".format(futil.package_deb.has_backports())
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        print "EPEL: {0}".format(futil.package_rpm.has_epel())
        print futil.package_rpm.repolist_dict()
    if backports_epel_report:
        """ Consider backports_epel_report similar to verbose=true
        way of requesting more info """
        futil.selinuxstatus.sestatus1print()
    return


def sshd_summary_verbose():
    """ Print ssh summary - list 'enabled' ssh config lines """
    for line in futil.ssh_lokkit.sshd_config_keylines_enabled():
        print line
    #print futil.ssh_lokkit.sshd_config_keylines_enabled_linesep()
    print futil.ssh_lokkit.sshd_motd_yes_preview(verbose=True)
    return


def sshd_summary():
    """ Print ssh summary - shorter list of selected ssh config lines
    futil.ssh_lokkit.sshd_config_summary(True,'/etc/sshd/sshd_config ')
    """
    futil.ssh_lokkit.sshd_config_summary(True,'filepath')
    return


def iptables_save():
    """ Request remote server saves its iptables entries to a file. """
    return futil.iptables_firewall.iptables_save()


def iptables_save_verbose():
    """ Request remote server saves its iptables entries to a file (and report count) """
    return futil.iptables_firewall.iptables_save(verbose_counts=True)


@first_arg_false_to_false
def en_gb_and_archive(verbose=True):
    """ Non-destructive en_GB locale generation on remote server """
    archive_res = futil.locale_deb.en_gb_and_archive(False,int(verbose))
    # Above we set first param False so non-destructive
    return archive_res


@first_arg_forced_boolean
@verbose_forced_boolean
def en_gb_and_archive_permanent(verbose=False):
    """ en_GB locale generation & save to archive - permanent change to default locale """
    archive_res = futil.locale_deb.en_gb_and_archive(True,int(verbose))
    # Above we set first param True so locale change is made permanent
    return archive_res


@first_arg_forced_boolean
@verbose_forced_boolean
def list_archive_contents_en(verbose=True):
    """ locale archive - list contents relevant to 'en' like """
    archive_res = futil.locale_deb.list_archive_contents_en(verbose)
    """ if verbose:
        print archive_res """
    return archive_res


def sysstat_regular():
    """ Enable system statistics (sysstat)
    Reminder about the contract of conf_get_set.py and order or operations next...
    Do the queries first. Do use the reliable write features (when implemented)
    However you are discouraged from performing updates and then attempt to query things further.

    No contract is given here that detailed queries will be reliable once updates
    have taken place. Reiterating to clarify: Query, Update (optional), Write the changes
    """
    conf_filepath = '/etc/default/sysstat'
    sysstat_return = False
    with getset.Keyedequals(conf_filepath) as conf:
        print conf.lines_current_simple()
        if 'ENABLED' in conf:
            #print conf.toggle_safe('ENABLED','true',True,1)
            toggle_res = conf.toggle_safe('ENABLED','"true"',True)
            if toggle_res:
                print conf.lines_for_put_containing('ENABLED')
                sedlines = conf.sedlines(True)
                #sedres = getset.sed_remote_apply('a'*32,conf_filepath,sedlines,'medium')
                #sedres = getset.sed_remote_apply(None,conf_filepath,sedlines,'medium',True,0.75,True)
                #md5true = '72a09b2f30c4b1cab7e9a366ee50988c'
                # md5current is what we supply so md5current=md5false
                md5false = '6f015cbda1bb151c4bcd0ba1b84c2568'
                """ /etc/default/sysstat is almost all comments so disable ratio of changes checking by
                supplying 1.0 instead of 0.75
                #sedres = getset.sed_remote_apply(md5false,conf_filepath,sedlines,'medium',True,0.75,True)
                #sedres = getset.sed_remote_apply(md5false,conf_filepath,sedlines,'medium',True,0,True)
                sedres = getset.sed_remote_apply(md5false,conf_filepath,sedlines,'medium',True,1.0,True)
                """
                sedres = getset.sed_remote_apply(md5false,conf_filepath,sedlines,'medium',True,1.0,True)
                if sedres is True:
                    sysstat_return = True
                else:
                    print "sed_remote_apply returned {0}".format(sedres)
                print conf.lines_for_put_containing('ENABLED')
                print conf.lines_current_containing('ENABLED')
                sedline = conf.sedlines(True)
                #print conf.lines_for_put_simple()
            else:
                print 'Is sysstat already ENABLED="true"? toggle_safe returned {0}'.format(toggle_res)
    return sysstat_return


def sysstat_regular_preview():
    """ Verbose / debug version of sysstat_regular()
    which supplements the test suite """
    with getset.Keyedequals('/etc/default/sysstat') as conf:
        #print conf.tuples_all
        #return conf.keylines_typed(2,['ENABLED'])
        if 'ENABLED' in conf:
            #val = conf['ENABLED']
            print conf.keylines(['ENABLED'])
            #if val in conf.FALSELIST
            print conf.tuples_all_from_key('ENABLED')
            print "Expect next 3 booleans to read False,True,False"
            print conf.toggle_safe('ENABLED','true',False)
            print conf.toggle_safe('ENABLED','true',True,1) # (says safe=True and autoquote_level=1)
            print conf.toggle_safe('ENABLED','true',False)
            #for sed in conf.list_of_sedlines: print sed
            for linenum,sed in conf.dict_of_seds.iteritems():
                print "{0}    {1}".format(linenum,sed)
            lines_current = conf.lines_current_simple()
            print len(lines_current)
            print "Above was lines_current ; next is put_lines"
            put_lines = conf.lines_for_put()
            print len(put_lines)
            scriptlines,md5after,lines = conf.sed_checksum(conf.list_of_sedlines,
                                                           debug_filename='sysstat_regular_preview')
            if md5after is not None:
                print md5after
            conf.dequeue_all()
            print len(conf.dict_of_seds)
            #print conf.sedline('ENABLED','yes')
    return


@first_arg_false_to_false
def netselect_apt(verbose=False):
    """ Non destructive use of netselect-apt to generate sources.list recommended mirror """
    print_preamble()
    return futil.package_deb.netselect_apt(verbose)


def apache2ports():
    """ Report port extracts from apache configuration.
    conf_get_set has no concept of nesting or sections so is probably not
    well suited to trying to toggle values in apache2 configurations.
    However if you want to just treat the whole file as a flat structure and
    ask for what conf_get_set thinks is enabled, then what you will see
    returned is the LAST value of Listen and the LAST value of NameVirtualHost
    """
    with getset.Keyedspace2('/etc/apache2/ports.conf') as conf:
        if 'Listen' in conf:
            print conf['Listen']
        print "Now casting net wider we obtain the following"
        #print conf.keylist_frozen
        for conf_k in conf:
            print conf_k
            #print conf[conf_k]
        print conf.keylines_typed(1,['Listen'])
        for line_listen in conf.lines_enabled_contains('Listen'):
            print line_listen
        #print conf.keylines_typed(2,['NameVirtualHost'])
        for tup in conf.tuples_all:
            if tup[0] is not None and 'Listen' in tup[0]:
                print tup


def apache2max2examples(maxspare=10):
    """ Update 2 entries in a typical Debian apache2.conf including MaxSpareServers.
    The MaxSpareServers change would have no effect if you were
    running an mpm other than prefork.
    """
    maxspare_int = 0
    try:
        maxspare_int = int(maxspare)
    except ValueError:
        maxspare_int = -1
    if maxspare_int < 5 or maxspare_int > 100:
        return False
    max2return = False
    conf_filepath = '/etc/apache2/apache2.conf'
    """ with getset.Keyedspace(conf_filepath) as conf:
    Use Keyedspace2() if you are wanting whitespace between
    keys and values preserved exactly
    """
    with getset.Keyedspace2(conf_filepath) as conf:
        spares_return = conf.sedline_safe_wrapper0('MaxSpareServers',maxspare_int)
        #print spares_return,spares_return,spares_return
        requests_return = conf.sedline_safe_wrapper0('MaxKeepAliveRequests',500)
        if spares_return and requests_return:
            print 'Both Max changes have stored seds'
            md5apache22ver22 = '95c489c44ce638dca8c44be18ebc8353'
            sedlines = conf.sedlines(True)
            print len(sedlines),sedlines
            """ Note about the first argument to sed_remote_apply()
            The first argument is named md5current to indicate that the md5sum
            you supply (if not None) is the md5sum of the file with its CURRENT CONTENT
            """
            verbose = True
            sedres = getset.sed_remote_apply(md5apache22ver22,conf_filepath,conf.sedlines(),
                                             'high',True,0.20,verbose)
            if sedres is True:
                max2return = True
            else:
                print "sed_remote_apply returned {0}".format(sedres)

        else:
            print 'One or both Max changes did not complete'
    return max2return


def apache2potential(verbose=True):
    """ Apache2 MPM and Thread summary - some indicators of potential performance """
    futil.web_wsgi.apache2mpm_lines(True)
    threads = futil.web_wsgi.apache2threads('www-data',True)
    return threads


def nameserver_check(domain=None,verbose=False):
    """ Count nameserver entries and test a nameserver lookup """
    futil.dns.nameserver_count_excluding_print(futil.dns.GOOGLELIST)
    #futil.dns.nameserver_count_excluding_print('google')
    if verbose:
        futil.dns.resolvconf_dirs_more()
    futil.dns.nameserver_count_startswith('192.',True,True)
    futil.dns.drill_or_dig_domain(domain)
    spf_lines = futil.dns.drill_or_dig_spf('random',verbose,True)
    if len(spf_lines) > 0:
        pass
    elif domain is not None:
        futil.dns.drill_or_dig_txt(domain,verbose)
    return


@verbose_forced_boolean
def installed_size(packagename=None,verbose=True,alphabetical=False,section=None):
    if alphabetical in ['true','True','TRUE']:
        alphabetical = True
    else:
        alphabetical = False
    sizes_available = False
    sizes = futil.package_deb.dpkg_size(packagename,verbose,alphabetical,section)
    if len(sizes) > 0:
        sizes_available = True
        for line in sizes:
            print line
    return sizes_available


def varnish_port_unforgiving(port=None,verbose=True):
    """ For varnish configured with DAEMON_OPTS in multiline form - set port to given.
    Note: If the multiline form of DAEMON_OPTS is not in use and / or varnish is
    configured using other techniques then the sed change this produces would not
    be appropriate.

    Assumes that the port is currently 6081 and you want to change it to something else.
    """
    if port is None:
        return False
    port_int = 0
    try:
        port_int = int(port)
    except ValueError:
        port_int = -1
    if port_int < 80:
        return False
    elif port_int == 6082:
        # Avoid clashing with varnish 'control port' Authentication required
        return False
    varnish_return = False
    conf_filepath = '/etc/default/varnish'
    with getset.Keyedequals(conf_filepath) as conf:
        keyname = 'DAEMON_OPTS'
        new_value = '"-a :{0:d} \\'.format(port_int)
        if conf.sedline_safe_wrapper(keyname,new_value,None):
            md5port6081 = '189f82b61ddefb8fe0f0eb238f6ca239'
            #sedlines = conf.sedlines(True)
            """ Note about the first argument to sed_remote_apply()
            The first argument is named md5current to indicate that the md5sum
            you supply (if not None) is the md5sum of the file with its CURRENT CONTENT
            """
            sedres = getset.sed_remote_apply(md5port6081,conf_filepath,conf.sedlines(),
                                             'high',True,0.25,verbose)
            if sedres is True:
                varnish_return = True
            else:
                print "sed_remote_apply returned {0}".format(sedres)
    return varnish_return


def varnish_port(port=None,verbose=True):
    """ For varnish configured with DAEMON_OPTS in multiline form - set port to given even if unchanged.

    Note: If the multiline form of DAEMON_OPTS is not in use and / or varnish is
    configured using other techniques then the sed change this produces would not
    be appropriate.

    Example1: port is currently 6081 and you ask for port=6088 - result is success
    Example2: port is currently 6081 and you ask for port=6081 - result is success
    Unlike varnish_port_unforgiving() Example2 would not give a failure.
    Unlike varnish_port_unforgiving() we supply None as first argument to sed_remote_apply() to
    indicate we do not want to do an md5sum check on the current file before the change takes place.

    This function is a good illustration of how to use \\ to deal with lines in
    the remote configuration that contain continuation characters.
    """
    if port is None:
        return False
    port_int = 0
    try:
        port_int = int(port)
    except ValueError:
        port_int = -1
    if port_int < 80:
        return False
    elif port_int == 6082:
        # Avoid clashing with varnish 'control port' Authentication required
        return False
    varnish_return = False
    conf_filepath = '/etc/default/varnish'
    with getset.Keyedequals(conf_filepath) as conf:
        keyname = 'DAEMON_OPTS'
        print conf[keyname]
        new_value = '"-a :{0:d} \\'.format(port_int)
        if conf.sedline_safe_wrapper0(keyname,new_value,None):
            """ Using conf.sedlines_containing() as a check is usually
            unnecessary when using sedline_safe_wrapper() but here
            we are using sedline_safe_wrapper0() instead.
            """
            if len(conf.sedlines_containing(port)) == 0:
                # No changes to make on remote system
                varnish_return = True
            else:
                # Make the change on the remote system
                #sedlines = conf.sedlines(True)
                sedres = getset.sed_remote_apply(None,conf_filepath,conf.sedlines(),
                                                 'medium',True,0.50,verbose)
                if sedres is True:
                    varnish_return = True
                else:
                    print "sed_remote_apply returned {0}".format(sedres)
        else:
            print "port change not actioned"
    return varnish_return


def dget_build(dscurl=None):
    if dscurl == 'psutil1':
        dscurl = 'ftparchive.gplhost.com/debian/pool/icehouse-backports/main/p/python-psutil/python-psutil_1.2.1-1~bpo70+1.dsc'
    archive_list = futil.package_deb.dget_dsc(dscurl)
    print archive_list
    for arc in archive_list:
        if arc.endswith('debian.tar.gz'):
            pass
        elif arc.endswith('.tar.gz'):
            pass
        else:
            archive_list.pop()

    untarred_count = 0

    untar_built = futil.package_deb.untar_and_build(archive_list)

    return untar_built


def sudoers2remote():
	put2 = False
	put1 = futil.sudoers.sudoers8default2remote()
	if put1:
		put2 = futil.sudoers.sudoers8permies2remote()
	return put2


def sudoers_mismatches():
	return futil.sudoers.sudo_mismatches(True)

