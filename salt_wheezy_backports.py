#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd minion_localhost_master
# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd minion_conf_reset
# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd install_localonly_populate:slscopy=dj14cms,cms='djcms'
# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd install_localonly_populate:slscopy=dj14cms,cms='fein'
# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd install_localonly_populate:slscopy=sentry5,version=5.4
# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd local_functions_summary
# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd local_version

from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
import fabutil as futil
from fabutil import conf_get_set as getset
#from fabutil import saltstack as salt
from fabutil import saltdaemons as saltd
#from fabutil import package_deb as pdeb


def verbosity_kwarg_int(function_to_decorate):
    """ Decorator to ensure, even if verbosity is supplied as a string,
    then we do our utmost to convert it to an int.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbosity' in kwargs:
            kval = kwargs['verbosity']
            verbosity_int=None
            try:
                verbosity_int = int(kval)
            except Exception:
                pass
            if verbosity_int is None:
                """ The user of this decorator would expect that
                non-numeric arguments blocked, so better to set
                verbosity zero than let through a bad value
                """
                #kwargs[verbosity]=0
                kwargs.pop('verbosity', None)
                """ Above we have removed verbosity if it cannot
                be converted, which means any default in wrapped
                function signature would take effect
                """
            else:
                kwargs['verbosity']=verbosity_int
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbosity_kwarg_int_verbose(function_to_decorate):
    """ Decorator to ensure, even if verbosity is supplied as a string,
    then we do our utmost to convert it to an int.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    Verbose variant which has print statements reporting before and after
    and also sets verbosity=0 on conversion error.
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbosity' in kwargs:
            kval = kwargs['verbosity']
            verbosity_int=None
            try:
                print("Attempting to int({0}) ".format(kval))
                verbosity_int = int(kval)
            except Exception:
                raise

            if verbosity_int is None:
                """ The user of this decorator would expect that
                non-numeric arguments blocked, so better to set
                verbosity zero than let through a bad value
                """
                #kwargs[verbosity]=0
                kwargs.pop('verbosity', None)
                print("kwarg_int wrapper has removed verbosity key word arg")
                """ Above we have removed verbosity if it cannot
                be converted, which means any default in wrapped
                function signature would take effect
                """
            else:
                kwargs['verbosity']=verbosity_int
        print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


@verbosity_kwarg_int
def master_conf_reset(verbosity=1):
    """ Reset configuration for master using the original package configuration """
    reset_res = saltd.salt_conf_reset('master',(verbosity>1))
    if verbosity > 0 and reset_res is not True:
        print "conf_reset returned {0}".format(reset_res)
    return reset_res


def master_detailed_extra(print_flag=True):
    """ Print a summary (detailed extra) of master config file """
    return saltd.detailed_extra('master',print_flag)


def master_fingerprint():
    """ Fetch the public key fingerprint from the master """
    printed = saltd.print_fingerprint('master',False)
    return printed


def master_fingerprint_highlighted():
    """ Fetch the public key fingerprint from the master retaining any highlighting """
    printed = saltd.print_fingerprint('master',True,True)
    return printed


def master_localhost_only(verbose=False):
    """ Set salt master to listen LOCALHOST ONLY for minion communication / control """
    if verbose is False or verbose in ['false','False','FALSE']:
        verbosity = 1
    else:
        verbosity = 2
    return saltd.master_localhost_only(verbosity)


@verbosity_kwarg_int
def minion_conf_reset(verbosity=1):
    """ Reset configuration for minion using the original package configuration """
    reset_res = saltd.salt_conf_reset('minion',(verbosity>1))
    if verbosity > 0 and reset_res is not True:
        print "conf_reset returned {0}".format(reset_res)
    return reset_res


def minion_detailed_extra(print_flag=True):
    """ Print a summary (detailed extra) of minion config file """
    return saltd.detailed_extra('minion',print_flag)


def minion_localhost_master():
    """ Set salt minion to look to LOCALHOST ONLY for configuration / instructions """
    return saltd.minion_localhost_master(verbosity=2)


def minion_verify_fingerprint_used(fingerprint):
    """ Verify that the supplied fingerprint matches the minion fingerprint in use """
    if fingerprint is None:
        return False
    used_flag = saltd.verify_fingerprint_used_minion(fingerprint,True)
    return used_flag


def minion_verify_fingerprint_hardcoded(fingerprint):
    """ Verify that the supplied fingerprint matches the minion hardcoded fingerprint """
    return saltd.verify_fingerprint_hardcoded_minion(fingerprint,verbose=True)


def print_both_fingerprints(highlighted=True):
    """ Fetch and print the fingerprints for both 'master' and 'minion' """
    printed = saltd.print_fingerprint('master',True,highlighted)
    printed = saltd.print_fingerprint('minion',True,highlighted)
    return
    

def state_highstate_print():
    """ Print INFO lines from state.highstate """
    saltd.state_highstate_print(False)
    return


def minion_hardcode_master_supplied(fingerprint,verbose=False):
    """ Hardcode the supplied fingerprint into the config file of the minion """
    if verbose is False or verbose in ['false','False','FALSE']:
        verbose = False
    hardcoded_flag = saltd.pub_hardcode_minion(fingerprint,verbose)
    return


def minion_hardcode_master_fingerprint(fingerprint='localhost'):
    """ Fetch master fingerprint and hardcode it into the config file of the minion """
    if fingerprint is None:
        return False
    if fingerprint.startswith('local'):
        local_fingerprint = salt_pub_fetch('master')
        minion_hardcode_master_supplied(local_fingerprint)
    return


def salt_localonly():
    """ Ensure Master listens localhost only and ensure Minion looks localhost only for tasks """
    localonly_res = False
    master_res = master_localhost_only()
    # Above ensure master listens on internal ip address only.
    if master_res is True:
        """ Next make sure minions only look to localhost for instructions """
        localonly_res = minion_localhost_master()
    return localonly_res


def local_version(verbose=False):
    vres = saltd.local_version(verbose,True)
    return vres


def local_functions_summary(verbose=False):
    """ Use salt sys.list_functions command to present some summary information about
    availability of module functions.
    """
    for mod in ['deb-apache','file','lxc']:
        #functions = saltd.sysdoc_functions_search(module=mod)
        saltd.module_having_functions(module=mod,print_flag=True)
    matches = saltd.sysdoc_functions_egrep('hg','clone|latest|pull|revision|update')
    #salt_local_version_list = saltd.local_version(False,False)
    salt_local_version = saltd.local_version_string()
    print "Salt {0} --local searching of hg module for clone|pull reports:".format(
        salt_local_version)
    for idx,match in enumerate(matches):
        print "{0} {1}".format(idx,match)
    #print "{0} matches total".format(matches_total)
    return


def install_localonly_populate(slscopy='dj14cms',version='0',revision=None,cms=None):
    """ Install salt from Debian backports (master and minion) and make both 'localhost only'.
    Optionally fetch and install automation files at version or at revision.
    """    
    # If you want fabric command line interaction with an argument
    # then expect it to be a string. May need to int() what you get.
    # Will not int(version) as need to support version style numbers

    localonly_return = False
    print futil.package_deb.has_backports()
    #print futil.package_deb.selections_including(['held'])
    futil.package_deb.held_packages_summary(True,True)
    # install salt-master salt-common python-zmq=13.1.0-1~bpo70+1
    #reqres = futil.package_deb.requires_backported_package('salt-master',True,False)
    #reqres = futil.package_deb.requires_backported_packages(['salt-master','python-zmq'],True,False)
    reqres = futil.package_deb.requires_backported_packages(['salt-master','python-zmq=13.1.0-1~bpo70+1']
                                                            ,True,False)
    print futil.package_deb.dpkg_installed_version(['python-zmq'])
    if not reqres:
        futil.files_directories.abort_with_message("Install of salt-master did not work as expected",2)
        return localonly_return

    """ Use ('salt-master',True,True) if you want dmidecode to be pulled down also """
    # echo 'salt-master hold' | dpkg --set-selections
    # echo 'salt-common hold' | dpkg --set-selections
    # Using aptitude to hold packages is local to aptitude so better to do above.
    with settings(warn_only=True):
        ros_res = futil.cmdbin.run_or_sudo("{0} /etc/salt/pki".format(futil.files_directories.CMD_MKDIRP))
    if ros_res.failed:
        print "Exists already /etc/salt/pki or other mkdir issue"
        return localonly_return
    #futil.cmdbin.moreish_etc_subdir('dhcp',['dhclient-enter-hooks.d','dhclient-exit-hooks.d'])
    requires_res = futil.package_deb.requires_backported_package('salt-minion',True,True)
    if not requires_res:
        futil.files_directories.abort_with_message("Install of salt-minion did not work as expected",2)
        return localonly_return
    requires_debconfutils_res = futil.package_deb.requires_dpkg_package('debconf-utils',True)
    if not requires_debconfutils_res:
        futil.files_directories.abort_with_message('Extra package debconf-utils install failed.')

    localonly_res = salt_localonly()
    if localonly_res is True:
        localonly_return = True
    local_functions_summary()
    return localonly_return


def current_level(verbose=True):
    """ Print quality of current setup (levels 0,1,2,3) 
    Set verbose=False is you do not want detail
    Uses local_level() which is a one master and one minion local only setup
    """
    if verbose is True or verbose in ['true','True','TRUE']:
        verbosity = 2
    else:
        # local_level() will be called with verbosity=1
        verbosity = 1
    return saltd.local_level(verbosity)
        

def salt_quality(quality_level=None,verbosity=1):
    """ Set quality to supplied level (1,2,3)

    Quality level is an arbitrary combination of Listen, keys, and security limiting.

    Call salt_quality(3) to have your existing setup set to maximum quality.
    Call salt_quality() to have returned a value 0->3 reflecting quality of current setup.

    Rather than use this arbitrary combination, you can treat this function as
    a demonstrator and documenter of useful functions to call in establishing a salt setup.
    """
    verbosity_int=1
    try:
        verbosity_int = int(verbosity)
    except Exception:
        pass
    if quality_level is None:
        if verbosity_int == 0:
            return saltd.current_level(1)
        else:
            # Call saltd.current_level with verbosity=2
            return saltd.current_level(2)
    quality_int=1
    try:
        quality_int = int(quality_level)
    except Exception:
        raise Exception("Quality level must be 1,2,3. Level supplied {0}".format(quality_level))
    if quality_int < 1 or quality_int > 3:
        raise Exception("Quality level must be 1,2,3. You supplied {0}".format(quality_int))
    return saltd.quality_level(quality_int,verbosity_int)

