#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2018 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import fabutil as futil
from fabutil import conf_get_set as getset
# import pprint; pp2 = pprint.PrettyPrinter(indent=2)
from fabutil import iptables_firewall as ipt
from fabutil import catproc as croc

# fab -a -f firewall_cmd_ipt.py -u root -H aa.bb.cc.dd print_preamble
# fab -a -f firewall_cmd_ipt.py -u root -H aa.bb.cc.dd net_forwarding_summary_all
# fab -a -f firewall_cmd_ipt.py -u root -H aa.bb.cc.dd net_forwarding_summary:all_or_perint=all
# fab -a -f firewall_cmd_ipt.py -u root -H aa.bb.cc.dd net_forwarding_summary_all2    # preambler
# fab -a -f firewall_cmd_ipt.py -u root -H aa.bb.cc.dd iptables_save
# fab -a -f firewall_cmd_ipt.py -u root -H aa.bb.cc.dd iptables_save_verbose


""" Report on best guess of the type of firewalling active.
Support querying firewall commands.
Relies on Centos7 iptables.
See instead firewall_cmd_npt.py for a variant that relies on nftables
"""

def first_arg_false_to_false(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is True or first_arg in ['true','True','TRUE']:
                # Ensure we never interfere with any first arg intended as positive boolean
                pass
            elif first_arg is False:
                # This branch will likely never be encountered
                pass
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def first_arg_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if len(args) > 0:
            #print 'Wrapped function', function_to_decorate.__name__, 'has args'
            first_arg = args[0]
            if first_arg is True:
                # Ensure we never interfere with any first arg intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as True'
                pass
            elif first_arg is False:
                # Ensure we never interfere with any first arg intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as False'
                pass
            elif first_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to True'
                args_list = list(args)
                args_list[0] = True
                args = tuple(args_list)
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to False'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if 'verbose' in kwargs:
            #print "Wrapped function", function_to_decorate.__name__, "has kwarg 'verbose'"

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as True'
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as False'
                pass
            elif verbose_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to True'
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to False'
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


@first_arg_forced_boolean
@verbose_forced_boolean
def centoslike_contract(verbose=False):
    """ Returns several indicators. Most useful where network delays are not a factor
    Do use the wrapper functions centoslike() or centoslike2() as appropriate.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    #print pretty_tuple
    rline = futil.cmdbin.release_line()
    #print rline
    #if futil.cmdbin.cpu64():
    #    print "CPU has 64 bit support"
    centos_or_centoslike = None
    debian_or_debianlike = False
    if pretty_tuple[1] in futil.cmdbin.DEBIAN_PLUS_DERIVATIVES2:
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif 'ebianlike' in rline:
        debian_or_debianlike = True
        centos_or_centoslike = False
    else:
        pass
    if pretty_tuple[1] == 'debian':
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif debian_or_debianlike:
        debian_or_debianlike = True
        centos_or_centoslike = False
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        centos_or_centoslike = True
        debian_or_debianlike = False
        if verbose is True:
            print "EPEL: {0}".format(futil.package_rpm.has_epel())
            print futil.package_rpm.repolist_dict()

    if centos_or_centoslike is None:
        # If inconclusive then set as False
        centos_or_centoslike = False

    return centos_or_centoslike


@first_arg_forced_boolean
@verbose_forced_boolean
def centoslike(verbose=False):
    """ Wrapper around centoslike_contract() to which you can add any
    optional behaviour you choose later
    """
    centos_or_centoslike = centoslike_contract(verbose)
    return centos_or_centoslike


def centoslike2(verbose=True):
    """ Wrapper around centoslike_contract with verbose forced True
    Because selinux config file might be considered sensitive we make 
    pulling that file locally a feature of this wrapper rather than
    being default behaviour of main centoslike() function
    """
    centos_or_centoslike = centoslike_contract(verbose)
    if centos_or_centoslike:
        futil.selinuxstatus.sestatus1print()
        keyed = futil.selinuxstatus.seconfig_keyed()
        #print "keyed has length {0}".format(len(keyed))
        #print keyed
        if 'SELINUX' in keyed:
            print "SELINUX set {0}".format(keyed['SELINUX'])
        if 'SELINUXTYPE' in keyed:
            print "SELINUXTYPE set {0}".format(keyed['SELINUXTYPE'])
    return centos_or_centoslike


@first_arg_false_to_false
def print_preamble(backports_epel_report=True):
    """ Print preamble showing some short fingerprint style markers for the server.
    Even if we know we are Debian or RPM based, we might
    still distinguish servers based on availability of extra repositories.
    backports_epel_true=True (default) gives you some feedback about extra
    repositories.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print pretty_tuple
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    if not backports_epel_report:
        # Do not bother reporting about backports / epel
        pass
    elif pretty_tuple[1] == 'debian':
        print "Backports: {0}".format(futil.package_deb.has_backports())
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        print "EPEL: {0}".format(futil.package_rpm.has_epel())
        print futil.package_rpm.repolist_dict()

    if backports_epel_report:
        """ Consider backports_epel_report similar to verbose=true
        way of requesting more info """
        futil.selinuxstatus.sestatus1print()
    return


@first_arg_false_to_false
def print_preamble_fast(verbosity=0):
    """ Print preamble showing some short fingerprint style markers for the server.
    This variant avoids checking centos for epel to avoid time consuming 'yum repolist'.
    """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print(pretty_tuple)
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    if pretty_tuple[1] == 'debian':
        pass
        # fast so skip: print "Backports: {0}".format(futil.package_deb.has_backports())
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        # Best guess: We are centos or centoslike
        centoslike2(verbose=False)
        """ futil.selinuxstatus.sestatus1print()
        By calling centoslike2() we already get some commentary about
        selinux status
        """
    else:
        pass
    return


def net_forwarding_summary(all_or_perint='all'):
    """ Summary of net section related to system ip forwarding """
    return croc.net_forwarding_summary(all_or_perint,print_flag=True)


def net_forwarding_summary_all():
    """ Alias of net_forwarding_summary for 'all' """
    return net_forwarding_summary('all')


def net_forwarding_summary_all2():
    """ Alias of net_forwarding_summary for 'all' which includes a ...preamble() call """
    print_preamble(backports_epel_report=True)
    return net_forwarding_summary('all')


def iptables_save():
    """ Request remote server saves its iptables entries to a file. """
    return ipt.iptables_save()


def iptables_save_verbose():
    """ Request remote server saves its iptables entries to a file (and report count) """
    # futil.iptables_firewall.iptables_save(verbose_counts=True)
    return ipt.iptables_save(verbose_counts=True)


def iptables_counts_incl_nat():
    """ Summary counts of entries in each of 5 or more tables INPUT, FORWARD, OUTPUT, ... """    
    #lines = croc.netforwardtyped('both',verbosity=1)
    lines = croc.netforward_summary(verbosity=1)
    return ipt.iptables_counts_incl_nat()


def iptables_counts3():
    """ Summary counts of entries in each of 3 main tables INPUT, FORWARD, OUTPUT """
    return ipt.iptables_counts_incl_nat()


def forwarding_summary():
    lines = croc.netforward_summary(verbosity=1)
    return lines





