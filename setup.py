from setuptools import setup

setup(name='fabutil',
      version='0.1.0',
      author='Gary Wright',
      author_email='gnubyexample+commit@googlemail.com',
      packages=['fabutil'],
      url='https://bitbucket.org/wrightsolutions/django14fabric',
      license='LICENSE',
      description='Helper for delimited conf files using fabric.',
      long_description='Fabric util helper for delimited conf files and other tasks.',
      install_requires=["fabric == 1.8.5"],
)
