#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f examples_get_set.py -u root -H aa.bb.cc.dd nsswitch_db
# fab -a -f examples_get_set.py -u root -H aa.bb.cc.dd nsswitch_files
# fab -a -f examples_get_set.py -u root -H aa.bb.cc.dd sshd_config
# fab -a -f examples_get_set.py -u root -H aa.bb.cc.dd apache2listen:verbose=True
# fab -a -f examples_get_set.py -u root -H aa.bb.cc.dd php5ini_found

""" The purpose of this examples file is to demonstrate
some example use of the api.

Nothing in here is in any way destructive, but if you amend this file,
do ensure any toggle or other updates to values you do is followed by
a conf.dequeue_all() - this is good practice to ensure updates are not written.

/etc/nsswitch.conf exists on most GNU/Linux distributions so makes a good first example

Ssh has been chosen as an example because the configuration file does not
vary much between different GNU/Linux distributions.

Php5 is included for illustration and to clarify that the use of sectioned
ini files is not fully supported. There are better solutions for parsing .ini files
in Python (ConfigParser). In additon the mixed case 'On' and 'Off' are not
something that is currently supported in the toggle functionality.
"""

from fabric.contrib.files import exists
import fabutil as futil
from fabutil import conf_get_set as getset
from fabutil import cmdbin


def nsswitch_db():
    with getset.Keyedfile('/etc/nsswitch.conf') as conf:
        assert conf.keysep == ':'
        for line_db in conf.lines_enabled_contains(' db'):
            print line_db
    return

def nsswitch_files():
    """ Rather than use lines_enabled_contains(' files') we
    choose an alternative approach as in this program we are
    attempting to provide examples / illustrations of the api
    """
    with getset.Keyedfile('/etc/nsswitch.conf') as conf:
        # Using lines_enabled() instead of keylines_enabled() to drop line ending
        for line in conf.lines_enabled():
            if ' files' in line:
                print line
        assert conf.keysep == ':'
    return


def sshd_config():
    print futil.ssh_lokkit.sshd_config_keylines_enabled_linesep()
    return


def sshd_enabled():
    for line in futil.ssh_lokkit.sshd_config_keylines_enabled():
        print line
    print futil.ssh_lokkit.sshd_motd_yes_preview()
    return


def apache2listen(verbose=False):
    """ conf_get_set has no concept of nesting or sections so is probably not
    well suited to trying to toggle values in apache2 configurations.
    However if you want to just treat the whole file as a flat structure and
    ask for what conf_get_set thinks is enabled, then what you will see
    returned is the LAST value of Listen.
    Note about .items() and dictionaries. Here I present a counter example as
    a reminder of what NOT TO DO when looking for key entries:
            if 'Listen' in conf.items():
                print conf['Listen']
    Not only is conf.items() more typing than the correct 'in conf:' but it
    will also lead to poor interpretation of results. See below for correct:
            if 'Listen' in conf:
                print conf['Listen']
    """
    listen_return = False
    if verbose in ['true','True',True]:
        """ Arguments from fabric are strings and we want a boolean """
        verbose = True
    else:
        verbose = False
    large_flag = False
    ptype = cmdbin.deb_or_rpm()
    if ptype is not None:
        if ptype == 'deb':
            conf_listen = '/etc/apache2/ports.conf'
        elif ptype == 'rpm':
            conf_listen = '/etc/httpd/conf/httpd.conf'
            large_flag = True
        else:
            conf_listen = '/usr/local/apache2/conf/httpd.conf'
            large_flag = True

    if not exists(conf_listen):
        print "Listen directives not found at {0} so looking elsewhere.".format(conf_listen)
        large_flag = False
        """ Best guesses next """
        conf_listen = '/etc/apache2/conf.d/ports.conf'
        if not exists(conf_listen):
            conf_listen = '/etc/apache2/conf.d/listen.conf'
            if not exists(conf_listen):
                conf_listen = '/etc/apache2/apache2.conf'
                large_flag = True
                if not exists(conf_listen):
                    conf_listen = '/etc/apache2/httpd.conf'
                    large_flag = True
        print "Listen directives at {0} will be loaded.".format(conf_listen)

    if not exists(conf_listen):
        print "No suitable conf file found. Is Apache installed?"
        return False

    """ Although conf_get_set was not written with large (1000 plus lines) configuration
    files in mind, we are going with it to illustrate in an example. """
    if large_flag:
        
        with getset.Keyedlarge(conf_listen) as conf:
            prefix_large = "Processing using subclass Keyedlarge()"
            print "{0} a conf having {1} lines".format(prefix_large,conf.linecount)
            if 'Listen' in conf.items():
                print conf['Listen']
            if verbose:
                print "Above is the definitive answer so stop there or keep reading for examples"
                for line_listen in conf.lines_enabled_contains('Listen'):
                    print line_listen
            listen_return = True

    else:

        with getset.Keyedfile(conf_listen) as conf:
            if 'Listen' in conf:
                print conf['Listen']
            if verbose:
                print "^ Above is the definitive answer so stop there or keep reading for examples"
                #print conf.keylist_frozen
                """
                for conf_k in conf:
                    print "{0}DDD".format(conf_k)
                    #print conf_k,conf[conf_k]
                """
                for line_enabled in conf.lines_enabled():
                    print line_enabled
                print conf.keylines_typed(1,['Listen'])
                for line_listen in conf.lines_enabled_contains('Listen'):
                    print line_listen
                #print conf.keylines_typed(2,['NameVirtualHost'])
                for tup in conf.tuples_all:
                    if tup[0] is not None and 'Listen' in tup[0]:
                        print tup
                listen_return = True
            else:
                listen_return = True

    return listen_return



def php5ini_found():
    php5return = False
    php5ini = '/etc/php5/apache2/php.ini'
    if not exists(php5ini):
        print "php5 configuration not found at {0} so looking elsewhere.".format(php5ini)
        ptype = cmdbin.deb_or_rpm()
        if ptype is not None:
            if ptype == 'deb':
                """ Example so using the cli php.ini instead is no problem """
                php5ini = '/etc/php5/cli/php.ini'
            elif ptype == 'rpm':
                php5ini = '/etc/php.ini'
            else:
                php5ini = '/etc/php.ini'
        if not exists(php5ini):
            print "No php ini file found. Is php installed?"
            php5ini = None
    if php5ini is not None:
        """ We do not really support:
        (.) ini files
        (.) Mixed case values for 'on' and 'off'
        So proceed no further and do not attempt any toggling.
        """
        php5return = True
    print "php5ini() returning {0}".format(php5return)
    return php5return
