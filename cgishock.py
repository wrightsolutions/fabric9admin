#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f cgishock.py -u root -H aa.bb.cc.dd cgi_available
# fab -a -f cgishock.py -u root -H aa.bb.cc.dd cgi_enabled
# fab -a -f cgishock.py -u root -H aa.bb.cc.dd cgi_both
# fab -a -f cgishock.py -u root -H aa.bb.cc.dd cgi_dump_modules
# fab -a -f cgishock.py -u root -H aa.bb.cc.dd extensions_enabled
# fab -a -f cgishock.py -u root -H aa.bb.cc.dd extensions_enabled:exttype=shared
# fab -a -f cgishock.py -u root -H aa.bb.cc.dd sites_available
# fab -a -f cgishock.py -u root -H aa.bb.cc.dd sites_available_nonssl
# fab -a -f cgishock.py -u root -H aa.bb.cc.dd sites_enabled
# fab -a -f cgishock.py -u root -H aa.bb.cc.dd execcgi_enabled
# fab -a -f cgishock.py -u root -H aa.bb.cc.dd execcgi_enabled:verbose=true
# fab -a -f cgishock.py -u root -H aa.bb.cc.dd summary

import fabutil as futil
from fabutil import web_cgi


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbose' in kwargs:

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                pass
            elif verbose_arg in ['true','True','TRUE']:
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        function_return = function_to_decorate(*args, **kwargs)
        return function_return

    return inner_accepting_args


def mods_avail(search='both',fileext='load',filestem=None,excluding=None):
    mods_list = web_cgi.mods_avail(search,fileext,filestem,excluding)
    for mod in mods_list:
        print mod
    return mods_list


def cgi_available(search='available'):
    mods_list = web_cgi.mods_avail(search,fileext='load',filestem='*gi*',excluding='proxy_')
    for mod in mods_list:
        print mod
    return mods_list


def cgi_enabled(search='enabled'):
    mods_list = web_cgi.mods_avail(search,fileext='load',filestem='*gi*',excluding='proxy_')
    for mod in mods_list:
        print mod
    return mods_list


def cgi_both(search='both'):
    mods_list = web_cgi.mods_avail(search,fileext='load',filestem='*gi*',excluding='proxy_')
    for mod in mods_list:
        print mod
    return mods_list


def extensions_enabled(exttype=None):
    extensions_list = web_cgi.extensions_enabled(exttype)
    for ext in extensions_list:
        print ext
    return extensions_list


@verbose_forced_boolean
def dump_modules(verbose=True):
    """ Supplying None to the web_cgi function says that
    we do not want to restrict the extensions list to
    either shared or static.
    """
    if verbose:
        extensions_list =web_cgi.extensions_enabled(None)
    else:
        extensions_list = web_cgi.extensions_enabled('shared')
    for ext in extensions_list:
        print ext
    return extensions_list


@verbose_forced_boolean
def cgi_dump_modules(verbose=True):
    """ Supplying None to the web_cgi function says that
    we do not want to restrict the extensions list to
    either shared or static.
    """
    if verbose:
        extensions_list =web_cgi.extensions_enabled(None)
    else:
        extensions_list = web_cgi.extensions_enabled('shared')
    filtered_list = []
    for ext in extensions_list:
        if 'cgi' in ext and 'proxy_' not in ext:
            filtered_list.append(ext)
            print ext
    return filtered_list


def sites_available():
    sites_list = web_cgi.sites_avail('available')
    for site in sites_list:
        print site
    return sites_list


def sites_available_nonssl():
    sites_list = web_cgi.sites_avail('available','ssl')
    """ Second argument above is an exclude facility, but it
    is an exclusion simply on filename rather than anything
    more robust.
    """
    for site in sites_list:
        print site
    return sites_list


def sites_enabled():
    sites_list = web_cgi.sites_avail('enabled')
    for site in sites_list:
        print site
    return sites_list


@verbose_forced_boolean
def execcgi_enabled(verbose=False):
    """ ExecCGI in site-enabled """
    if verbose is True:
        elines = web_cgi.execcgi(False,verbose)
    else:
        elines = web_cgi.execcgi(False,verbose)
    for line in elines:
        print line
    return elines



@verbose_forced_boolean
def summary(verbose=False):
    cgi_enabled(search='enabled')
    sites_enabled()
    sites_available()
    execcgi_enabled(verbose)
    return

