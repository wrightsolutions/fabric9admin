#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f cent6phpmysql.py -u root -H aa.bb.cc.dd phpmysql:drupal=6

from __future__ import with_statement
from fabric.api import abort, cd, run, settings
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
import os.path

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

cmd_fgrep = "/bin/fgrep "
cmd_rpm = "/bin/rpm "
cmd_rpm_install = "/bin/rpm -ivh "
cmd_rpm_test = "/bin/rpm --test "
cmd_wget = "/usr/bin/wget --no-check-certificate "
cmd_yum = "/usr/bin/yum "
cmd_yum_assumeyes = "/usr/bin/yum --assumeyes "
cmd_yum_clean_all = "/usr/bin/yum clean all "
cmd_gpg_recv = "gpg --keyserver keys.gnupg.net --recv-keys "
cmd_gpg_verify = "gpg --verify --no-tty --batch "
cmd_gpg_list = "gpg --list-key --no-tty --batch "
cmd_alternatives_display = "/usr/sbin/update-alternatives --display "
cmd_useradd = "/usr/sbin/useradd "
cmd_useradd_defaults = "/usr/sbin/useradd --defaults "
cmd_ln_s = "/bin/ln -s "
dir_fab = "cent6phpmysql.fab"
dirpath_fab = "~/" + dir_fab


def tilde_expand(path):
    if path.startswith('~/'):
        path_expanded = run("dirname ~/.")
    else:
        return path
    return path_expanded


def rmdir_existing(dirpath,ignore_missing=True,rm_verbose=True):
    if dirpath.startswith('~/'):
        """ set function default return to True """
        rmdir_flag = True
    else:
        """ As a safety feature against dangerous rm -fR
        commands, any directory path that does not begin
        with ~/ will be rejected (function returns False) """
        return False
    exists_dirpath = True
    with settings(warn_only=True):
        """ Tilde expansion only applies when '~' is unquoted """
        exists_dirpath = exists(tilde_expand(dirpath))
        if not exists_dirpath:
            return ignore_missing
    if exists_dirpath:
        if rm_verbose:
            run("rm -vfR " + dirpath)
        else:
            run("rm -fR " + dirpath)
    return True

def abort_with_cleanup(dirpath,abort_message,ignore_missing=True,rm_verbose=True):
    rmdir_existing(dirpath,ignore_missing,rm_verbose)
    abort(abort_message)
    return

def mkdir_existing(dirpath,remove_existing=True):
    if dirpath.startswith('~/'):
        """ set function default return to True """
        mkdir_flag = True
    else:
        """ As a safety feature against dangerous rm -fR
        commands, any directory path that does not begin
        with ~/ will be rejected (function returns False) """
        return False
    exists_dirpath = True
    with settings(warn_only=True):
        """ Tilde expansion only applies when '~' is unquoted """
        # exists_dirpath = exists(dirpath,verbose=True)
        exists_dirpath = exists(tilde_expand(dirpath))
        #print "exists_dirpath=%s, remove_existing=%s\n" \
        #% (exists_dirpath,remove_existing)
    if exists_dirpath and remove_existing:
        run("rm -vfR " + dirpath)
    mkdir_res = run("mkdir " + dirpath)
    if mkdir_res.failed:
        mkdir_flag = False
    return mkdir_flag


def gpg_recv_and_verify(signed_file):
    gpgres = run(cmd_gpg_list + "CE0222D0EB8CFF0E")
    if gpgres.failed:
       gpgres = run(cmd_gpg_recv + "CE0222D0EB8CFF0E")
       if not gpgres.failed:
       	  abort("Aborting as preparation for gpg verification failed.")
    gpgres = run(cmd_gpg_verify + signed_file + ".sig")	 
    #gpgres = contains(tar,'02 Feb 2012 18:48:46 GMT using DSA key ID EB8C')	 
    return gpgres


def phpmysql(newuser='testing',drupal='0'):
    """ If you want fabric command line interaction with an argument
    then expect it to be a string. May need to int() what you get """
    drupal_int = int(drupal)

    with settings(warn_only=True):
        kern64res = run("uname -a | fgrep 'x86_64'")

    if kern64res.failed:
        kern64_flag = False
    else:
        kern64_flag = True

    with settings(warn_only=True):
        yum_res = run(cmd_yum + "install wget")

    tar = 'openatrium-6.x-1.7-core.tar.gz'
    tar_compression = 'gzip'
    # lzip available for Centos5 and similar, but version incompatibility possible
    tar_url = "http://ftp.drupal.org/files/projects/%s" % tar
    mkdir_existing(dirpath_fab)

    dirpath_fab_expanded = tilde_expand('~/') + '/' + dir_fab
    print "Working directory will be %s." % dirpath_fab_expanded

    with settings(warn_only=True):
	 """
	 res = local('curl -I http://www.wrightsolutions.co.uk/',
                     capture=True)
	 """
         with cd(dirpath_fab):
             res = run(cmd_wget + tar_url)
             """ capture=True can only be used for local - not okay for run() """
             """
             if not res.failed:
             res = gpg_recv_and_verify(tar)
             """


	
    if res.failed and not confirm("Download of openatrium failed. Continue anyway?"):
        abort_with_cleanup(dirpath_fab,"Aborting at user request.")

    if tar_compression == 'gzip':
        run("which gzip")
    else:
        run("which bzip2")        # No pv required this script so run("which time bzip2 pv") 

    with cd(dirpath_fab):
	if tar_compression.endswith('zip2'):
            res = run("bzip2 -d " + tar)
	else:
            res = run("gzip -d " + tar)
	if not res.failed:

            tar = os.path.splitext(tar)[0]	# drop the .lz or bz2
            if kern64_flag:
                res = run("tar -xf " + tar)
            else:
                """ Unable to detect 64 bit kernel, or, kernel is likely 32 bit """
                res = run("tar -xf " + tar)
                #res = run("tar --exclude=pfgw32 --exclude=pfgw32s --exclude=pfgw64s -xf " + tar)

        yum_res = run(cmd_yum_assumeyes + "install system-config-firewall-tui mysql-server php")
        """ mod_rewrite, mod_userdir, mod_suexec are included in the main httpd build on CentOS """
        """ php will bring down httpd if not installed anyway """

        rpm_filename = "epel-release-6-8.noarch.rpm"

        if kern64_flag:
            rpm_url = "http://download.fedoraproject.org/pub/epel/6/x86_64/%s" % rpm_filename
        else:
            """ Unable to detect 64 bit kernel, or, kernel is likely 32 bit """
            rpm_url = "http://download.fedoraproject.org/pub/epel/6/i386/%s" % rpm_filename

        wget_res = run(cmd_wget + rpm_url)
        if wget_res.failed:
            abort_with_cleanup(dirpath_fab,"Aborting as epel rpm not found.")
        else:
            yum_res = run(cmd_yum_clean_all)

        with settings(warn_only=True):
            rpm_res = run(cmd_rpm_install + rpm_filename)
            """
            if rpm_res.failed:
            abort_with_cleanup(dirpath_fab,"Aborting as epel rpm already installed / rpm error.")
            """

        if drupal_int == 6:
            yum_res = run(cmd_yum_assumeyes + "install drupal6-views php-cli php-mysql php-mbstring php-gd")
            if yum_res.failed:
                abort_with_cleanup(dirpath_fab,"Aborting as drupal6 packages could not be installed.",True,False)

        with settings(warn_only=True):
            rpm_res = run(cmd_rpm + "-e nano")
            if not rpm_res.failed:
                print "Bye bye to tool for editor challenged."

        with settings(warn_only=True):
            rpm_res = run(cmd_rpm_test + "-e openldap lua cyrus-sasl cyrus-sasl-lib cyrus-sasl-plain")
            if not rpm_res.failed:
                print "Okay to remove all of those listed in --test -e above"

        useradd_res = run(cmd_useradd + "-G wheel " + newuser)
        if useradd_res.failed:
            abort("Aborting as failed trying to useradd " + newuser + ".")

        alternatives_res = run(cmd_alternatives_display + 'mta | ' + cmd_fgrep + "'sendmail'")

        with settings(warn_only=True):
            yum_res = run(cmd_yum + "install yum-utils yum-plugin-downloadonly")

        with settings(warn_only=True):
            yum_res = run(cmd_yum + "check-update")

        with settings(warn_only=True):
            yum_res = run(cmd_yum + "update")

        with settings(warn_only=True):

            yum_res = run(cmd_yum + "install gc")
            #yum_res = run(cmd_yum + "install gc zile")

            if kern64_flag:
                rpm_filename = "zile-2.4.9-1.el6.x86_64.rpm"
                rpm_url = "ftp://ftp.upjs.sk/pub/mirrors/epel/6/x86_64/%s" % rpm_filename

                wget_res = run(cmd_wget + rpm_url)
                if not wget_res.failed:
                    rpm_res = run(cmd_rpm_install + rpm_filename)

            else:
                """ Unable to detect 64 bit kernel, or, kernel is likely 32 bit """
                yum_res = run(cmd_yum + "install emacs-nox")
                #rpm_url = "http://ftp.upjs.sk/pub/fedora/linux/releases/16/Everything/i386/os/Packages/%s" % rpm_filename

        if drupal_int == 6:
            ln_res = run(cmd_ln_s + "/usr/share/drupal6 /var/www/html/drupal6fab")
            if ln_res.failed:
                abort_with_cleanup(dirpath_fab,"Aborting as drupal6fab url could not be activated.",True,False)

        """ Now AFTER you have issued a manual apache2 restart the drupal6 site
        will be available at /drupal6fab/install.php for further setup """

    return

