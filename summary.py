#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2013 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f summary.py -u root -H aa.bb.cc.dd summary
# fab -a -f summary.py -u root -H aa.bb.cc.dd summary_quick
# fab -a -f summary.py -u root -H aa.bb.cc.dd summary_quickest
# fab -a -f summary.py -u root -H aa.bb.cc.dd summary_file:/etc/sysctl.conf,equals
# fab -a -f summary.py -u root -H aa.bb.cc.dd summary_file:/etc/sysctl.conf
# fab -a -f summary.py -u root -H aa.bb.cc.dd free_swap

import fabutil as futil


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbose' in kwargs:

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                pass
            elif verbose_arg in ['true','True','TRUE']:
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        function_return = function_to_decorate(*args, **kwargs)
        return function_return

    return inner_accepting_args


def installed_count(verbose=False):
    count = futil.package_deb.dpkg_installed_count()
    if verbose is True:
        if count < 5:
            print "Installed count of {0} is suspect, log on to server and check".format(count)
        else:
            print "Installed count is {0}".format(count)
    return count


def summary_contract(verbose=False):
    """ Default use. Returns several indicators. Most useful where network delays are not a factor """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print pretty_tuple
    rline = futil.cmdbin.release_line()
    print rline
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    debian_or_debianlike = False
    if pretty_tuple[1] in futil.cmdbin.DEBIAN_PLUS_DERIVATIVES2:
        debian_or_debianlike = True
    elif 'ebianlike' in rline:
        debian_or_debianlike = True
    else:
        pass
    if pretty_tuple[1] == 'debian':
        print "Backports: {0}".format(futil.package_deb.has_backports())
    elif debian_or_debianlike:
        print 'Backports status not checked for Debianlike'
    elif pretty_tuple[2] in futil.cmdbin.NUM_AS_WORDS_TO_TWENTY:
        print "EPEL: {0}".format(futil.package_rpm.has_epel())
        print futil.package_rpm.repolist_dict()
    #print futil.cmdbin.cpu_metrics(True)
    print futil.cmdbin.cpu_metrics(True,True,True)
    swap_total = futil.sysctl_plus.free_marked(True)
    if verbose is True:
        if swap_total > 0:
            futil.sysctl_plus.swap_summary(True)
        futil.sysctl_plus.proc_sys_vm(True)
        print futil.ssh_lokkit.max3return2()
        futil.selinuxstatus.sestatus1print()
    futil.locale_deb.lang_lc_ctype_report()
    cg_flag = futil.cgroups_plus.cgroup_summary(True)
    print "Cgroups active: {0}".format(cg_flag)
    installed_count = 0
    if debian_or_debianlike is True:
        installed_count, count_info = futil.package_deb.dpkg_installed_counts(True)
        futil.package_state.info_manual(print_flag=True,count=10)
        futil.package_deb.held_packages_summary(True,True)
    return installed_count


@verbose_forced_boolean
def summary(verbose=False):
    """ Use summary_contract() or set verbose=False in sensitive
    environments to avoid pulling sshd config locally """
    return summary_contract(verbose)


@verbose_forced_boolean
def summary2(verbose=True):
    """ summary including small amount of sensitive examination.
    Use summary_contract() or set verbose=False in sensitive
    environments to avoid pulling sshd config locally """
    return summary_contract(verbose)


@verbose_forced_boolean
def summary_quick(verbose=True):
    """ Summary with fewer indicators so less network round trips """
    pretty_tuple = futil.cmdbin.pretty_name_tuple()
    print pretty_tuple
    rline = futil.cmdbin.release_line()
    if pretty_tuple[1] in futil.cmdbin.DEBIAN_PLUS_DERIVATIVES2:
        futil.package_deb.sources_list_extract(True,False)
    if futil.cmdbin.cpu64():
        print "CPU has 64 bit support"
    if verbose is True:
        print futil.cmdbin.cpu_metrics(True,True,True)
    #swap_total = futil.sysctl_plus.free_marked()
    in_count = 0
    if verbose is True:
        if pretty_tuple[1] == 'debian':
            in_count = installed_count(True)
        else:
            rline = futil.cmdbin.release_line()
            if 'ebianlike' in rline:
                in_count = installed_count(True)
    if in_count > 0:
        futil.package_state.info_manual5print()
    return in_count


def summary_quickest():
    """ Summary with fewest indicators so less network round trips """
    return summary_quick(False)


def summary_file(filepath,keyedtype='guess',preamble=True):
    """ Show a summary of the format / content of a configuration file fetched from remote server """
    summary_return = False
    if preamble is False or preamble in ['false','False','FALSE']:
        pass
    else:
        summary_quick(False)
    if keyedtype is None:
        return False
    else:
        type_lower = keyedtype.lower()
    keyed_class_name = 'Keyedfile'
    if type_lower.startswith('guess'):
        pass
    elif type_lower == 'colon':
        keyed_class_name = 'Keyedcolon'
    elif type_lower == 'equals2':
        keyed_class_name = 'Keyedequals2'
    elif type_lower == 'equals':
        keyed_class_name = 'Keyedequals'
    elif type_lower == 'space2':
        keyed_class_name = 'Keyedspace2'
    elif type_lower == 'space':
        keyed_class_name = 'Keyedspace'
    elif type_lower == 'large':
        keyed_class_name = 'Keyedlarge'

    with getattr(futil.conf_get_set, keyed_class_name)(filepath) as conf:
        conf.summary_detailed(True)
    return


def free_swap():
    """ Output from 'free' annotated plus details of available swap (if any) """
    swap_total = futil.sysctl_plus.free_marked(True)
    if swap_total > 0:
        futil.sysctl_plus.swap_summary(True)
    return swap_total


