#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd ndoutils_install
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd ndoutils_installed
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd ndoutils_enable
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd ndoutils_detailed
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd ndoutils_enabled_lines
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd ndo2db_configure
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd nagios_cfg_default
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd nagios_cfg_check
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd nagios_check
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd nagios_start
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd nagios_stop
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd nagios_restart
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd perfdata_enable_all
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd nagios_spoolcount
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd nagios_spoolcount10
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd nagios_poller_summary:verbosity=1
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd nagios_poller_type:verbosity=1
# fab -a -f wheezy7nagios.py -u root -H aa.bb.cc.dd nagios_poller_port:verbosity=1

from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
import fabutil as futil
from fabutil import conf_get_set as getset
from fabutil import nagios3deb as nd
#from fabutil import package_deb as pdeb


def first_arg_false_to_false(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is True or first_arg in ['true','True','TRUE']:
                # Ensure we never interfere with any first arg intended as positive boolean
                pass
            elif first_arg is False:
                # This branch will likely never be encountered
                pass
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def first_arg_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if len(args) > 0:
            #print 'Wrapped function', function_to_decorate.__name__, 'has args'
            first_arg = args[0]
            if first_arg is True:
                # Ensure we never interfere with any first arg intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as True'
                pass
            elif first_arg is False:
                # Ensure we never interfere with any first arg intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as False'
                pass
            elif first_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to True'
                args_list = list(args)
                args_list[0] = True
                args = tuple(args_list)
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to False'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if 'verbose' in kwargs:
            #print "Wrapped function", function_to_decorate.__name__, "has kwarg 'verbose'"

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as True'
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as False'
                pass
            elif verbose_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to True'
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to False'
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbosity_kwarg_int(function_to_decorate):
    """ Decorator to ensure, even if verbosity is supplied as a string,
    then we do our utmost to convert it to an int.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbosity' in kwargs:
            kval = kwargs['verbosity']
            verbosity_int=None
            try:
                verbosity_int = int(kval)
            except Exception:
                pass
            if verbosity_int is None:
                """ The user of this decorator would expect that
                non-numeric arguments blocked, so better to set
                verbosity zero than let through a bad value
                """
                #kwargs[verbosity]=0
                kwargs.pop('verbosity', None)
                """ Above we have removed verbosity if it cannot
                be converted, which means any default in wrapped
                function signature would take effect
                """
            else:
                kwargs['verbosity']=verbosity_int
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbosity_kwarg_int_verbose(function_to_decorate):
    """ Decorator to ensure, even if verbosity is supplied as a string,
    then we do our utmost to convert it to an int.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    Verbose variant which has print statements reporting before and after
    and also sets verbosity=0 on conversion error.
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbosity' in kwargs:
            kval = kwargs['verbosity']
            verbosity_int=None
            try:
                print("Attempting to int({0}) ".format(kval))
                verbosity_int = int(kval)
            except Exception:
                raise

            if verbosity_int is None:
                """ The user of this decorator would expect that
                non-numeric arguments blocked, so better to set
                verbosity zero than let through a bad value
                """
                #kwargs[verbosity]=0
                kwargs.pop('verbosity', None)
                print("kwarg_int wrapper has removed verbosity key word arg")
                """ Above we have removed verbosity if it cannot
                be converted, which means any default in wrapped
                function signature would take effect
                """
            else:
                kwargs['verbosity']=verbosity_int
        print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


@first_arg_forced_boolean
def ndoutils_install(verbose=False):
    return nd.ndoutils_install()


def ndoutils_installed(version=14,packagename=nd.NDOUTILS_PACKAGE):
    version_int = int(version)
    installed_version = futil.package_deb.dpkg_installed_version_numeric(packagename)
    if installed_version > 0:
        print "Ndoutils version {0} is installed".format(installed_version)
    else:
        pass
        #print "Ndoutils is not installed!"
    return installed_version


@first_arg_false_to_false
def ndoutils_enable(verbose=False):
    """ Enable system statistics (ndoutils)
    Reminder about the contract of conf_get_set.py and order or operations next...
    Do the queries first. Do use the reliable write features (when implemented)
    However you are discouraged from performing updates and then attempt to query things further.

    No contract is given here that detailed queries will be reliable once updates
    have taken place. Reiterating to clarify: Query, Update (optional), Write the changes.
    """
    nd.nagios_broker()
    if verbose is True:
        print "Remember: You have to enable ndoutils broker module (in ndomod.cfg) first to use ndoutils"
    ndoutils_return = nd.ndoutils_enable(verbose)
    return ndoutils_return


def ndoutils_enable_preview():
    """ Verbose / debug version of ndoutils_enable()
    which supplements the test suite """
    with getset.Keyedequals(nd.NDOUTILS_FPATH_DEFAULT) as conf:
        #print conf.tuples_all
        #return conf.keylines_typed(2,['ENABLE_NDOUTILS'])
        if 'ENABLE_NDOUTILS' in conf:
            #val = conf['ENABLE_NDOUTILS']
            print conf.keylines(['ENABLE_NDOUTILS'])
            #if val in conf.FALSELIST
            print conf.tuples_all_from_key('ENABLE_NDOUTILS')
            print "Expect next 3 booleans to read False,True,False"
            print conf.toggle_safe('ENABLE_NDOUTILS','true',False)
            print conf.toggle_safe('ENABLE_NDOUTILS','true',True,1) # (says safe=True and autoquote_level=1)
            print conf.toggle_safe('ENABLE_NDOUTILS','true',False)
            #for sed in conf.list_of_sedlines: print sed
            for linenum,sed in conf.dict_of_seds.iteritems():
                print "{0}    {1}".format(linenum,sed)
            lines_current = conf.lines_current_simple()
            print len(lines_current)
            print "Above was lines_current ; next is put_lines"
            put_lines = conf.lines_for_put()
            print len(put_lines)
            scriptlines,md5after,lines = conf.sed_checksum(conf.list_of_sedlines,
                                                           debug_filename='ndoutils_enable_preview')
            if md5after is not None:
                print md5after
            conf.dequeue_all()
            print len(conf.dict_of_seds)
            #print conf.sedline('ENABLE_NDOUTILS','yes')
    return


@first_arg_forced_boolean
def ndoutils_detailed(verbose=False):
    nd.detailed_extra_ndomod()
    nd.detailed_extra_ndo2db()
    return


@first_arg_forced_boolean
def ndoutils_enabled_lines(verbose=False):
    nd.lines_enabled_ndomod()
    #nd.detailed_enabled_ndo2db(print_flag=True)
    nd.lines_enabled_ndo2db()
    return


def nagios_cfg_default():
    nagios_stop()
    reinstalled_flag = nd.nagios_cfg_default()
    nagios_start()
    return reinstalled_flag


@first_arg_forced_boolean
def nagios_cfg_check(verbose=False):
    """ A verbose=False defaulting variant of nagios_check """
    return nd.nagios_syntax_check(verbose)


@first_arg_forced_boolean
def nagios_check(verbose=True):
    return nd.nagios_syntax_check(verbose)


def nagios_start():
    return nd.nagios_start()


def nagios_stop():
    return nd.nagios_stop()


def nagios_restart():
    return nd.nagios_restart()


def perfdata_enable_all(verbose=True):
    return nd.perfdata_enable_all(verbose)


def nagios_spoolcount(dirpath='/var/lib/nagios3/spool',pattern=None):
    spoolcount = len(futil.files_directories.dir_files(dirpath,'hour'))
    print("{0} files in {1}".format(spoolcount,dirpath))
    return spoolcount


def nagios_spoolcount10(dirpath='/var/lib/nagios3/spool',pattern=None):

    slist = futil.files_directories.dir_files(dirpath,'hour')

    if len(slist) == 0:
        print("zero files in {0}".format(dirpath))
    else:
        print("{0} files in {1}".format(len(slist),dirpath))
        for idx,line in enumerate(slist):
            print(line)
            if idx == 9: break

    return slist


@verbosity_kwarg_int
def nagios_poller_summary(verbosity=1,config_ndomod=None,config_ndo2db=None):
    """ Print or return a summary of poller settings for ndomod and ndo2db
    """
    # nagios_poller_summary(verbosity=1,config_ndomod=NDOMOD_CONF,config_ndo2db=NDO2DB_CONF):
    poller_lines = []
    if config_ndomod is None and config_ndo2db is None:
        poller_lines = nd.nagios_poller_summary(verbosity)
    elif config_ndomod is None:
        poller_lines = nd.nagios_poller_summary(verbosity,config_ndo2db=config_ndo2db)
    elif config_ndo2db is None:
        poller_lines = nd.nagios_poller_summary(verbosity,config_ndomod=config_ndomod)
    else:
        poller_lines = nd.nagios_poller_summary(verbosity,config_ndomod,config_ndo2db)
    return poller_lines


@verbosity_kwarg_int
def nagios_poller_type(verbosity=1,config_ndomod=None):
    """ Print or return poller port if socket type is 'tcp' """

    if config_ndomod is None:
        ptype = nd.nagios_poller_type(verbosity)
    else:
        ptype = nd.nagios_poller_type(verbosity,config_ndomod)
    if ptype is None:
        if verbosity > 0:
            print("Unknown. Do check poller directives manually using nagios_poller_summary()")
    elif ptype:
        if verbosity > 0:
            print("poller type is {0}".format(ptype))
    else:
        pass

    return ptype


@verbosity_kwarg_int
def nagios_poller_port(verbosity=1,config_ndomod=None):
    """ Print or return poller port if socket type is 'tcp' """
    if config_ndomod is None:
        port = nd.nagios_poller_port(verbosity)
    else:
        port = nd.nagios_poller_port(verbosity,config_ndomod)

    if port is None:
        if verbosity > 0:
            print("No appropriate port. Do check poller type using nagios_poller_type()")
    elif port > 0 and verbosity > 0:
        print("poller port is {0}".format(port))

    return port


@verbosity_kwarg_int
def nagios_poller_both_force_tcp(verbosity=1,config_ndomod=None,config_ndo2db=None,port=None):

    if config_ndomod is None and config_ndo2db is None and port is None:
        made_both = nd.nagios_poller_both_force_tcp(verbosity)
    else:
        made_both = nd.nagios_poller_both_force_tcp(verbosity,config_ndomod,config_ndo2db,port)

    return made_both

