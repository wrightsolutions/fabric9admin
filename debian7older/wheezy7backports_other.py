#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2014 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd backports_enable
# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd backports_enable:cdn=true
# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd haproxy
# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd haproxy:version=15
# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd haproxy_init_managed
# fab -a -f salt_wheezy_backports.py -u root -H aa.bb.cc.dd haproxy_init_managed:version=15

from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
import fabutil as futil
from fabutil import conf_get_set as getset
#from fabutil import package_deb as pdeb


def first_arg_false_to_false(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is True or first_arg in ['true','True','TRUE']:
                # Ensure we never interfere with any first arg intended as positive boolean
                pass
            elif first_arg is False:
                # This branch will likely never be encountered
                pass
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def first_arg_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if len(args) > 0:
            #print 'Wrapped function', function_to_decorate.__name__, 'has args'
            first_arg = args[0]
            if first_arg is True:
                # Ensure we never interfere with any first arg intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as True'
                pass
            elif first_arg is False:
                # Ensure we never interfere with any first arg intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as False'
                pass
            elif first_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to True'
                args_list = list(args)
                args_list[0] = True
                args = tuple(args_list)
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to False'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if 'verbose' in kwargs:
            #print "Wrapped function", function_to_decorate.__name__, "has kwarg 'verbose'"

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as True'
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as False'
                pass
            elif verbose_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to True'
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to False'
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


@first_arg_forced_boolean
def backports_enable(cdn=True,always_update=True):
    return futil.package_deb.enable_backports(cdn=cdn,always_update=always_update)


def haproxy(version=14):
    version_int = int(version)
    backport_res = False
    installed_version = futil.package_deb.dpkg_installed_version_numeric('haproxy')
    if installed_version > 0:
        print "HAProxy version {0} is installed".format(installed_version)
        backport_res = True
    else:
        pass
        #print "HAProxy is not installed!"
    if backport_res is False:
        has_backports = futil.package_deb.has_backports()

        if version_int == 15:
            reqres = futil.package_deb.requires_backported_packages(['haproxy=1.5.4-1~bpo70+1']
                                                                    ,True,False)
            print "http://haproxy.debian.net has haproxy_1.5.4-1~bpo70+1_amd64"
            print "deb http://haproxy.debian.net wheezy-backports main"
            # Not implemented yet
            backport_res = False
        else:
            reqres = futil.package_deb.requires_backported_package_tidy('haproxy',True,False)

            print futil.package_deb.dpkg_installed_version(['haproxy'])
            #print futil.package_deb.selections_including(['held'])
            futil.package_deb.held_packages_summary(True,True)
            if reqres:
                backport_res = True
            else:
                futil.files_directories.abort_with_message(
                    "Install of haproxy did not work as expected",2)
    return backport_res


def haproxy_init_managed(version=14):
    """ Enable init management of haproxy """
    version_int = int(version)
    haproxy_return = False
    installed_version = futil.package_deb.dpkg_installed_version_numeric('haproxy')
    if installed_version > 0:
        print "HAProxy version {0} is installed".format(installed_version)
    else:
        print "HAProxy is not installed!"
        return haproxy_return
    conf_filepath = '/etc/default/haproxy'
    if version_int == 15:
        #md5zero = 'fe5c1890fce351cde863fb7f9814beff'
        haproxy_return = True
        print "HAProxy version {0} does not need ENABLED=1 set as runs by default anyway".format(version_int)
        print "...although you still need to have set the .cfg directive frontend or listen"
        print "...in order to see HAProxy listening on a port."
        return haproxy_return
    with getset.Keyedequals(conf_filepath) as conf:
        print conf.lines_current_simple()
        if 'ENABLED' not in conf:
            print "HAProxy ENABLED keyword not found in {0}".format(conf_filepath)
        else:
            # Found 'ENABLED' in conf:
            #print conf.toggle_safe('ENABLED','true',True,1)
            toggle_res = conf.toggle_safe('ENABLED','1',True,0)
            if toggle_res:
                print conf.lines_for_put_containing('ENABLED=')
                sedlines = conf.sedlines(True)
                #sedres = getset.sed_remote_apply('a'*32,conf_filepath,sedlines,'medium')
                #sedres = getset.sed_remote_apply(None,conf_filepath,sedlines,'medium',True,0.75,True)
                #md5one = '2777318f6eabefe7be15a3b9eaa3951b'
                # md5current is what we supply so md5current=md5zero
                if version_int == 15:
                    #md5zero15 = 'fe5c1890fce351cde863fb7f9814beff'
                    md5zero = 'fe5c1890fce351cde863fb7f9814beff'
                else:
                    md5zero = 'a1f2deb7c7a10e55dc7c971a2288f5d4'
                """ /etc/default/sysstat is almost all comments so disable ratio of changes checking by
                supplying 1.0 instead of 0.75
                #sedres = getset.sed_remote_apply(md5zero,conf_filepath,sedlines,'medium',True,0.75,True)
                #sedres = getset.sed_remote_apply(md5zero,conf_filepath,sedlines,'medium',True,0.5,True)
                #sedres = getset.sed_remote_apply(md5zero,conf_filepath,sedlines,'medium',True,1.0,True)
                """
                sedres = getset.sed_remote_apply(md5zero,conf_filepath,sedlines,'medium',True,1.0,True)
                if sedres is True:
                    haproxy_return = True
                else:
                    print "sed_remote_apply returned {0}".format(sedres)
                print conf.lines_for_put_containing('ENABLED')
                print conf.lines_current_containing('ENABLED')
                # Make the change on the remote system
            else:
                print 'Is haproxy already ENABLED=1? toggle_safe returned {0}'.format(toggle_res)
    return haproxy_return

