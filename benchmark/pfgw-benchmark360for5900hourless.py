#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from __future__ import with_statement
from fabric.api import abort, cd, run, settings
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
import os.path

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

cmd_wget = "wget -nd -m -l 0 "
cmd_gpg_recv = "gpg --keyserver keys.gnupg.net --recv-keys "
cmd_gpg_verify = "gpg --verify --no-tty --batch "
cmd_gpg_list = "gpg --list-key --no-tty --batch "
dir_fab = "pfgw-benchmark.fab"
dirpath_fab = "~/" + dir_fab


def tilde_expand(path):
    if path.startswith('~/'):
        path_expanded = run("dirname ~/.")
    else:
        return path
    return path_expanded


def rmdir_existing(dirpath,ignore_missing=True):
    if dirpath.startswith('~/'):
        """ set function default return to True """
        rmdir_flag = True
    else:
        """ As a safety feature against dangerous rm -fR
        commands, any directory path that does not begin
        with ~/ will be rejected (function returns False) """
        return False
    exists_dirpath = True
    with settings(warn_only=True):
        """ Tilde expansion only applies when '~' is unquoted """
        exists_dirpath = exists(tilde_expand(dirpath))
        if not exists_dirpath:
            return ignore_missing
    if exists_dirpath:
        run("rm -vfR " + dirpath)
    return True


def abort_with_cleanup(dirpath,abort_message,ignore_missing=True):
    rmdir_existing(dirpath,ignore_missing)
    abort(abort_message)
    return


def mkdir_existing(dirpath,remove_existing=True):
    if dirpath.startswith('~/'):
        """ set function default return to True """
        mkdir_flag = True
    else:
        """ As a safety feature against dangerous rm -fR
        commands, any directory path that does not begin
        with ~/ will be rejected (function returns False) """
        return False
    exists_dirpath = True
    with settings(warn_only=True):
        """ Tilde expansion only applies when '~' is unquoted """
        # exists_dirpath = exists(dirpath,verbose=True)
        exists_dirpath = exists(tilde_expand(dirpath))
        #print "exists_dirpath=%s, remove_existing=%s\n" \
        #% (exists_dirpath,remove_existing)
    if exists_dirpath and remove_existing:
        run("rm -vfR " + dirpath)
    mkdir_res = run("mkdir " + dirpath)
    if mkdir_res.failed:
        mkdir_flag = False
    return mkdir_flag


def gpg_recv_and_verify(signed_file):
    gpgres = run(cmd_gpg_list + "CE0222D0EB8CFF0E")
    if gpgres.failed:
       gpgres = run(cmd_gpg_recv + "CE0222D0EB8CFF0E")
       if not gpgres.failed:
       	  abort("Aborting as preparation for gpg verification failed.")
    gpgres = run(cmd_gpg_verify + signed_file + ".sig")	 
    #gpgres = contains(tar,'02 Feb 2012 18:48:46 GMT using DSA key ID EB8C')	 
    return gpgres


def pfgw_get_bin(step='10',exponent_start='5900',pfgw_version='346'):
    """ If you want fabric command line interaction with an argument
    then expect it to be a string. May need to int() what you get """
    step_int = int(step)
    thirtysix_exponent_start = int(exponent_start)
    pfgwver = int(pfgw_version)
    #print "pfgw_get_and_build rows=%d\n" % rows_limit

    with settings(warn_only=True):
        kern64res = run("uname -a | fgrep 'x86_64'")

    if kern64res.failed:
        kern64_flag = False
    else:
        kern64_flag = True

    tar_compression = 'lzip'
    if pfgwver > 346:
	tar_compression = 'lzip'
	if pfgwver == 360:
            if kern64_flag:
                tar = 'openpfgw360__2011Q4linux64.tar.lz'
            else:
		tar = 'openpfgw360__2011Q4linux.tar.lz'
		# Debian 6 (squeeze) or Centos6 or Red Hat 6 or newer
		# meet the glib >= 2.7 requirement of the version 360 of pfgw
	else:
		abort("Aborting as unsure where to find version %s of pfgw." % pfgwver)
    else:
	tar = 'openpfgw346__2011Q1linux64.tar.bz2'
	tar_compression = 'bzip2'
	# glib 2.5 systems such as Centos5 or Red Hat 5 might prefer version 346
	# lzip available for Centos5 and similar, but version incompatibility possible
    tar_url = "http://c809294.r94.cf3.rackcdn.com/%s" % tar
    mkdir_existing(dirpath_fab)

    dirpath_fab_expanded = tilde_expand('~/') + '/' + dir_fab
    print "Working directory will be %s." % dirpath_fab_expanded

    with settings(warn_only=True):
	 """
	 res = local('curl -I http://www.wrightsolutions.co.uk/',
                     capture=True)
	 """
         with cd(dirpath_fab):
             res = run(cmd_wget + tar_url + ".sig")
             if not res.failed:
		 res = run(cmd_wget + tar_url)
		 """ capture=True can only be used for local - not okay for run() """
		 if not res.failed:
		    	res = gpg_recv_and_verify(tar)
	
    if res.failed and not confirm("Download of pfgw failed. Continue anyway?"):
        abort_with_cleanup(dirpath_fab,"Aborting at user request.")

    if tar_compression == 'lzip':
        run("which time lzip")
    else:
        run("which time bzip2")        # No pv required this script so run("which time bzip2 pv") 

    if thirtysix_exponent_start == 5900:
        print "Timed benchmark run being prepared.\n"
    else:
        print "Timed run being prepared with exponent_start=%d.\n" % \
		thirtysix_exponent_start

    with cd(dirpath_fab):
	if tar_compression.startswith('bz'):
            res = run("bzip2 -d " + tar)
	else:
            res = run("lzip -d " + tar)
	if not res.failed:

            tar = os.path.splitext(tar)[0]	# drop the .lz or bz2
            if kern64_flag:
                res = run("tar --exclude=pfgw64s -xf " + tar)
                """ Unable to detect 64 bit kernel, or, kernel is likely 32 bit """
            else:
                res = run("tar --exclude=pfgw32 --exclude=pfgw32s --exclude=pfgw64s -xf " + tar)

        timed_run_flag = False
        if thirtysix_exponent_start < 5901:
            timed_run_flag = True
        elif thirtysix_exponent_start < 6501:
            if confirm("Timed run may take an hour to complete. Continue?"):
                timed_run_flag = True
        elif confirm("Timed run may take between 2 hours and 8 hours to complete. Continue?"):
            timed_run_flag = True

        if timed_run_flag:

            pfgwstem = "primerange5460step%dloop_%dfabric" % (step_int,thirtysix_exponent_start)
            pfgwini = pfgwstem + '.ini'
            pfgwout = pfgwstem + '.out'
            pfgwini_expanded = dirpath_fab_expanded + "/" + pfgwini
            print "pfgw Initialisation file %s will be created in directory %s" % \
		(pfgwini,dirpath_fab_expanded)

            abc2line = "ABC2 15*36^%d+6*36^%d+11+5460*(1+24*$a)" % \
		(thirtysix_exponent_start + 1,thirtysix_exponent_start)
            append(pfgwini_expanded,abc2line)
            if step == 1:
	            append(pfgwini_expanded,"a: from 0 to 9999")
            else:
	            step_line = "a: from 0 to 50000 step %d" % step_int
	            append(pfgwini_expanded,step_line)
	            # to 50000 step 10 would be 5000 actual pfgw tests
            exists_pfgwini = exists(pfgwini_expanded)
            if exists_pfgwini:
	            print "pfgw Initialisation file %s populated, pfgw about to begin." % \
			pfgwini_expanded
	            pfgw_bin = "./openpfgw%d*/pfgw64 -l%s" % (pfgwver,pfgwout)
	            res = run("date;time " + pfgw_bin + " -f " + pfgwini_expanded + ";date")
            else:
	            abort_with_cleanup(dirpath_fab,"Aborting as pfgw initialisation file not found.")
 
	#res = run("date;time ./" + pfgw + ".o | pv -f -i 60 --buffer-size 1k -b > /dev/null;date" )
	""" fgrep of output will perhaps not show immediately due to buffering / other delay """
        #res = run("date;time ./" + pfgw + ".o | fgrep 'processing begun...';date" )

