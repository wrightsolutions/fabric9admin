#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2013 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f django14snail.py -u root -H aa.bb.cc.dd poor_config:target=prettify/local-database/rc/py,data=full,visible=both,totaltime=2,concurrent=1
# Above is my suggestion for first attempt and then later some of the examples below / from the wiki.
# fab -a -f django14snail.py -u root -H aa.bb.cc.dd poor_config:target=counts,data=full,totaltime=2,concurrent=1
# fab -a -f django14snail.py -u root -H aa.bb.cc.dd poor_config:target=counts,data=full
# fab -a -f django14snail.py -u root -H aa.bb.cc.dd poor_config:target=counts,data=full,visible=both
# fab -a -f django14snail.py -u root -H aa.bb.cc.dd poor_config:target=counts,data=full,visible=both,totaltime=5
# fab -a -f django14snail.py -u root -H aa.bb.cc.dd poor_config:target=counts,data=full,totaltime=30
# fab -a -f django14snail.py -u root -H aa.bb.cc.dd poor_config:target=counts,data=full,totaltime=5,concurrent=2
# fab -a -f django14snail.py -u root -H aa.bb.cc.dd poor_config

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, put, run, settings, sudo
from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
import re

""" res.succeeded is preferred for newer fabric, but not res.failed
is used where backward compatibility is preferable. """

cmd_egrep = "/bin/egrep "
cmd_fgrep = "/bin/fgrep "
cmd_rpm = "/bin/rpm "
cmd_rpm_install = "/bin/rpm -ivh "
cmd_rpm_test = "/bin/rpm --test "
cmd_wget = "/usr/bin/wget --no-check-certificate "
cmd_wget1 = "/usr/bin/wget --no-check-certificate --timeout=1 "
cmd_apt = "/usr/bin/apt-get "
cmd_apt_assumeyes = "/usr/bin/apt-get --assume-yes "
cmd_apt_update = "/usr/bin/apt-get update "
cmd_yum = "/usr/bin/yum "
cmd_yum_assumeyes = "/usr/bin/yum --assumeyes "
cmd_gpg_recv = "gpg --keyserver keys.gnupg.net --recv-keys "
cmd_gpg_verify = "gpg --verify --no-tty --batch "
cmd_gpg_list = "gpg --list-key --no-tty --batch "
cmd_kern_release = "/bin/uname -r "
cmd_lsof_internet = "/usr/bin/lsof -i "
cmd_mercurial_clone = "/usr/bin/hg clone "
cmd_alternatives_display = "/usr/sbin/update-alternatives --display "
cmd_useradd = "/usr/sbin/useradd "
cmd_useradd_defaults = "/usr/sbin/useradd --defaults "
cmd_stat = "/usr/bin/stat "
cmd_stat_octal = "/usr/bin/stat -c%a "
cmd_stat_gid = "/usr/bin/stat -c%g "
cmd_which = "/usr/bin/which "
cmd_chgrp = "/bin/chgrp --preserve-root -c "
cmd_chmod = "/bin/chmod --preserve-root -c "
cmd_chmod_recursive = "/bin/chmod --preserve-root -c -R "
cmd_chown = "/bin/chown --preserve-root -c "
cmd_cp = "/bin/cp "
cmd_cp_timestamps = "/bin/cp --preserve=timestamps "
cmd_cpr = "/bin/cp -R "
cmd_cpr_timestamps = "/bin/cp -R --preserve=timestamps "
cmd_dlp = "/bin/vdir -dlp "
cmd_dpkg_status = "/usr/bin/dpkg -s "
cmd_dpkg = "/usr/bin/dpkg "
cmd_rmv = "/bin/rm -v "
cmd_uid = "/usr/bin/id -u "

dir_fab = "django14snail.fab"
""" dirpath_fab_short = "{0}{1}".format('~/',dir_fab)
dirpath_fab_short = "{0}{1}".format('~/public_html/',dir_fab)
"""
# DIRSTEM_DATA_SHORT = '~/'
DIRSTEM_DATA_SHORT = '/var/local'
PORT_WSGI = '8080'
USER_WEBSERVER = 'www-data'
GROUP_WEBSERVER = 'www-data'
GROUP_WRITE_EXT_LIST = ['log','db3','sqlite','sqlite3']
APP_NAME='local2dj14'

re_first_alpha = re.compile('[a-z]+')
re_var_lib_alpha = re.compile('/var/lib/[a-z][a-z]+')
re_opt_local_alpha = re.compile('/opt/local/[a-z][a-z]+')
re_var_local_alpha = re.compile('/var/local/[a-z][a-z]+')
re_status_installed = re.compile('Status:.*installed')
list_dot_d_array = []

pretty_name_id = None
pretty_name_word = None

def abort_with_cleanup(dirpath,abort_message,ignore_missing=True,rm_verbose=True):
    rmdir_existing(dirpath,ignore_missing,rm_verbose)
    if len(list_dot_d_array) > 0:
        for sl in list_dot_d_array:
            sl_ext = path.splitext(sl)[1]
            if sl_ext == '.list':
                rm_line = "{0} /etc/apt/sources.list.d/{1}".format(cmd_rmv,sl)
                with settings(warn_only=True):
                    run_or_sudo(rm_line)
    abort(abort_message)
    return


def print_success_and_failed(fab_object,dirpath=None):
    succ_plus_failed = -999
    try:
        succ_plus_failed = int(fab_object.succeeded) + \
            int(fab_object.failed)
    except Exception:
        succ_plus_failed = -1

    if dirpath:
        print "{0} success and failed: {1} {2}".format(
            dirpath,fab_object.succeeded,fab_object.failed)
    else:
        print "{0} success and failed: {1} {2}".format(
            dirpath,fab_object.succeeded,fab_object.failed)
    return succ_plus_failed


def slash_count(dirpath,relative_okay=False,slash='/'):
    counted = 0
    if dirpath and relative_okay:
        """ No need to check if startswith slash """
        return dirpath.count(slash)

    if dirpath.startswith(slash):
        counted = dirpath.count(slash)
    return counted


def kern64():
    with settings(warn_only=True):
        kern64line = "/bin/uname -a | {0} 'x86_64'".format(cmd_fgrep)
        kern64res = run(kern64line)
    if kern64res.failed:
        return False
    return True


def pretty_name():
    pretty_name = 'Unknown!'
    with settings(warn_only=True):
        pretty_grep = "{0} '^PRETTY_NAME' /etc/os-release".format(cmd_egrep)
        pretty_res = run(pretty_grep)

        if pretty_res.succeeded:
            pretty_last = pretty_res.split(chr(61))[-1]
            """ Both types of quotes stripped below """
            pretty_name = pretty_last.strip('"\\\'')

    return pretty_name


def user_uid(user='syslog'):
    return_uid = -1
    uid_line = None
    if re_first_alpha.match(user):
        uid_line = "{0} {1}".format(cmd_uid,user)
    """ Easiest thing in the world to give a uid rather
    than user name to this function, so some attempt
    made to trap that. """
    if uid_line:
        with settings(warn_only=True):
            uid_res = run(user_line)
            if uid_res.succeeded:
                return_uid = int(uid_res)
    return return_uid


def idnum_between(idnum=101,lower=None,upper=65535):
    int_lower = 0
    try:
        int_lower = int(lower)
    except:
        return False
    
    if not idnum or not lower or int_lower < 0:
        return False
    #print "Test {0} <= {1} <= {2}".format(lower,idnum,upper)
    if int_lower <= int(idnum) <= int(upper):
        return True
    #print "Failed test {0} <= {1} <= {2}".format(lower,idnum,upper)
    return False


def user_uid_between(user='syslog',lower=None,upper=65535):
    if not user or not lower or lower < 0:
        return False
    uid = user_uid(user)
    if lower <= uid <= upper:
        return True
    return False


def run_or_sudo(run_cmd,run_always=False):
    if env.user == 'root':
        return run(run_cmd)
    elif run_always:
        """ run_always flag set True means
        that sudo will never be used. """
        return run(run_cmd)
    return sudo(run_cmd)


def chgrp_or_sudo(dirpath,newgroup=None):
    if newgroup and len(newgroup) > 1:
        grp = newgroup
    else:
        grp = GROUP_WEBSERVER
        
    chgrp_line = "{0} {1} {2}".format(cmd_chgrp,grp,dirpath)
    chgrp_res = run_or_sudo(chgrp_line)
    if print_success_and_failed(chgrp_res,dirpath) < 1:
        abort("Aborted during group setting for directory {0}".format(dirpath))


def dirpath_dlp(dirpath):
    dlp_res = None
    dlp_line = "{0} {1}".format(cmd_dlp,dirpath)
    with settings(warn_only=True):
        dlp_res = run(dlp_line)
    return dlp_res


def group_writable(dirpath):
    """ Only operate where the change affects a known
    path or a known file type. """
    chmod_line = None

    startswith_flag = False
    if dirpath.startswith('/var/www/pub'):
        startswith_flag = True
    elif dirpath.startswith('/var/l'):
        startswith_flag = True

    ext = path.splitext(dirpath)[1]
    if ext and len(ext) > 1:
        """ Has extension """
        print "{0} for {1} having ext of {2}".format(
            startswith_flag,dirpath,ext)
        if ext.lstrip('.') in GROUP_WRITE_EXT_LIST and startswith_flag:
            chmod_line = "{0} g+w {1}".format(cmd_chmod,dirpath)
    elif startswith_flag:
        """ No extension detected """
        chmod_line = "{0} g+w {1}".format(cmd_chmod,dirpath)

    if not chmod_line:
        return False
    return run_or_sudo(chmod_line)


def group_writable_recursive(dirpath):
    """ Only operate where the change affects a known path. """
    chmod_line = None

    startswith_flag = False
    if dirpath.startswith('/var/www/pub'):
        startswith_flag = True
    elif re_var_lib_alpha.match(dirpath):
        startswith_flag = True
    elif re_opt_local_alpha.match(dirpath):
        startswith_flag = True
    elif re_var_local_alpha.match(dirpath):
        startswith_flag = True

    if startswith_flag:
        stat_line = "{0} {1}".format(cmd_stat_gid,dirpath)
        run_res = run(stat_line)
        gid = -1
        if run_res.succeeded:
            gid = str(run_res)
        if idnum_between(gid,30,100):
            chmod_line = "{0} g+w {1}".format(cmd_chmod_recursive,dirpath)

    if not chmod_line:
        return False
    return run_or_sudo(chmod_line)


def put_using_named_temp(remote_filepath,lines,abort_during_words=None,put_sudo=True):

    """ remote_filepath should be a full path
    Example: '/etc/apache2/ports.conf.append'
    See the fabric put api for clarification.
    """
    import tempfile
    abort_message = "Aborting during {0}".format(abort_during_words)
    put_res = None
    named_temp = tempfile.NamedTemporaryFile()
    try:
        named_temp.write(lines)
        named_temp.flush()
        put_res = put(named_temp,remote_filepath,use_sudo=put_sudo)
    except Exception as e:
        abort_with_cleanup(dirpath_fab,"{0} - {1}".format(
                abort_message,unicode(e)) )
    finally:
        named_temp.close()

    if put_res:
        if put_res.failed:
            abort_with_cleanup(dirpath_fab,"{0} put.".format(abort_message))
    else:
            abort_with_cleanup(dirpath_fab,abort_message)
    return put_res


def tilde_expand(path_short,abort_on_fail=True):
    if path_short.startswith('~/'):
        """ path_expanded = path.expanduser(path_short)
        will not do as you are interested in the remote user
        not the user account on your local machine! """
        expandres = run("dirname ~/.")
        if expandres.failed:
            if abort_on_fail:
                abort("Failed during path expansion of {0}".format(path_short))
            path_expanded = path.short
        else:
            path_expanded = path_short.replace('~/',"{0}/".format(expandres),1)
        """ path_expanded = run("dirname ~/.") """
    else:
        return path_short
    return path_expanded


def dpkg_status_as_list(packagename=None):
    if not packagename:
        return []

    with settings(warn_only=True):
        """ run_or_sudo("{0} {1}".format(cmd_dpkg_status,packagename)) """
        status_res = run("{0} {1}".format(cmd_dpkg_status,packagename))
        print_success_and_failed(status_res)

    status_list = []
    if status_res.succeeded:
        status_list = [ ln for ln in status_res.split('\n') \
                            if re_status_installed.match(ln) ]

    return status_list


def requires_dpkg_package(packagename=None,assumeyes=False,list_dot_d=None):
    if packagename:
        requires_res = True
    else:
        return False

    if len(dpkg_status_as_list(packagename)) > 0:
        return True

    requires_res = False

    with settings(warn_only=True):
        if assumeyes:
            ros_res = run_or_sudo("{0} install {1}".format(
                    cmd_apt_assumeyes,packagename))
        else:
            ros_res = run_or_sudo("{0} install {1}".format(
                    cmd_apt,packagename))
        if ros_res.failed and list_dot_d:
            sl_name = "{0}.list".format(list_dot_d)
            sl_lines = None
            if pretty_name_id == 'debian':
                pass
            elif pretty_name_id == 'ubuntu':
                if list_dot_d in ['universe','multiverse']:
                    pretty_dict = {'id': pretty_name_id,
                                   'word': pretty_name_word,
                                   'verse': list_dot_d}
                    sl_lines = """
# added by Fabric of Django
deb http://archive.ubuntu.com/{id}/ {word} {verse}
deb-src http://archive.ubuntu.com/{id}/ {word} {verse}
""".format(**pretty_dict)
            if sl_lines:
                put_using_named_temp("/etc/apt/sources.list.d/{0}".format(sl_name),
                                     sl_lines,'Extra sources list put',False)
                list_dot_d_array.append(sl_name)

                run_or_sudo(cmd_apt_update)

                if assumeyes:
                    ros_res = run_or_sudo("{0} install {1}".format(
                            cmd_apt_assumeyes,packagename))
                else:
                    ros_res = run_or_sudo("{0} install {1}".format(
                            cmd_apt,packagename))

    status_list = dpkg_status_as_list(packagename)
    print "{0} - {1}".format(packagename, status_list)
    # if len(dpkg_status_as_list(packagename)) > 0:
    if len(status_list) > 0:
        return True

    return requires_res


def requires_binary_package(binary_unpathed=None,packagename_suggestion=None,
                            assumeyes=False):
    """ The word 'binary' here refers to the fact that you supply a binary
    name as the first argument. """
    which_res = None
    requires_which = cmd_kern_release
    with settings(warn_only=True):
        which_res = run("{0} {1}".format(cmd_which,binary_unpathed))

    print "{0} {1}".format(which_res.succeeded,which_res.failed)
    if which_res and which_res.succeeded:
        """ .succeeded is supported in Fabric 1.0 and newer """
        requires_which = which_res
        print which_res
        return requires_which
    elif which_res is not None and which_res.failed:
        if assumeyes:
            run_or_sudo("{0} install {1}".format(
                    cmd_apt_assumeyes,packagename_suggestion))
        else:
            run_or_sudo("{0} install {1}".format(
                    cmd_apt,packagename_suggestion))

        with settings(warn_only=True):
            which_res = run("{0} {1}".format(cmd_which,binary_unpathed))

        if which_res and not which_res.failed:
            requires_which = which_res
            print which_res
            return requires_which
    else:
        return which_res
    return False


def rmdir_existing(dirpath,ignore_missing=True,rm_verbose=True):
    """ run_or_sudo() is not always called in preference to plain run()
    so as we do not use sudo unless absolutely necessary.
    Deleting from the users own home directory would not need sudo
    """
    rmdir_flag = False
    if dirpath.startswith('~/'):
        """ set function default return to True """
        rmdir_flag = True
    elif '/djapian/' in dirpath:
        """ set function default return to True """
        rmdir_flag = True
    else:
        """ As a safety feature against dangerous rm -fR
        commands, any directory path that does not begin
        with ~/ will be rejected (function returns False) """
        return False
    exists_dirpath = True
    with settings(warn_only=True):
        """ Tilde expansion only applies when '~' is unquoted """
        exists_dirpath = exists(tilde_expand(dirpath))
        if not exists_dirpath:
            return ignore_missing
    if exists_dirpath:
        if rmdir_flag:
            if rm_verbose:
                rm_line = "{0} -fR {1}".format(cmd_rmv,dirpath)
                run(rm_line)
            else:
                run("rm -fR " + dirpath)
        else:
            if rm_verbose:
                rm_line = "{0} -fR {1}".format(cmd_rmv,dirpath)
                run_or_sudo(rm_line)
            else:
                run_or_sudo("rm -fR " + dirpath)
    return rmdir_flag


def abort_with_cleanup(dirpath,abort_message,ignore_missing=True,rm_verbose=True):
    rmdir_existing(dirpath,ignore_missing,rm_verbose)
    abort(abort_message)
    return


def mkdir_existing(dirpath,mode=None,remove_existing=True,
                   mk_verbose=False,non_tilde_remove=False):
    """ With many safety features this function is complex. Removing safety
    feature would result in a simple function of around 10 lines.
    I choose to implement these safety features so that I can make actions
    using this function on production servers with a degree of confidence that
    the features may help prevent unwanted / miscalculated recursive removals.
    """
    """  The following example would fail:
    mkdir_existing("{0}/djapian".format('/var/local'),2751,True,True)
    because optional 5th arg non_tilde_remove is specified True (or not specified).
    When the third argument is False (or default) then the fifth argument
    is irrelevant. However if you try to set the third argument True and
    dirpath is outside of tilde then you need fifth argument True or you fail.
    """
    mkdir_mode = 2711
    if mode:
        mkdir_mode = mode
    return_res = False
    short_flag = False
    userdir_flag = False
    if dirpath.startswith('~/'):
        short_flag = True
        if 'public_html' in dirpath:
            userdir_flag = True
        """ set function default return to True """
        return_res = True
    elif non_tilde_remove:
        """ non_tilde_remove acts as an override for safety feature described
        below providing directory path begins /var/l or /var/www/pub """
        if dirpath.startswith('/var/l'):
            """ set function default return to True """
            return_res = True
        elif dirpath.startswith('/var/www/pub'):
            """ set function default return to True """
            return_res = True
        else:
            return False
    elif remove_existing:
        """ As a safety feature against dangerous rm -fR
        commands, any directory path that does not begin
        with ~/ will be rejected (function returns False) """
        return False

    print dirpath
    exists_dirpath = True
    with settings(warn_only=True):
        """ Tilde expansion only applies when '~' is unquoted """
        # exists_dirpath = exists(dirpath,verbose=True)
        if short_flag:
            #exists_dirpath = exists(tilde_expand(dirpath),verbose=True)
            exists_dirpath = exists(tilde_expand(dirpath))
        else:
            exists_dirpath = exists(dirpath)
        # exists_dirpath = exists(tilde_expand(dirpath))
        #print "exists_dirpath=%s, remove_existing=%s\n" \
        #% (exists_dirpath,remove_existing)

    if exists_dirpath and remove_existing:
        if short_flag:
            run("rm -vfR " + dirpath)
        elif return_res:
            rm_line = "rm -vfR " + dirpath
            print rm_line
            run_or_sudo(rm_line,short_flag)
        else:
            rm_line = "rm -vfR " + dirpath
            print "Refusing to execute command: {0}".format(rm_line)

    if remove_existing and mode:
        if dirpath.startswith('/var/www/pub'):
            mkdir_line = "mkdir -p -m {0} {1}".format(mkdir_mode,dirpath)
        else:
            mkdir_line = "mkdir -m {0} {1}".format(mkdir_mode,dirpath)
        if mk_verbose:
            print mkdir_line
        mkdir_res = run_or_sudo(mkdir_line,short_flag)

    elif remove_existing and userdir_flag:
        mkdir_line = "mkdir -p {0}".format(dirpath)
        print mkdir_line
        mkdir_res = run_or_sudo(mkdir_line,short_flag)

    elif remove_existing:   # Default for function remove_existing=True
        if dirpath.startswith('/var/www/pub'):
            mkdir_line = "mkdir -p {0}".format(dirpath)
        else:
            mkdir_line = "mkdir " + dirpath
        if mk_verbose:
            print mkdir_line
        mkdir_res = run_or_sudo(mkdir_line,short_flag)

    elif exists_dirpath and mode:
        octal_res = run(cmd_stat_octal + dirpath)
        if octal_res.failed:
            return False
        """ octal_stat = oct(stat(dirpath)[ST_MODE]) """
        if str(octal_res).endswith(str(mode)):
            """ If mode requested matches exactly existing directory
            then use it as is. """
            return True
        else:
            return False
    elif exists_dirpath:
        return_res = False
        """ We are going to fail but still execute for useful feedback """
        mkdir_line = "mkdir {1}".format(dirpath)
        if mk_verbose:
            print "Prefail: {0}".format(mkdir_line)
        mkdir_res = run_or_sudo(mkdir_line,short_flag)

    elif mode and dirpath.startswith('/var/l'):
        mkdir_line = "mkdir -p -m {0} {1}".format(mkdir_mode,dirpath)
        print mkdir_line
        mkdir_res = run_or_sudo(mkdir_line,short_flag)
    elif userdir_flag:
        mkdir_line = "mkdir -p -m {0} {1}".format(mkdir_mode,dirpath)
        print mkdir_line
        mkdir_res = run_or_sudo(mkdir_line,short_flag)
    else:
        mkdir_line = "mkdir -m {0} {1}".format(mkdir_mode,dirpath)
        print mkdir_line
        mkdir_res = run_or_sudo(mkdir_line,short_flag)

    if mkdir_res.failed:
        return_res = False
    return return_res


def gpg_recv_and_verify(signed_file):
    gpgres = run(cmd_gpg_list + "CE0222D0EB8CFF0E")
    if gpgres.failed:
       gpgres = run(cmd_gpg_recv + "CE0222D0EB8CFF0E")
       if not gpgres.failed:
       	  abort("Aborting as preparation for gpg verification failed.")
    gpgres = run(cmd_gpg_verify + signed_file + ".sig")	 
    #gpgres = contains(tar,'02 Feb 2012 18:48:46 GMT using DSA key ID EB8C')	 
    return gpgres


def repo_clone(repo_url,revision=None):
    if revision:
        cloneres = run("{0} -r {1} {2}".format(cmd_mercurial_clone,
                                               revision,repo_url))
    else:
        cloneres = run(cmd_mercurial_clone)

    if cloneres.failed and not confirm("Repo clone failed. Continue anyway?"):
        abort_with_cleanup(dirpath_fab,"Aborting at user request.")


def append_not_already(append_file,append_line,append_bin=None):
    if append_bin is None:
        append_bin = '/opt/local/local-bin/append_not_already.sh'

    append_run = "{0} {1} '{2}'".format(append_bin,append_file,append_line)
    print append_run
    append_res = run(append_run,pty=False)
    if not append_res.failed:
        return True
    return False


def manage_py_or_sudo(manage_command='syncdb',dirproject=None):
    managed_it_flag = False
    manage_dir = None
    if dirproject:
        manage_dir = dirproject
    else:
        try:
            if slash_count(dirpath_django_project) > 0:
                manage_dir = dirpath_django_project
        except Exception:
            manage_dir = None

    if manage_dir and slash_count(manage_dir) > 0:
        with cd(manage_dir):
            manage_line = "./manage.py {0}".format(manage_command)
            if run_or_sudo(manage_line):
                managed_it_flag = True
    return managed_it_flag


def poor_config(target='counts',data='initial',concurrent=2,totaltime=0,visible='internal'):
    """ Poor config to deliberately stress a system for the purposes of
    benchmarking. Having 2 or more concurrent users requesting pygments
    prettified output (non cached) is usually enough to run a test benchmark
    against a low budget cloud server.
    A properly configured Apache mod_wsgi can work well, but in order to
    stress our system here, and to cause least disruption to any existing setup,
    we employ the minimum of Apache configuration effort
    An Apache configuration which has not been tailored to local conditions on
    the server is probably a poor configuration.
    Such configurations can introduce system stress and make a taxing benchmark.
    """
    global pretty_name_id, pretty_name_word
    """ If you want fabric command line interaction with an argument
    then expect it to be a string. May need to int() what you get """
    ab_concurrent = int(concurrent)
    ab_totaltime = int(totaltime)
    requires_ab_res = None
    requires_ab_which = cmd_kern_release
    if ab_totaltime > 0:
        requires_ab_res = requires_binary_package('ab','apache2-utils',assumeyes=True)
        if requires_ab_res:
            requires_ab_which = requires_ab_res

    pname = pretty_name()
    if not pname.endswith(chr(33)):
        pname_array = pname.split()
        pretty_name_id = pname_array[0].lower()
        if 'GNU' in pname:
            """ Last word """
            pretty_name_word = pname_array[-1].strip('()')
        else:
            """ 2nd word / middle word """
            pretty_name_word = pname_array[1]

    print pretty_name_id
    print pretty_name_word

    dirstem_data = tilde_expand(DIRSTEM_DATA_SHORT)
    dirpath_data = "{0}/django14data".format(dirstem_data)
    mkdir_res = mkdir_existing("{0}/django14data".format(DIRSTEM_DATA_SHORT),
                               2771,False)
    """ mkdir_res = run("mkdir -m 2771 {0}".format(
            "{0}/django14data".format(dirstem_data)))
    """
    if mkdir_res:
        dirpath_dlp(dirpath_data)
        #print_success_and_failed(mkdir_res,dirpath_data)
    else:
        pass
        """ abort_with_cleanup(dirpath_fab,'Aborting during directory initialisation.') """
        # abort('Aborted during initialisation of directory django14data.')

    chown_line = "{0} root:www-data {1}".format(cmd_chown,dirpath_data)
    chown_res = run_or_sudo(chown_line)
    print "{0} {1}".format(chown_res.succeeded,chown_res.failed)
    if chown_res.failed:
        abort('Aborted during initialisation of directory django14data ownership.')


    dirpath_djapian_app = "{0}/djapian/{1}".format(dirstem_data,APP_NAME)
    rmdir_res = rmdir_existing(dirpath_djapian_app,True)
    if not rmdir_res:
        pass
        """ abort_with_cleanup(dirpath_fab,'Aborting during directory initialisation.') """
        #abort('Aborting during remove of djapian indexes for app.')

    mkdir_res = mkdir_existing("{0}/djapian".format(DIRSTEM_DATA_SHORT),2771,True,True,True)
    if not mkdir_res:
        """ abort_with_cleanup(dirpath_fab,'Aborting during directory initialisation.') """
        abort('Aborting during initialisation of directory djapian.')

    """ Read and understand function mkdir_existing and safety features to see why generally
    it is preferred to send any tilde or short form of directory to that function.
    That knowledge helps explain why dirpath_djapian is defined here rather than being
    used in the call to mkdir_existing() above.
    """
    dirpath_djapian = "{0}/djapian".format(dirstem_data)

    """ Now above we gave group write via 2771, but here we need to set the group to which
    that group write privilege will belong. """
    chgrp_or_sudo(dirpath_djapian)

    if env.user == 'root':
        dirpath_fab_short = "/var/www/public_html/{0}".format(dir_fab)
    else:
        dirpath_fab_short = "{0}{1}".format('~/public_html/',dir_fab)

    externally_visible_flag = False
    if 'both' in visible:
        externally_visible_flag = True

    repo_url_django_name = 'local2django14bench'
    repo_url_django = "https://bitbucket.org/wrightsolutions/{0}".format(
        repo_url_django_name)
    repo_url_localbin = "https://bitbucket.org/wrightsolutions/local-bin"
    repo_url_localwebserver = "https://bitbucket.org/wrightsolutions/local-webserver"
    """ Make temporary directory in user home to hold cloned repo """
    mkdir_res = mkdir_existing(dirpath_fab_short,2755,True,True,True)
    if mkdir_res:
        print "Created directory {0}".format(dirpath_fab_short)
    else:
        abort('Aborted during initialisation of .fab directory.')

    dirpath_fab = tilde_expand(dirpath_fab_short)
    """ Above we have created dirpath_fab by expanding (if necessary) the
    variable dirpath_fab_short, which may or may not have contained tilde (~)
    """
    print "Cloning into directory {0}".format(dirpath_fab)
    with cd(dirpath_fab):
        repo_clone(repo_url_django,'fd96e9c')
        repo_clone(repo_url_localbin,'4e08290')
        repo_clone(repo_url_localwebserver,'335ac7c')

    requires_django_res = requires_binary_package('django-admin',
                                                  'python-django',assumeyes=True)
    if requires_django_res:
        requires_django_which = requires_django_res

    dirstem_django = dirpath_fab
    dirpath_django_project = "{0}/{1}".format(dirstem_django,
                                              repo_url_django_name)
    dirpath_django_app = "{0}/{1}".format(dirpath_django_project,APP_NAME)

    """
    dirstem_django = "{0}/django".format(dirstem_data)
    dirpath_django_project = "{0}/{1}".format(dirstem_django,
                                              repo_url_django_name)
    mkdir_res = mkdir_existing(dirstem_django,2771,True,False,True)
    if mkdir_res:
        print "Directory {0} exists and okay.".format(dirstem_django)
    else:
        abort('Aborted during initialisation of django directory.')

    dirpath_from = "{0}/{1}".format(dirpath_fab,repo_url_django_name)
    # Use / and strings rather than path.join to allow for where
    # local and remote are not using same path separator.

    cp_line = "{0} --target-directory={1} {2}".format(cmd_cpr_timestamps,
                                                      dirstem_django,
                                                      dirpath_from)
    cp_res = run_or_sudo(cp_line)
    """
    
    path_line = """
<IfModule mod_wsgi.c>
WSGIPythonPath {0}:{1}
</IfModule>""".format(dirstem_django,dirpath_django_project)
    # .format(dirstem_django,repo_url_django_name)
    if env.user == 'root':
        append('/etc/apache2/mods-available/wsgi.conf',path_line)
    else:
        append('/etc/apache2/mods-available/wsgi.conf',path_line,use_sudo=True)

    from jinja2 import Environment

    local_settings_tpl = """
# Django local_settings for {{ project }} project.
DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '{{ dbpath }}/{{ dbname }}.sqlite', # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'log_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filename': '{{ djangolog }}',
            'maxBytes': 4194304, # 4 * 1048576
            'backupCount': 5,
        },
    },
    'loggers': {
        '': {
            'handlers': ['log_file'],
            'level': 'DEBUG',
            'propagate': True
        },
        'django.request': {
            'handlers': ['log_file'],
            'level': 'WARNING',
            'propagate': False,
        },
    }
}

DJAPIAN_DATABASE_PATH = '{{ dirdjapian }}'
"""

    dirpath_django_log = "{0}/local2.log".format(dirpath_data)

    template_dict = {
        'dbpath': dirstem_data,
        'dbname': repo_url_django_name,
        'djangolog': dirpath_django_log,
        'dirdjapian': dirpath_djapian,
        'project': repo_url_django_name,
        }
    local_settings = Environment().from_string(local_settings_tpl).render(template_dict)

    put_using_named_temp("{0}/local_settings.py".format(dirpath_django_project),
                         local_settings,'local_settings construction',False)

    wsgi_py = """
# WSGI config for {0} project.
from os import environ

environ.setdefault("DJANGO_SETTINGS_MODULE", "{1}")

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
                
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
""".format(repo_url_django_name,'settings')

    put_using_named_temp("{0}/wsgi.py".format(dirpath_django_app),
                         wsgi_py,'wsgi.py construction',False)

    requires_djapian_res = requires_dpkg_package('python-django-djapian',
                                                 True,'universe')
    if not requires_djapian_res:
        abort_with_cleanup(dirpath_fab,'Aborting during djapian availability check.')

    requires_yaml_res = requires_dpkg_package('python-yaml',True)
    if not requires_yaml_res:
        abort_with_cleanup(dirpath_fab,'Aborting during yaml availability check.')

    requires_pygments_res = requires_dpkg_package('python-pygments',True)
    if not requires_pygments_res:
        abort_with_cleanup(dirpath_fab,'Aborting during pygments availability check.')

    apache2conf_tpl = """
<VirtualHost *:{{ port_host }}>

DocumentRoot {{ docroot_slashless }}

<Directory />
   Options -Indexes
   AllowOverride None
   Order allow,deny
   allow from all
</Directory>

<Directory "{{ docroot_slashless }}/">
   Options -Indexes
   AllowOverride None
   Order allow,deny
   allow from all
</Directory>

   WSGIDaemonProcess {{ app }} user=www-data group=www-data processes=1 threads=5
   WSGIScriptAlias / {{ docroot_slashless }}/{{ project }}/{{ app }}/wsgi.py
   #WSGIPythonPath /opt/local/webdata/local2bottle/wsgi/# wsgi.conf

   Alias /static {{ docroot_slashless }}/{{ project }}/{{ app }}/static
   #Alias /static {{ docroot_slashless }}/local2django14/local2dj14/static
</VirtualHost>
"""

    template_dict = {
        'port_host': PORT_WSGI,
        'docroot_slashless': dirpath_fab,
        'project': repo_url_django_name,
        'app': APP_NAME,
        }

    apache2conf = Environment().from_string(apache2conf_tpl).render(template_dict)
    # docroot_slashless=dirstem_data + '/django14')

    apache2conf_res = None
    import tempfile
    named_temp = tempfile.NamedTemporaryFile()
    try:
        named_temp.write(apache2conf)
        named_temp.flush()
        apache2conf_res = put(named_temp,"/etc/apache2/sites-enabled/{0}".format(APP_NAME),
                              use_sudo=True)
    finally:
        named_temp.close()

    if externally_visible_flag:
        listen_line = "Listen {0}".format(PORT_WSGI)
    else:
        listen_line = "Listen 127.0.0.1:{0}".format(PORT_WSGI)

    port_lines = """
<IfModule mod_wsgi.c>
# NameVirtualHost *:{0}
{1}
</IfModule>
""".format(PORT_WSGI,listen_line)

    named_temp = tempfile.NamedTemporaryFile()
    try:
        named_temp.write(port_lines)
        named_temp.flush()
        apache2port_res = put(named_temp,'/etc/apache2/ports.conf.append',use_sudo=True)
    finally:
        named_temp.close()

    if apache2port_res:
        if apache2conf_res.failed:
            abort_with_cleanup(dirpath_fab,'Aborting during apache2 port put.')
    else:
            abort_with_cleanup(dirpath_fab,'Aborting during apache2 port.')

    append_bin = "{0}/local-bin/append_not_already_dot_append.sh".format(dirpath_fab)
    append_res = run(append_bin + ' /etc/apache2/ports.conf ' + "'Listen.*" + PORT_WSGI + "'")
    if append_res.failed:
        abort_with_cleanup(dirpath_fab,'Aborting during apache2 port addition.')

    if apache2conf_res:
        if apache2conf_res.failed:
            abort_with_cleanup(dirpath_fab,'Aborting during apache2 configuration put.')
    else:
            abort_with_cleanup(dirpath_fab,'Aborting during apache2 configuration.')

    """ apache2re_res = sudo("/etc/init.d/apache2 reload",group='adm') """
    apache2re_res = sudo("/etc/init.d/apache2 reload")
    if apache2re_res.failed:
        abort_with_cleanup(dirpath_fab,'Aborting during apache2 reload.')

    if 'root' in env.user:
        pass
    else:
        apache2enmod_res = sudo("/usr/sbin/a2enmod userdir")
        if apache2enmod_res.failed:
            abort_with_cleanup(dirpath_fab,'Aborting during apache2 Enable Module userdir.')

    """ apache2re_res = sudo("/etc/init.d/apache2 reload") """
    restart_bin = "{0}/{1}/apache2/apache2stop5startconditional.sh".format(
        dirpath_fab,'local-webserver')
    apache2re_res = sudo(restart_bin)
    if apache2re_res.failed:
        abort_with_cleanup(dirpath_fab,'Aborting during apache2 stop 5 start.')

    with settings(warn_only=True):
        apache2port_res = sudo(cmd_lsof_internet + ' :' + PORT_WSGI)
        if apache2port_res.failed:
            abort_with_cleanup(dirpath_fab,'Aborting during apache2 port test.')

    with settings(warn_only=True):
        url_cdn = 'http://c5871e0b1f1c144e2f0e-7fd212704b537f6e17de67217009ee51.r73.cf3.rackcdn.com'
        url_filename = 'constructionMStankieA14znak20111228164215derivativePublicDomainPx125.png'
        with cd(dirpath_fab):
            run(cmd_wget + "{0}/{1}".format(url_cdn,url_filename))
            # if png_res.failed:

    index_html = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<meta name="robots" content="noarchive,follow" />
<title>Under Construction</title>
</head>
<body>
<p><img src="{0}" alt="Under Construction Logo" /> coming soon...</p>
<p>
    <a href="http://validator.w3.org/check?uri=referer"><img
        src="http://www.w3.org/Icons/valid-xhtml10-blue"
        alt="Valid XHTML 1.0 Strict" height="31" width="88" /></a>
  </p>
</body>
</html>
""".format(url_filename)

    index_path = "{0}/index.html".format(dirpath_fab)
    with cd(dirpath_fab):
        put_using_named_temp(index_path,index_html,'index.html construction',False)

    sync_res = manage_py_or_sudo('syncdb',dirpath_django_project)

    load_res = manage_py_or_sudo('loaddata local_hyphen_source_def',dirpath_django_project)

    perm_res = group_writable(dirpath_django_log)

    index_res = manage_py_or_sudo('index --rebuild',dirpath_django_project)
    """ Multiple runs of --rebuild might produce spurious errors as it is
    only supposed to be used the first time the index is built. 
    This is why there is a directory removal earlier in the script to try
    and avoid such issue where multiple runs / re-run of this benchmark.
    """

    group_write_res = group_writable_recursive(dirpath_djapian)
    """ Above gives appropriate access on Djapian subdirectories which
    helps to avoid DatabaseLockError: Unable to get write lock on ...
    """
    if not group_write_res:
        abort_with_cleanup(dirpath_fab,'Aborting during Djapian index permission setting.')        

    url_counts = 'http://127.0.0.1:8080/local2/counts/'
    url_benchmark = "http://127.0.0.1:8080/local2/{0}".format(target)

    if requires_ab_res and ab_totaltime > 1:
        """ To get things primed, do a single fetch first then short sleep. """
        run("{0} {1}".format(cmd_wget1,url_counts))
        from time import sleep
        sleep(2)
        """ Benchmark the url """
        """ run("/usr/bin/ab -kc {0:d} -t {1:d}".format(ab_concurrent,ab_totaltime)) """
        run("{0} -kc {1:d} -t {2:d} {3}/".format(requires_ab_which,ab_concurrent,
                                                ab_totaltime,url_benchmark))
    elif ab_totaltime == 1:
        """ In this context we use timeout 1 for our single fetch """
        run("{0} {1}".format(cmd_wget1,url_benchmark))
    else:
        """ No --timeout flag given to wget here """
        run("{0} {1}".format(cmd_wget,url_benchmark))
