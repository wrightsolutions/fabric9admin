#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2015 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd datadir
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd datadir_login
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_logfile_path
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_logfile_path:real=True
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_logfile_path_real
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_logfile_path_real1
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_logfile_count
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_logfile_count10
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_logfile_slow
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_logfile_real
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_logfile_real:dir_to_list=/var
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_order_list:verbose=True
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_count:verbose=True
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_dict:verbose=True
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_list:verbose=True
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_list | fgrep kB
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_list | fgrep uffer
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_onreload:verbose=True
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_onreload2:verbose=True
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_local_tmp
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_local_tmp_onreload
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_local_tmp_log_only
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_local_tmp_wal_only
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_local_tmp_differ_plus
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_settings_local_tmp_differ
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_base_backup_from_given_master:master4=aa.bb.cc.dd
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd postgres_base_backup_from_given_master:master4=aa.bb.cc.dd,data_dir=/home/db
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd cactive
# fab -a -f postgres9debian.py -u root -H aa.bb.cc.dd cactive4:client4=aa.bb.cc.dd

from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
import fabutil as futil
from fabutil import conf_get_set as getset
from fabutil import postgres9deb as pg
#from fabutil import package_deb as pdeb


def first_arg_false_to_false(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if len(args) > 0:
            first_arg = args[0]
            if first_arg is True or first_arg in ['true','True','TRUE']:
                # Ensure we never interfere with any first arg intended as positive boolean
                pass
            elif first_arg is False:
                # This branch will likely never be encountered
                pass
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def first_arg_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if len(args) > 0:
            #print 'Wrapped function', function_to_decorate.__name__, 'has args'
            first_arg = args[0]
            if first_arg is True:
                # Ensure we never interfere with any first arg intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as True'
                pass
            elif first_arg is False:
                # Ensure we never interfere with any first arg intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg unaltered as False'
                pass
            elif first_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to True'
                args_list = list(args)
                args_list[0] = True
                args = tuple(args_list)
            elif first_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'first arg altered to False'
                args_list = list(args)
                args_list[0] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbose_forced_boolean(function_to_decorate):
    """ Decorator to ensure that any first argument intended as False
    really does end up as boolean False rather than string.
    Similar fixup for True also.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        #print 'Wrapped function', function_to_decorate.__name__
        if 'verbose' in kwargs:
            #print "Wrapped function", function_to_decorate.__name__, "has kwarg 'verbose'"

            verbose_arg = kwargs['verbose']
            if verbose_arg is True:
                # Ensure we never interfere with 'verbose' intended as positive boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as True'
                pass
            elif verbose_arg is False:
                # Ensure we never interfere with 'verbose' intended as negative boolean
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg unaltered as False'
                pass
            elif verbose_arg in ['true','True','TRUE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to True'
                args_list = list(args)
                kwargs['verbose'] = True
                args = tuple(args_list)
            elif verbose_arg in ['false','False','FALSE']:
                #print 'Wrapped function', function_to_decorate.__name__, 'kwarg altered to False'
                args_list = list(args)
                kwargs['verbose'] = False
                args = tuple(args_list)
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbosity_kwarg_int(function_to_decorate):
    """ Decorator to ensure, even if verbosity is supplied as a string,
    then we do our utmost to convert it to an int.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbosity' in kwargs:
            kval = kwargs['verbosity']
            verbosity_int=None
            try:
                verbosity_int = int(kval)
            except Exception:
                pass
            if verbosity_int is None:
                """ The user of this decorator would expect that
                non-numeric arguments blocked, so better to set
                verbosity zero than let through a bad value
                """
                #kwargs[verbosity]=0
                kwargs.pop('verbosity', None)
                """ Above we have removed verbosity if it cannot
                be converted, which means any default in wrapped
                function signature would take effect
                """
            else:
                kwargs['verbosity']=verbosity_int
        #print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        #print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def verbosity_kwarg_int_verbose(function_to_decorate):
    """ Decorator to ensure, even if verbosity is supplied as a string,
    then we do our utmost to convert it to an int.
    Fabric docs: 'Additionally, since this [pre-task arguments] process involves
    string parsing, all values will end up as Python strings, so plan accordingly.'
    Verbose variant which has print statements reporting before and after
    and also sets verbosity=0 on conversion error.
    """
    def inner_accepting_args(*args, **kwargs):
        if 'verbosity' in kwargs:
            kval = kwargs['verbosity']
            verbosity_int=None
            try:
                print("Attempting to int({0}) ".format(kval))
                verbosity_int = int(kval)
            except Exception:
                raise

            if verbosity_int is None:
                """ The user of this decorator would expect that
                non-numeric arguments blocked, so better to set
                verbosity zero than let through a bad value
                """
                #kwargs[verbosity]=0
                kwargs.pop('verbosity', None)
                print("kwarg_int wrapper has removed verbosity key word arg")
                """ Above we have removed verbosity if it cannot
                be converted, which means any default in wrapped
                function signature would take effect
                """
            else:
                kwargs['verbosity']=verbosity_int
        print "Wrapping function", function_to_decorate.__name__
        function_return = function_to_decorate(*args, **kwargs)
        print "Wrapped function returned", function_return
        return function_return

    return inner_accepting_args


def postgres_logfile_path_real(dir_to_list=None,print_flag=True):
    """
    Return a non-unique list containing the logfile that is in
    use by the active postgresql processes.

    pg.active_file_reg_from_dir signature is:
    dir_to_list='/var/log/postgresql',return_requested='lines',
    verbosity=0,print_flag=False)
    """
    return pg.postgres_logfile_real(dir_to_list,print_flag=print_flag,verbosity=1)


def postgres_logfile_path_real1(dir_to_list=None,print_flag=True):
    """
    Return a single element list containing the logfile that is in
    use by the active postgresql process.

    pg.active_file_reg_from_dir signature is:
    dir_to_list='/var/log/postgresql',return_requested='lines',
    verbosity=0,print_flag=False)
    """
    return pg.postgres_logfile_real(dir_to_list,return_requested='unique',
                                    verbosity=1,print_flag=print_flag)


@first_arg_forced_boolean
def postgres_logfile_path(real=False):
    """ Wrapper around postgres_logfile_path_real when real=True
    but consults postgres config when real=False
    """
    if real is True:
        return postgres_logfile_path_real()
    
    # TODO
    return


def postgres_logfile_real(dir_to_list=None):
    """ Wrapper around postgres_logfile_path_real to act as alias. """
    return postgres_logfile_path_real(dir_to_list)


def postgres_logfile_real1(dir_to_list=None):
    """ Wrapper around postgres_logfile_path_real1 to act as alias. """
    return postgres_logfile_path_real1(dir_to_list)


def postgres_logfile_count(dir_to_list='/var/log/postgresql',pattern=None):
    period = 'hour'
    logcount = len(futil.files_directories.dir_files(dir_to_list,period))
    print("{0} files in {1} for past {2}".format(logcount,dir_to_list,period))
    return logcount


def postgres_logfile_count10(dir_to_list='/var/log/postgresql',pattern=None):

    period = 'hour'
    slist = futil.files_directories.dir_files(dir_to_list,period)

    if len(slist) == 0:
        print("zero files in {0} for past {1}".format(dir_to_list,period))
    else:
        print("{0} files in {1} for past {2}".format(len(slist),dir_to_list,period))
        for idx,line in enumerate(slist):
            print(line)
            if idx == 9: break

    return slist


@verbose_forced_boolean
def postgres_logfile_slow(dir_to_list='/var/log/postgresql',config='running',verbose=True):
    """ Report on how slow a query has to be for it to show up in any ENABLED logfile
    for the 'running' or 'configured' or 'both' setup in place on the server.

    Regardless of the config 'running', 'configured', 'both' setting we only ever
    report on the logfile name 'running' meaning open as a writable file by postgres now.

    Consult log_min_duration_statement to help provide an answer.

    logging_collector might alternatively be named logging_guarantee_but_locker and
    log entries will still appear even when logging_collector is showing as 'off'

    min_duration_list is set from the dict lookup and will be a list similar to that shown below:
    ['log_min_duration_statement', '2000', 'superuser', 'integer', '-1', '2000', 'Reporting and Logging']

    setting - current setting
    boot_val - default setting from compiled in or configuration file or similar
    reset_val - value that will be set if you choose to RESET this variable
    """

    secs = 9999

    logfile_list = postgres_logfile_path_real1(dir_to_list,print_flag=True)

    if len(logfile_list) == 0:
        secs = -1
        if verbose is True:
            print("Postgresql is not logging so no slow query logging either.")
    else:
        logging_directives = pg.postgres_logging_summary_as_dict(print_flag=False)
        if len(logging_directives) < 1:
            if verbose is True:
                print("Postgresql is NOT logging slow queries.")
        elif 'log_min_duration_statement' in logging_directives:
            min_duration_list = logging_directives['log_min_duration_statement']
            # name|setting|context|vartype|boot_val|reset_val|category
            if config == 'running':
                min_duration = min_duration_list[1]
            elif config == 'configured':
                min_duration = min_duration_list[4]
            else:
                min_duration = min_duration_list[1]
                if min_duration_list[4] == min_duration_list[1]:
                    pass
                else:
                    report_line = "min_duration in milliseconds is setting={0} ".format(min_duration)
                    report_line = "{0} and configured={1} which do not agree.".format(min_duration_list[4])
                    print(report_line)
            if verbose is True:
                print("min_duration is set in milliseconds to {0}".format(min_duration))
            if min_duration == '-1':
                if verbose is True:
                    print("Postgresql is NOT logging slow queries.")
            else:
                try:
                    milliseconds = int(min_duration)
                    secs = milliseconds//1000
                except:
                    secs = -1

        if verbose is True:
            #for logging_directive in logging_directives:
            #    print(logging_directive)
            print("Postgresql is logging queries that are slower than {0} seconds.".format(secs))

    #logging_directives = pg.postgres_logging_summary_to_conf(print_flag=True)

    return secs


@verbose_forced_boolean
def postgres_settings_order_list(verbose=True):
    return pg.postgres_settings_order_list(print_flag=verbose)


@verbose_forced_boolean
def postgres_settings_list(verbose=True):
    return pg.postgres_settings_list(print_flag=verbose)


@verbose_forced_boolean
def postgres_settings_dict(verbose=True):
    return pg.postgres_settings_dict(print_flag=verbose)


@verbose_forced_boolean
def postgres_settings_count(verbose=False):
	settings_dict = pg.postgres_settings_dict(print_flag=(verbose is True))
	settings_count = len(settings_dict)
	print("Postgresql has {0} settings.".format(settings_count))
	return settings_count


def postgres_settings_sighup(verbose=True):
    """ Common settings viewer as 'list of lists' - sighup / file based settings """
    return pg.postgres_settings_context_as_list('sighup',print_flag=verbose) 


def postgres_settings_onreload(verbose=True):
    """ Common settings viewer as 'list of lists' - onreload is a local alias for sighup """
    return pg.postgres_settings_context_as_list('sighup',print_flag=verbose) 


def postgres_settings_onreload2(verbose=True):
    """ Settings viewer - set intersection of 'onreloadprogressive' and 'onreload' """
    list1 = pg.postgres_settings_context_as_list('onreload',print_flag=False)
    list2 = pg.postgres_settings_context_as_list('onreloadprogressive',print_flag=False)
    listboth = list1[:]
    listboth.extend(list2)
    if verbose is True:
        for setting in listboth:
            print(setting)

    return listboth


@first_arg_forced_boolean
def postgres_settings_local_tmp(verbose=False):
	""" Call ... _tmp1 in postgres9deb to write copy of remote postgresql
	conf locally
	"""
	return pg.postgres_settings_local_tmp1(verbose)


@first_arg_forced_boolean
def postgres_settings_local_tmp_sorted(verbose=False):
	return pg.postgres_settings_local_tmp(verbose,sortkeys=True)


@first_arg_forced_boolean
def postgres_settings_local_tmp_onreload(verbose=False):
	return pg.postgres_settings_local_tmp_pcontext(verbose,pcontext='sighup',sortkeys=False)


@first_arg_forced_boolean
def postgres_settings_local_tmp_log_only(sortkeys=False):
	return pg.postgres_settings_local_tmp_log_only(False,sortkeys)


@first_arg_forced_boolean
def postgres_settings_local_tmp_log(sortkeys=True):
	""" Alias for postgres_settings_local_tmp_log_only() but with sortkeys=True
	being the default
	"""
	return pg.postgres_settings_local_tmp_log_only(False,sortkeys)


@first_arg_forced_boolean
def postgres_settings_local_tmp_wal_only(sortkeys=False):
	return pg.postgres_settings_local_tmp_wal_only(False,sortkeys)


@first_arg_forced_boolean
def postgres_settings_local_tmp_wal(sortkeys=True):
	""" Alias for postgres_settings_local_tmp_wal_only() but with sortkeys=True
	being the default
	"""
	return pg.postgres_settings_local_tmp_wal_only(False,sortkeys)


@first_arg_forced_boolean
def postgres_settings_local_tmp_differ_only(sortkeys=False):
	return pg.postgres_settings_local_tmp_differ(sortkeys=sortkeys)


@first_arg_forced_boolean
def postgres_settings_local_tmp_differ(sortkeys=True):
	""" Alias for postgres_settings_local_tmp_differ_only() but with sortkeys=True
	being the default
	"""
	return pg.postgres_settings_local_tmp_differ(sortkeys=sortkeys)


@first_arg_forced_boolean
def postgres_settings_local_tmp_differ_plus_only(sortkeys=False):
	return pg.postgres_settings_local_tmp_differ_plus(False,sortkeys)


@first_arg_forced_boolean
def postgres_settings_local_tmp_differ_plus(sortkeys=True):
	""" Alias for postgres_settings_local_tmp_differ_plus_only() but with sortkeys=True
	being the default
	"""
	return pg.postgres_settings_local_tmp_differ_plus(False,sortkeys,selective=True)


def postgres_base_backup_from_given_master(master4=None):
	if master4 is None:
		return False
	return pg.postgres_base_backup_from_given_master(master4)


def cactive():
	""" Connections active """
	return pg.connections_active()


def cactive4(client4=None):
	""" Connections active """
	return pg.connections_active4(client4)


@verbose_forced_boolean
def datadir(verbose=False):
	settings_dict = pg.postgres_settings_dict(print_flag=(verbose is True))
	settings_count = len(settings_dict)
	print("Postgresql has {0} settings.".format(settings_count))
	if 'data_directory' in settings_dict:
		datadir = '[unknown]'
		try:
			datadir_list = settings_dict['data_directory']
			datadir = datadir_list[1]
		except:
			datadir = '[invalid]'
		print("data directory of Postgresql is {0}".format(datadir))
	return settings_count


def datadir_login(fieldnum=5):
	""" Print the home/data directory as recorded in password entry for postgres """
	return pg.postgres_passline(fieldnum,True)


def tablespace_size(tspacename=None):
	return pg.tablespace_size(tspacename)


def tablespace_sizes(tspacename=None):
	return pg.tablespace_sizes(tspacename)

