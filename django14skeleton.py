#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Copyright 2014 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0

# Run startproject and startapp to create a new (empty) project and new (empty) app

# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd start2
# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd start2:repos=True
# fab -a -f django14skeleton.py -u root --port=22222 -H 127.0.0.1 start2:repos=True
# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd grep2
# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd grep2stem:stem=/var/www/public_html/example
# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd staticfiles:stem=/var/www/public_html/example
# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd staticfiles:stem=/var/www/public_html/example,overwrite=false
# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd debug_disable
# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd debug_enable
# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd debug_disable_verbose
# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd debug_enable_verbose
# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd diffsettings6
# fab -a -f django14skeleton.py -u root -H aa.bb.cc.dd diffsettings6:project_path=/var/www/public_html/example

# Tested against fabric 1.4.3
# http://ftp.uk.debian.org/debian/pool/main/f/fabric/fabric_1.4.3-1.debian.tar.gz

from __future__ import with_statement
from fabric.api import abort, cd, env, get, hide, put, run, settings, sudo
#from fabric.contrib.console import confirm
from fabric.contrib.files import exists,append
from os import path
from os import linesep as os_linesep
import re
from string import ascii_letters, digits, punctuation
import fabutil as futil
from fabutil import conf_get_set as getset
from fabutil import cmdbin
from fabutil import package_deb as pdeb

dir_fab = "django14skeleton.fab"
APP_NAME='app14'
PROJECT_NAME='proj14'
DIRPATH_STEM='/var/www/public_html/poor_cycle.fab/local2django14bench'
GREP_MODULE_TARGET = "'django\.contrib\.|\.csrf\.|\.sessions|[a-z]*\.module\.[a-z]*'"
STATICFILES_INIT = '/usr/share/pyshared/django/contrib/staticfiles/__init__.py'
repo_url_django = 'https://bitbucket.org/wrightsolutions/local2django14bench'
repo_url_localbin = 'https://bitbucket.org/wrightsolutions/local-bin'
#repo_url_localwebserver = 'https://bitbucket.org/wrightsolutions/local-webserver'
dirpath_fab = '/tmp'
dirpath_repos = '/tmp'


def kern64():
    with settings(warn_only=True):
        kern64line = "/bin/uname -a | {0} 'x86_64'".format('/bin/fgrep')
        kern64res = run(kern64line)
    if kern64res.failed:
        return False
    return True


def pretty_name():
    pretty_name = 'Unknown!'
    with settings(warn_only=True):
        pretty_grep = "{0} '^PRETTY_NAME' /etc/os-release".format('/bin/egrep')
        pretty_res = run(pretty_grep)

        if pretty_res.succeeded:
            pretty_last = pretty_res.split(chr(61))[-1]
            """ Both types of quotes stripped below """
            pretty_name = pretty_last.strip('"\\\'')

    return pretty_name


def tilde_expand(path_short,abort_on_fail=True):
    if path_short.startswith('~/'):
        """ path_expanded = path.expanduser(path_short)
        will not do as you are interested in the remote user
        not the user account on your local machine! """
        expandres = run("dirname ~/.")
        if expandres.failed:
            if abort_on_fail:
                abort("Failed during path expansion of {0}".format(path_short))
            path_expanded = path.short
        else:
            path_expanded = path_short.replace('~/',"{0}/".format(expandres),1)
        """ path_expanded = run("dirname ~/.") """
    else:
        return path_short
    return path_expanded


def dirpath_dlp(dirpath):
    dlp_res = None
    dlp_line = "/bin/vdir -dlp {0}".format(dirpath)
    with settings(warn_only=True):
        dlp_res = run(dlp_line)
    return dlp_res


def repo_clone(repo_url,revision=None):
    if revision:
        cloneres = run("/usr/bin/hg clone -r {0} {1}".format(revision,repo_url))
    else:
        cloneres = run("/usr/bin/hg clone {0}".format(repo_url))
    if cloneres.failed:
        repo_url = None
    return repo_url


def run_or_sudo(run_cmd,run_always=False):
    if env.user == 'root':
        return run(run_cmd)
    elif run_always:
        """ run_always flag set True means
        that sudo will never be used. """
        return run(run_cmd)
    return sudo(run_cmd)


def rmdir_existing(dirpath,ignore_missing=True,rm_verbose=True):
    """ run_or_sudo() is not always called in preference to plain run()
    so as we do not use sudo unless absolutely necessary.
    Deleting from the users own home directory would not need sudo
    """
    rmdir_flag = False
    if dirpath.startswith('~/'):
        """ set function default return to True """
        rmdir_flag = True
    elif '/djapian/' in dirpath:
        """ set function default return to True """
        rmdir_flag = True
    else:
        """ As a safety feature against dangerous rm -fR
        commands, any directory path that does not begin
        with ~/ will be rejected (function returns False) """
        return False
    exists_dirpath = True
    with settings(warn_only=True):
        """ Tilde expansion only applies when '~' is unquoted """
        exists_dirpath = exists(tilde_expand(dirpath))
        if not exists_dirpath:
            return ignore_missing
    if exists_dirpath:
        if rmdir_flag:
            if rm_verbose:
                rm_line = "{0} -fR {1}".format('/bin/rm -v ',dirpath)
                run(rm_line)
            else:
                run("rm -fR " + dirpath)
        else:
            if rm_verbose:
                rm_line = "{0} -fR {1}".format('/bin/rm -v ',dirpath)
                run_or_sudo(rm_line)
            else:
                run_or_sudo("rm -fR " + dirpath)
    return rmdir_flag


def local_settings_debug_disable(project_path=
                                 '/var/www/public_html/poor_cycle.fab/local2django14bench/local_settings.py'):
    """ Make DEBUG=False in local settings
    Argument project_path will take a stem or a full path (ending local_settings.py or similar)
    """
    toggle_res = futil.web_wsgi.local_settings_debug(project_path=project_path,debug=False,
                                                     verbosity=1,md5current=None)
    return toggle_res


def debug_disable(project_path=
                  '/var/www/public_html/poor_cycle.fab/local2django14bench/local_settings.py'):
    """ Convenience wrapper around local_settings_debug_disable """
    return local_settings_debug_disable(project_path)


def debug_disable_verbose(project_path=
                          '/var/www/public_html/poor_cycle.fab/local2django14bench/local_settings.py'):
    """ Set DEBUG = False in local settings with verbose feedback """
    toggle_res = futil.web_wsgi.local_settings_debug(project_path=project_path,debug=False,
                                                     verbosity=2,md5current=None)
    return toggle_res


def local_settings_debug_enable(project_path=
                                '/var/www/public_html/poor_cycle.fab/local2django14bench/local_settings.py'):
    """ Make DEBUG=True in local settings
    Argument project_path will take a stem or a full path (ending local_settings.py or similar)
    """
    toggle_res = futil.web_wsgi.local_settings_debug(project_path=project_path,debug=True,
                                                     verbosity=1,md5current=None)
    return toggle_res


def debug_enable(project_path=
                 '/var/www/public_html/poor_cycle.fab/local2django14bench/local_settings.py'):
    """ Convenience wrapper around local_settings_debug_enable """
    return local_settings_debug_enable(project_path)


def debug_enable_verbose(project_path=
                         '/var/www/public_html/poor_cycle.fab/local2django14bench/local_settings.py'):
    """ Set DEBUG = True in local settings with verbose feedback """
    toggle_res = futil.web_wsgi.local_settings_debug(project_path=project_path,debug=True,
                                                     verbosity=2,md5current=None)
    return toggle_res


def grep2(dirpath_stem=DIRPATH_STEM,target=GREP_MODULE_TARGET,local='local_settings.py'):
    """ Grep for relevant module information in settings and local setttings.
    When target is supplied, this will grep your supplied target rather than
    predefined GREP_MODULE_TARGET
    """
    #global dirpath_fab, dirpath_repos
    grep_flag = False
    with settings(warn_only=True):
        grep_res = run_or_sudo("/bin/egrep -B1 -A1 {0} {1}/settings.py".format(
                target,dirpath_stem))
    if grep_res.succeeded:
        grep_flag = True
        with settings(warn_only=True):
            with hide('warnings'):
                grep_res = run_or_sudo("/bin/egrep -B1 -A1 {0} {1}/{2}".format(
                        target,dirpath_stem,local))
    return grep_flag


def grep2stem(stem=DIRPATH_STEM):
    """ Grep for relevant module info in settings & local setttings in path with supplied stem """
    return grep2(dirpath_stem=stem)


def start2(project=None,app=None,repos=False):
    """ Run startproject and startapp to create default (empty) project and app
    When repos is True also pull some useful repositories to aid with setup
    """
    if project is None:
        project_name = PROJECT_NAME
    else:
        project_name = str(project)
    if app is None:
        app_name = APP_NAME
    else:
        app_name = str(app)
    global pretty_name_id, pretty_name_word
    global dirpath_fab, dirpath_repos
    if repos is True:
        pass
    elif repos in ['false','FALSE']:
        repos = False
    else:
        # true rather than camelcase True would be dealt with here
        repos = True
    pname = pretty_name()
    if not pname.endswith(chr(33)):
        pname_array = pname.split()
        pretty_name_id = pname_array[0].lower()
        if 'GNU' in pname:
            """ Last word """
            pretty_name_word = pname_array[-1].strip('()')
        else:
            """ 2nd word / middle word """
            pretty_name_word = pname_array[1]

    print "{0} {1}".format(pretty_name_id,pretty_name_word)

    rmdir_existing("~/{0}".format(dir_fab),True,True)
    dirpath_fab = tilde_expand("~/{0}".format(dir_fab))
    mkdir_res = run("mkdir -m 2755 {0}".format(dirpath_fab))
    if mkdir_res.succeeded:
        dirpath_dlp(dirpath_fab)
        #print_success_and_failed(mkdir_res,dirpath_fab)
    else:
        abort('Aborted during initialisation of directory.')
        #abort_with_cleanup(dirpath_fab,'Aborting during directory initialisation.')

    requires_django_res = pdeb.requires_binary_package('django-admin',
                                                  'python-django',assumeyes=True)

    startapp_res = False
    print "Creating project below directory {0}".format(dirpath_fab)
    dirpath_fab_project = None
    # chr(47) is forward slash (/)
    with cd(dirpath_fab):
        startproject_res = run("django-admin startproject {0}".format(project_name))
        if startproject_res.succeeded:
            dirpath_fab_project = "{0}{1}{2}".format(dirpath_fab,chr(47),project_name)
            with cd(project_name):
                startapp_res = run("django-admin startapp {0}".format(app_name))
                if startapp_res.succeeded:
                    start2return = True

    if start2return:

        try:
            from jinja2 import Environment
        except ImportError:
            print 'Need jinja2 available on local machine. apt-get install python-jinja2. Quitting!'
            start2return = False

    if start2return:
        secret_key = cmdbin.secret_crypto()
        if secret_key is None:
            start2return = False
            print "Failure during secret_key generation. secret_key={0}".format(secret_key)
        else:
            start2return = True

    if start2return:
        start2return = False

        local_settings_tpl = """SECRET_KEY = '{{ secret_key }}'
DEBUG = {{ debug_flag }}
TEMPLATE_DEBUG = DEBUG
# ADMINS ; MANAGERS ; DATABASES
# LOCAL_SETTINGS_DIR = {{ dirpath_fab_project }}
from os import path; LOCAL_SETTINGS_DIR = path.dirname(path.abspath(__file__))
#DATABASES = {
#  'default': {
#     'ENGINE': 'django.db.backends.sqlite3',             
#     'NAME': '/var/www/dj.sqlite',
#     'HOST': '',                           # Set to empty string for localhost. Not used with sqlite3.
#     'OPTIONS': {},
#     'PASSWORD': u'********************',  # Not used with sqlite3.
#     'PORT': '',                           # Set to empty string for default. Not used with sqlite3.
#     'TEST_CHARSET': None,
#     'TEST_COLLATION': None,
#     'TEST_MIRROR': None,
#     'TEST_NAME': None,
#     'TIME_ZONE': 'UTC',
#     'USER': ''                            # Not used with sqlite3.
#  }
#}
# STATIC_ROOT; STATIC_URL ; STATICFILES_DIRS ; STATICFILES_FINDERS ; SECRET_KEY
"""
        template_dict = {
            'debug_flag': True,
            'dirpath_fab_project': dirpath_fab_project,
            'secret_key': secret_key,
            }
        local_settings = Environment().from_string(local_settings_tpl).render(template_dict)
        """ futil.files_directories.put_using_named_temp(
        "{0}/local_settings.py".format(dirpath_django_project),
        local_settings,'local_settings construction',False)
        """
        local_res = cmdbin.put_using_named_temp_cmdbin(
            "{0}{2}{1}{2}local_settings.py".format(dirpath_fab_project,project_name,chr(47)),
            local_settings)
        if local_res is True:
            start2return = True

    if start2return:

        sed_res = futil.web_wsgi.settings_append_sed("{0}{1}{2}".format(dirpath_fab_project,
                                                                        chr(47),project_name))
        print sed_res,sed_res

        start2return = False
        with hide('output'):
            rmdir_existing("~/{0}.repos".format(dir_fab),True,True)
        dirpath_repos = tilde_expand("~/{0}.repos".format(dir_fab))
        if repos is False:
            start2return = True
            print "Did not create any repos in {0}".format(dirpath_repos)
        else:
            # mkdir and populate from remote repositories
            mkdir_res = run("mkdir -m 2755 {0}".format(dirpath_repos))

            with cd(dirpath_repos):
                requires_res = pdeb.requires_binary_package('hg','mercurial',assumeyes=True)
                if requires_res:
                    start2return = True
                    clone_res = repo_clone(repo_url_django,'fd96e9c')
                    clone_res = repo_clone(repo_url_localbin,
                                           'dcbd2b2057cdce99fb19fee5b711e7f24ed52923')

        grep2("{0}/{1}/{1}".format(dirpath_fab,project_name))

    if start2return:
        start2return = False
        manage_res = futil.web_wsgi.manage_py_or_sudo('validate',dirproject=dirpath_fab_project)
        if manage_res is True:
            start2return = True            

    if start2return is True:
        print "Start2 process completed startproject and startapp"
    else:
        print "Start2 process startproject and startapp not completed as expected"
    return start2return


def staticfiles(stem=DIRPATH_STEM,overwrite=False):
    """ ./manage.py collectstatic --dry-run --noinput
    or when overwrite=True do ./manage.py collectstatic --noinput
    """
    static_flag = False
    if stem is None or not exists(stem):
        print "Not issuing collectstatic for stem={0}".format(stem)
        return static_flag
    if overwrite is True:
        pass
    elif overwrite in ['false','FALSE']:
        overwrite = False
    else:
        # true rather than camelcase True would be dealt with here
        overwrite = True
    confidence_level = 0
    contains = futil.web_wsgi.diffsettings_installed_apps_contains(manage_dir=stem,
                                                                   target='django.contrib.staticfiles',
                                                                   print_flag=False)
    if contains:
        confidence_level += 2
    grep_res = grep2(dirpath_stem=stem,target='staticfiles')
    if grep_res is True:
        confidence_level += 1
    if confidence_level >= 3:
        print "Confidently proceeding with collectstatic confidence_level is {0}.".format(
            confidence_level)
    else:
        print "Proceeding with collectstatic although expecting failure as confidence_level is {0}.".format(
            confidence_level)
    if overwrite is True:
        manage_collectstatic = "./manage.py collectstatic --noinput;"
    else:
        manage_collectstatic = "./manage.py collectstatic --dry-run --noinput;"

    with settings(warn_only=True):
        with hide('stdout','warnings'):
            with cd(stem):
                failed_collectstatic = 'Failed. Is collectstatic'
                manually_run = 'Visit server and manually run'
                # Above 2 definitions are just strings for message forming.
                static_res = run_or_sudo(manage_collectstatic)
                if static_res.succeeded:
                    static_flag = True
                    static_line = cmdbin.run_res_lines_stripped_matching1(
                        static_res,'^\s?\d+\s+static\s+')
                    if len(static_line) > 2:
                        if overwrite is not True:
                            print "--dry-run {0}".format(static_line)
                        else:
                            print static_line
                    else:
                        print "{0} output as expected? {1} ./manage.py collectstatic".format(
                            failed_collectstatic,manually_run)
                elif 'nknown' in static_res:
                    print "{0} enabled? Check settings.py and local settings".format(
                        failed_collectstatic)
                else:
                    report_line = ''
                    re_matching = re.compile('^.*Error:\sNo\smodule')
                    for line in static_res.split(os_linesep):
                        if re_matching.match(line):
                            report_line = cmdbin.printable_only(line.strip(),True)
                    if len(report_line) > 2:
                        print report_line
                    print "{0} working? Does {1} exist?".format(
                        failed_collectstatic,STATICFILES_INIT)
    return static_flag


def diffsettings6(project_path=
                  '/var/www/public_html/poor_cycle.fab/local2django14bench',empty_included=False):
    """ Print tuples from result of 'diffsetting' command for INSTALLED_APPS ; MIDDLEWARE_CLASSES ; etc
    INSTALLED_APPS ; MIDDLEWARE_CLASSES
    TEMPLATE_CONTEXT_PROCESSORS ; TEMPLATE_LOADERS
    STATICFILES_DIRS ; STATICFILES_FINDERS
    """
    tuple_non_empty_count = futil.web_wsgi.diffsettings6print(manage_dir=project_path,
                                                              empty_included=empty_included)
    if tuple_non_empty_count < 1:
        print "None of those 6 tuples from 'diffsetting' showed a difference."
        print "You supplied project_path={0}".format(project_path)
    return tuple_non_empty_count
